{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "nbpresent": {
     "id": "abaeef91-5553-4553-8ebf-2c22a87d7137"
    }
   },
   "source": [
    "<img src=\"http://www.cs.wm.edu/~rml/images/wm_horizontal_single_line_full_color.png\">\n",
    "\n",
    "<h1 style=\"text-align:center;\">CSCI 141-01, Fall 2023 course notes</h1>\n",
    "\n",
    "<h1 style=\"text-align:center;\">Booleans and boolean expressions</h1>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Contents\n",
    "\n",
    "* [Booleans](#booleans)\n",
    "* [Boolean expressions](#expressions)\n",
    "* <a href=\"#assignment\">Boolean variables</a>\n",
    "* <a href=\"#not\">Logical NOT</a>\n",
    "* <a href=\"#and\">Logical AND</a>\n",
    "* <a href=\"#or\">Logical OR</a>\n",
    "* [The very useful <code class=\"kw\">in</code> statement](#in)\n",
    "* [The very scary <code class=\"kw\">is</code> statement 😱 🐞](#is)\n",
    "* [Conditional expressions 🤯](#conditions_expr)\n",
    "* <a href=\"#exercises\">Exercises</a>\n",
    "* [Lagniappe: the XOR function 🤯](#xor)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Booleans <a id=\"booleans\"></a>\n",
    "\n",
    "Boolean variables are logical variables that can take on one of two values: <code class=\"kw\">True</code> or <code class=\"kw\">False</code>.\n",
    "\n",
    "\"Boolean\" honors the 19th century English mathematician and logician [George Boole](http://www-groups.dcs.st-and.ac.uk/~history/Biographies/Boole.html).  The word is frequently not capitalized and written as \"boolean\".\n",
    "\n",
    "In Python, the type for booleans is <code>bool</code>."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbpresent": {
     "id": "16d1d3f0-d571-456e-9d27-f8277a5fc703"
    }
   },
   "outputs": [],
   "source": [
    "T = True\n",
    "F = False\n",
    "print(T, F)\n",
    "print(type(True))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbpresent": {
     "id": "55186210-b60a-40d0-a6e4-4904eec09db1"
    }
   },
   "source": [
    "# Boolean expressions <a id=\"expressions\"/>\n",
    "\n",
    "Much more interesting are <b>boolean expressions</b>.\n",
    "\n",
    "The boolean expression <code>1 == 2</code> asks \"does 1 equal 2?\" and returns either True or False as the result."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "nbpresent": {
     "id": "2a7bfdf2-e569-4400-b342-15bbf99e81b4"
    },
    "scrolled": true
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "False\n"
     ]
    }
   ],
   "source": [
    "# Does 1 equal 2?\n",
    "print(1 == 2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Make sure you understand the difference:\n",
    "* One equal sign is assignment.\n",
    "* Two equal signs asks whether two values are the same."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Other comparison operations are more obvious.\n",
    "* <code><, <=</code> &nbsp; strictly less than, less than or equal to\n",
    "* <code>>, >=</code> &nbsp; strictly greater than, greater than or equal to\n",
    "* <code>!=&nbsp;&nbsp;&nbsp;</code> &nbsp; not equal to"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbpresent": {
     "id": "e08031fe-754c-495e-bbf6-014fffe70cd8"
    }
   },
   "outputs": [],
   "source": [
    "# Is 2 strictly greater than 1?\n",
    "print(2 > 1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbpresent": {
     "id": "4db06223-4cc0-485a-b07e-381d97372d8a"
    }
   },
   "outputs": [],
   "source": [
    "# Is 1 strictly greater than 1?\n",
    "print(1 > 1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbpresent": {
     "id": "3fb51a78-74c6-4000-aac8-0267a58fda97"
    }
   },
   "outputs": [],
   "source": [
    "# Is 1 greater than or equal to 1?\n",
    "print(1 >= 1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x = 3\n",
    "y = 4\n",
    "\n",
    "# Is 3 not equal to 3?\n",
    "print(3 != 3)\n",
    "\n",
    "# Is x not equal to y?\n",
    "print(x != y)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<p class=\"try_it\">\n",
    "<b>Try it yourself.</b> Write boolean expressions using \n",
    "<code style=\"background:inherit;\">==, !=, >, >=, <, <=</code> and print the results.\n",
    "</p>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Boolean variables <a id=\"assignment\"/>\n",
    "\n",
    "Boolean expressions can appear on the right-hand side of assignments, so we can capture their values in variables:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Is 2 equal to 3?  Assign the result to the variable A.\n",
    "A = (2 == 3)\n",
    "print(A)\n",
    "print(type(A))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As you can see, the resulting variable on the left-hand side is then a boolean."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In situations like this parentheses make the logical statements more recognizable.\n",
    "\n",
    "For example, consider"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x = 3\n",
    "y = 4\n",
    "A = x + y == 7\n",
    "print(A)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To make it easier to see what's going on, use parentheses around the logical operation in the assignment to <code>A</code>:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "True\n"
     ]
    }
   ],
   "source": [
    "x = 3\n",
    "y = 4\n",
    "A = (x + y == 7)  # Let's make the right-hand side obvious!\n",
    "print(A)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = 1\n",
    "b = 0\n",
    "print(a > b)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Boolean values can be stored in variables.  In the following example, the logical statement \"is <code>a</code> strictly greater than <code>b</code>\" is true."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = 1\n",
    "b = 0\n",
    "a_is_bigger = (a > b)\n",
    "print(a_is_bigger)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here is a more complex example of a Boolean expression, which asks whether <code>x</code> lies between -2 and 1."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x = 3\n",
    "x_is_in_range = (-2 <= x <= 1)\n",
    "print(x_is_in_range)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<p class=\"try_it\">\n",
    "<b>Try it yourself.</b> Write boolean expressions using \n",
    "<code style=\"background:inherit;\">==, !=, >, >=, <, <=</code> and print the results.\n",
    "</p>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Logical operations\n",
    "\n",
    "The basic operations on boolean variables are \n",
    "* <code>not</code>, \n",
    "* <code>and</code>,\n",
    "* <code>or</code>."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbpresent": {
     "id": "1c2e0864-67b9-4ae4-a6f2-30bfb33c0e9e"
    }
   },
   "source": [
    "# Logical NOT <a id=\"not\"/>\n",
    "\n",
    "Applying the NOT operation to a statement produces its negation.\n",
    "\n",
    "For example, \n",
    "<ul>\n",
    "    <li>the statement \"llamas are larger than frogs\" is true, so its negation, \"llamas are not larger than frogs\", is false;</li>\n",
    "    <li>the statement \"llamas have a beak to eat honey\" is false, so its negation, \"llamas do not have a beak to eat honey\", is true.</li>\n",
    "</ul>\n",
    "\n",
    "In general, if <code>A</code> is a boolean variable, then \n",
    "```python\n",
    "not A\n",
    "```\n",
    "is\n",
    "<ul>\n",
    "    <li>True, if <code>A</code> is False, and</li>\n",
    "    <li>False, if <code>A</code> is True.</li>\n",
    "</ul>\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"not True: \", not True)\n",
    "print(\"not False:\", not False)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbpresent": {
     "id": "e8c2bd6c-25cf-400d-891e-9fdf944ac795"
    }
   },
   "outputs": [],
   "source": [
    "fake_gnus = False\n",
    "print(fake_gnus)\n",
    "print(not fake_gnus)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbpresent": {
     "id": "4fa73ea2-b2d2-43bc-9c21-eefb927d5549"
    }
   },
   "outputs": [],
   "source": [
    "good_gnus = True\n",
    "print(good_gnus)\n",
    "print(not good_gnus)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "stmt = (2 > 1)  # 2 > 1 is true.\n",
    "print(stmt)\n",
    "print(not stmt)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<p class=\"try_it\">\n",
    "<b>Try it yourself.</b> \n",
    "Create a boolean expression and print the <b>not</b> of it.\n",
    "</p>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbpresent": {
     "id": "defdeda3-e2bd-465c-a9f6-ba49ad58a818"
    }
   },
   "source": [
    "# Logical AND  <a id=\"and\"></a>\n",
    "\n",
    "If <code>A</code> and <code>B</code> are two boolean variables, then \n",
    "```python\n",
    "A and B\n",
    "```\n",
    "is\n",
    "<ul>\n",
    "    <li>True, if <b>both</b> <code>A</code> and <code>B</code> are True, and</li>\n",
    "    <li>False, otherwise.</li>\n",
    "</ul>\n",
    "\n",
    "In other words, both A <b>and</b> B must be true for the expression\n",
    "```python\n",
    "A and B\n",
    "```\n",
    "\n",
    "to be true."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"True and True:  \", True and True)\n",
    "print(\"True and False: \", True and False)\n",
    "print(\"False and True: \", False and True)\n",
    "print(\"False and False:\", False and False)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbpresent": {
     "id": "8492d870-1bd3-4694-9249-836380372f04"
    }
   },
   "outputs": [],
   "source": [
    "print((2 > 1) and (1 > 1))  # (2 > 1) is true but (1 > 1) is false."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbpresent": {
     "id": "57b5ac26-420f-4404-b591-e5bb1aae3220"
    }
   },
   "outputs": [],
   "source": [
    "print((2 > 1) and (1 >= 0))  # (2 > 1) is true and (1 >= 0) is true."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x = 3\n",
    "y = 4\n",
    "A = (x + y > 6) # true,  since x + y = 7 > 6\n",
    "B = (x*y > 21)  # false, since x*y = 12 < 21\n",
    "print(A and B)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<p class=\"try_it\">\n",
    "<b>Try it yourself.</b> \n",
    "Create two boolean expressions and print the <code class=\"kw\">and</code> of them.\n",
    "</p>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Python stops evaluating a boolean expression as soon as it can determine whether the statement is true or false.  This is why the following code works &ndash; Python stops after seeing the <code class=\"kw\">and</code> because it knows the statement can't be true.  As a consequence, it avoids the division-by-zero error that would occur computing <code>x/y</code>."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "hoo!\n"
     ]
    }
   ],
   "source": [
    "x = 1\n",
    "y = 0\n",
    "if y != 0 and x/y > 42:\n",
    "    print('boo!')\n",
    "else:\n",
    "    print('hoo!')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can sometimes avoid the need for <code class=\"kw\">and</code>.  For instance, we can rewrite"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "42\n"
     ]
    }
   ],
   "source": [
    "x = 42\n",
    "if x > 40 and x < 54:\n",
    "    print(x)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "more clearly and conveniently as"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "42\n"
     ]
    }
   ],
   "source": [
    "x = 42\n",
    "if 40 < x < 54:\n",
    "    print(x)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbpresent": {
     "id": "53727740-065c-4d71-a33d-b0eef7e27c24"
    }
   },
   "source": [
    "# Logical OR <a id=\"or\"/>\n",
    "\n",
    "If <code>A</code> and <code>B</code> are two boolean variables, then \n",
    "```python\n",
    "A or B\n",
    "```\n",
    "is\n",
    "<ul>\n",
    "    <li>True, if <b>at least one</b> of <code>A</code> and <code>B</code> is True, and</li>\n",
    "    <li>False, otherwise.</li>\n",
    "</ul>\n",
    "Thus,\n",
    "<pre>\n",
    "(it is raining) or (it is not raining)\n",
    "</pre>\n",
    "is <b>always true</b> since one of the two statements must be true."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"danger\"></div><div class=\"danger\"></div>\n",
    "This is not the everyday meaning of \"or\".  In everyday speech when we say \"or\", we imply a choice of two statements, <b>only one of which is true</b>.  This is called the <b>exclusive OR</b>, or <b>XOR</b>, which is discussed below."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This means that if you are a Real Computer Scientist&trade; and someone asks a question such as, \"Do you want pizza tonight, or not?\", you should always answer \"Yes!\", since one of those two statements must be true. (However, even though my wife is also a computer scientist she doesn't find this nearly as amusing as I do.)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"True or True:  \", True or True)\n",
    "print(\"True or False: \", True or False)\n",
    "print(\"False or True: \", False or True)\n",
    "print(\"False or False:\", False or False)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbpresent": {
     "id": "08b7819c-dfec-457f-9781-4d83d84014fb"
    }
   },
   "outputs": [],
   "source": [
    "T = True\n",
    "F = False\n",
    "print(T or F)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbpresent": {
     "id": "77d39584-3f29-436b-9be1-9a732f4ba504"
    }
   },
   "outputs": [],
   "source": [
    "print(T or T)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbpresent": {
     "id": "d1c6f796-6528-4f9a-9cfa-8e0be9cf256a"
    }
   },
   "outputs": [],
   "source": [
    "print(F or F)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print((2 > 1) or (1 > 1)) # (2 > 1) is true but (1 > 1) is false."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print((2 > 1) or (1 >= 0)) # (2 > 1) is true and (1 >= 0) is true."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# The very useful <code class=\"kw\">in</code> statement  <a id=\"in\"/>\n",
    "\n",
    "The <code class=\"kw\">in</code> statement allows us to quickly check if a character appears in a string:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('s' in 'cats')\n",
    "print('s' in 'cat')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "word = 'ghost'\n",
    "if 'g' in word:\n",
    "    print('boo!')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Later we will see <code class=\"kw\">in</code> used to test for membership in other types of collections."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# The very scary <code class=\"kw\">is</code> statement 😱  🐞<a id=\"is\"></a>\n",
    "\n",
    "The <code class=\"kw\">is</code> statement is very dangerous.  It tests whether two objects are just different names for the same object, as opposed to testing whether the values of the two objects are the same."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"danger\"></div><div class=\"danger\"></div>\n",
    "Don't use <code class=\"kw\">is</code> (except in one very special case).  Its behavior is too unpredictable, which makes it dangerous."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's look at some examples where it works like you might think:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "x is 256?     True\n",
      "x == 256?     True\n",
      "x is not 256? False\n"
     ]
    },
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "<>:2: SyntaxWarning: \"is\" with a literal. Did you mean \"==\"?\n",
      "<>:4: SyntaxWarning: \"is not\" with a literal. Did you mean \"!=\"?\n",
      "<>:2: SyntaxWarning: \"is\" with a literal. Did you mean \"==\"?\n",
      "<>:4: SyntaxWarning: \"is not\" with a literal. Did you mean \"!=\"?\n",
      "<ipython-input-5-100b317c723e>:2: SyntaxWarning: \"is\" with a literal. Did you mean \"==\"?\n",
      "  print(f'x is 256?     {x is 256}')\n",
      "<ipython-input-5-100b317c723e>:4: SyntaxWarning: \"is not\" with a literal. Did you mean \"!=\"?\n",
      "  print(f'x is not 256? {x is not 256}')\n"
     ]
    }
   ],
   "source": [
    "x = 256\n",
    "print(f'x is 256?     {x is 256}')\n",
    "print(f'x == 256?     {x == 256}')\n",
    "print(f'x is not 256? {x is not 256}')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "What's less intuitive is why Python is warning us about our code.  This warning was introduced in Python 3.8; it's a sign that <code class=\"kw\">is</code> is trouble."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let's look at some unintuitive behavior that explains the warning:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "x is 257?     False\n",
      "x == 257?     True\n",
      "x is not 257? True\n"
     ]
    },
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "<>:2: SyntaxWarning: \"is\" with a literal. Did you mean \"==\"?\n",
      "<>:4: SyntaxWarning: \"is not\" with a literal. Did you mean \"!=\"?\n",
      "<>:2: SyntaxWarning: \"is\" with a literal. Did you mean \"==\"?\n",
      "<>:4: SyntaxWarning: \"is not\" with a literal. Did you mean \"!=\"?\n",
      "<ipython-input-9-9f1e5c091d83>:2: SyntaxWarning: \"is\" with a literal. Did you mean \"==\"?\n",
      "  print(f'x is 257?     {x is 257}')\n",
      "<ipython-input-9-9f1e5c091d83>:4: SyntaxWarning: \"is not\" with a literal. Did you mean \"!=\"?\n",
      "  print(f'x is not 257? {x is not 257}')\n"
     ]
    }
   ],
   "source": [
    "x = 257\n",
    "print(f'x is 257?     {x is 257}')\n",
    "print(f'x == 257?     {x == 257}')\n",
    "print(f'x is not 257? {x is not 257}')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Whoa!  What's going on?**  Why is the statement <code>x is 256</code> true in the first example, but the statement <code>x is 257</code> is false in the second example?\n",
    "\n",
    "Let's use the built-in [<code class=\"kw\">id()</code>](https://docs.python.org/3/library/functions.html#id) function.  This function returns a unique identifier (an integer) that identifies the object internally (in the reference CPython implementation of Python [it's the address in memory where the object is stored](https://docs.python.org/3/reference/datamodel.html#objects-values-and-types))."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "4402829632\n",
      "4402829632\n",
      "x is 256?     True\n",
      "x == 256?     True\n"
     ]
    },
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "<>:4: SyntaxWarning: \"is\" with a literal. Did you mean \"==\"?\n",
      "<>:4: SyntaxWarning: \"is\" with a literal. Did you mean \"==\"?\n",
      "<ipython-input-7-d58b3991c4f5>:4: SyntaxWarning: \"is\" with a literal. Did you mean \"==\"?\n",
      "  print(f'x is 256?     {x is 256}')\n"
     ]
    }
   ],
   "source": [
    "x = 256\n",
    "print(id(x))\n",
    "print(id(256))\n",
    "print(f'x is 256?     {x is 256}')\n",
    "print(f'x == 256?     {x == 256}')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As you can see, in this case <code>x</code> and <code>256</code> are the same object, so <code class=\"kw\">is</code> evaluates to <code class=\"kw\">True</code>.  On the other hand,"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "140448389658608\n",
      "140448389658640\n",
      "x is 257?     False\n",
      "x == 257?     True\n"
     ]
    },
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "<>:4: SyntaxWarning: \"is\" with a literal. Did you mean \"==\"?\n",
      "<>:4: SyntaxWarning: \"is\" with a literal. Did you mean \"==\"?\n",
      "<ipython-input-8-73418bc1f9eb>:4: SyntaxWarning: \"is\" with a literal. Did you mean \"==\"?\n",
      "  print(f'x is 257?     {x is 257}')\n"
     ]
    }
   ],
   "source": [
    "x = 257\n",
    "print(id(x))\n",
    "print(id(257))\n",
    "print(f'x is 257?     {x is 257}')\n",
    "print(f'x == 257?     {x == 257}')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here <code>x</code> and <code>257</code> are <b>not</b> the same object.  \n",
    "\n",
    "This is an idiosyncracy of Python &ndash; sometimes it creates a new object; sometimes it uses an existing one. Per the <a href=\"https://docs.python.org/3/c-api/long.html#c.PyLong_FromLong\">Python documentation</a>:\n",
    "<blockquote>\n",
    "The current implementation keeps an array of integer objects for all integers between -5 and 256, when you create an int in that range you actually just get back a reference to the existing object. \n",
    "</blockquote>\n",
    "It keeps these numbers on hand since they are commonly used.\n",
    "\n",
    "If we change the variable then Python creates a new object, since otherwise we could change the value of 256:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x = 256\n",
    "print(id(x))\n",
    "print(id(256))\n",
    "print(x is 256)\n",
    "\n",
    "print('Now Python creates a new object...')\n",
    "x = 257\n",
    "print(id(x))\n",
    "print(id(257))\n",
    "print(x is 257)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "At this point <code>x</code> is not the same object as <code>256</code> or <code>257</code>."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To keep things interesting, none of these rules apply to floats:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x = 42.0\n",
    "y = 42.0\n",
    "print(x is y)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In addition to all these entertaining features of <code class=\"kw\">is</code>, its behavior in certain situations can also depend [on which implementation of Python you are using (e.g., CPython vs PyPy)](https://doc.pypy.org/en/latest/cpython_differences.html#object-identity-of-primitive-values-is-and-id).\n",
    "\n",
    "**So:**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"danger\"></div>\n",
    "Don't use <code class=\"kw\">is</code> (except in one very special case).  Its behavior is too unpredictable, which makes it dangerous."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h3>The one situation where you should use <code class=\"kw\">is</code></h3>\n",
    "\n",
    "The one place where it is safe to use <code class=\"kw\">is</code> (and is, in fact, encouraged) is in connection with <code class=\"kw\">None</code>.  This is because there is one, and only one, instance of the value <code class=\"kw\">None</code>.  \n",
    "\n",
    "To illustrate this, consider"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = None\n",
    "b = None"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We have"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(f'id(a):    {id(a)}')\n",
    "print(f'id(b):    {id(b)}')\n",
    "print(f'id(None): {id(None)}')\n",
    "print(a is b)\n",
    "print(a is None)\n",
    "print(b is None)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "From this we see that <code>a</code> and <code>b</code> are simply aliases for the one and only instance of <code class=\"kw\">None</code>, so it is safe to use <code class=\"kw\">is</code>."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The proper way to negate <code class=\"kw\">is</code> is to use <code class=\"kw\">is not</code>:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "n = 42\n",
    "print(n is not None)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This is equivalent to "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(not (n is None))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "but reads more clearly."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Conditional expressions 🤯  <a id=\"conditions_expr\"/>\n",
    "\n",
    "Python provides an alternative idiom to express if-then-else logic that effectively returns a single value.\n",
    "\n",
    "Consider the following, which sets <code>smaller</code> to the smaller of <code>a</code> and <code>b</code> (of course, the correct way to do this is to use the [min() function](https://docs.python.org/3/library/functions.html?highlight=built#min))."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = 54\n",
    "b = 42\n",
    "\n",
    "if a < b:\n",
    "    smaller = a\n",
    "else:\n",
    "    smaller = b\n",
    "    \n",
    "print(smaller)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This can also be expressed using a <b>conditional expression</b>:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = 54\n",
    "b = 42\n",
    "\n",
    "smaller = a if a < b else b  # The conditional expression is on the right-hand side.\n",
    "\n",
    "print(smaller)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Conditional assignments can be nested, which allows us to mimic if-elif-else logic.  Here's another example, which finds the median of the values <code>a</code>, <code>b</code>, and <code>c</code>.  Using if-elif-else we have"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = 54\n",
    "b = 42\n",
    "c = 64\n",
    "\n",
    "if (a < b < c) or (c < b < a):\n",
    "    median = b\n",
    "elif (b < a < c) or (c < a < b):\n",
    "    median = a\n",
    "else:\n",
    "    median = c\n",
    "    \n",
    "print(median)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can write this much more cryptically using conditional assignments:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = 54\n",
    "b = 42\n",
    "c = 64\n",
    "median = b if (a < b < c) or (c < b < a) else a if (b < a < c) or (c < a < b) else c\n",
    "print(median)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Parentheses make the preceding (slightly) less cryptic:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = 54\n",
    "b = 42\n",
    "c = 64\n",
    "median = b if ((a < b < c) or (c < b < a)) else (a if ((b < a < c) or (c < a < b)) else c)\n",
    "print(median)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### When should you use conditional expressions?\n",
    "\n",
    "As the previous example shows, conditional expressions can quickly get out of hand.  Conditional expressions should only be used when replacing simple if-else logic:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "foo = 54\n",
    "bar = 42\n",
    "foobar = 1 if foo < bar else 2\n",
    "print(foobar)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class="
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"danger\"></div>  Use conditional expressions <b>only if they enhance readability of the code</b>."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Exercises <a id=\"exercises\"/>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"exercise\">\n",
    "What is printed by the following code?  Try to trace through the code by hand before running it.\n",
    "<pre>\n",
    "x =  5\n",
    "y = -4\n",
    "a = -5\n",
    "b =  5\n",
    "print(y < x)\n",
    "print(a < x < b)\n",
    "print(a < y < b)\n",
    "print(x < (a+b))\n",
    "print((x < y) or (a < b))\n",
    "print((x < y) and (a < b))\n",
    "print(not (x < y))\n",
    "</pre>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b>Answer.</b>\n",
    "<div class=\"voila\">\n",
    "<pre>\n",
    "True\n",
    "False\n",
    "True\n",
    "False\n",
    "True\n",
    "False\n",
    "True\n",
    "</pre>\n",
    "<div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"exercise\">\n",
    "Find boolean values of a, b, and c so that the following code prints <code>True</code>.\n",
    "<pre>\n",
    "a = \n",
    "b = \n",
    "c =\n",
    "\n",
    "e = (not a) and (b or c) and (a or b) and (a or c)\n",
    "print(e)\n",
    "</pre>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b>Answer.</b>\n",
    "<div class=\"voila\">\n",
    "<pre>\n",
    "a = False\n",
    "b = True\n",
    "c = True\n",
    "\n",
    "e = (not a) and (b or c) and (a or b) and (a or c)\n",
    "print(e)\n",
    "</pre>\n",
    "This is the only possible set of values that make <code>e</code> True, because for <code>e</code> to be True, all four expressions pm the right must be true.  Thus,\n",
    "<ul>\n",
    "<li><code>not a</code> must be True, so <code>a</code> must be False;\n",
    "<li><code>a or b</code> must be True, so <code>b</code> must be True since <code>a</code> is False;\n",
    "<li><code>a or c</code> must be True, so <code>c</code> must be True since <code>a</code> is False.\n",
    "</ul>\n",
    "<div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"exercise\">\n",
    "<b>XOR.</b>\n",
    "The exclusive OR of <code>A</code> and <code>B</code> is True if exactly one of the two variables is True, and False otherwise.  How can you implement the exclusive OR of the boolean variables <code>A</code> and <code>B</code> using the other boolean operations?  There are multiple ways to do this.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b>Answer.</b>\n",
    "<div class=\"voila\">\n",
    "<b>Solution 1.</b>\n",
    "The exclusive OR means that either \n",
    "<pre class=\"voila\">\n",
    "A is true and B is false,\n",
    "</pre>\n",
    "or\n",
    "<pre class=\"voila\">\n",
    "A is false and B is true,\n",
    "</pre>\n",
    "The condition <code>A is true and B is false</code> is true if and only if\n",
    "<pre>\n",
    "(A and not B) is true,\n",
    "</pre>\n",
    "while the condition <code>A is false and B is true</code> is true if and only if\n",
    "<pre>\n",
    "(not A and B) is true,\n",
    "</pre>\n",
    "Putting these together, we see that the XOR of <code>A</code> and <code>B</code> is equivalent fo\n",
    "<pre>\n",
    "(A and not B) or (not A and B).\n",
    "</pre>\n",
    "\n",
    "<b>Solution 2.</b>\n",
    "A shorter but somewhat cryptic way to express the XOR of <code>A</code> and <code>B</code> is\n",
    "<pre>\n",
    "A != B.\n",
    "</pre>\n",
    "This statement is true if and only if one of the variables is true and the other is false.\n",
    "<div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Lagniappe: the XOR (exclusive OR) function 🤯  <a id=\"xor\"/>\n",
    "\n",
    "The exclusive OR, or XOR, is true if and only **exactly one** of the statements it connects is true.  When humans use the word \"or\", they typically mean the exclusive OR.\n",
    "\n",
    "There is an <code>xor()</code> function in Python, but it requires us to load the [<code>operator</code> module](https://docs.python.org/3/library/operator.html)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from operator import xor\n",
    "\n",
    "# Note that xor() is a function, not a statement like \"a xor b\".\n",
    "print(xor(True, False))\n",
    "print(xor(True, True))\n",
    "print(xor(False, False))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Remember that <code>xor(a,b)</code> is <code class=\"kw\">True</code> if and only if <b>exactly one</b> of <code>a</code> and <code>b</code> are true:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "good_gnus = True\n",
    "fake_gnus = False\n",
    "no_gnus = good_gnus\n",
    "old_gnus = no_gnus\n",
    "\n",
    "print(xor(good_gnus, fake_gnus))\n",
    "print(xor(no_gnus, old_gnus))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h4>This notebook was brought to you by the First Rule of Debugging:</h4>\n",
    "\n",
    "How often have I said to you that when you have eliminated the impossible, whatever remains, however improbable,\n",
    "must be the truth? <br/>\n",
    "&ndash; Sherlock Holmes, The Sign of Four"
   ]
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
