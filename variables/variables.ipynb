{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "nbpresent": {
     "id": "abaeef91-5553-4553-8ebf-2c22a87d7137"
    }
   },
   "source": [
    "<img src=\"http://www.cs.wm.edu/~rml/images/wm_horizontal_single_line_full_color.png\">\n",
    "\n",
    "<h1 style=\"text-align:center;\">CSCI 141-01, Fall 2023 course notes</h1>\n",
    "\n",
    "<h1 style=\"text-align:center;\">Statements, literals, and variables</h1>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Contents\n",
    "\n",
    "* [Statements](#Statements)\n",
    "    * <a href=\"#splitting_statements\">Splitting statements across lines</a>\n",
    "    * <a href=\"#semicolon\">Multiple statements on one line</a>\n",
    "* [Comments](#comments)    \n",
    "* [Variables](#variables)\n",
    "    * [NameError](#NameError)\n",
    "* [Basic types of variables](#types)\n",
    "    * <a href=\"#type_function\">Querying a variable's type</a>\n",
    "* [Naming rules](#naming)\n",
    "* [Literals](#literals)\n",
    "* [Multiple assignments](#multiple_assignment)\n",
    "* [More exercises](#exercises)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Statements\n",
    "\n",
    "A <b>statement</b> is a unit of code that specifies some action to be carried out.  For instance,"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('Hello, world!')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "tells Python to print the string 'Hello, world!'.\n",
    "\n",
    "Other examples include"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x = 2 + 2\n",
    "print(x)\n",
    "\n",
    "y = x + x\n",
    "print(x, y)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The statement <code>x = 2 + 2</code> is an example of an <b>assignment statement</b>.  The value <code>2 + 2</code> is assigned to the variable <code>x</code>.  In an assignment statement the object on the left-hand side of <code>=</code> is assigned the value of the object on the right-hand side."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Splitting statements across lines <a id=\"splitting_statements\"></a>\n",
    "\n",
    "In Python, all the parts of a statement are usually expected to be on a single line. \n",
    "\n",
    "For instance, the following yields an error:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x = 2 +\n",
    "2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Lines can be as long as you wish, but the people behind Python [recommend a limit of 79 characters](https://www.python.org/dev/peps/pep-0008/#maximum-line-length):\n",
    "<blockquote>\n",
    "Limit all lines to a maximum of 79 characters. <br/>\n",
    "\n",
    "For flowing long blocks of text with fewer structural restrictions (docstrings or comments), the line length should be limited to 72 characters.\n",
    "    \n",
    "Limiting the required editor window width makes it possible to have several files open side-by-side, and works well when using code review tools that present the two versions in adjacent columns.\n",
    "</blockquote>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you do need to split a statement across multiple lines, the [preferred method](https://www.python.org/dev/peps/pep-0008/#maximum-line-length) is to enclose the statement in parentheses:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x = (2\n",
    "+\n",
    "2)\n",
    "\n",
    "print(x)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Multiple statements on one line <a id=\"semicolon\"></a>\n",
    "\n",
    "Python allows more than one statement to appear on a single line provided the statements are separated by semicolons.\n",
    "\n",
    "I rarely if ever use this syntax, but it's good to know it in case you encounter it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Let's put a bunch of statements on a single line using semicolons!\n",
    "print('Hello, world!'); print('Nice to meet you!'); print('Argh!')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Some languages, such as C, C++, and Java, use semicolons to terminate statements and are required.  This is <b>not</b> the case with Python: the semicolon's only role is to separate statements.\n",
    "\n",
    "If you encounter an errant semicolon ending a Python statement it is harmless and probably just means that the author has spent a lot of time coding in a language that requires semicolons.  When I come back to Python after having spent time writing in C or C++ I find myself sprinkling semicolons in my Python code."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('This is OK!');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Comments in Python code <a id=\"comments\"></a>\n",
    "\n",
    "In Python, anything following a [pound sign](https://en.wikipedia.org/wiki/Number_sign) &#35; to the end of the line is treated as a <b>comment</b> and ignored."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Nothing should happen when we execute this cell.\n",
    "# print('Hello, world!')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "By adding a &#35; to the start of a line we turned the print statement into an inert comment.  This is called <b>commenting out</b> the code and is useful when you want to temporarily deactivate code but don't want to delete it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# More examples of comments.\n",
    "\n",
    "# Do not mix with bleach or other chemicals.\n",
    "\n",
    "# If rash persists, see a doctor.\n",
    "\n",
    "# If you have ice cream, I will give it to you. \n",
    "# If you have no ice cream, I will take it away from you. \n",
    "# This is an ice cream koan."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As your programs become more complicated, or as they are handed off to others, comments become important so that your code can be understood."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Variables <a id=\"variables\"/>\n",
    "\n",
    "A variable is a name we use to represent a value (e.g., a number, a string of letters) in a program.  Variable names are instances of what are called **identifiers** in the field of programming languages; other examples include the names of functions and keywords (discussed below)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbpresent": {
     "id": "b2a8fc22-cad1-441f-9bec-573571817fa7"
    }
   },
   "outputs": [],
   "source": [
    "message = 'Hello, world!'  # message is a variable\n",
    "print(message)\n",
    "r = 7  # r is a variable\n",
    "print('r:', r)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<p class=\"try_it\"><b>Try it yourself.</b> Place your own message in a variable and print it to the screen.</p>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbpresent": {
     "id": "2157c0cb-fb60-4704-8495-67e6ea85cd4c"
    }
   },
   "source": [
    "<div class=\"danger\"></div><div class=\"danger\"></div> The symbol \"=\" means <b>assignment</b>.  A single equal sign is used for assign a value to a variable.  It does not mean equality!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This means that there is perfectly fine Python code that would make your algebra teacher gnash their teeth:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbpresent": {
     "id": "aad38e00-034a-4c0f-acf8-850868b139d3"
    }
   },
   "outputs": [],
   "source": [
    "x = 42\n",
    "print('x is', x)\n",
    "x = x + 12       # If = meant equality rather than assignment, this would be nonsense!\n",
    "print('x is', x)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<p class=\"try_it\"><b>Try it yourself.</b> Create a new variable with the value 54 and print the result.</p>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbpresent": {
     "id": "13f3868c-36ee-40a0-989b-7cd5f01bb33d"
    }
   },
   "source": [
    "## Variables can vary!\n",
    "\n",
    "We can change the value and type of a variable as we wish."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbpresent": {
     "id": "875168b7-1da5-4ac6-b9d8-56eaeea091b4"
    }
   },
   "outputs": [],
   "source": [
    "x = 3.14\n",
    "print('x is', x)\n",
    "\n",
    "x = 'Hello, world!'\n",
    "print('Now x is', x)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<p class=\"try_it\"><b>Try it yourself.</b> Create a new variable with the value 54 and alter its value.</p>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the preceding example the variable <code>x</code> changed from being a number to a character string.  Changing the type of a variable, on the other hand, is generally considered a bad idea as it can lead to confusion."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#  Referencing undefined variables: NameError <a id=\"NameError\"/>\n",
    "\n",
    "If we try to use a variable before it has been brought into being we will trigger a \n",
    "<code class=\"error\">NameError</code>.  Python will complain we are trying to use a variable (\"name\") that has not been defined."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x = 42\n",
    "print(x)\n",
    "z = x + y  # y has not yet been defined\n",
    "print(z)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Observe that Python helpfully indicates the line where it recognized the error."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<p class=\"try_it\">\n",
    "<b>Try it yourself.</b> Write your own code that triggers a NameError.\n",
    "</p>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbpresent": {
     "id": "3b067de2-5b3e-4eca-8a01-de2758d30f2e"
    }
   },
   "source": [
    "# Basic Python variable types <a id=\"types\"></a>\n",
    "\n",
    "Variables in Python (and other computer languages) come in various flavors.  \n",
    "\n",
    "Python has a number of built-in variable types:\n",
    "* character strings, or strings: <code>\"Zounds!\", 'foo'</code>,</li>\n",
    "* integers (whole numbers): <code>-5, 0, 42</code>,</li>\n",
    "* floats (numbers with fractional parts): <code>1.2, 42.0</code>,</li>\n",
    "* booleans: <code>True, False</code>,</li>\n",
    "* others we'll see later (e.g., lists, dictionaries).</li>\n",
    "\n",
    "In addition, Python allows you to create your own types, called **classes**."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbpresent": {
     "id": "0cd94eae-fbeb-4877-b61a-315e89ba0f50"
    }
   },
   "source": [
    "## Querying the type of a variable <a id=\"type_function\"></a>\n",
    "\n",
    "You can determine the type of a Python variable using the <code class=\"kw\">type()</code> function.  Python refers to some of its built-in types using an abbreviated form of the name."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbpresent": {
     "id": "ab60390f-6349-4015-8538-f95a5ae5773d"
    }
   },
   "outputs": [],
   "source": [
    "# A string of characters.\n",
    "s = 'hello, world!'\n",
    "print(type(s))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbpresent": {
     "id": "d8dea1bc-b35d-4237-afcd-12b339054516"
    }
   },
   "outputs": [],
   "source": [
    "# An integer.\n",
    "i = 42\n",
    "print(type(i))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbpresent": {
     "id": "42aa862e-4e69-4e59-a0f5-2f250c095e5a"
    }
   },
   "outputs": [],
   "source": [
    "# A floating point number.\n",
    "x = 54.0\n",
    "print(type(x))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbpresent": {
     "id": "51765c29-164d-451d-b9f7-4c1584af752f"
    }
   },
   "outputs": [],
   "source": [
    "# A boolean.\n",
    "f = True\n",
    "print(type(f))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<p class=\"try_it\">\n",
    "<b>Try it yourself.</b> Create some new variables and print their types.\n",
    "</p>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbpresent": {
     "id": "492e71e2-98a6-4a4b-afa1-62e90d872282"
    }
   },
   "source": [
    "# Variables vs literals <a id=\"literals\"/>\n",
    "\n",
    "Only variables can be assigned values.\n",
    "\n",
    "Remember that in an assignment, the variable on the left is assigned the value on the right.  So what happens when we execute the following?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbpresent": {
     "id": "8be67179-8b27-405d-9176-645a3bc86546"
    },
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "2 = x"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbpresent": {
     "id": "15a7afb3-2755-40ca-8c6c-2227b6dd8721"
    }
   },
   "source": [
    "## Why did this error occur?\n",
    "\n",
    "The error occurs because the object on the left-hand side of the assignment is what is known as a <b>literal</b>.  A literal represents a fixed value.\n",
    "\n",
    "For example, [42](https://en.wikipedia.org/wiki/42_(number)#The_Hitchhiker's_Guide_to_the_Galaxy) is a fixed integer value, while 'Hello, world!' is a fixed string of characters.  \n",
    "```python\n",
    "h = 'Hitler'  # The string on the right-hand side is literally Hitler, \n",
    "              # while h is a variable.\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If we could assign values to literals, than we could legally do things like this,\n",
    "```python\n",
    "42 = 54\n",
    "```\n",
    "which is somewhat confusing, as <code>42</code> would then have the value <code>54</code>.\n",
    "\n",
    "We can, however, assign a variable the value of a literal:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbpresent": {
     "id": "5237167f-8146-43c3-8b5b-b964943b77b9"
    }
   },
   "outputs": [],
   "source": [
    "x = 2\n",
    "print(x)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<p class=\"try_it\">\n",
    "<b>Try it yourself.</b> \n",
    "Try assigning a literal a value to trigger an error.\n",
    "</p>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbpresent": {
     "id": "853be6d0-28e8-49cd-88db-5a5960292f70"
    }
   },
   "source": [
    "# Naming rules for variables <a id=\"naming\"></a>\n",
    "\n",
    "<ul>\n",
    "<li>\n",
    "    Variables can only contain letters, numbers, and underscores:\n",
    "    <ul>\n",
    "        <li><code>x</code> is ok,</li>\n",
    "        <li><code>x2</code> is ok,</li>\n",
    "        <li><code>ACK</code> is ok,</li>\n",
    "        <li><code>hello_world</code> is ok,</li>\n",
    "        <li><code>____</code> is ok (but not a good idea).</li>\n",
    "    </ul>\n",
    "</li>\n",
    "<li>\n",
    "    Variable names can start with a letter or an underscore, but not with a number:\n",
    "    <ul>\n",
    "        <li><code>retcd</code> is ok,</li>\n",
    "        <li><code>_retcd</code> is ok,</li>\n",
    "        <li><code>2x</code> is <b>not</b> ok, but</li>\n",
    "        <li><code>_2x</code> is ok.</li>\n",
    "    </ul>\n",
    "</li>\n",
    "<li>\n",
    "    Spaces are not allowed in variable names.  Use underscores instead.  E.g., \n",
    "    <ul>\n",
    "        <li><code>hello_world</code> is ok, but</li>\n",
    "        <li><code>hello world</code> is <b>not</b> ok.</li>\n",
    "    </ul>\n",
    "</li>\n",
    "<li>\n",
    "    Variable names should be descriptive, though not ridiculously long.\n",
    "</li>\n",
    "</ul>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbpresent": {
     "id": "86cf7b35-7ae3-423e-92c4-874357a3efac"
    }
   },
   "outputs": [],
   "source": [
    "hello_world = 3.14\n",
    "print('hello_world:', hello_world)\n",
    "\n",
    "x2 = 'hello, world'\n",
    "print('x2:' , x2)\n",
    "\n",
    "_retcd = 1\n",
    "print('_retcd:', _retcd)\n",
    "\n",
    "__ = 42\n",
    "print('__:', __)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As we have seen, if we allowed numbers (literals) as variable names, then chaos would ensue:\n",
    "```python\n",
    "42 = 6*9  # Suppose 42 were a legal name for a variable.\n",
    "x = 42    # What is the resulting value of x? 42? 54?\n",
    "```\n",
    "One could imagine allowing variables to start with a number provided that there is also one or more letter or underscore in the name:\n",
    "```python\n",
    "12345x = 54321  # This is only for illustration!\n",
    "```\n",
    "However, Python (and most other languages) don't allow this for a number of technical and historical reasons.  For one thing, allowing variables to start with a number it complicates parsing the code.  In the preceding statement, for instance, we don't know that \"12345x\" is the name of a variable until we have read six characters."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You should be aware that variables beginning and ending with underscores have [special meaning in some contexts](https://docs.python.org/3/reference/lexical_analysis.html#reserved-classes-of-identifiers)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbpresent": {
     "id": "8b19e0f4-ff7e-4334-ab86-8404e936d7f8"
    }
   },
   "source": [
    "## Keywords: words that cannot be used as variable names\n",
    "\n",
    "In every computer language there are certain words called <b>keywords</b> that cannot be used as the names of variables because they are already used as part of the language. Here are the \n",
    "<a href=\"https://docs.python.org/3/reference/lexical_analysis.html#keywords\">Python keywords</a> as of Python 3.11:\n",
    "\n",
    "<pre>\n",
    "False      await      else       import     pass\n",
    "None       break      except     in         raise\n",
    "True       class      finally    is         return\n",
    "and        continue   for        lambda     try\n",
    "as         def        from       nonlocal   while\n",
    "assert     del        global     not        with\n",
    "async      elif       if         or         yield\n",
    "</pre>\n",
    "\n",
    "Let's see what happens if we try to use keywords as names of variables."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbpresent": {
     "id": "f3228d4a-20dd-41b0-97a9-5248a9165b16"
    },
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "True = 42"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "return = 3.14"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<p class=\"try_it\">\n",
    "<b>Try it yourself.</b> \n",
    "Try using one of the keywords above as a variable and see what happens.\n",
    "</p>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbpresent": {
     "id": "9c5ac1e8-cee1-488b-8730-ee593b9b4de0"
    }
   },
   "source": [
    "# Multiple assignments <a id=\"multiple_assignment\"></a>\n",
    "\n",
    "Python allows two forms of multiple assignments.  The first works like you might think:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbpresent": {
     "id": "f0aba0de-d364-4026-a3d5-f0b4d6646c18"
    }
   },
   "outputs": [],
   "source": [
    "# Set both a and b to 1.\n",
    "a = b = 1\n",
    "print(a, b)\n",
    "\n",
    "# Now change b.\n",
    "b = 2\n",
    "print(a, b)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbpresent": {
     "id": "6a818063-dcb8-40e4-b3c5-3a534fe7ce99"
    }
   },
   "source": [
    "The other form is a bit freaky-deaky:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbpresent": {
     "id": "4ece9a9c-840c-45c4-8bea-ad93dd9d9fd3"
    }
   },
   "outputs": [],
   "source": [
    "a, b = 3, 4\n",
    "print(a, b)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbpresent": {
     "id": "046f3be2-60b1-4595-a4a9-d311b7a1f593"
    }
   },
   "source": [
    "Why would anyone want the latter type of assignment?\n",
    "\n",
    "One place where it is useful is in swapping values.  Let's try swapping the values of two variables.  We'll do it the wrong way first:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbpresent": {
     "id": "632eb57b-29cc-4550-a651-c40f5d1b29c3"
    },
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "a = 42\n",
    "b = 54\n",
    "print(a, b)\n",
    "\n",
    "# Let's swap the values a and b the wrong way.\n",
    "a = b  # a = 54, b = 54\n",
    "b = a  # a = 54, b = 54\n",
    "print(a, b)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbpresent": {
     "id": "eaa94a84-6727-4feb-a7a7-8136db016b9e"
    }
   },
   "source": [
    "Woops! that didn't work!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"exercise\">\n",
    "    <strong>Exercise</strong>\n",
    "    Can you see why this didn't work?\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Answer.**\n",
    "<div class=\"voila\">\n",
    "<p>\n",
    "We need to save the value of <code>a</code> before we overwrite it with the value of <code>b</code>.  \n",
    "</p>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We'll fix this with a temporary variable <code>c</code>:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbpresent": {
     "id": "0e6b3333-8081-463a-9a4b-e7de31f7ea2c"
    }
   },
   "outputs": [],
   "source": [
    "a = 42\n",
    "b = 54\n",
    "print(a, b)\n",
    "\n",
    "# Let's swap the values a and b, using a temporary variable.\n",
    "c = a  # a = 42, b = 54, c = 42\n",
    "a = b  # a = 54, b = 54, c = 42\n",
    "b = c  # a = 54, b = 42, c = 42\n",
    "print(a, b)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbpresent": {
     "id": "db701ae6-7d20-4d57-924e-3c77c07078d9"
    }
   },
   "source": [
    "Alternatively, with Python's multiple assignment, we can do the swap in one swell foop:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbpresent": {
     "id": "06e3a1d4-04d0-4c43-a92e-bb08311edef4"
    },
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "a = 42\n",
    "b = 54\n",
    "print(a, b)\n",
    "\n",
    "# Let's swap the values a and b.\n",
    "a, b = b, a\n",
    "print(a, b)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# More exercises <a id=\"exercises\"></a>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"exercise\">\n",
    "<b>Debug this!</b>\n",
    "Why, exactly, does the following code trigger a <code class=\"error\">NameError</code>?  Execute the code to see the problem.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "ml = 17\n",
    "n1 = 42\n",
    "print(m1+n1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b>Answer.</b> <div class=\"voila\">Look closely &ndash; are those ones or ells?</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"exercise\">\n",
    "<b>Trace this!</b>\n",
    "What will be printed by the following code?\n",
    "<code>\n",
    "c = 42\n",
    "a = b = c\n",
    "print(a, b)\n",
    "c = 54\n",
    "x, y = a, c\n",
    "print(x, y)\n",
    "x = b = 27\n",
    "x, y = y, x\n",
    "print(b, x, y)\n",
    "</code>\n",
    "See if you can figure out first without executing the code.  Check your answer by cutting and pasting the code into the following cell and executing it.\n",
    "</div>    "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b>Answer.</b>\n",
    "<div class=\"voila\">\n",
    "<pre>\n",
    "42 42\n",
    "42 54\n",
    "27 54 27\n",
    "</pre>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"exercise\">\n",
    "<b>Let's break things!</b>\n",
    "When you are learning a new programming language it is very helpful to make mistakes so you will recognize the error messages and how to fix them.\n",
    "    \n",
    "Try the following:\n",
    "<ul>    \n",
    "    <li>What happens if you leave the <code>+</code> out of <code>2 + 2</code>?</li>\n",
    "    <li>What happens if you leave the first <code>2</code> out of <code>2 + 2</code>?</li>\n",
    "    <li>What happens if you omit the first quotation mark from \"hello\"?  the last quotation mark?</li>\n",
    "    <li>What happens if you omit the first parenthesis in <code>print(\"hello, world!\")</code>?  the last one?  both of them?\n",
    "</ul>        \n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h4>This notebook was brought to you by facts:</h4>\n",
    "    \n",
    "Facts are meaningless.  You could use facts to prove anything that's even remotely true! <br/>\n",
    "&ndash; Homer Simpson"
   ]
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
