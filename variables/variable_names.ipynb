{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "nbpresent": {
     "id": "abaeef91-5553-4553-8ebf-2c22a87d7137"
    }
   },
   "source": [
    "<img src=\"http://www.cs.wm.edu/~rml/images/wm_horizontal_single_line_full_color.png\">\n",
    "\n",
    "<h1 style=\"text-align:center;\">CSCI 141-01, Fall 2023 course notes</h1>\n",
    "<h1 style=\"text-align:center;\">What's in a name?  Choosing names for variables</h1>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Contents\n",
    "\n",
    "* [Choosing variable names](#choosing_names)\n",
    "* [Make sure constants are obvious](#constants)\n",
    "* [Use variable names that are meaningful and apt](#meaningful)\n",
    "* [Eschew sesquipedalian variable names](#brevity)\n",
    "* [Choose variable names that won't be confused](#confusion)\n",
    "    * [Be careful using certain characters](#bad_characters)\n",
    "    * [Avoid long names that differ only at the end](#long_names)\n",
    "    * [Take care when abbreviating](#abbreviating)\n",
    "    * [Similar variable names are dangerous, in general](#similar_names)\n",
    "* [K&R style vs camelCase](#KR)\n",
    "* [Exercises](#exercises)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Choosing variable names <a id=\"choosing_names\"/>\n",
    "\n",
    "<blockquote>\n",
    "Programs must be written for people to read, and only incidentally for machines to execute. <br/>\n",
    "&ndash; Harold Abelson, Gerald Jay Sussman, and Julie Sussman, <i>Structure and Interpretation of Computer Programs</i>\n",
    "</blockquote>\n",
    "Like naming babies, naming variables can have long-term consequences.  Poor choices of variable names will make it harder to understand the code, easier to introduce bugs, and harder to find bugs.\n",
    "\n",
    "This discussion owes greatly to the book <i>The Elements of Programming Style</i> by Brian Kernighan and Phillip J. Plaugher."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Use variables to express constants\n",
    "\n",
    "Some variables actually represent constants (so they're \"constant variables\").  We will refer to these as <b>named constants</b>.  \n",
    "\n",
    "Using variables rather than explicit values makes code more intelligible and less error-prone.  Consider the following equivalent code snippets for computing the diameter and area of a circle."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "PI = 3.141592653589793\n",
    "\n",
    "radius = 2\n",
    "diameter = 2 * PI * radius\n",
    "area = PI * radius**2\n",
    "print(diameter, area)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "radius = 2\n",
    "diameter = 2 * 3.141592653589793 * radius\n",
    "area = 3.141592653589793 * radius**2\n",
    "print(diameter, area)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Using the named constant <code>PI</code> makes the code clearer and less error-prone because it makes the expression for the area of the circle read like the formula as we tend to say it.  (It also makes it easier to update the code in case the value of $\\pi$ changes.)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"danger\"></div> Use named constants.  Avoid  <a href=\"https://en.wikipedia.org/wiki/Magic_number_(programming)\" target=\"_blank\">magic numbers</a>."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b>Aside:</b> Familiar mathematical constants such as $\\pi$ and $e$ are best retrieved from the <code>math</code> module:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import math\n",
    "print(math.pi)\n",
    "print(math.e)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Capitalize the names of named constants \n",
    "\n",
    "Make sure constants are obvious.\n",
    "\n",
    "Python, unlike some other languages, does not have a special way of indicating constants.\n",
    "The [Python style guide](https://www.python.org/dev/peps/pep-0008/#constants) recommends that variables that represent constants should be capitalized.\n",
    "\n",
    "For instance, we might write the decimal value of different US coins as follows:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "PENNY   = 0.01\n",
    "NICKEL  = 0.05\n",
    "DIME    = 0.10\n",
    "QUARTER = 0.25\n",
    "HALF_DOLLAR = 0.50"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"danger\"></div> Follow this convention.  It's a bit of defensive programming &ndash; if you see a variable with a capitalized name being changed after being initialized, then you know you're looking at an error."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Use variable names that are meaningful and apt <a id=\"meaningful\"/>\n",
    "\n",
    "Choose variable names that are meaningful and descriptive of what the variable represents.  Which of the following makes the most sense to you?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Bad choices of variable names.  \n",
    "\n",
    "# I once worked somewhere where my boss and I wrote code like \n",
    "# this to protest the company's lack of coding standards.\n",
    "\n",
    "_  = 42\n",
    "__ = 54\n",
    "___ = _ * __\n",
    "print(___)\n",
    "\n",
    "____ = 3\n",
    "print(____)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Bad choices of variable names.\n",
    "x = 42\n",
    "xx = 54\n",
    "a = x * xx\n",
    "two = 3"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Good choices of variable names.\n",
    "width  = 42\n",
    "height = 54\n",
    "area = width * height\n",
    "TWO = 2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Observe that the latter code is self-explanatory because the names indicate what is going on."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Eschew sesquipedalian variable names <a id=\"brevity\"/>\n",
    "\n",
    "While variable names should descriptive, they should not be overly verbose.  For instance, these are excessive:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Compute the area of a rectangle with excessively long variable names.\n",
    "we_enjoy_typing_so_lets_make_our_variable_names_really_long = 3\n",
    "the_length_of_the_right_hand_side_of_the_rectangle = 42\n",
    "the_length_of_the_top_side_of_the_rectangle = 54\n",
    "the_area_of_the_rectangle = the_length_of_the_right_hand_side_of_the_rectangle * the_length_of_the_top_side_of_the_rectangle"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Besides being ridiculously long, we are apt to introduce typographical errors in names of this length, and it will be harder to find these typos because the names are so long."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"danger\"></div>Remember: the more you type, the more likely you are to make a mistake."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The following is a bit better:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Compute the area of a rectangle with shorter but still not colloquial variable names.\n",
    "right_length = 42\n",
    "top_length   = 54\n",
    "area = right_length * top_length"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "However, most of us would say \"the area of a rectangle is its width times its height\", so why not choose variable names that reflect this?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Compute the area of a rectangle using variable names so that the code corresponds to \n",
    "# what we say when we speak.\n",
    "width  = 54\n",
    "height = 42\n",
    "area = width * height"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Choose variable names that won't be confused <a id=\"confusion\"/>\n",
    "\n",
    "Don't shoot yourself in the foot with infelicitous choices of variable names."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Be careful using certain characters <a id=\"bad_characters\"/>\n",
    "\n",
    "Watch out for characters that are easily confused, e.g., \n",
    "<ul>\n",
    "    <li>the lowercase letter l (\"ell\") and the number 1,</li>\n",
    "    <li>the uppercase letter O (\"oh\") and the number 0,</li>\n",
    "    <li>the uppercase letter S (\"ess\") and the number 5.</li>\n",
    "</ul>    \n",
    "\n",
    "For instance, a variable named <code>l</code> is a Bad Idea&#0153; since it looks very much like a <code>1</code>, as the following code illustrates:\n",
    "<pre>\n",
    "l = 7\n",
    "x = 7 - l\n",
    "y = 7 - 1\n",
    "</pre>\n",
    "Here are some more examples of unfortunate variable names:\n",
    "<pre>\n",
    "lO = 42\n",
    "x  = 54 - lO\n",
    "y  = 54 - 10\n",
    "</pre>\n",
    "and\n",
    "```python\n",
    "I1 = 3\n",
    "Il = 6\n",
    "N05S = 1\n",
    "N055 = 2\n",
    "NOSS = 3\n",
    "NO5S = 4\n",
    "NO55 = 5\n",
    "# Here are some good opportunities for bugs:\n",
    "Il = I1 + 1 \n",
    "NO5S = NOSS + 1\n",
    "```\n",
    "\n",
    "(the last set of examples was taken from <i>The Elements of Programming Style</i>).\n",
    "\n",
    "While the differences might be visible to you in these short snippets, they might not be so obvious to you in a larger segment of code, or at 3 am in the morning.\n",
    "\n",
    "These sorts of variable name make it easier to introduce bugs and harder to find them.  Avoid them."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Avoid long names that differ only at the end <a id=\"long_names\"/>\n",
    "\n",
    "For instance,\n",
    "```python\n",
    "xpos = 42\n",
    "ypos = 54\n",
    "```\n",
    "are better than\n",
    "```python\n",
    "positionx = 42\n",
    "positiony = 54\n",
    "```\n",
    "The latter are much more likely to lead to bugs, particularly if we are cutting and pasting code.  It's also harder to distinguish the latter variables since we have to read the names all the way to the end."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Take care when abbreviating <a id=\"abbreviating\"/>\n",
    "\n",
    "When abbreviating,\n",
    "<ul>\n",
    "    <li>always keep first letters,</li>\n",
    "    <li>favor pronouncable variable names (e.g., <code>xpos</code> rather than <code>xpstn</code>) so you can read your code aloud,</li>\n",
    "    <li>and above all, be consistent (e.g., don't name the variable for the x-coordinate <code>xpos</code> and one for the y-coordinate <code>posy</code>).</li>\n",
    "</ul>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Similar variable names can be dangerous <a id=\"similar_names\"/>\n",
    "\n",
    "For instance,\n",
    "```python\n",
    "k = 42\n",
    "n = k\n",
    "nn = k**2\n",
    "nnn = k**3\n",
    "```\n",
    "is dangerous because we might type <code>nn</code> where we mean <code>nnn</code>.\n",
    "\n",
    "This is better:\n",
    "```python\n",
    "k = 42\n",
    "n     = k\n",
    "nsq   = k**2\n",
    "ncube = k**3\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# K&amp;R style vs camel case <a id=\"KR\"/>\n",
    "\n",
    "There are two main approaches to dealing with variable names consisting of multiple words.\n",
    "\n",
    "One approach &ndash; the right one &ndash; we will call <strong>K&amp;R style</strong> (sometimes it's called [<strong>snake case</strong>](https://en.wikipedia.org/wiki/Snake_case)).  It has been around since the 1960s but was widely popularized by its use in the extremely influential book <i>The C Programming Language</i> by Brian Kernighan and Dennis Ritchie (K&amp;R) in the 70s and 80s. \n",
    "\n",
    "K&amp;R style looks like this:\n",
    "```python\n",
    "i_am_a_variable = 3.14\n",
    "left_margin = 42\n",
    "```\n",
    "Words are not capitalized and are separated by underscores.\n",
    "\n",
    "The alternative convention for multi-word variable names is called [<strong>camelCase</strong> or <strong>CamelCase</strong>](https://en.wikipedia.org/wiki/Camel_case).  You have encountered this style in such \"words\" as PowerPoint, FedEx, and iPhone.  In camelCase the first letter is not capitalized, but the initial letters of subsequent words are:\n",
    "```python\n",
    "iAmAVariable = 3.14\n",
    "leftMargin = 42\n",
    "```\n",
    "In CamelCase all initial letters are capitalized:\n",
    "```python\n",
    "IAmAVariable = 3.14\n",
    "LeftMargin = 42\n",
    "```\n",
    "\n",
    "You should use K&amp;R style names:\n",
    "<ul>\n",
    "<li>Kernighan and Ritchie are giants in the history of computer science (<a href=\"https://amturing.acm.org/award_winners/ritchie_1506389.cfm\">Ritchie was a Turing Award recipient</a>) and you could do a whole lot worse than adopting their programming style.</li>\n",
    "<li>Space between words (as in K&amp;R style) is seen as <a href=\"https://www.sup.org/books/title/?id=683\">\n",
    "    a great advance in Western history</a>.</li>\n",
    "<li>The evils of camelCase have been decried in <a href=\"https://www.nytimes.com/2009/11/29/magazine/29FOB-onlanguage-t.html?_r=2&ref=magazine&\">the pages of the New York Times</a>.</li>\n",
    "<li>The proponents of Java encourage the use of camelCase.  The association of camelCase with Java, the worst programming language ever, is reason enough to avoid it.</li>\n",
    "<li>\n",
    "It's been scientifically established that K&amp;R style <a href=\"http://www.cs.kent.edu/~jmaletic/papers/ICPC2010-CamelCaseUnderScoreClouds.pdf\">improves our comprehension of programs</a>.  You don't want to be a science denier, do you?\n",
    "</li>    \n",
    "</ul>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"danger\"></div>  Use K&amp;R style."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Exercises <a id=\"exercises\"></a>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"exercise\">\n",
    "    <b>Exercise</b>\n",
    "    <p>Should you ever date someone who uses camelCase?</p>\n",
    "</div>    "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b>Answer</b>\n",
    "\n",
    "<div class=\"voila\">\n",
    "    No.  You should pray for them instead.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"exercise\">\n",
    "    <b>Exercise</b> \n",
    "    <p>Can you be friends with someone who uses camelCase?</p>\n",
    "</div>    "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b>Answer</b>\n",
    "\n",
    "<div class=\"voila\">\n",
    "    Of course you can!  But you should still pray for them.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### This notebook was brought to you by civilization.\n",
    "\n",
    "Each new generation born is in effect an invasion of civilization by little barbarians,\n",
    "who must be civilized before it is too late. <br/>\n",
    "&ndash; Thomas Sowell"
   ]
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
