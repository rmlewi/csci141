{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"http://www.cs.wm.edu/~rml/images/wm_horizontal_single_line_full_color.png\">\n",
    "\n",
    "<h1 style=\"text-align:center;\">CSCI 141, Fall 2023 course notes</h1>\n",
    "<h1 style=\"text-align:center;\">Sets</h1>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Contents\n",
    "* [Sets](#sets)\n",
    "    * [The set constructor](#constructor)\n",
    "* [Mutability of sets](#mutable)\n",
    "* [Testing for membership](#in)\n",
    "* [Testing for overlap](#overlap)\n",
    "* [Sets do not allow duplicates](#no_duplicates)\n",
    "* [Iterating over sets](#iterating)\n",
    "* [Sets cannot be indexed](#no_indexing)\n",
    "* [Set comparison](#comparison)\n",
    "* [Frozen sets](#frozenset)\n",
    "    * [Sets of sets of lists](#sets_o_sets)\n",
    "* [Set methods](#methods)\n",
    "* [Set comprehensions](#comprehensions)\n",
    "* [Sets and references](#references)\n",
    "* [Exercises](#exercises)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Sets <a id=\"sets\"></a>\n",
    "\n",
    "In Python, a <b>set</b> is an <b>unordered</b> collection with no duplicate elements. \n",
    "\n",
    "Note that the set consisting of 1 and 'cat' is the same as the set consisting of 'cat' and 1.  \n",
    "\n",
    "Sets may be used to test for membership and also to eliminate duplicate entries. \n",
    "\n",
    "Sets also support mathematical set operations like union, intersection, set difference, and symmetric set difference.\n",
    "\n",
    "The full 411 on sets [may be found in the Python documentation](https://docs.python.org/3/library/stdtypes.html#set-types-set-frozenset).\n",
    "\n",
    "Sets are indicated by squiggly brackets <code>{}</code>."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ducks = {'huey', 'dewie', 'louie'}\n",
    "primes = {2, 3, 5, 7, 9, 11, 13, 15}\n",
    "\n",
    "print(ducks)\n",
    "print(type(ducks))\n",
    "print(primes)\n",
    "print(type(primes))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b>Question.</b>  How can Python distinguish sets from dictionaries, since both use squiggly brackets?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b>Answer.</b>\n",
    "<div class=\"voila\">\n",
    "Dictionaries have key:value pairs; sets do not.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can mix types inside a single set:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pies = {'lemon chess', 'cherry', 3.14159}\n",
    "print(pies)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A set may be empty."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"danger\"></div> To create an empty set you have to use the constructor <code>set()</code>, not {}.  The latter creates an empty dictionary."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# an empty set...\n",
    "em = set()\n",
    "print('em:', em)\n",
    "print('em is a', type(em))\n",
    "\n",
    "# ...vs an empty dictionary\n",
    "pty = {}\n",
    "print('pty:', pty)\n",
    "print('pty is a', type(pty))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can determine the number of elements in a set using the <code>len()</code> function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(len(pies))\n",
    "print(len(primes))\n",
    "print(len(em))\n",
    "pies.add('mud')\n",
    "print(len(pies))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The <code>set()</code> constructor <a id=\"constructor\"/>\n",
    "\n",
    "Based on what you've seen for <code>list</code>, what do you think happens if we pass the <code>set()</code> constructor a string?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "foo = list('pahoehoe')\n",
    "print(foo)\n",
    "\n",
    "foo = set('pahoehoe')\n",
    "print(foo)\n",
    "\n",
    "foo = set('possumkitten')\n",
    "print(foo)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As with lists, the string is broken down into its constituent characters.\n",
    "\n",
    "In addition, duplicate characters are dropped from the set."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If we pass a list of strings, then we obtain a set of strings:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "foo = set(['word', 'word'])\n",
    "print(foo)\n",
    "\n",
    "foo.add('word')\n",
    "print(foo)  # no change since 'word' is a duplicate.\n",
    "\n",
    "foo = set(['possum', 'kitten'])\n",
    "print(foo)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### In general...\n",
    "\n",
    "...the <code>set()</code> constructor takes a single argument, and that argument **must be iterable**.  The set is then constructed from the individual items in the iterable argument, with duplicates omitted.\n",
    "\n",
    "For instance, we could pass the constructor a list."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "c = set(ducks)\n",
    "print(c)\n",
    "print(type(c))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "foo = set([1, 2, 1, 4, 5, 4])\n",
    "print('Look! no duplicates:', foo)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Sets are mutable <a id=\"mutable\"/>\n",
    "\n",
    "A number of set methods enable us to modify sets.  Here we look at a few examples, the [<code>add()</code>, <code>remove()</code>, <code>discard()</code>, and <code>pop()</code> methods](https://docs.python.org/3/library/stdtypes.html#set-types-set-frozenset)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "A = {1, 2, 3, 4}\n",
    "print(A)\n",
    "A.add(42)\n",
    "print(A)\n",
    "A.add(1) # duplicate value; ignored\n",
    "print(A)\n",
    "A.remove(3)\n",
    "print(A)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "What happens if we try to remove an element of the set that is not present?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "A = {1, 2, 3, 4}\n",
    "A.remove(54)\n",
    "print(A)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The <code>discard()</code> method removes a specified element but does <b>not</b> throw an error if the requested element is not present."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "A = {1, 2, 3, 4}\n",
    "A.discard(54)\n",
    "print(A)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The <code>pop()</code> method returns an element of the set and removes the element from the set.  Since sets have no order, there is no telling which element you will get."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "A = {42, 1, 2, 3, 4}\n",
    "print(A.pop())\n",
    "print(A)\n",
    "\n",
    "print(A.pop())\n",
    "print(A)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Testing for membership <a id=\"in\"/>\n",
    "\n",
    "You can check whether a value appears in a set using <code class=\"kw\">in</code>."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "A = {1, 2, 3, 4}\n",
    "x = 3\n",
    "y = 42\n",
    "\n",
    "if (x in A):\n",
    "    print('x is in A!')\n",
    "else:\n",
    "    print('x in not in A!')\n",
    "\n",
    "if (y in A):\n",
    "    print('y is in A!')\n",
    "else:\n",
    "    print('y in not in A!')\n",
    "\n",
    "if (y not in A):\n",
    "    print('ack!')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Testing for overlap  <a id=\"overlap\"/>\n",
    "\n",
    "There is an <code>isdisjoint()</code> method to check whether two sets have any common elements:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "A = {1, 2, 3, 4}\n",
    "B = {3, 4, 5, 6}\n",
    "C = {5, 6, 7, 8}\n",
    "\n",
    "print(A.isdisjoint(B))\n",
    "print(A.isdisjoint(C))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Sets do not allow duplicates <a id=\"no_duplicates\"/>\n",
    "\n",
    "If there are duplicates in the set you specify, the duplicates are dropped."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "buffalo = {'buffalo', 'buffalo', 'buffalo', 'buffalo'}\n",
    "print('The set buffalo consists of', buffalo)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Of course, with strings, case matters:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "buffalo = {'Buffalo', 'buffalo', 'Buffalo',\n",
    "           'buffalo', 'buffalo', 'buffalo', \n",
    "           'Buffalo', 'buffalo'}\n",
    "print('The set buffalo consists of', buffalo)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "With integers the value matters, not how we express the integer:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "S = {2, 4//2}\n",
    "print(S)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b>Question.</b>  What will the following set be?\n",
    "<code>\n",
    "S = {2, 4/2} ?\n",
    "</code>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "S = {2, 2.0}\n",
    "print(S)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b>Answer.</b>\n",
    "<div class=\"voila\">\n",
    "<p>\n",
    "This behavior might seem odd at first, since 2 is an integer and 2.0 is a float.  However, if you run the following code you'll see that Python sez 2 is equal to 2.0:\n",
    "<code>\n",
    "if (2 == 2.0):\n",
    "    print('2 == 2.0 !!')\n",
    "</code>\n",
    "Since they are equal, one of them is a duplicate and is omitted.\n",
    "</p>\n",
    "<p>\n",
    "As to which representation is retained in a set (integer vs float), Python's rule appears to be to keep the more complex representation: float beats integer, and complex beats float and integer.  If you remember the notion of <b>narrowing conversions</b> from the discussion of floats, you can see that Python prefers the more expressive representation when deciding which duplicate to keep.\n",
    "</p>\n",
    "<p>\n",
    "As another illustration, here is 1 written as an integer, a float, and as a complex number.  Try running the code and see what you get:\n",
    "</p>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "S = {1, 1.0, 1+0j}\n",
    "print(S)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"danger\"></div>\n",
    "Watch out when mixing types in sets."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Iterating over sets <a id=\"iterating\"/>\n",
    "\n",
    "We can iterate over sets in the same way as we do lists, tuples, &amp;c."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ducks = {'huey', 'dewie', 'louie'}\n",
    "pies = {'lemon chess', 'cherry', 3.14159, 'possum'}\n",
    "primes = {2, 3, 5, 7, 9, 11, 13, 15}\n",
    "\n",
    "for p in pies:\n",
    "    print(p, end=' ')\n",
    "print()\n",
    "\n",
    "for d in ducks:\n",
    "    print(d, end=' ')\n",
    "print()\n",
    "\n",
    "for p in primes:\n",
    "    print(p, end=' ')\n",
    "print()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Sets cannot be indexed <a id=\"no_indexing\"/>\n",
    "\n",
    "Indexing sets makes no sense, since there are no keys associated with the elements of the set, and there are no numerical indices, either, because sets are <b>unordered</b>."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(primes)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(primes[0])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Set comparison <a id=\"comparison\"/>\n",
    "\n",
    "Comparison of Python sets behaves like it does in mathematics.  \n",
    "\n",
    "If $A$ and $B$ are sets, then\n",
    "* <code>A &lt; B</code> is true if and only if every element of $A$ is in $B$, but $A \\neq B$ (i.e., $A$ is a proper subset of $B$, or $A$ &#8842; $B$)\n",
    "* <code>A &le; B</code> is true if and only if every element of $A$ is in $B$ (i.e., $A$ is a subset of $B$, or $A$ &sube; $B$)\n",
    "* <code>A == B</code> is true if and only if $A$ = $B$\n",
    "* <code>A &ge; B</code> is true if and only if every element of $B$ is in $A$ (i.e., $A$ is a superset of $B$, or $A$ &supe; $B$)\n",
    "* <code>A &gt; B</code> is true if and only if every element of $B$ is in $A$, but $A \\neq B$ (i.e., $A$ is a proper superset of $B$, or $A$ &#8843; $B$)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "A = {1, 2, 3, 4, 5, 6}\n",
    "B = {1, 2, 3}\n",
    "C = {1, 2, 3}\n",
    "\n",
    "print('A < B is', A < B)\n",
    "print('A > B is', A > B)\n",
    "print('A == B is', A == B)\n",
    "print('B <= A is', B <= A)\n",
    "print('B < C is', B < C)\n",
    "print('B <= C is', B <= C)\n",
    "print('B == C is', B == C)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Not all sets can be ordered using <code>&lt;, &le;, ==, &gt;, &ge;</code>\n",
    "\n",
    "Set inclusion is an example of what is called a [<b>partial order</b>](https://en.wikipedia.org/wiki/Partially_ordered_set#Formal_definition).\n",
    "\n",
    "For some sets A and B we can have <b>all</b> of the following evaluate to false:\n",
    "* A < B\n",
    "* A <= B\n",
    "* A == B\n",
    "* A >= B\n",
    "* A > B\n",
    "\n",
    "Not all pairs of sets satisfy one of the order relations, hence the name *partial* order.\n",
    "\n",
    "For instance, consider the following."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "A = {1, 2, 3, 4}\n",
    "B = {1, 2, 3, 5}\n",
    "print('A <  B is', A < B)\n",
    "print('A <= B is', A <= B)\n",
    "print('A == B is', A == B)\n",
    "print('A >= B is', A >= B)\n",
    "print('A >  B is', A > B)\n",
    "print('A != B is', A != B)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# The frozenset type <a id=\"frozenset\"/>\n",
    "\n",
    "The <code>frozenset</code> type is an immutable version of <code>set</code>, in the way <code>tuple</code> is an immutable version of <code>list</code>."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "A = {1, 2, 3}\n",
    "print(type(A))\n",
    "B = frozenset(A)\n",
    "print(type(B))\n",
    "\n",
    "A.add(54)\n",
    "print(A)\n",
    "\n",
    "B.add(42)\n",
    "print(B)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## When might we use a frozenset?\n",
    "\n",
    "Because sets are mutable, they are not hashable, so they cannot be used as keys in a dictionary, for instance.\n",
    "\n",
    "Frozensets, on the other hand, are immutable, so they are hashable.  This means that we can use frozensets as keys in a dictionary."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Sets of sets or lists <a id=\"sets_o_sets\"/>\n",
    "\n",
    "This type allows us to create sets of lists or sets of sets.\n",
    "\n",
    "Look what happens when we try to create a set of sets in what might seem the obvious way."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "S = {{1}, {2}}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This error occurs because of the way Python internally represents the set $S$.  Given $S$, Python builds a data structure known as a **hash table** and stores the contents of $S$ in the hash table.  The location where each element of the table is stored depends on the element.  If the elements are mutable, as they are with the sets <code>{1}</code> and <code>{2}</code>, then the values could change and Python would no longer know where to find them in the hash table.\n",
    "\n",
    "Since <code>frozenset</code> object are immutable, they are hashable, and we can avoid this problem. \n",
    "\n",
    "To create a set of sets, we first create a list of sets.  This is ok since lists are indexed, not hashed:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "T = [{1}, {2}, {3}, {4}]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we use a list comprehension to convert the sets in our list into frozensets, and pass the result to the <code>set</code> constructor:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "S = set([frozenset(t) for t in T])\n",
    "print(S)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The list comprehension \"froze\" the contents of the sets in the list making them immutable, and thus we can put them in a set:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print([frozenset(t) for t in T])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We encounter the same problem with sets of lists:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "S = {[1], [2]}\n",
    "print(S)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the exercises you are asked to find the workaround."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Some other set methods <a id=\"methods\"/>\n",
    "\n",
    "Operations such as union and intersection work as in mathematics."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "A = {1, 2, 3}\n",
    "B = {1, 4, 5, 6}\n",
    "D = {1, 2, 4}\n",
    "C = A.union(B)\n",
    "print(C)\n",
    "C = B.intersection(A)\n",
    "print(C)\n",
    "\n",
    "print(B.intersection(D))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can take the union and intersection of more than one set."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(A.union(B, C))\n",
    "print(A.intersection(B, C))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Some less familiar set operations\n",
    "\n",
    "* Set difference: $A - B$ is the set of elements of $A$ that are not in $B$.\n",
    "* Symmetric difference: the set of elements in one, and only one, of $A$ and $B$.  This is the same as $(A-B) \\cup (B-A)$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "A = {1, 2, 3, 4}\n",
    "B = {3, 4, 5, 6}\n",
    "print('A - B = ', A.difference(B))\n",
    "print('B - A = ', B.difference(A))\n",
    "print('symmetric difference = ', A.symmetric_difference(B))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Alternative ways to write set operations\n",
    "\n",
    "You can also write set operations using operators we have seen before.\n",
    "\n",
    "For instance, the intersection of the sets $A$ and $B$ is the set of things are in $A$ <b>and</b> $B$:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "A = {1, 2, 3, 4}\n",
    "B = {3, 4, 5, 6}\n",
    "\n",
    "C = A & B\n",
    "print(C)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "On the other hand, the union of the sets $A$ and $B$ is the set of things are in $A$ <b>or</b> $B$:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "A = {1, 2, 3, 4}\n",
    "B = {3, 4, 5, 6}\n",
    "\n",
    "C = A | B\n",
    "print(C)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Set difference can be written using <code>-</code>, and the symmetric difference can be written using <code>^</code>:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "A = {1, 2, 3, 4}\n",
    "B = {3, 4, 5, 6}\n",
    "print('A - B = ', A - B)\n",
    "print('B - A = ', B - A)\n",
    "print('symmetric difference = ', A ^ B)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Set comprehensions <a id=\"comprehensions\"/>\n",
    "\n",
    "We can define sets using <b>set comprehension</b> in a manner simliar to list comprehension.  The main difference is that we use { } rather than [ ]."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "n = 7\n",
    "A = {i for i in range(0,n)}\n",
    "print(A)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If we want a list of the squares of the first n integers we would do this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "n = 7\n",
    "A = {i**2 for i in range(0,n)}\n",
    "print(A)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can also include a conditional that filters the list.  For instance, to make a list of the odd numbers from 0 to n,"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "n = 7\n",
    "A = {i for i in range(0,n) if (i%2 == 0)}\n",
    "print(A)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Compare and contrast with a dictionary comprehension:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "words = ['fee', 'fie', 'foe', 'fum']\n",
    "foo = {i:i for i in range(0,3)}\n",
    "print(foo)\n",
    "foo = {i:words[i] for i in range(0,4)}\n",
    "print(foo)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Sets and references <a id=\"references\"/>\n",
    "\n",
    "Like lists, the names of a set are only references to the thing in itself."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "A = {1, 2, 3, 4}\n",
    "B = A\n",
    "print(A)\n",
    "B.add(42)\n",
    "print(A)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Adding to B also added to A.  \n",
    "\n",
    "As with lists, the assignment <code>B = A</code> creates a new variable <code>B</code> that references <b>the same set</b> as <code>A</code>.\n",
    "\n",
    "Thus, when we change <code>B</code>, we are changing the set both <code>A</code> and <code>B</code> reference!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## How to clone a set\n",
    "\n",
    "As with lists we distinguish between a <b>shallow copy</b> (copying the thing that refers to the list) and a <b>deep copy</b> (cloning the actual contents of the list).\n",
    "\n",
    "We can do this using the <code>deepcopy()</code> function in the <code>copy</code> module."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import copy\n",
    "\n",
    "A = {1, 2, 3, 4}\n",
    "B = copy.deepcopy(A) # clone A\n",
    "print(A)\n",
    "B.add(54)\n",
    "print(A)    # no change to A this time!\n",
    "print(B)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true,
    "jupyter": {
     "outputs_hidden": true
    }
   },
   "source": [
    "# Exercises <a id=\"exercises\"/>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"exercise\">\n",
    "<b>Exercise.</b>\n",
    "Given a list of strings, give one elegant and one kludgey way to eliminate duplicate strings using only built-in Python features.  The result should return a list.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = ['bison', 'bison', 'bison', 'bison', 'bison', 'bison']"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b>Answer.</b>\n",
    "<div class=\"voila\">\n",
    "<p>\n",
    "The elegant way is clear: feed the list to the <code>set()</code> constructor:\n",
    "<code>\n",
    "b = list(set(a))\n",
    "print(b)\n",
    "</code>\n",
    "</p>\n",
    "\n",
    "<p>\n",
    "One kludgey way is to turn the list into a dictionary with the members of the list as keys, and then turn the keys of the dictionary into a list:\n",
    "<code>\n",
    "c = list(dict(zip(a,a)).keys())\n",
    "print(c)\n",
    "</code>\n",
    "Is your solution even kludgier?\n",
    "</p>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"exercise\">\n",
    "<b>Exercise.</b>\n",
    "How would you create a set of lists?  E.g., how could you make the following work?\n",
    "<code>\n",
    "S = {[1], [2]}\n",
    "</code>\n",
    "?\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b>Answer.</b>\n",
    "<div class=\"voila\">\n",
    "<p>\n",
    "In the case of a set of sets we needed to make the sets inside the set immutable.  We did so using <code>frozenset</code>.\n",
    "</p>\n",
    "<p>\n",
    "In the case of a set of lists we need to make the lists inside the set immutable.  This we can do with <code>tuple</code>.\n",
    "<code>\n",
    "T = [{1}, {2}]\n",
    "S = set([tuple(t) for t in T])\n",
    "print(S)\n",
    "</code>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"exercise\">\n",
    "<b>Exercise.</b>\n",
    "Write a function <code>max(S)</code> which given a set <code>S</code> returns the maximum value in <code>S</code>.  You may assume the set consists entirely of objects that are comparable to one another.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b>Answer.</b>\n",
    "<div class=\"voila\">\n",
    "<p>\n",
    "Solution 1.\n",
    "<code>\n",
    "def max(S):\n",
    "    max_S = None\n",
    "    for s in S:\n",
    "        if (max_S is None):\n",
    "            max_S = s\n",
    "        else:\n",
    "            if (s > max_S):\n",
    "                max_S = s\n",
    "    return s\n",
    "\n",
    "S = {-1, -2, -3, -4}\n",
    "print(max(S))\n",
    "\n",
    "S = {3, 1, 42, 7, 19}\n",
    "print(max(S))\n",
    "\n",
    "S = {'foe', 'fee', 'fum', 'fie'}\n",
    "print(max(S))\n",
    "</code>\n",
    "If <code>S</code> were a list we could initialize <code>max_S</code> to <code>S[0]</code>.  Since <code>S</code> is a set, there is not \"first\" elements of <code>S</code> so we resort to the kludge involving <code>None</code>.\n",
    "</p>\n",
    "<p>\n",
    "Solution 2.\n",
    "<code>\n",
    "def max(S):\n",
    "    max_S = S.pop()\n",
    "    for s in S:\n",
    "        if (s > max_S):\n",
    "            max_S = s\n",
    "    return s\n",
    "\n",
    "S = {-1, -2, -3, -4}\n",
    "print(max(S))\n",
    "\n",
    "S = {3, 1, 42, 7, 19}\n",
    "print(max(S))\n",
    "\n",
    "S = {'foe', 'fee', 'fum', 'fie'}\n",
    "print(max(S))\n",
    "</code>\n",
    "This time we initialize <code>max_S</code> by popping an element from <code>S</code>.  This approach has the advantage that we are not repeatedly testing the condition <code>if (max_S is None)</code> as we do in Solution 1, which we know is only true on the first iteration of the loop.\n",
    "</p>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"exercise\">\n",
    "<p>\n",
    "<b>Exercise.</b>\n",
    "</p>\n",
    "<ul>\n",
    "<li>Find the words that appear in both the Gettysburg Address and Lincoln's Second Inaugural Address.</li>\n",
    "<li>Find the words that appear in only one or the other.</li>\n",
    "</ul>\n",
    "<p>\n",
    "You should ignore case and all punctuation.\n",
    "</p>\n",
    "\n",
    "<b>The Gettysburg Address</b>\n",
    "\n",
    "Four score and seven years ago our fathers brought forth on this continent, \n",
    "a new nation, conceived in Liberty, and dedicated to the proposition that \n",
    "all men are created equal.\n",
    "\n",
    "Now we are engaged in a great civil war, testing whether that nation, \n",
    "or any nation so conceived and so dedicated, can long endure. We are met \n",
    "on a great battle-field of that war. We have come to dedicate a portion \n",
    "of that field, as a final resting place for those who here gave their \n",
    "lives that that nation might live. It is altogether fitting and proper \n",
    "that we should do this.\n",
    "\n",
    "But, in a larger sense, we can not dedicate - we can not consecrate - we can \n",
    "not hallow - this ground. The brave men, living and dead, who struggled here, \n",
    "have consecrated it, far above our poor power to add or detract. The world \n",
    "will little note, nor long remember what we say here, but it can never \n",
    "forget what they did here. It is for us the living, rather, to be dedicated \n",
    "here to the unfinished work which they who fought here have thus far so \n",
    "nobly advanced. It is rather for us to be here dedicated to the great task \n",
    "remaining before us - that from these honored dead we take increased devotion \n",
    "to that cause for which they gave the last full measure of devotion - that we \n",
    "here highly resolve that these dead shall not have died in vain - that this \n",
    "nation, under God, shall have a new birth of freedom—and that government \n",
    "of the people, by the people, for the people, shall not perish from the earth.\n",
    "\n",
    "<b>A Mad Tea-Party</b>\n",
    "\n",
    "There was a table set out under a tree in front of the house, and the\n",
    "March Hare and the Hatter were having tea at it: a Dormouse was sitting\n",
    "between them, fast asleep, and the other two were using it as a\n",
    "cushion, resting their elbows on it, and talking over its head. “Very\n",
    "uncomfortable for the Dormouse,” thought Alice; “only, as it’s asleep,\n",
    "I suppose it doesn’t mind.”\n",
    "\n",
    "The table was a large one, but the three were all crowded together at\n",
    "one corner of it: “No room! No room!” they cried out when they saw\n",
    "Alice coming. “There’s _plenty_ of room!” said Alice indignantly, and\n",
    "she sat down in a large arm-chair at one end of the table.\n",
    "\n",
    "“Have some wine,” the March Hare said in an encouraging tone.\n",
    "\n",
    "Alice looked all round the table, but there was nothing on it but tea.\n",
    "“I don’t see any wine,” she remarked.\n",
    "\n",
    "“There isn’t any,” said the March Hare.\n",
    "\n",
    "“Then it wasn’t very civil of you to offer it,” said Alice angrily.\n",
    "\n",
    "“It wasn’t very civil of you to sit down without being invited,” said\n",
    "the March Hare.\n",
    "\n",
    "“I didn’t know it was _your_ table,” said Alice; “it’s laid for a great\n",
    "many more than three.”\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b>Answer.</b>\n",
    "<div class=\"voila\">\n",
    "Here is one solution.\n",
    "<code>\n",
    "gettysburg = '''Four score and seven years ago our fathers brought forth on this continent, a new nation, conceived in Liberty, and dedicated to the proposition that all men are created equal.\n",
    "    \n",
    "Now we are engaged in a great civil war, testing whether that nation, or any nation so conceived and so dedicated, can long endure. We are met on a great battle-field of that war. We have come to dedicate a portion of that field, as a final resting place for those who here gave their lives that that nation might live. It is altogether fitting and proper that we should do this.\n",
    "\n",
    "But, in a larger sense, we can not dedicate - we can not consecrate - we can not hallow - this ground. The brave men, living and dead, who struggled here, have consecrated it, far above our poor power to add or detract. The world will little note, nor long remember what we say here, but it can never forget what they did here. It is for us the living, rather, to be dedicated here to the unfinished work which they who fought here have thus far so nobly advanced. It is rather for us to be here dedicated to the great task remaining before us - that from these honored dead we take increased devotion to that cause for which they gave the last full measure of devotion - that we here highly resolve that these dead shall not have died in vain - that this nation, under God, shall have a new birth of freedom—and that government of the people, by the people, for the people, shall not perish from the earth.'''\n",
    "\n",
    "alice = '''There was a table set out under a tree in front of the house, and the March Hare and the Hatter were having tea at it: a Dormouse was sitting between them, fast asleep, and the other two were using it as a cushion, resting their elbows on it, and talking over its head. “Very uncomfortable for the Dormouse,” thought Alice; “only, as it’s asleep, I suppose it doesn’t mind.”\n",
    "    \n",
    "The table was a large one, but the three were all crowded together at one corner of it: “No room! No room!” they cried out when they saw Alice coming. “There’s plenty of room!” said Alice indignantly, and she sat down in a large arm-chair at one end of the table.\n",
    "    \n",
    "“Have some wine,” the March Hare said in an encouraging tone.\n",
    "    \n",
    "Alice looked all round the table, but there was nothing on it but tea. “I don’t see any wine,” she remarked.\n",
    "    \n",
    "“There isn’t any,” said the March Hare.\n",
    "    \n",
    "“Then it wasn’t very civil of you to offer it,” said Alice angrily.\n",
    "    \n",
    "“It wasn’t very civil of you to sit down without being invited,” said the March Hare.\n",
    "    \n",
    "“I didn’t know it was your table,” said Alice; “it’s laid for a great many more than three.”\n",
    "</code>\n",
    "\n",
    "1. Convert all the words to lower case, nuke the punctuation, and split into a list.\n",
    "There are cleverer ways to remove the punctuation, but this will get the job done.\n",
    "<pre>\n",
    "gettysburg = gettysburg.lower().replace('.', ' ').replace(',', ' ').replace('-', ' ').split()\n",
    "alice = alice.lower().replace('.', ' ').replace(',', ' ').replace('-', ' ').split()\n",
    "</pre>\n",
    "2.  Remove duplicates by converting the lists to sets.\n",
    "<pre>\n",
    "gettysburg = set(gettysburg)\n",
    "alice = set(alice)\n",
    "</pre>\n",
    "3.  Look at the intersection of the sets of words.\n",
    "<pre>\n",
    "print('Words in both texts:')\n",
    "print(gettysburg & alice)\n",
    "print()\n",
    "</pre>\n",
    "4.  Look at the difference of the sets of words.\n",
    "<pre>\n",
    "print('Words only in the Gettysburg Address:')\n",
    "print(gettysburg - alice)\n",
    "print()\n",
    "print('Words only in the Mad Tea-Party')\n",
    "print(alice - gettysburg)\n",
    "</pre>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h4>This notebook is brought to you by baseness.</h4>\n",
    "\n",
    "Baseness attracts everyone.  If you see something being done suddenly by many people,\n",
    "you may assume that it is base. <br/>\n",
    "&ndash; Goethe, \"Venetian Epigrams\""
   ]
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
