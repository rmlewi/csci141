{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"http://www.cs.wm.edu/~rml/images/wm_horizontal_single_line_full_color.png\">\n",
    "\n",
    "<h1 style=\"text-align:center;\">CSCI 141-01, Fall 2023 course notes</h1>\n",
    "<h1 style=\"text-align:center;\">List comprehensions</h1>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Contents\n",
    "\n",
    "* [List comprehensions](#comprehensions)\n",
    "* [A list of squares of the first n nonnegative integers](#squares)\n",
    "* [A list of the first n nonnegative odd integers](#odd)\n",
    "* [A list of pairs of nonequal values from two lists](#two_lists)\n",
    "* [Looking for words that do/don't  contain a given substring](#substring)\n",
    "* [A comprehension gotcha 🐞](#gotcha)\n",
    "* [Exercises](#exercises)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# List comprehensions <a id=\"comprehensions\"/>\n",
    "\n",
    "Imagine you wanted to make a list containing the integers from 0 to n-1, where n is a value that can vary.\n",
    "\n",
    "One approach would be to use a loop and lots of calls to <code>append()</code>."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from time import perf_counter\n",
    "n = 10_000_000\n",
    "a = []\n",
    "start = perf_counter()\n",
    "for i in range(0, n):\n",
    "    a.append(i)\n",
    "end = perf_counter()\n",
    "print('elapsed time:', end-start, 'seconds')\n",
    "\n",
    "print('value of i at the end of the loop:', i)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The previous approach involves a lot of calls to <code>append()</code>.  This is inefficient because Python is having to repeatedly allocate space for new elements rather than allocating space once and for all.\n",
    "\n",
    "This approach also creates a variable <code>i</code> that hangs around after we have built our list.  This is a bit inelegant as it keeps around the incidental machinery we used to create the list of integers from 0 to n-1."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A powerful alternative is a <b>list comprehension</b>.  As is frequently the case, it is most easily understood by example."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from time import perf_counter\n",
    "n = 10_000_000\n",
    "start = perf_counter()\n",
    "\n",
    "# Here comes the list comprehension!\n",
    "a = [i for i in range(0, n)]\n",
    "\n",
    "end = perf_counter()\n",
    "print('elapsed time:', end-start)\n",
    "print(a[0:4])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can have conditional statements in the comprehension:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Generate only the squares for even numbers.\n",
    "t = [i**2 for i in range(0,8) if (i % 2 == 0)]\n",
    "print(t)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This time the variable <code>i</code> is a dummy variable that does not exist outside the list comprehension.  We begin by checking whether the local variable <code>i</code> exists and, if so, we nuke it so we can start with a clean slate.  We use the built-in [local()](https://docs.python.org/3/library/functions.html?highlight=built%20functions#locals) function to get a list of the names of local variables."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Check whether there is a local variable named 'i'; if so, delete it.\n",
    "if 'i' in locals():\n",
    "    del i\n",
    "\n",
    "from time import perf_counter\n",
    "n = 10_000_000\n",
    "start = perf_counter()\n",
    "\n",
    "# Here comes the list comprehension!\n",
    "a = [i for i in range(0, n)]\n",
    "\n",
    "end = perf_counter()\n",
    "print('elapsed time:', end - start)\n",
    "print(a[0:4])\n",
    "\n",
    "# if variable i exists, print its value; otherwise note its nonexistance.\n",
    "if 'i' in locals():\n",
    "    print('value of i at the end of the loop:', i)\n",
    "else:\n",
    "    print('the variable \"i\" does not exist!')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Aside\n",
    "\n",
    "Alternatively, we can create this particular list by explicitly passing the result of a call to <code class=\"kw\">range()</code> to the <code class=\"kw\">list</code> class constructor."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "n = 10_000_000\n",
    "start = perf_counter()\n",
    "\n",
    "a = list(range(0, n))\n",
    "\n",
    "end = perf_counter()\n",
    "print(f'elapsed time: {end - start}')\n",
    "print(a[0:4])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b>Question.</b>  What property does the output of the call <code class=\"kw\">range(0, n)</code> have that makes it valid to pass to the <code>list</code> constructor?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b>Answer.</b>\n",
    "<div class=\"voila\">\n",
    "It is iterable.  Remember that anything passed to the <code>list</code> constructor must be iterable.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# More examples\n",
    "\n",
    "Per the [Python docs](https://docs.python.org/2/tutorial/datastructures.html#list-comprehensions),\n",
    "<blockquote>\n",
    "A list comprehension consists of brackets containing an expression followed by a for clause, then zero or more for or if clauses. The result will be a new list resulting from evaluating the expression in the context of the for and if clauses which follow it.\n",
    "</blockquote>\n",
    "\n",
    "Let's look at more examples to unwrap this description."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# A list of squares of the first n nonnegative integers <a id=\"squares\"></a>\n",
    "\n",
    "If we want a list of the squares of the first n nonnegative integers we would do this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "n = 10_000_000\n",
    "start = perf_counter()\n",
    "\n",
    "a = [i**2 for i in range(0, n)]\n",
    "\n",
    "end = perf_counter()\n",
    "print(f'elapsed time: {end - start}')\n",
    "print(a[0:4])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b>Aside.</b>  Which do you think is faster: \n",
    "```python\n",
    "a = [i**2 for i in range(0,n)]\n",
    "```\n",
    "or\n",
    "```python\n",
    "a = [i*i for i in range(0,n)] ?\n",
    "```\n",
    "Let's find out:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "n = 10_000_000\n",
    "start = perf_counter()\n",
    "a = [i*i for i in range(0, n)]\n",
    "end = perf_counter()\n",
    "print(f'elapsed time: {end - start}')\n",
    "print(a[0:4])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# A list of the first n nonnegative odd integers <a id=\"odd\"></a>\n",
    "\n",
    "We can also include a conditional that filters the list.  For instance, to make a list of the odd numbers from 0 to n,"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "n = 10000000\n",
    "start = perf_counter()\n",
    "\n",
    "a = [i for i in range(0, n) if i % 2 == 1]\n",
    "\n",
    "end = perf_counter()\n",
    "print(f'elapsed time: {end - start}')\n",
    "print(a[0:4])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The comprehension \n",
    "```python\n",
    "[i for i in range(0,n) if (i % 2 == 1)]\n",
    "```\n",
    "says\n",
    "<blockquote>\n",
    "Give me the <code>i</code> in the range 0 to n-1 that are odd.\n",
    "</blockquote>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The list comprehension is equivalent to"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = []\n",
    "for i in range(0, n):\n",
    "    if i % 2 == 1:\n",
    "        a.append(i)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# A list of pairs of nonequal values from two lists <a id=\"two_lists\"></a>\n",
    "\n",
    "The next example looks at all pairs of numbers from two lists and returns the pair if the two numbers are different.\n",
    "\n",
    "The pairs themselves are returned as lists, so we have a list of lists."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = [[x, y] for x in [1,2] for y in [0,1,2,4] if x != y]\n",
    "print(a)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This list comprehension is equivalent to"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = []\n",
    "for x in [1,2]:\n",
    "    for y in [0,1,2,3,4]:\n",
    "        if x != y:\n",
    "            a.append([x,y])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Looking for words that do/don't  contain a given substring <a id=\"substring\"></a>\n",
    "\n",
    "Let's look for words that do and don't contain a given substring, say, \"at\".\n",
    "\n",
    "We will take the Gettysburg Address, break it up into words, and use a comprehension to extract the words of interest."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "gettysburg = '''\\\n",
    "Four score and seven years ago our fathers brought forth on this continent, \n",
    "a new nation, conceived in Liberty, and dedicated to the proposition that \n",
    "all men are created equal.\n",
    "\n",
    "Now we are engaged in a great civil war, testing whether that nation, \n",
    "or any nation so conceived and so dedicated, can long endure. We are met \n",
    "on a great battle-field of that war. We have come to dedicate a portion \n",
    "of that field, as a final resting place for those who here gave their \n",
    "lives that that nation might live. It is altogether fitting and proper \n",
    "that we should do this.\n",
    "\n",
    "But, in a larger sense, we can not dedicate - we can not consecrate - we can \n",
    "not hallow - this ground. The brave men, living and dead, who struggled here, \n",
    "have consecrated it, far above our poor power to add or detract. The world \n",
    "will little note, nor long remember what we say here, but it can never \n",
    "forget what they did here. It is for us the living, rather, to be dedicated \n",
    "here to the unfinished work which they who fought here have thus far so \n",
    "nobly advanced. It is rather for us to be here dedicated to the great task \n",
    "remaining before us - that from these honored dead we take increased devotion \n",
    "to that cause for which they gave the last full measure of devotion - that we \n",
    "here highly resolve that these dead shall not have died in vain - that this \n",
    "nation, under God, shall have a new birth of freedom—and that government \n",
    "of the people, by the people, for the people, shall not perish from the earth.\n",
    "'''"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Turn the text into a list of words.\n",
    "s = gettysburg.split()\n",
    "#print(s)\n",
    "\n",
    "substr = \"at\"\n",
    "\n",
    "do = [word for word in s if substr in word]\n",
    "print(f'The following words contain \"{substr}\":')\n",
    "print(do)\n",
    "print('')\n",
    "\n",
    "dont = [word for word in s if substr not in word]\n",
    "print(f'The following words do not contain \"{substr}:\"')\n",
    "print(dont)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# A comprehension gotcha 🐞  <a id=\"gotcha\"/>\n",
    "\n",
    "The following code gives us a list with 4 copies of a list:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = [1,2,3,4]\n",
    "b = 4*[a]\n",
    "print(b)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So let's change one of the lists in <code>b</code>:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "b[0][0] = 42\n",
    "print(b[0])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let's look at <code>b</code>:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(b)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Can we get around this behavior with a comprehension?  Let's try:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "b = [a for _ in range(4)]\n",
    "print(b)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "b[0][0] = 42\n",
    "print(b)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Well, that didn't work.  In the comprehension Python used 4 *shallow copies* of the list <code>a</code>.  Thus, when we changed one copy, we changed them all.\n",
    "\n",
    "We can get the desired behavior as follows:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from copy import deepcopy\n",
    "\n",
    "a = [1,2,3,4]\n",
    "b = [deepcopy(a) for _ in range(4)]\n",
    "print(b)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "b[0][0] = 42\n",
    "print(b)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true,
    "jupyter": {
     "outputs_hidden": true
    }
   },
   "source": [
    "# Exercises <a id=\"exercises\"/>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true,
    "jupyter": {
     "outputs_hidden": true
    }
   },
   "source": [
    "<div class=\"exercise\">\n",
    "<b>Exercise.</b>  Create a list comprehension that will produce a list equivalent to the list <code>a</code> that results from the following code:\n",
    "<pre>\n",
    "a = []\n",
    "for i in range(0,n):\n",
    "    if i % 4 == 0:\n",
    "        a.append(i)\n",
    "</pre>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b>Answer.</b>\n",
    "<div class=\"voila\">\n",
    "<pre>\n",
    "a = [i for i in range(0, n) if (i % 4 == 0)]\n",
    "</pre>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h4>This notebook was brought to you by politics:</h4>\n",
    "\n",
    "Conservative, n.  A statesman who is enamored of existing evils, as distinguished from a liberal, who wishes to\n",
    "replace them with others. <br/>\n",
    "&ndash; Ambrose Bierce, The Devil's Dictionary"
   ]
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
