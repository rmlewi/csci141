{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"http://www.cs.wm.edu/~rml/images/wm_horizontal_single_line_full_color.png\">\n",
    "\n",
    "<h1 style=\"text-align:center;\">CSCI 141-01, Fall 2023 course list</h1>\n",
    "<h1 style=\"text-align:center;\">Lists</h1>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Contents\n",
    "\n",
    "* [Lists](#lists)\n",
    "* [The <code>list</code> constructor](#constructor)\n",
    "* [Lists are mutable](#mutable)\n",
    "* [The string <code>split()</code> and <code>join()</code> methods](#split)\n",
    "* [Similarities of lists and strings](#similarities)\n",
    "    * [Iterating over lists](#iterable)\n",
    "    * [Concatenation and duplication](#concat_dup)\n",
    "    * [Indexing and slicing](#indexing)\n",
    "    * [Testing for membership](#in)\n",
    "* [Lists and references](#references)\n",
    "* [Passing lists to functions](#call_by_ref)\n",
    "* [Exercises](#exercises)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Lists <a id =\"lists\"/>\n",
    "\n",
    "<b>Lists</b> are a built-in Python data structure that allow us to group data together in a single container.\n",
    "\n",
    "They can be created using the <code>list()</code> constructor, or, more frequently, by using brackets <code>[]</code>.\n",
    "\n",
    "Lists behave in many ways like strings."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ducks = ['huey', 'dewie', 'louie']\n",
    "primes = [2, 3, 5, 7, 9, 11, 13, 15]\n",
    "\n",
    "print(ducks)\n",
    "print(primes)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"try_it\">\n",
    "<b>Try it yourself.</b>  Create a few lists of your own.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can mix types inside a single list:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pies = ['lemon chess pie', 'puppy potpie', 3.14159]\n",
    "print(pies)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A list may be empty:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "em = []\n",
    "print(em)\n",
    "em = [   ]\n",
    "print(em)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can determine the number of elements in a list using the <code>len()</code> function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(len(pies))\n",
    "print(len(primes))\n",
    "print(len(em))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"try_it\">\n",
    "<b>Try it yourself.</b>  Print the lengths of the lists you created above.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# The <code>list()</code> constructor <a id=\"constructor\"/>\n",
    "\n",
    "We can either use the bracket notation to indicate a list, or we can explicitly call the <code>list()</code> class constructor.\n",
    "\n",
    "If we pass the <code>list()</code> class constructor a string, something interesting happens:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "aa = list('pahoehoe')\n",
    "print(aa)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "What happens if we pass something other than a string to the constructor?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "aaa = list(42)\n",
    "print(aaa)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "In general, [if an argument is passed to the <code>list()</code> constructor that argument must be iterable](https://docs.python.org/3/library/stdtypes.html#lists).\n",
    "\n",
    "For instance, we could pass the constructor a list."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "ducks = ['huey', 'dewie', 'louie']\n",
    "c = list(ducks)\n",
    "print(c)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Lists are mutable, unlike strings! <a id=\"mutable\"/>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"danger\"></div>  This means we can change the contents of a list."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "al = ['man', 'bear', 'pig']\n",
    "print(al)\n",
    "\n",
    "al[0] = 'wombat'\n",
    "print(al)\n",
    "\n",
    "al[1] = 42\n",
    "print(al)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"try_it\">\n",
    "<b>Try it yourself.</b>  Confirm for yourself that you can modify the contents of a list.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# The string <code>split()</code> and <code>join()</code> methods <a id=\"split\"/>\n",
    "\n",
    "The string [<code>split()</code>](https://docs.python.org/3/library/stdtypes.html#str.split) and [<code>join()</code>](https://docs.python.org/3/library/stdtypes.html#str.join) methods illustrate a use of lists.\n",
    "\n",
    "The <code>split()</code> method takes a string, splits it up into words, and returns the words as a list."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s = 'Colorless green ideas sleep furiously'\n",
    "w = s.split()\n",
    "print(w)\n",
    "print(type(w))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can optionally pass <code>split()</code> a character that serves as the separator in your string.\n",
    "\n",
    "In the second part of the following example, we split the string at the commas.  The spaces are retained."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s = 'Colorless, green, ideas, sleep, furiously'\n",
    "w = s.split()\n",
    "print(w)\n",
    "\n",
    "w = s.split(',')\n",
    "print(w)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"try_it\">\n",
    "<b>Try it yourself.</b>  Try splitting up some strings of your own.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The inverse operation is <code>join()</code>.  It puts a list of strings together, with the strings separated by the string that calls <code>join()</code>:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "'a'.join(['b', 'c', 'd', 'e'])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "'|'.join(['huey', 'dewey', 'louie'])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "'CAT'.join(['b', 'c', 'd', 'e'])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "' '.join(['b', 'c', 'd', 'e'])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "''.join(['b', 'c', 'd', 'e'])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### The <code>''.join()</code> idiom\n",
    "\n",
    "The last example illustrates a common Python idiom for combining a bunch of characters or strings:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s = 'Colorless green ideas sleep furiously'\n",
    "w = s.split()\n",
    "s = ''.join(w)\n",
    "print(s)\n",
    "\n",
    "s = '\\n'.join(w)\n",
    "print(s)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here the separator is an empty string, so there is no space between the items being joined."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"danger\"></div>  Remember the <code>''.join()</code> idiom.  It is useful, and you are likely to encounter it in the wild."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Similarities of lists and strings <a id=\"similarities\"></a>\n",
    "\n",
    "Lists have many similarities to strings.  "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Lists are iterable <a id=\"iterable\"/>\n",
    "\n",
    "For instance, lists are iterable:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pies = ['lemon chess pie', 'puppy potpie', 3.14159]\n",
    "\n",
    "for p in pies:\n",
    "    print(p)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ducks = ['huey', 'dewie', 'louie']\n",
    "\n",
    "for d in ducks:\n",
    "    print(d)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"try_it\">\n",
    "<b>Try it yourself.</b>  Try your hand at iterating over some lists.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Lists can be concatenated and duplicated <a id=\"concat_dup\"/></a>\n",
    "\n",
    "These operations are done exactly as with strings."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(ducks + pies)\n",
    "print(2*ducks)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"try_it\">\n",
    "<b>Try it yourself.</b> Try concatenating and duplicating some lists of your own.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"danger\"></div> You cannot concatenate a list with anything other than a list:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "print(ducks + 2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can fix this by making the 2 into a list:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# This will work!\n",
    "print(ducks + [2])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Lists can be indexed and sliced <a id=\"indexing\"/></a>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "primes = [2, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21]\n",
    "print(primes)\n",
    "print(primes[0])\n",
    "print(primes[0:4])\n",
    "print(primes[0::2])\n",
    "print(primes[-1::-1])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"try_it\">\n",
    "<b>Try it yourself.</b> Print the elements of <code>prime</code> at locations 1, 3, 5, &hellip;\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b>Answer.</b>\n",
    "<div class=\"voila\">\n",
    "<pre>\n",
    "print(primes[1::2])\n",
    "</pre>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "More fun with slicing &hellip;"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "duck_pie = ducks + pies\n",
    "print(duck_pie)\n",
    "print(duck_pie[1])\n",
    "print(duck_pie[3:])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Assignment to a slice\n",
    "\n",
    "Because lists are mutable, we can use slicing to assign values to a slice of the list.\n",
    "\n",
    "The slice being assigned and the values being assigned to the slice must have the same size."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = [0, 1, 2, 3, 4, 5, 6]\n",
    "print(a)\n",
    "\n",
    "# Change the elements a[0], a[2], a[4], a[6]\n",
    "a[::2] = [9, 9, 9, 9]\n",
    "print(a)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If the slice and the list being assigned do not conform in size, there will be an error."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = [0, 1, 2, 3, 4, 5, 6]\n",
    "print(a)\n",
    "\n",
    "# Change the elements a[0], a[2], a[4], a[6]\n",
    "a[::2] = [9, 9]\n",
    "print(a)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"try_it\">\n",
    "<b>Try it yourself.</b> Try assigning to a slice.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Testing for membership in a list <a id=\"in\"/>\n",
    "\n",
    "As with strings, we can test whether a value is in a list using <code class=\"kw\">in</code>."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "lunchmeats = ['bologna', 'spam', 'bloater paste', 'possum loaf', 'potted cat']\n",
    "delicacies = ['possum loaf', 'puppy potpie']\n",
    "\n",
    "for delicacy in delicacies:\n",
    "    if delicacy in lunchmeats:\n",
    "        print(f'{delicacy} is a lunchmeat')\n",
    "    else:\n",
    "        print(f'{delicacy} is not a lunchmeat')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can also test whether a value is not in a list using <code class=\"kw\">not in</code>."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "lunchmeats = ['bologna', 'spam', 'bloater paste', 'possum loaf', 'potted cat']\n",
    "delicacies = ['possum loaf', 'puppy potpie']\n",
    "\n",
    "for delicacy in delicacies:\n",
    "    if delicacy not in lunchmeats:\n",
    "        print(f'{delicacy} is a lunchmeat')\n",
    "    else:\n",
    "        print(f'{delicacy} is not a lunchmeat')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Lists and references <a id=\"references\"/>\n",
    "\n",
    "Lists have what at first sight appears to be very strange behavior when <code>=</code> is used."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = [1, 2, 3, 4]\n",
    "b = a\n",
    "print('a =', a)\n",
    "\n",
    "b[0] = 42\n",
    "print('a =', a)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b>Zounds!</b>  Changing the first term in <code>b</code> changed the first term in <code>a</code>!  \n",
    "\n",
    "**This is not a bug &ndash; it's a feature!**\n",
    "\n",
    "1. The statement <code>a = [1, 2, 3, 4]</code> creates a list and a variable <code>a</code> that refers to the list.\n",
    "2. The assignment <code>b = a</code> then creates a new variable <code>b</code> that refers to <b>the same list</b> as <code>a</code>.\n",
    "\n",
    "Thus, when we change <code>b[0]</code>, we are changing the first term of the underlying list both <code>a</code> and <code>b</code> refer to!\n",
    "\n",
    "In this context <code>a</code> and <code>b</code> are called **references** to the same list."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = [1, 2, 3, 4]  # Think of a as an alias of the underlying list.\n",
    "b = a             # Now b is another alias of the same list.\n",
    "print('a =', a)\n",
    "\n",
    "b[0] = 42\n",
    "print('a = ', a)\n",
    "\n",
    "a[3] = 42\n",
    "print('b =', b)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can confirm that <code>a</code> and <code>b</code> are the same entity using [the built-in <code>id()</code> function](https://docs.python.org/3/library/functions.html#id):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(f'the id of a is {id(a)}')\n",
    "print(f'the id of b is {id(b)}')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"danger\"></div><div class=\"danger\"></div><div class=\"danger\"></div>  This behavior is the source of many delightful and entertaining bugs &ndash; be careful!!! 🐞 🐞 🐞"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here is another example.  This time we include a list inside another list.  When we include the list, only the reference to the list is included &ndash; not a clone of the original list. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = [1, 2, 3, 4]\n",
    "b = a        # b refers to the same object as a\n",
    "c = [a, 42]  # the list c contains a reference to the same object as a\n",
    "print(f'{c = }')\n",
    "b[0] = 54    # this changes the 1st term in a!\n",
    "print(f'{a = }')\n",
    "print(f'{c = }')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Watch out! 🐞\n",
    "\n",
    "This behavior is manifest in list multiplication:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[[1, 2, 3, 4], [1, 2, 3, 4], [1, 2, 3, 4], [1, 2, 3, 4]]\n"
     ]
    }
   ],
   "source": [
    "a = [1,2,3,4]\n",
    "b = 4*[a]\n",
    "print(b)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we have a list of lists.  Let's change one of the lists:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[442, 2, 3, 4]\n",
      "[42, 2, 3, 4]\n"
     ]
    }
   ],
   "source": [
    "print(b[0])\n",
    "b[0][0] = 42\n",
    "print(b[0])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's look at <code>b</code>:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[[42, 2, 3, 4], [42, 2, 3, 4], [42, 2, 3, 4], [42, 2, 3, 4]]\n"
     ]
    }
   ],
   "source": [
    "print(b)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Ack!  <code>b</code> consists of 4 references to <code>a</code>!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## How can we clone a list?!\n",
    "\n",
    "How can we create a clone of a list if we can't use the assignment operator?\n",
    "\n",
    "We distinguish between a <b>shallow copy</b> (copying the thing that refers to the list) and a <b>deep copy</b> (copying the actual contents of the list).\n",
    "\n",
    "We can clone a list using the <code>deepcopy()</code> function in the [<code>copy</code> module](https://docs.python.org/3/library/copy.html)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from copy import deepcopy\n",
    "\n",
    "a = [1, 2, 3, 4]\n",
    "b = deepcopy(a) # clone a\n",
    "c = [a, 42] # the list c contains a reference to the same object as a\n",
    "print('c =', c)\n",
    "b[0] = 54  \n",
    "print('c =', c)    # no change to c or a this time!\n",
    "\n",
    "print(f'the id of a is {id(a)}')\n",
    "print(f'the id of b is {id(b)}')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a_tuple = (1, 3, 5, 'boo!')\n",
    "a_list = list(a_tuple)\n",
    "\n",
    "print(a_list)\n",
    "print('a_list is a', type(a_list))\n",
    "print(a_tuple)\n",
    "print('a_tuple is a', type(a_tuple))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"danger\"></div><div class=\"danger\"></div> Be mindful of the difference between shallow copy and deep copy."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Passing lists to functions <a id=\"call_by_ref\"/>\n",
    "\n",
    "What happens when we pass lists to functions?  Are we passing the original lists, or are we passing copies of them?\n",
    "\n",
    "We can check this using <code>id()</code>:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def foo(a):\n",
    "    print(f'id of variable passed to foo(): {id(a)}')\n",
    "    \n",
    "a = [3,1,4,1,6]\n",
    "print(f'id of a: {id(a)}')\n",
    "\n",
    "foo(a)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This shows that we are actually passing the original list, not a copy.  That is, Python passes copies of <b>references</b> to lists when it calls functions.\n",
    "\n",
    "In turn, this means we can alter <code>a</code> inside the function and have the changes persist after we return from the function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def foo(a):\n",
    "    a[0] = 42\n",
    "    \n",
    "a = [3,1,4,1,6]\n",
    "print(f'a before function call: {a}')\n",
    "\n",
    "foo(a)\n",
    "print(f'a after function call:  {a}')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So &ndash; functions are allowed to modify inputs that are references in ways that persist when the function exits.\n",
    "\n",
    "Whether this is what you want to do is another matter.  This is a common technique in languages such as C, C++, and Fortran, but it can introduce bugs if you are not aware (or forget) that a function changes its inputs."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"danger\"></div>\n",
    "Be careful when making persistent changes to a function input.  Be sure to document this behavior in the function's docstring. 🐞"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"try_it\">\n",
    "<b>Try it yourself.</b>\n",
    "\n",
    "Write a short function like <code>foo()</code> that modifies a mutable input and confirm the behavior just discussed.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you want to pass a copy of a list to a function rather than the actual list you can explicitly create a new list:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def foo(a):\n",
    "    print(f'id of variable passed to foo(): {id(a)}')\n",
    "    a[0] = 42\n",
    "    \n",
    "a = [3,1,4,1,6]\n",
    "print(f'id of a: {id(a)}')\n",
    "\n",
    "foo(list(a))\n",
    "print(f'a after function call: {a}')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Alternatively, you can do so by passing a slice that is the entire list (though this may puzzle those unfamiliar with this trick):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = [3,1,4,1,6]\n",
    "print(f'id of a: {id(a)}')\n",
    "\n",
    "foo(a[:])\n",
    "print(f'a after function call: {a}')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Keeping in bounds: a useful programming pattern\n",
    "\n",
    "The following code draws an 18-sided regular polygon, with each segment a different color:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from mobilechelonian import Turtle\n",
    "t = Turtle()\n",
    "t.speed(5)\n",
    "colors = [\"red\", \"blue\", \"yellow\", \"brown\", \"black\", \"purple\", \"green\"]\n",
    "\n",
    "t.penup(); t.left(90); t.forward(200);t.right(90);t.pendown()\n",
    "for i in range (0,18):\n",
    "    t.pencolor(colors[i%7])\n",
    "    t.right(20)\n",
    "    t.forward(50)\n",
    "\n",
    "t.penup()    \n",
    "t.right(180)\n",
    "t.home()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Notice how this handles the issue of having 18 sides to color but only 7 colors, so we need to reuse colors.  The code uses the modulus operator to ensure that the index into the list is always between 0 and 6:\n",
    "```python\n",
    "for i in range (0,18):\n",
    "    t.pencolor(colors[i%7])  # we are guaranteed that 0 <= i%7 <= 6.\n",
    "    t.right(20)\n",
    "    t.forward(50)\n",
    "```\n",
    "\n",
    "Using the modulus operator to ensure that a number is guaranteed to remain in a specified range is a useful trick."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true,
    "jupyter": {
     "outputs_hidden": true
    }
   },
   "source": [
    "# Exercises <a id=\"exercises\"/>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true,
    "jupyter": {
     "outputs_hidden": true
    }
   },
   "source": [
    "<div class=\"exercise\">\n",
    "<b>Exercise.</b>\n",
    "What is printed by the following code?\n",
    "<code>\n",
    "a = [0, 1, 2, 3]\n",
    "b = a\n",
    "b[3] = 'three'\n",
    "print(a)\n",
    "</code>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b>Answer.</b>\n",
    "<div class=\"voila\">\n",
    "<code>\n",
    "[0, 1, 2, 'three']\n",
    "</code>\n",
    "<p>\n",
    "Remember that <code>b = a</code> copies the reference to the original list, rather than cloning the list.  Thus, <code>b</code> and <code>a</code> are aliases for the same list, so changing <code>b</code> changes <code>a</code>.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"exercise\">\n",
    "<b>Exercise.</b>\n",
    "How could you take the string 'abcde' and produce a string with the last letter changed to a question mark?\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b>Answer.</b>\n",
    "<div class=\"voila\">\n",
    "The challenge is that strings are immutable.  Here is one solution:\n",
    "<code>\n",
    "s = 'abcde'\n",
    "# Break the string down into its characters using list():\n",
    "c = list(s)\n",
    "# Change the last character:    \n",
    "c[4] = '?'\n",
    "# Put it back together:\n",
    "s = ''.join(c)\n",
    "print(s)    \n",
    "<code>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h4>This notebook was brought to you by the Abyss:</h4>\n",
    "\n",
    "And if you look long into the Abyss, the Abyss looks into you.\n",
    "\n",
    "<i>Und wenn du lange in einen Abgrund blickst, blickt der Abgrund auch in dich hinein.</i><br/>\n",
    "&ndash; Friedrich Nietzsche, <i>Beyond Good and Evil</i>\n",
    "\n",
    "<img img style=\"max-height: 50%; width: auto;\" src=\"http://www.cs.wm.edu/~rml/images/abyss.jpeg\"/>"
   ]
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
