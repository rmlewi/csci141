{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"http://www.cs.wm.edu/~rml/images/wm_horizontal_single_line_full_color.png\">\n",
    "\n",
    "<h1 style=\"text-align:center;\">CSCI 141, Fall 2023 course notes</h1>\n",
    "<h1 style=\"text-align:center;\">Tuples</h1>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Contents\n",
    "\n",
    "* [Tuples](#tuples)\n",
    "* [Converting between lists and tuples](#conversion)\n",
    "* [Tuples are a lot like lists](#lists)\n",
    "* [Mutable objects as defaults 😱](#mutable_defaults)\n",
    "* [Returning multiple objects](#multiple_returns)\n",
    "* [Tuples of mutable objects](#subtlety)\n",
    "* [Named tuples](#named_tuples)\n",
    "* [Exercises](#exercises)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true,
    "jupyter": {
     "outputs_hidden": true
    }
   },
   "source": [
    "# Tuples <a id=\"tuples\"/>\n",
    "\n",
    "Tuples are like lists, only they are immutable.  Everything about lists that does not involve mutability applies to tuples."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"danger\"></div>  Use tuples for data that should not change, as tuples will prevent you from accidentally changing that data."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For instance, suppose we are storing the number of days in the months (ignoring leap years):\n",
    "```python\n",
    "(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31)\n",
    "```\n",
    "Storing this information in a tuple insures that no one (i.e., you) can accidentally change it.\n",
    "\n",
    "Whereas a list is indicated by brackets <code>[]</code>, a tuple is indicated by parentheses <code>()</code>."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "too = (1, 2, 3)\n",
    "print(too)\n",
    "print(type(too))\n",
    "for t in too:\n",
    "    print(t)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can also omit the parentheses in an assignment, as long as it is clear there is a sequence:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = 1, 2, 3\n",
    "print(a)\n",
    "print(type(a))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's confirm that it is immutable."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a[0] = 42"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you want a tuple with a single element, you can't use"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = 1    # Not a sequence.\n",
    "print(type(a))\n",
    "a = (1)  # Just a parenthesized 1.\n",
    "print(type(a))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Instead, you need to include a comma to indicate you mean a tuple:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = 1,    # A sequence with only one term.\n",
    "print(type(a))\n",
    "a = (1,)  # A tuple with only one term.\n",
    "print(type(a))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Converting between lists and tuples <a id=\"conversion\"></a>\n",
    "\n",
    "You can convert lists into tuples."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a_list = [1, 3, 5, 'boo!']\n",
    "a_tuple = tuple(a_list)\n",
    "\n",
    "print(a_list)\n",
    "print('a_list is a', type(a_list))\n",
    "print(a_tuple)\n",
    "print('a_tuple is a', type(a_tuple))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can also turn tuples into lists."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a_tuple = (1, 3, 5, 'boo!')\n",
    "a_list = list(a_tuple)\n",
    "\n",
    "print(a_list)\n",
    "print('a_list is a', type(a_list))\n",
    "print(a_tuple)\n",
    "print('a_tuple is a', type(a_tuple))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Tuples are a lot like lists <a id=\"lists\"></a>\n",
    "\n",
    "Tuples behave a lot like lists and have many similar operations.\n",
    "\n",
    "For instance, tuples can be sliced to produce other tuples:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "a_tuple = (1, 3, 5, 'boo!', 'hoo!')\n",
    "print(a_tuple[0:2])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can iterate over tuples:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for t in a_tuple:\n",
    "    print(t)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Creation of a tuple from an iterable critter behaves in the same way as lists:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "too = tuple('monkeypox')\n",
    "print(too)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"danger\"></div> However, there are no tuple comprehensions. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "What looks like a tuple comprehension actually yields a very different critter called a <b>generator</b>, which we will discuss later."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a_tuple = (i**2 for i in range(0, 7))\n",
    "print(type(a_tuple))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Tuple assignment is also done with references.  This is less of a problem than with lists since we cannot change a tuple, so there is no danger of changing a tuple and have another tuple change as well."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = (1,2,3,4)\n",
    "b = a\n",
    "print(a is b)\n",
    "print(id(a))\n",
    "print(id(b))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Mutable objects as default values 😱  <a id=\"mutable_defaults\"/>\n",
    "\n",
    "Tuples are useful as default values for functions.\n",
    "\n",
    "Using a mutable object as a default value opens the door to having the default values change!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def foo(a=[3,1,4,1,6]):\n",
    "    x = [i**2 for i in a]\n",
    "    a[0] = 42  # change the default list.\n",
    "    return x\n",
    "\n",
    "# Since no arguments are passed, the default is used.\n",
    "print(foo()) # prints a**2\n",
    "print(foo()) # prints something different!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this example we changed the default value of <code>a[0]</code> in the first call to <code>foo()</code>, so the result of the second call to <code>foo()</code> was different.\n",
    "\n",
    "Default parameter values are evaluated **when the function is called**.\n",
    "\n",
    "This means your default values that are mutable can change, with unpredictable and undesireable results!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"danger\"></div>\n",
    "So, don't use mutable objects as default values."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this example, rather than use a list, use a tuple and you will see the error of your ways:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def foo(a=(3,1,4,1,6)):\n",
    "    x = [i**2 for i in a]\n",
    "    a[0] = 42\n",
    "    return x\n",
    "\n",
    "print(foo())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Returning multiple objects <a id=\"multiple_returns\"/>\n",
    "\n",
    "When multiple objects are returned from a Python function, they are returned as a <b>tuple</b>."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def foo(x):\n",
    "    return x**2, x**3\n",
    "\n",
    "a = foo(3)\n",
    "print(type(a), a)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This gives us two ways to deal with multiple outputs:\n",
    "* either as a tuple, or \n",
    "* by assigning multiple values in the calling code.\n",
    "\n",
    "For instance, we can also write"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "b, c = foo(4)\n",
    "print(type(b), b)\n",
    "print(type(c), c)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"try_it\">\n",
    "<b>Try it yourself.</b>\n",
    "\n",
    "Write and call a function like <code>foo()</code> that returns multiple outputs and call it both ways.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The ability to treat the output of a function as a tuple is useful if the function can return a variable number of outputs.\n",
    "\n",
    "For instance, consider the following function for computing the roots of $ax^{2} + bx + c = 0$.  If $a = 0$ or $a = b = 0$ it returns only one root; otherwise it returns two."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import cmath, math\n",
    "def roots(a, b, c):\n",
    "    \"\"\"\n",
    "    roots(a,b,c) returns the roots of ax**2 + bx + c = 0.\n",
    "    Special cases:\n",
    "      * If a = 0, then we return one root.\n",
    "      * If a = b = 0, then we return the value None.\n",
    "    \"\"\"\n",
    "    if (a != 0):\n",
    "        discriminant = b**2 - 4*a*c\n",
    "        if (discriminant >= 0):\n",
    "            r0 = (-b - math.sqrt(discriminant))/(2*a)\n",
    "            r1 = (-b + math.sqrt(discriminant))/(2*a)\n",
    "        else:\n",
    "            r0 = (-b - cmath.sqrt(discriminant))/(2*a)\n",
    "            r1 = (-b + cmath.sqrt(discriminant))/(2*a)\n",
    "        return r0, r1\n",
    "    elif (b != 0):\n",
    "        r0 = -c/b\n",
    "        return r0\n",
    "    else:\n",
    "        return None"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When we get the result from this function, we can tailor our output depending on whether there are two roots or not.   We can test to see if we have a tuple using the built-in function [<code>isinstance()</code>](https://docs.python.org/3/library/functions.html#isinstance)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# The polynomial is x^2 - 1 = (x-1) * (x+1).\n",
    "a =  1\n",
    "b =  0\n",
    "c = -1\n",
    "\n",
    "r = roots(a, b, c)\n",
    "\n",
    "if (isinstance(r, tuple)):\n",
    "    print(f'The roots are: {r[0]} and {r[1]}.')\n",
    "elif (r is None):\n",
    "    print('This is a constant polynomial!')\n",
    "else:\n",
    "    print('There is a single root {r}.')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Tuples of references to mutable objects <a id=\"subtlety\"/>\n",
    "\n",
    "How does the immutability of a tuple interact with mutability of the objects in the tuple?\n",
    " \n",
    "Consider the following tuple of lists:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = ([1, 2], [3, 4])\n",
    "print(a)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Since <code>a</code> is immutable, we cannot change it:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a[0] = [1,2,3,4]\n",
    "print(a)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b>Question.</b>  Now, what will happen if we try modifying <code>a[0]</code>, which is a list?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(a)\n",
    "a[0][0] = 42\n",
    "print(a)\n",
    "a[0].append(54)\n",
    "print(a)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b>Answer.</b>\n",
    "\n",
    "<div class=\"voila\">\n",
    "Well, this is interesting.  It appears that <code>a[0]</code> is mutable.\n",
    "Can we have an immutable tuple <code>a</code> of mutable objects?\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We have uncovered a subtlety of tuples of references to mutable objects:\n",
    "\n",
    "Consider:\n",
    "* The tuple <code>a</code> consists of <b>references</b> to lists.  The references cannot be changed.\n",
    "* When we try <code>a[0] = 42</code>, we are changing a reference to a list that is immutable because it is in a tuple.\n",
    "* However, <code>a[0][0]</code> is the first entry in the list referred to by <code>a[0]</code>, and we can change this entry.\n",
    "\n",
    "So, the references in the tuple are immutable, but the objects they refer to are mutable."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"try_it\">\n",
    "<b>Try it yourself.</b>\n",
    "\n",
    "Create a tuple of dictionaries and confirm that while you cannot alter the references contained in the tuple, you can alter the contents of the dictionaries contained in the tuple.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Named tuples <a id=\"named_tuples\"></a>\n",
    "\n",
    "[Named tuples](https://docs.python.org/3/library/collections.html#collections.namedtuple) are an extension of regular tuples found in [the <code>collections</code> module](https://docs.python.org/library/collections.html).  They allow us to give names to the elements of a tuple.  Here we store the standard hexadecimal RGB (red/green/blue) color codes:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from collections import namedtuple \n",
    "\n",
    "Colors = namedtuple('Colors', 'red, green, blue')\n",
    "colors = Colors(red=0xFF0000, green=0x00FF00, blue=0x0000FF)\n",
    "\n",
    "print(colors)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can access the elements by index, as with a standard tuple, and also by **field name**:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(colors[0])\n",
    "print(colors.red)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Named tuples are useful when you have a number of related constants.  For instance, we could imagine organizing different levels of verbosity to control output:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Verbosity = namedtuple('Verbosity', 'silent, taciturn, chatty, verbose, logorrheic')\n",
    "verbosity = Verbosity(silent=0, taciturn=1, chatty=2, verbose=3, logorrheic=4)\n",
    "\n",
    "print(verbosity)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"try_it\"><b>Question.</b>  What happens if you leave out one or more of the field names when you create a particular instance of a named tuple?  I.e.,\n",
    "\n",
    "<code>\n",
    "verbosity = Verbosity(silent=0, taciturn=1, chatty=2, verbose=3)\n",
    "</code>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can also create a named tuple from a list without using the field names:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "verbosity = Verbosity._make([0,1,2,42,6*9])\n",
    "print(verbosity)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true,
    "jupyter": {
     "outputs_hidden": true
    }
   },
   "source": [
    "# Exercises <a id=\"exercises\"/>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true,
    "jupyter": {
     "outputs_hidden": true
    }
   },
   "source": [
    "<div class=\"exercise\">\n",
    "<b>Exercise.</b>  Can you use <code class=\"kw\">del</code> to delete items in a tuple?\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b>Answer.</b>\n",
    "<div class=\"voila\">\n",
    "No.  Tuples are truly immutable:\n",
    "<pre>\n",
    "a = (0, 1, 2, 3)\n",
    "del a[1]\n",
    "</pre>\n",
    "You can, however, use <code>del</code> to delete an entire tuple:\n",
    "<pre>\n",
    "a = (0, 1, 2, 3)\n",
    "del a\n",
    "</pre>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h4>This notebook was brought to you by dogs:</h4>\n",
    "\n",
    "The greatest pleasure of a dog is that you may make a fool of yourself with him, and not only will he not scold\n",
    "you, but he will make a fool of himself too. <br/>\n",
    "&ndash; Samuel Butler"
   ]
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
