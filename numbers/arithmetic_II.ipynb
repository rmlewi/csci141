{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "nbpresent": {
     "id": "abaeef91-5553-4553-8ebf-2c22a87d7137"
    }
   },
   "source": [
    "<img src=\"http://www.cs.wm.edu/~rml/images/wm_horizontal_single_line_full_color.png\">\n",
    "\n",
    "<h1 style=\"text-align:center;\">CSCI 141-01, Fall 2023 course notes</h1>\n",
    "\n",
    "<h1 style=\"text-align:center;\">More arithmetic</h1>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Contents\n",
    "\n",
    "* [Less familiar arithmetic operations](#arithmetic)\n",
    "* [Floored quotient (//)](#floored_quotient)\n",
    "* [Remainder (%)](#remainder)\n",
    "    * [The <code class=\"kw\">divmod()</code> function](#divmod)\n",
    "* [Eschew <code>%</code> and <code>//</code> involving floats! 😱](#floating_point_mod)\n",
    "* [Augmented assignment operators](#op=)\n",
    "* [Exercises](#int_exercises)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Less familiar arithmetic operations <a id=\"arithmetic\"/>\n",
    "\n",
    "In this lesson we will look at some arithmetic operations that may be less familiar, but are very common in computer science."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbpresent": {
     "id": "9e67acfd-594c-45cb-88e5-df96cace4520"
    }
   },
   "source": [
    "# Floor quotient <a id=\"floored_quotient\"/>\n",
    "\n",
    "In computer science and mathematics, \n",
    "* the <b>floor</b> of a number $x$ is the largest whole number less than or equal to $x$, and\n",
    "* the <b>ceiling</b> of a number $x$ is the smallest whole number greater than or equal to $x$.  \n",
    "\n",
    "There are functions in the <code class=\"kw\">math</code> module that return the floor and ceiling:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from math import ceil, floor\n",
    "x = 3.14\n",
    "print(floor(x), ceil(x))\n",
    "\n",
    "x = -3.14\n",
    "print(floor(x), ceil(x))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Floor division** (or **floored quotient**), denoted by <code>//</code>, first performs standard division but then rounds down to the nearest whole number."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"danger\"></div>\n",
    "The floored quotient should only be used for integers.  It is still defined if floats are involved, but for reasons we will describe shortly, it should not be used for floats."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(8 // 3)\n",
    "print(type(8 // 3))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Why use the floored quotient?\n",
    "\n",
    "The floored quotient is useful in situations like the following:  Suppose we have bags that will each hold 3 possums.  How many bags do we need to store 31 possums?\n",
    "\n",
    "If we performed regular division, the answer would be 31/3 = 10 1/3.  But this isn't right since a fractional number of bags doesn't make sense.\n",
    "\n",
    "Instead, the correct answer is to round 31/3 down and then add one bag: <code>(31//3) + 1 = 10 + 1 = 11</code>.\n",
    "\n",
    "In general, the floored quotient is handy when you are dealing with items that cannot be subdivided."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Watch out if negative numbers are involved! 😱 \n",
    "\n",
    "Note that for negative numbers, rounding down means rounding to the next whole number to the left.  Thus,\n",
    "$$\n",
    "\\frac{-8}{3} = -2 \\frac{2}{3},\n",
    "$$\n",
    "which is rounded down to $-3$:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(-8 // 3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"danger\"></div>\n",
    "Observe that as a consequence, -(8 // 3) is not the same as (-8 // 3) !"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('-(8 // 3):', -(8 // 3))\n",
    "print('(-8 // 3):', (-8 // 3))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<p class=\"try_it\">\n",
    "<b>Try it yourself.</b>\n",
    "Create your own examples of integer division and print the results.\n",
    "</p>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbpresent": {
     "id": "abe68ee5-4c2f-4faf-bb82-e3587ce0c0bf"
    }
   },
   "source": [
    "# Remainder <a id=\"remainder\"/>\n",
    "\n",
    "The <b>remainder</b> or <b>modulo</b> or (or simply <b>mod</b>) operation is represented in Python by\n",
    "```python\n",
    "n % m\n",
    "```\n",
    "This returns the remainder when <code>n</code> is divided by <code>m</code>.\n",
    "\n",
    "If <code>m</code> and <code>n</code> are two integers, then <a href=\"https://docs.python.org/3/reference/expressions.html#binary-arithmetic-operations\">the following relation always holds</a>:\n",
    "```python\n",
    "n = ((n // m) * m) + (n % m).\n",
    "```\n",
    "Observe that the division on the right-hand side is floor division, not regular division.\n",
    "\n",
    "As we will see, the moduluo operator turns out to be pretty useful when programming, which is why you find it in most computer languages."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The remainder for positive numbers\n",
    "\n",
    "The remainder is what you would expect if the numbers involved are positive."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbpresent": {
     "id": "b43e002f-a355-4b83-bdd0-a36e08458464"
    }
   },
   "outputs": [],
   "source": [
    "print(8 % 3)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbpresent": {
     "id": "19164425-4246-4f79-bb60-0c3cabddac43"
    }
   },
   "outputs": [],
   "source": [
    "print(3 % 8)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbpresent": {
     "id": "b7a0aae8-4b0b-430c-9d64-073ed81d9aa9"
    }
   },
   "outputs": [],
   "source": [
    "print(0 % 8)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbpresent": {
     "id": "7e8116b3-3fc0-47ca-bd30-62b92f27aefe"
    }
   },
   "source": [
    "What do you think happens if we try to find the remainder after division by 0?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbpresent": {
     "id": "5e7976f6-4a01-4908-a546-21481142bb5e"
    },
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "print(8 % 0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbpresent": {
     "id": "f4b6d988-aaa9-41a6-9578-7d16a055b7db"
    }
   },
   "source": [
    "<b>Answer.</b>\n",
    "<div class=\"voila\">\n",
    "<code>n % 0</code> is not defined and produces an error!\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbpresent": {
     "id": "8dac3d4a-ca44-4c48-a5a4-6d6d901725f8"
    }
   },
   "source": [
    "## The remainder if negative numbers are involved 😱 🐞"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"danger\"></div> If negative numbers are involved, the definition of the remainder is a bit more complicated, and might not yield the result you expect."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Fortunately, remainder operations involving negative numbers are infrequent.\n",
    "\n",
    "We want to ensure that we always have\n",
    "```python\n",
    "n = ((n // m) * m) + (n % m).\n",
    "```\n",
    "The remainder operator applied to <code>n % m</code> \n",
    "<a href=\"https://docs.python.org/3/reference/expressions.html#binary-arithmetic-operations\">works as follows</a>.  Find integers q and r such that\n",
    "<ol>\n",
    "<li>n = qm + r,</li>\n",
    "<li>r and m have the same sign, and</li>\n",
    "<li>| r | &leq; | m |.</li>\n",
    "</ol>\n",
    "Then <code>n % m = r</code>.  \n",
    "\n",
    "So, <code>5 % -3</code> is computed from\n",
    "$$\n",
    "5 = (-2 \\times -3) - 1,\n",
    "$$\n",
    "rather than\n",
    "$$\n",
    "5 = (-1 \\times -3) + 2,\n",
    "$$\n",
    "since in the latter expression the remainder is +2, which does not have the same sign as -3."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbpresent": {
     "id": "65a051f2-8695-4c31-99d8-76620010a677"
    }
   },
   "outputs": [],
   "source": [
    "print(5 % -3)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbpresent": {
     "id": "646c61ec-6753-43f5-bbc3-c107198ace90"
    }
   },
   "outputs": [],
   "source": [
    "print(-5 % 3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbpresent": {
     "id": "fd61e21b-7aff-484b-a4d5-0ed7c3ebab2e"
    }
   },
   "source": [
    "<b>Question</b>: How is $-5$ represented in terms of $-3$ in the following calculation?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbpresent": {
     "id": "c66ecff7-293b-4ead-b2f1-1a47da10a281"
    }
   },
   "outputs": [],
   "source": [
    "print(-5 % -3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbpresent": {
     "id": "c079083a-b697-4419-9968-568ddcffa52b"
    }
   },
   "source": [
    "<b>Answer.</b>\n",
    "<div class=\"voila\">\n",
    "We use -5 = (1 x -3) + -2 since the sign of the remainder -2 is the same as the sign of -3.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<p class=\"try_it\">\n",
    "<b>Try it yourself.</b>\n",
    "Create your own examples of the remainder operation and print the results.\n",
    "</p>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The <code class=\"kw\">divmod()</code> function <a id=\"divmod\"></a>\n",
    "\n",
    "There is also a built-in function named [<code class=\"kw\">divmod()</code>](https://docs.python.org/3/library/functions.html?highlight=divmod#divmod) that computes both the floor quotient and the remainder at one time:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# divmod(m, n) computes m // n and m % n.\n",
    "\n",
    "quo, rem = divmod(21, 10)\n",
    "print(quo, rem)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Eschew <code>%</code> and <code>//</code> involving floats! 😱🐞 <a id=\"floating_point_mod\"/>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"danger\"></div> Don't use the modulo and floored quotient when floats are involved.  Some of the time the results are what you'd expect, but when they aren't, the results can be startling."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here are some cases where floor division works the way you would expect:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(9.0 // 4.0)\n",
    "print(7.0 // 2.1)\n",
    "print(2.6 // 0.5)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here is a case where floor division behaves badly. This example is based on [this discussion](https://stackoverflow.com/questions/2019588/integer-division-compared-to-floored-quotient-why-this-surprising-result).\n",
    "\n",
    "We begin with the modulo operation.  The exact answer is 0."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('11 % 1.1 =', 11 % 1.1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Gadzooks! This isn't even close!  But it gets weirder&hellip;"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's compute the floored quotient for 11 and 1.1.\n",
    "\n",
    "We compute the result two ways:\n",
    "1. We perform regular division of 1.1 into 11, and use the <code>floor()</code> function from the <code>math</code> module to round down to the biggest integer less than or equal to the result.\n",
    "2. We apply the floored quotient operator.\n",
    "\n",
    "The exact answer is 10."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import math\n",
    "print(math.floor(11 / 1.1))\n",
    "print(11 // 1.1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Great oogly-moogly!  Not only is the modulo wrong, so is the floored quotient!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "But wait &ndash; there's more!  Now let's try to recover the original number 11 from both calculations:\n",
    "1. <code>math.floor(11 /  1.1) * 1.1 + (11 % 1.1)</code>\n",
    "2. <code>          (11 // 1.1) * 1.1 + (11 % 1.1)</code>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(math.floor(11 /  1.1) * 1.1 + (11 % 1.1))\n",
    "print(          (11 // 1.1) * 1.1 + (11 % 1.1))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we see why the value of <code>11 // 1.1</code> was so wonky &ndash; it compensated for the error in <code>11 % 1.1</code> in order to guarantee the result \n",
    "```python\n",
    "11 = (11 // 1.1) * 1.1 + (11 % 1.1)\n",
    "```\n",
    "\n",
    "So despite what you might have been taught, sometimes two wrongs <b>do</b> make a right!\n",
    "\n",
    "I rooted around in [the source for CPython, the reference Python implementation](https://github.com/python/cpython/blob/master/Objects/floatobject.c), and confirmed that floating point floor division is implemented so that this result is guaranteed."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## What went wrong\n",
    "\n",
    "Guido van Rossum [explained the strange behavior of floating point floor division](https://mail.python.org/pipermail/python-dev/2007-January/070708.html) thusly:\n",
    "<blockquote>\n",
    "Probably because I tend not to know what I'm doing when numerics are concerned. :-(\n",
    "</blockquote>\n",
    "\n",
    "In this case, because of tiny floating point errors we have"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(11.0 - 9*1.1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This means that in floating point, $$11.0 - 9*1.1 < 1.1$$ so <code>11 // 1.1</code> is 9, not 10.\n",
    "\n",
    "If we calculate the remainder by hand, the result we obtain is not quite the one returned by <code>%</code>:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(11.0 - 9*1.1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Tracking down the reason for this difference would require delving in the math library of the C programming language used in CPython."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Augmented assignment operators: -=, *=, +=, /=, //=, %= <a id=\"op=\"/>\n",
    "\n",
    "A frequently encountered programming pattern is the modification of a variable via an arithmetic operation:\n",
    "<pre>\n",
    "n = n + 1\n",
    "t = t + dt\n",
    "</pre>\n",
    "This sort of operation occurs, for instance, when counting the number of times something is found.\n",
    "\n",
    "For such updating operations Python provides an abbreviated notation:\n",
    "<pre>\n",
    "x +=  42  is the same as  x = x + 42\n",
    "x -=  42  is the same as  x = x - 42\n",
    "x &#42;=  42  is the same as  x = x &#42; 42\n",
    "x /=  42  is the same as  x = x / 42\n",
    "x //= 42  is the same as  x = x // 42\n",
    "x %=  42  is the same as  x = x % 42\n",
    "x &#42;&#42;= 42  is the same as  x = x&#42;&#42;42\n",
    "</pre>\n",
    "These operators are called <b>augmented assignment operators</b> in Python."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x = 17\n",
    "y =  6\n",
    "\n",
    "x += y\n",
    "print(x)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x = 17\n",
    "y =  6\n",
    "\n",
    "x -= y\n",
    "print(x)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Why should I use augmented assignment operators?\n",
    "\n",
    "A student's bug from Fall 2016 suggests one reason to use these operators.  This student was trying to decrease the variable <code>remaining_lives</code> by one.  Unfortunately, there was a typo in his code:\n",
    "```python\n",
    "reamining_lives = remaining_lives - 1\n",
    "```\n",
    "Python brought a new variable <code>reamining_lives</code> on the left into being, and left <code>remaining_lives</code> unchanged, which was not the desired effect.\n",
    "\n",
    "On the other hand,\n",
    "```python\n",
    "reamining_lives -= 1\n",
    "```\n",
    "would have resulted in a <code class=\"error\">NameError</code> because there was no variable of this name already in existance."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"danger\"></div> <b>Remember:</b> the more you type, the more likely you are to make a mistake."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here are the rest of these operators in action."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x = 17\n",
    "y =  6\n",
    "\n",
    "x *= y\n",
    "print(x)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x = 17\n",
    "y =  6\n",
    "\n",
    "x /= y\n",
    "print(x)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x = 17\n",
    "y =  6\n",
    "\n",
    "x //= y\n",
    "print(x)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x = 17\n",
    "y =  6\n",
    "\n",
    "x %= y\n",
    "print(x)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x = 6\n",
    "x **= 2\n",
    "print(x)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Exercises <a id=\"int_exercises\"></a>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<p class=\"exercise\">\n",
    "What is the remainder when 12345678987654321 is divided by 12345678?\n",
    "</p>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b>Answer.</b>\n",
    "<p class=\"voila\">\n",
    "<code>12345678987654321 % 12345678 = 81</code>.\n",
    "</p>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<p class=\"exercise\">\n",
    "Suppose $m$ and $n$ are positive integers.  What is the value of \n",
    "<code>n - m * (n // m) - (n % m)</code>?\n",
    "</p>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b>Answer.</b>\n",
    "<p class=\"voila\">\n",
    "    Since <code>n</code> and <code>m</code> are integers, <code>n - m * (n // m) - (n % m)</code> is always zero.\n",
    "</p>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### This notebook was brought to you by opening lines.\n",
    "\n",
    "<blockquote>\n",
    "Ludwig Boltzmann, who spent much of his life studying statistical mechanics, died in 1906, by his own hand.\n",
    "Paul Ehrenfest, carrying on the work, died similarly in 1933.  Now it is our turn to study statistical mechanics.<br/><br/>\n",
    "\n",
    "Perhaps it will be wise to approach the subject cautiously. <br/>\n",
    "&ndash; David L. Goldstein, [*States of Matter*](https://www.amazon.com/States-Matter-Dover-Books-Physics/dp/048664927X)\n",
    "</blockquote>"
   ]
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
