{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "nbpresent": {
     "id": "abaeef91-5553-4553-8ebf-2c22a87d7137"
    }
   },
   "source": [
    "<img src=\"http://www.cs.wm.edu/~rml/images/wm_horizontal_single_line_full_color.png\">\n",
    "\n",
    "<h1 style=\"text-align:center;\">CSCI 141-01, Fall 2023 course notes</h1>\n",
    "\n",
    "<h1 style=\"text-align:center;\">Numbers</h1>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Contents\n",
    "\n",
    "\n",
    "* [Integers](#integers)\n",
    "* [Writing integers](#writing_integers)\n",
    "* [Conversion to <code>int</code>](#conversion)\n",
    "* [Floating point numbers](#floats)\n",
    "* [Scientific notation](#scientific_notation)\n",
    "* [Conversion to and from <code>float</code>](#conversion)\n",
    "    * [Narrowing conversions](#narrowing)\n",
    "* [Mixing integers and floating point numbers](#mixed_type)\n",
    "* [Two special floats: inf and nan 🤯](#inf_nan)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbpresent": {
     "id": "b0b27c4a-a962-4fb3-8b52-198bf569cd28"
    }
   },
   "source": [
    "# Integers <a id=\"integers\"></a>\n",
    "\n",
    "Integers in Python are just as they are in mathematics: whole numbers.  Python refers to the integer class as <code class=\"kw\">int</code> (as do C, C++, Java, and Javascript, among other languages)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Writing integers <a id=\"writing_integers\"></a>\n",
    "\n",
    "An integer should never contain a decimal point.  For instance,"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "n = 314\n",
    "print(type(n))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "but"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "n = 314.0\n",
    "print(type(n))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Sometimes it is useful to use a separator in a number.  The [separator varies by country](https://en.wikipedia.org/wiki/Decimal_separator#Current_standards); in the United States we use the comma.  Let's try that in Python:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "n = 123,456,789\n",
    "print(n)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Whoa! that's not what we wanted!  In fact, Python has interpreted our commas as separating numbers in a sequence, and created a critter called a <b>tuple</b>, which we will study later:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(type(n))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Instead of a comma, Python allows you to use an underscore _ as a separator in numbers.  This is useful to avoid mistakes.  For instance, suppose we want to set the variable <code>n</code> to one billion:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "n = 100000000\n",
    "print(n)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Quick &ndash; is this correct?  Let's rewrite the number using underscores:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "n = 1_000_000_00"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This reveals that we have made a mistake.  This is another example of **defensive programming** &ndash; that is, program in a way that eliminates the possibility of certain bugs.  We will see more examples as we go."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"danger\"></div>  Always program defensively."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"try_it\">\n",
    "<b>Try it yourself.</b>\n",
    "Set <code>n</code> to have the value one trillion (1 followed by 12 zeros) using underscores as separators.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbpresent": {
     "id": "b9e4952b-3b15-40c8-b79a-d3f621ad4624"
    }
   },
   "source": [
    "# Type conversion <a id=\"conversion\"/>\n",
    "\n",
    "Earlier we saw how Python views input you type as a string.  However, sometimes we want the input to be interpreted as a number.\n",
    "\n",
    "We can accomplish this using a <b>type conversion</b>.  In the next code snippet, we convert the input strings into integers.  We do so using the <code>int()</code> function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbpresent": {
     "id": "98a5561a-bd0b-41d8-94ec-2fadf68529b0"
    }
   },
   "outputs": [],
   "source": [
    "s = input('What is the airspeed of a European swallow? ')\n",
    "t = input('What is the airspeed of an African swallow? ')\n",
    "s = int(s) # Using int() performs the conversion.\n",
    "t = int(t)\n",
    "print('Combined airspeed:', s+t)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "n = int(42.1)\n",
    "print(n)\n",
    "n = int(-42.9999)\n",
    "print(n)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Notice that the fractional part of -42.9999 was dropped by <code>int()</code>."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can do this more succinctly as follows:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s = int(input('What is the airspeed of a European swallow? '))\n",
    "t = int(input('What is the airspeed of an African swallow? '))\n",
    "print('Combined airspeed:', s+t)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbpresent": {
     "id": "4925b15d-536e-4f05-9aac-3f0177873c89"
    }
   },
   "source": [
    "If we try to convert a string that doesn't look like a number, then a <code>ValueError</code> occurs."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbpresent": {
     "id": "212d78e7-d698-4716-90e8-5e2e34bd530b"
    }
   },
   "outputs": [],
   "source": [
    "s = '42 mph'\n",
    "t = '54 mph'\n",
    "s = int(s)\n",
    "t = int(t)\n",
    "print('Combined airspeed:', s+t)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"try_it\">\n",
    "<b>Try it yourself.</b>\n",
    "Write your own code like the preceding that generates a <code>ValueError</code>.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Floating point numbers <a id=\"floats\"></a>\n",
    "\n",
    "Floating point numbers can represent any real number (e.g., $\\pi$), not just whole numbers.  This means that they have a fractional part.  In Python a floating point number is called a <code class=\"kw\">float</code>.\n",
    "\n",
    "The term <b>floating point</b> has to do with the way these numbers are represented and manipulated (an important topic we will discuss in detail later).  In the familiar decimal (base-10) number system, floating point numbers like\n",
    "$$\n",
    "1.2345 \\times 10^{2}\n",
    "$$\n",
    "or\n",
    "$$\n",
    "1.2345 \\times 10^{3}.\n",
    "$$\n",
    "The first number represent $123.45$ while the second number represents $1234.5$.  \n",
    "\n",
    "We can store either number using $1.2345$ and the exponent on the $10$ (either $2$ or $3$, in this case).  \n",
    "\n",
    "In both representations the decimal point \"floats\" to the same location, $1.2345$.\n",
    "\n",
    "The presence of a decimal point '.' means that the number is a <code class=\"kw\">float</code>.  \n",
    "\n",
    "This means <code>3</code> and <code>3.0</code> are of different types, though they represent the same numerical value:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "i = 3\n",
    "print(type(i))\n",
    "x = 3.0\n",
    "print(type(x))\n",
    "x = 3.\n",
    "print(type(x))\n",
    "pi = 3.14\n",
    "print(type(pi))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If there is no digit explicitly written to the left of the decimal point then the leading digit is assumed to be zero:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x = .3\n",
    "print(x)\n",
    "x = .0\n",
    "print(x)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If there is no digit to the right of the decimal point the fractional part is assumed to be zero:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x = 3.\n",
    "print(x)\n",
    "x = 0.\n",
    "print(x)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As with integers, you can use underscores as separators:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x = 1_000_000_000.0\n",
    "print(x)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Scientific notation <a id=\"scientific_notation\"/>\n",
    "\n",
    "You can also specify floating point numbers using scientific notation.\n",
    "\n",
    "For instance,\n",
    "$$\n",
    "1234 = 1.234 \\times 10^{3} = 0.1234 \\times 10^{4},\n",
    "$$\n",
    "which we can denote in Python using scientific notation as any of the following:\n",
    "<pre>\n",
    "1.234e3\n",
    "1.234e+3\n",
    "1.234e+03\n",
    "0.1234e4\n",
    "</pre>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x = 1234.0\n",
    "print(x)\n",
    "y = 1.234e3\n",
    "print(y)\n",
    "z = 0.1234e4\n",
    "print(z)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The exponent can be negative, as in <code>1.0e-2</code>, which is $1 \\times 10^{-2}$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x = 1.0e-2\n",
    "print(x)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The usual practice is to have one nonzero digit to the left of the decimal point, and the rest of the digits to the right of the decimal point, so\n",
    "<pre>\n",
    "x = 1.86282e5\n",
    "</pre>\n",
    "as opposed to\n",
    "<pre>\n",
    "x = 0.186282e6\n",
    "</pre>\n",
    "There need not be any digits to the right of the decimal point.  For instance, we can write <pre>x = 1000</pre> as <pre>x = 1e3</pre>\n",
    "\n",
    "However, <pre>x = 1000</pre> yields an <code>int</code>, while <pre>n = 1e3</pre> yields a <code>float</code>."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## When is scientific notation useful?\n",
    "\n",
    "Scientific notation is particular useful for numbers that are very large or very small.\n",
    "\n",
    "For instance, one billion can be written unamiguously as <tt>1e9</tt>.  \n",
    "\n",
    "Writing out all the digits in one billion, on the other hand, requires more care: quick!  which of the following is one billion?  <tt>100000000</tt>, <tt>1000000000</tt>, or <tt>10000000000</tt>?\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"danger\"></div><b>Remember:</b> the more you type, the more likely you are to make a mistake."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "What value do we obtain if there is no digit to the left of the decimal point?  I.e., what do\n",
    "<pre>\n",
    "x = .0e3\n",
    "x = .0e-6\n",
    "</pre>\n",
    "yield?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x = .0e3\n",
    "print(x)\n",
    "x = .0e-6\n",
    "print(x)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As before, if there is no digit is explicitly written to the left of the decimal point then the leading digit is assumed to be zero.  Thus, <tt>.0e3</tt> is <tt>0.0e3</tt>, or\n",
    "$$\n",
    "0 \\times 10^{3} = 0.\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"danger\"></div> When using scientific notation, don't forget the <code>e</code>! "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For instance, consider\n",
    "```python\n",
    "x = 1.0e-6  # x is 1/1,000,000.\n",
    "y = 1.0-6   # Oops!  y is -5.0!\n",
    "```\n",
    "Unfortunately, the latter is perfectly valid Python (and C, and C++, and Fortran, and Matlab &hellip;)  I've been burned by omitting the <code>e</code>."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbpresent": {
     "id": "4a04f645-1335-4e8d-9237-cde7d2e371a0"
    }
   },
   "source": [
    "# Conversions to and from <code>float</code> <a id=\"conversion\"/>\n",
    "\n",
    "Strings and integers can be converted to floats using <code>float()</code>."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s = '3.14'\n",
    "n = 42\n",
    "\n",
    "x = float(s)\n",
    "y = float(n)\n",
    "print(x, y)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Converting <code>float</code> to <code>int</code>: narrowing conversions <a id=\"narrowing\"/>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"danger\"></div>\n",
    "When a float is converted to an integer, the fractional part of the float is lost."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Conversions that lose information like this are called <b>narrowing conversions</b>.\n",
    "Be careful about narrowing conversions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbpresent": {
     "id": "04dbcbda-5d6f-43a2-aeaa-de0531dab6e8"
    },
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "x = 3.14\n",
    "n = int(x)\n",
    "print(n)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbpresent": {
     "id": "ce163ac7-c66f-45ba-aa6d-9e4aac6a3efb"
    }
   },
   "source": [
    "On the other hand, when an integer is converted to a float, typically no information is lost."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "n = 42\n",
    "x = float(n)\n",
    "print(x)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"danger\"></div>\n",
    "We say that <i>typically</i> no information is lost because it is possible that very large integers might not be exactly converted to a float:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "n = 2**53 + 1\n",
    "x = float(n)\n",
    "print(n)\n",
    "print(x)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Fortunately, it is uncommon to encounter such large numbers in practice.\n",
    "\n",
    "We will discuss the reason for this behavior when we delve into floating point more deeply. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbpresent": {
     "id": "d7946616-3dfb-4257-ab65-7d2ab800d892"
    }
   },
   "source": [
    "# Mixing integers and floating point numbers <a id=\"mixed_type\"/>\n",
    "\n",
    "Because floating point numbers can express a greater variety of values than integers, when the two types are combined the integer values are implicitly converted to floating point values so that information is not lost when they are all combined.  This is the reason that the presence of even one floating point number in an arithmetic expression means the result will be a floating point number."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbpresent": {
     "id": "7623349e-8c98-4814-abd3-2409115c08e7"
    }
   },
   "outputs": [],
   "source": [
    "i = 2\n",
    "x = 2.0\n",
    "print(i + x)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbpresent": {
     "id": "a2760c9d-c96a-4ed7-989b-d8fc65a88118"
    }
   },
   "outputs": [],
   "source": [
    "print(54/42.0)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbpresent": {
     "id": "627e130e-a456-4c08-9415-84fff21baf6f"
    }
   },
   "outputs": [],
   "source": [
    "print(1/3)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbpresent": {
     "id": "7fef72a2-bbb0-4b17-b94a-f760dea709b7"
    }
   },
   "outputs": [],
   "source": [
    "type(1/3)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbpresent": {
     "id": "80f2e29a-5f17-43cf-a654-9de60b1479ea"
    }
   },
   "outputs": [],
   "source": [
    "type(4/2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Two special floats: <code>inf</code> and <code>nan</code> &#x1f92f;  <a id=\"inf_nan\"></a> \n",
    "\n",
    "There are two special floats:\n",
    "* <code>inf</code>, for infinity, and\n",
    "* <code>nan</code>, for not-a-number.\n",
    "\n",
    "You can get them from the <code>math</code> module,"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from math import inf, nan\n",
    "print(inf, nan)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "or you could use a string to float conversion:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "inf nan\n"
     ]
    }
   ],
   "source": [
    "inf = float('inf')\n",
    "nan = float('nan')\n",
    "print(inf, nan)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "An <code>inf</code> behaves as you might expect &ndash; it is larger than any other number:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(inf > 10**300)\n",
    "print(-inf < -10**300)\n",
    "print(inf == inf)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Arithmetic involving <code>inf</code> and a finite number also behaves as you would expect:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(inf - 42)\n",
    "print(inf + 42)\n",
    "print(inf * 42)\n",
    "print(inf / 42)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Arithmetic involving two infinities is more interesting:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(inf + inf)\n",
    "print(inf * inf)\n",
    "print(inf - inf)\n",
    "print(inf / inf)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The results of &infin; - &infin; and &infin; / &infin; are not defined.  However, rather than treating them as errors, Python returns the special value <b>not-a-number</b>, or <code>nan</code>.\n",
    "\n",
    "Arithmetic involving <code>nan</code> results in another <code>nan</code>."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(nan + 42)\n",
    "print(nan * 42)\n",
    "print(nan - 42)\n",
    "print(nan / 42)\n",
    "print(nan + nan)\n",
    "print(nan * nan)\n",
    "print(nan - nan)\n",
    "print(nan / nan)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Check out the following:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "True or false? nan equals nan. False\n"
     ]
    }
   ],
   "source": [
    "print(f'True or false? nan equals nan. {nan == nan}')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Nothing can be equal to not-a-number, not even not-a-number &ndash; it's not a number, after all!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### What are they good for?\n",
    "\n",
    "The value <code>inf</code> is useful if you need a number that is guaranteed to be larger than anything.  Since <code>nan</code> should not come about from any normal operation, it is useful as a distinctive placeholder.  For instance, in the [pandas data analysis package](https://pandas.pydata.org), missing data are indicated by not-a-numbers."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h4>This notebook was brought to you by numbers:</h4>\n",
    "\n",
    "When you can measure what you are speaking about and express it in numbers, you know something about it;\n",
    "but when you cannot measure it, when you cannot express it in numbers, your knowledge is of a meager and\n",
    "unsatisfactory kind. <br/>\n",
    "&ndash; Lord Kelvin"
   ]
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
