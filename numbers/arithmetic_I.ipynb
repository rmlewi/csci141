{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "jp-MarkdownHeadingCollapsed": true,
    "nbpresent": {
     "id": "abaeef91-5553-4553-8ebf-2c22a87d7137"
    }
   },
   "source": [
    "<img src=\"http://www.cs.wm.edu/~rml/images/wm_horizontal_single_line_full_color.png\">\n",
    "\n",
    "<h1 style=\"text-align:center;\">CSCI 141-01, Fall 2023 course notes</h1>\n",
    "\n",
    "<h1 style=\"text-align:center;\">Basic arithmetic</h1>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Contents\n",
    "\n",
    "* [Basic arithmetic operations (+, -, &#42;, /)](#arithmetic)\n",
    "    * [Addition](#addition)\n",
    "    * [Subtraction](#subtraction)\n",
    "    * [Multiplication](#multiplication)\n",
    "    * [Division](#division)\n",
    "* [Mixing integers and floating point numbers](#mixing_types)\n",
    "* [Exponentiation (&#42;&#42;)](#exponentiation)\n",
    "* [Operator precedence and grouping of operations](#precedence)\n",
    "* [Floating point arithmetic is not always exact!](#inexactness)\n",
    "* [Exercises](#int_exercises)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Ambition, Distraction, Uglification, and Derision\n",
    "\n",
    "<a href=\"https://gutenberg.org/cache/epub/28885/pg28885-images.html\">\n",
    "<img style=\"float:left;\" src=\"http://www.cs.wm.edu/~rml/images/alice34a.gif\" width=160>\n",
    "</a>\n",
    "\"I couldn't afford to learn it.\" said the Mock Turtle with a sigh. \n",
    "\"I only took the regular course.\"\n",
    "\n",
    "\"What was that?\" inquired Alice.\n",
    "\n",
    "\"Reeling and Writhing, of course, to begin with,\" the Mock Turtle replied;\n",
    "\"and then the different branches of Arithmetic &ndash; Ambition, Distraction, Uglification, and Derision.\"\n",
    "\n",
    "\"I never heard of 'Uglification,'\" Alice ventured to say. \"What is it?\"\n",
    "\n",
    "The Gryphon lifted up both its paws in surprise. \"What! Never heard of\n",
    "uglifying!\" it exclaimed. \"You know what to beautify is, I suppose?\"\n",
    "\n",
    "\"Yes,\" said Alice doubtfully: \"it means &ndash; to &ndash; make &ndash; anything &ndash; prettier.\"\n",
    "\n",
    "\"Well, then,\" the Gryphon went on, \"if you don't know what to uglify is, you <i>are</i> a simpleton.\" "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Basic arithmetic operations <a id=\"arithmetic\"/>\n",
    "\n",
    "For the most part, arithmetic in Python behaves like what you have learned in math class.  The major difference involves the types (<code>int</code> vs <code>float</code>) of the numbers involved and the result."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbpresent": {
     "id": "639a48de-e393-4dda-8c89-d11a59737cdb"
    }
   },
   "source": [
    "## Addition <a id=\"addition\"/>\n",
    "\n",
    "<div>\n",
    "    The addition of two integers results in a value that is an integer.\n",
    "</div>\n",
    "<div>\n",
    "    The addition of a float with a float or an integer results in a value that is a float.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbpresent": {
     "id": "80e64b61-c4fb-42ca-baf8-d371d4820072"
    }
   },
   "outputs": [],
   "source": [
    "print(2 + 2)\n",
    "print(2.0 + 2.0)\n",
    "print(2.0 + 2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbpresent": {
     "id": "f87357fe-3cd5-4b8b-807b-50487bf2a1f3"
    }
   },
   "source": [
    "## Subtraction <a id=\"subtraction\"/>\n",
    "\n",
    "<div>\n",
    "    The difference of two integers results in a value that is an integer.\n",
    "</div>\n",
    "<div>\n",
    "    The difference of a float with a float or an integer results in a value that is a float.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbpresent": {
     "id": "11486217-8b20-4954-987f-558f70dac72c"
    }
   },
   "outputs": [],
   "source": [
    "print(54 - 42)\n",
    "print(54.0 - 42.0)\n",
    "print(54.0 - 42)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbpresent": {
     "id": "a57b08d9-3fa8-42b6-b67d-8510345c9f0d"
    }
   },
   "source": [
    "## Multiplication <a id=\"multiplication\"/>\n",
    "\n",
    "<div>\n",
    "    The product of two integers results in a value that is an integer.\n",
    "</div>\n",
    "<div>\n",
    "    The product of a float with a float or an integer results in a value that is a float.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbpresent": {
     "id": "aff2c09e-d72e-4d4d-993a-2512a7fb6f79"
    }
   },
   "outputs": [],
   "source": [
    "print(6 * 9)\n",
    "print(6.0 * 9.0)\n",
    "print(6.0 * 9)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbpresent": {
     "id": "52230075-9830-4471-ba15-ae4c2c04caab"
    }
   },
   "source": [
    "## Division <a id=\"division\"/>\n",
    "\n",
    "The result of dividing two numbers (any mix of integers and floats) is a floating point number (a number with a fractional part), even when the two divide evenly.  When they divide evenly the fractional part is zero, but still present."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbpresent": {
     "id": "85f77366-8870-4330-a962-6e4acd8d6985"
    }
   },
   "outputs": [],
   "source": [
    "print(5/2)\n",
    "print(6/2)\n",
    "print(type(5/2))\n",
    "print(type(4/2))\n",
    "\n",
    "print(5.0/2.0)\n",
    "print(6.0/2.0)\n",
    "print(type(5.0/2.0))\n",
    "print(type(6.0/2.0))\n",
    "\n",
    "print(5/2.0)\n",
    "print(6.0/2)\n",
    "print(type(5/2.0))\n",
    "print(type(6.0/2))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<p class=\"try_it\">\n",
    "<b>Try it yourself.</b>\n",
    "Create your own examples of addition, subtraction, multiplication, and division and print the results.\n",
    "</p>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Mixing integers and floating point numbers <a id=\"mixing_types\"/>\n",
    "\n",
    "Because floating point numbers can express a greater variety of values than integers, when the two types are combined the integer values are implicitly converted to floating point values so that information is not lost in the course of the computation.\n",
    "\n",
    "This is the reason that the presence of even a single floating point number in an arithmetic expression means the result will be a floating point number."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbpresent": {
     "id": "60b1edde-8417-41cf-a000-9b3c04f7d4f6"
    }
   },
   "source": [
    "# Exponentiation <a id=\"exponentiation\"></a>\n",
    "\n",
    "In Python, $a^{b}$ is written as <b>a&#42;&#42;b</b>.\n",
    "\n",
    "<div>\n",
    "If both <code>a</code> and <code>b</code> are integers, then <code>a&#42;&#42;b</code> is an integer.\n",
    "</div>\n",
    "<div>\n",
    "If either <code>a</code> or <code>b</code> is a float, then <code>a&#42;&#42;b</code> is an float.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(4**2)\n",
    "\n",
    "print(4.0**2)\n",
    "print(4**2.0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<p class=\"try_it\">\n",
    "<b>Try it yourself.</b>\n",
    "Create your own examples of exponentiation and print the results.\n",
    "</p>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"danger\"></div>\n",
    "A common mistake is using the symbol <code>^</code> for exponentiation.  Writing <code>a^b</code> performs an operation called the <b>bitwise exclusive OR</b> of <code>a</code> and <code>b</code>, which has <b>nothing</b> to do with exponentiation!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "3**3: 27\n",
      "3^3:  0\n"
     ]
    }
   ],
   "source": [
    "print('3**3:', 3**3)     # exponentiation\n",
    "print('3^3: ', 3^3)      # the bitwise exclusive OR of 3 and 3\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The bitwise exclusive OR <code>a^b</code> only makes sense if <code>a</code> and <code>b</code> are both integers; otherwise it results in an error:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "ename": "TypeError",
     "evalue": "unsupported operand type(s) for ^: 'float' and 'int'",
     "output_type": "error",
     "traceback": [
      "\u001b[0;31m---------------------------------------------------------------------------\u001b[0m",
      "\u001b[0;31mTypeError\u001b[0m                                 Traceback (most recent call last)",
      "\u001b[0;32m<ipython-input-3-2b73ca6a3d69>\u001b[0m in \u001b[0;36m<module>\u001b[0;34m\u001b[0m\n\u001b[0;32m----> 1\u001b[0;31m \u001b[0mprint\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m'3.0^3: '\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0;36m3.0\u001b[0m\u001b[0;34m^\u001b[0m\u001b[0;36m3\u001b[0m\u001b[0;34m)\u001b[0m  \u001b[0;31m# this results in an error!\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m",
      "\u001b[0;31mTypeError\u001b[0m: unsupported operand type(s) for ^: 'float' and 'int'"
     ]
    }
   ],
   "source": [
    "print('3.0^3: ', 3.0^3)  # this results in an error!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<p class=\"try_it\">\n",
    "<b>Try it yourself.</b>\n",
    "Create other examples to make sure you understand the difference between ** and ^.\n",
    "</p>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Operator precedence and grouping of operations <a id=\"precedence\"/>\n",
    "\n",
    "Python, like other programming languages, has an [implicit hierarchy that determines the order in which operations are applied](https://docs.python.org/3/reference/expressions.html#operator-precedence).\n",
    "\n",
    "For instance,\n",
    "```python\n",
    "9*8**2+1\n",
    "```\n",
    "is computed as if the operations were grouped as\n",
    "```python\n",
    "(9 * (8**2)) + 1.\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(9*8**2+1)\n",
    "print((9*(8**2)) + 1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[Arithmetic operations are applied in the following order:](https://docs.python.org/3/reference/expressions.html#operator-precedence)\n",
    "1. any sign change in an exponent first, then\n",
    "2. exponentiation, then\n",
    "3. any sign change, then\n",
    "4. multiplication, division, and modulo, then\n",
    "5. addition and subtraction.\n",
    "\n",
    "Thus, in evaluating <code>9&#42;8&#42;&#42;2+1</code>, \n",
    "1. we first evaluate <code>8**2</code>, \n",
    "2. then multiply by <code>9</code>, and \n",
    "3. then add <code>1</code>.\n",
    "\n",
    "In evaluating <code>9&#42;8&#42;&#42;-2+1</code> the order would be\n",
    "1. we first evaluate <code>-2</code>,\n",
    "1. then evaluate <code>8**-2</code>, \n",
    "2. then multiply by <code>9</code>, and \n",
    "3. then add <code>1</code>."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(9*8**-2+1)\n",
    "print(9 * 8**(-2) + 1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"danger\"></div> In a complex expression, use parentheses to make sure the grouping of operations is both correct and unambiguous to anyone reading your code (including you)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you're having to think hard about how an expression is being evaluated, or find that you need to carefully refer to the precedence rules, then you're doing it wrong!\n",
    "\n",
    "For instance: \n",
    "<pre>\n",
    "bad:       9+8**2*2\n",
    "better:    9 + ((8**2) * 2)\n",
    "overkill:  9 + ((8**(2)) * 2)\n",
    "</pre>\n",
    "and\n",
    "<pre> \n",
    "bad:       9+8**-2*2\n",
    "better:    9 + ((8**(-2)) * 2)\n",
    "</pre>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(9+8**2*2)\n",
    "print(9 + ((8**2) * 2))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(9+8**-2*2)\n",
    "print(9 + ((8**(-2)) * 2))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Parentheses are your friend\n",
    "\n",
    "For instance, what does the following do?\n",
    "\n",
    "```python\n",
    "2/3*2 + 1\n",
    "```\n",
    "Is it\n",
    "```python\n",
    "(2/3) * 2 + 1 = 7/3\n",
    "```\n",
    "or\n",
    "```python\n",
    "2/(3*2) + 1 = 4/3  ?\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(2/3*2 + 1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b>Answer.</b>\n",
    "<div class=\"voila\">\n",
    "It is equivalent to\n",
    "<pre>\n",
    "(2/3) * 2 + 1 = 7/3\n",
    "</pre>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Don't go overboard with parentheses\n",
    "\n",
    "In familar expressions it's ok to omit parentheses (assuming you have a passable knowledge of the precedence rules).  For instance,\n",
    "```python\n",
    "b**2 - 4 * a * c\n",
    "```\n",
    "is fine since we know that it's\n",
    "1. exponentation first, then\n",
    "2. multiplication, then\n",
    "3. subtraction.\n",
    "\n",
    "Something like \n",
    "```python\n",
    "(b**2) - (4 * (a * c))\n",
    "```\n",
    "is overkill."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Floating point arithmetic is not always exact! <a id=\"inexactness\"/>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"danger\"></div> Floating point operations do not always give exact answers:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "0.30000000000000004\n"
     ]
    }
   ],
   "source": [
    "print(0.1 + 0.2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here is an amusing example involving the precedence rules for arithmetic:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "1.0\n",
      "0.9999999999999999\n"
     ]
    }
   ],
   "source": [
    "print(49 *  1/49 )  # Computed as (49 * 1)/49.\n",
    "print(49 * (1/49))  # Computed as 49 * (1/49)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Don't be overly paranoid about the inexactness of floating-point arithmetic &ndash; most of the time it works quite well.  It's a matter of understanding what it can and cannot do, a point we will discuss in detail later.\n",
    "\n",
    "For now we will say that the inexactness stems from the fact that the set of floating point numbers is finite.  This means there are gaps where there are real numbers that cannot be exactly represented by a floating point number (1.1 is such a number).  This means exact values are being rounded, which results in small errors.\n",
    "\n",
    "We can guarantee that, in general, if there is an error in the answer of a floating-point operation (+, -, &#42;, /, &#42;&#42;, %, but <b>not</b> floating-point //), its magnitude is negligible compared to the magnitude of the correct answer (i.e., the <b>relative error</b> is small).  There are exceptions to this rule, however, that one can encounter in practice."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Exercises <a id=\"int_exercises\"></a>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"exercise\">\n",
    "<b>Rooting around.</b>\n",
    "Given a float $x$ and an integer $n$, how would you use exponentiation to find the $n$-th root of $x$ (i.e., $\\sqrt[n]{x}$).  You can try out your solution in the following cell.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b>Answer.</b>\n",
    "<div class=\"voila\">\n",
    "<p>\n",
    "You can use the fact that $\\sqrt[n]{x} = x^{1/n}$, or, in Python terms, <code>x**(1/n)</code>.\n",
    "</p>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<p class=\"exercise\">\n",
    "What is the value of the \n",
    "<a href=\"https://en.wikipedia.org/wiki/Mersenne_prime#List_of_known_Mersenne_primes\">15th Mersenne prime</a>, $2^{1279} - 1$ written out in all its glory?\n",
    "</p>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b>Answer.</b>\n",
    "<div class=\"voila\">\n",
    "<code>2**1279 - 1</code>, or\n",
    "<pre class=\"voila\">\n",
    "10407932194664399081925240327364085538\n",
    "61526224726670480531911235040360805967\n",
    "33602980122394417323241848424216139542\n",
    "81007791383566248323464908139906605677\n",
    "32076292412950938922034577318334966158\n",
    "35504729594205476898112116936771475484\n",
    "78866962501384438260291732348885311160\n",
    "82853841658502825560466622483189091880\n",
    "18470682222031405210266984354887329580\n",
    "28878050869736186900714720710555703168\n",
    "729087\n",
    "</pre>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<p class=\"exercise\">\n",
    "What is the difference between <code>**</code> and <code>^</code>?\n",
    "</p>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b>Answer.</b>\n",
    "<p class=\"voila\">\n",
    "Writing <code>a&#42;&#42;b</code> computes $a^{b}$, while writing <code>a^b</code> performs the bitwise exclusive OR of <code>a</code> and <code>b</code>.\n",
    "<p>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<p class=\"exercise\">\n",
    "Suppose $m$ and $n$ are positive integers.  What is the value of \n",
    "<code>n - (n // m)*m - (n % m)</code>?\n",
    "</p>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "m = 3\n",
    "n = -42\n",
    "n - m*(n // m) - (n % m)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b>Answer.</b>\n",
    "<p class=\"voila\">\n",
    "    Since <code>n</code> and <code>m</code> are integers, <code>n - m*(n // m) - (n % m)</code> is always zero.\n",
    "</p>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"exercise\">\n",
    "<ul>\n",
    "<li>Write code to compute $(3^{2})^{3} = 729$.</li>\n",
    "<li>Write code to compute $3^{2^{3}} = 6561$.</li>\n",
    "</ul>\n",
    "\n",
    "Use parentheses to clarify the calculations.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b>Answer.</b>\n",
    "<div class=\"voila\">\n",
    "To compute $(3^{2})^{3}$, use\n",
    "<pre>\n",
    "(3&#42;&#42;2)&#42;&#42;3\n",
    "</pre>\n",
    "To compute $3^{2^{3}}$ use\n",
    "<pre>\n",
    "3&#42;&#42;(2&#42;&#42;3)\n",
    "</pre>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h4>This notebook was brought to you by mathematicians:</h4>\n",
    "\n",
    "He was not a pleasing companion; like most mathematicians, he expected universal precision in everything said,\n",
    "or was forever denying or distinguishing upon trifles, to the disturbance of all conversation. <br/>\n",
    "&ndash; Benjamin Franklin"
   ]
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
