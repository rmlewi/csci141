{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"http://www.cs.wm.edu/~rml/images/wm_horizontal_single_line_full_color.png\">\n",
    "\n",
    "<h1 style=\"text-align:center;\">CSCI 141-01, Fall 2023 course notes</h1>\n",
    "\n",
    "<h1 style=\"text-align:center;\">Mathematical functions and modules</h1>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Contents\n",
    "* [The math module](#math)\n",
    "* [The <code class=\"kw\">import</code> statement](#import)\n",
    "* [The module search path](#sys.path)\n",
    "* [The random module](#random)\n",
    "* [Exercises](exercises)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# The <code>math</code> module <a id=\"math\"></a>\n",
    "\n",
    "Python includes most standard mathematical functions in its [<code>math</code> module](https://docs.python.org/3/library/math.html).\n",
    "\n",
    "Think of Python modules as libraries of utilities.  Modules are often assembled in larger collections of software called <b>packages</b>.  There is an enormous number of Python packages in existence.  [PyPI, the Python Package Index](https://pypi.org), is an enormous repository of publically accessible packages.\n",
    "\n",
    "If you need a module/package you don't have, you can install it by entering\n",
    "<pre>\n",
    "pip install package_name\n",
    "</pre>\n",
    "at the command line in a terminal window (Mac OS X) or a Terminal or PowerShell window (Windows).\n",
    "\n",
    "Later we will discuss how you can roll your own modules."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# The <code class=\"kw\">import</code> statement <a id=\"import\"></a>\n",
    "\n",
    "In order to access the functions in the <code>math</code> module, we need to <b>import</b> the module using an <code class=\"kw\">import</code> statement.  We should only import modules we need so we don't bloat our code with a lot of unnecessary software.\n",
    "\n",
    "Once we have imported the module, we can access the functions in the math library using the following syntax:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import math\n",
    "\n",
    "print(math.sqrt(9))     # The square root.\n",
    "print(math.log10(100))  # The base-10 logarithm.\n",
    "print(math.log(42))     # The natural logarithm.\n",
    "print(math.sin(3.14))   # The sine function."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The <code>math</code> module also contains some of our favorite constants (so we don't have to look them up elsewhere):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(math.pi)  # pi\n",
    "print(math.e)   # the base of the natural logarithm"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"try_it\">\n",
    "<b>Try it yourself.</b>\n",
    "Compute $\\cos(\\pi)$ and $\\log(e^{2})$.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Importing under an alias\n",
    "\n",
    "You can import a module under an alias of your choosing by specifying <code class=\"kw\">as</code>:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import math as m\n",
    "print(m.pi)\n",
    "print(m.e)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import math as bob\n",
    "print(bob.pi)\n",
    "print(bob.e)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Importing specific functions\n",
    "\n",
    "You can also import only specific functions or variables from a module:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from math import cos, pi\n",
    "\n",
    "# Notice that we do not need to say math.cos or math.pi.\n",
    "print(cos(pi))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Don't do this\n",
    "\n",
    "Here is a very bad practice that you will likely encounter in the wild.  The <code>*</code> means \"import everything from the module\":"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from math import *\n",
    "print(cos(e))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This is a Bad Idea because we now have to keep track of what comes from <code>math</code> and what comes from our own code &ndash; everything is all jumbled together and indistinguishable.  We may end up with collisions between the names of functions and variables in the module(s) and the names of functions and variables in our own code."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"danger\"></div>\n",
    "Don't use <code class=\"kw\">from</code> <code>module name import *</code>."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# The module search path  <a id=\"sys.path\"/>\n",
    "\n",
    "When you try to import a module, where does Python look for it?\n",
    "\n",
    "Per [the Python documentation](https://docs.python.org/3/tutorial/modules.html#the-module-search-path):\n",
    "<blockquote>\n",
    "<ul>    \n",
    "<li>The directory containing the input script (or the current directory when no file is specified).</li>\n",
    "<li>PYTHONPATH (a list of directory names, with the same syntax as the shell variable PATH).</li>\n",
    "<li>The installation-dependent default.</li>\n",
    "</ul>    \n",
    "</blockquote>\n",
    "\n",
    "You can view the directories (folders) wherein Python looks for modules using the [<code class=\"kw\">sys</code> module](https://docs.python.org/3/library/sys.html):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import sys\n",
    "print(sys.path)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Python searches these directories, in the order listed, until it finds a module with the specified name."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# The random module <a id=\"random\"></a>\n",
    "\n",
    "Another standard module containing useful mathematical functions is [<code>random</code>](https://docs.python.org/3/library/random.html).  It contains functions for generating random numbers.\n",
    "\n",
    "Technically these are <b>pseudo-random numbers</b>.  They are generated in a reproducible way (hence \"pseudo\") but they have the statistical properties of random numbers."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import random\n",
    "\n",
    "print(random.randint(0, 42))  # Get a random integer between 0 and 42, inclusive."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(random.random())  # Get a random number between 0 and 1, inclusive."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"try_it\">\n",
    "<b>Try it yourself.</b>\n",
    "Compute some random integers between 0 and 1000.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Obtaining reproducible results"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Observe that repeated calls to a random number generator with the same inputs yields different results:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(random.randint(0, 42))\n",
    "print(random.randint(0, 42))\n",
    "print(random.randint(0, 42))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"danger\"></div> \n",
    "If you want the random number generator to yield reproducible results, you can control its initialization using the <code>seed()</code> function."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The <b>seed</b> is a value used to initialize the random number algorithm (by default, the current time is used).  You are free to choose any number as the seed."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "random.seed(54)\n",
    "print(random.randint(0, 42))\n",
    "print(random.randint(0, 42))\n",
    "print(random.randint(0, 42))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "By setting the seed we can obtain reproducible behavior.  This is very important when generating random test cases &ndash; it is very annoying to generating a case that breaks a program, only to be unable to reproduce it!  (Trust me.)\n",
    "\n",
    "Let's re-run the preceding block of code using the same seed:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "random.seed(54)\n",
    "print(random.randint(0, 42))\n",
    "print(random.randint(0, 42))\n",
    "print(random.randint(0, 42))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Exercises  <a id=\"exercises\"/>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"exercise\">\n",
    "    What happens when you import the <code>this</code> module?\n",
    "</div>    "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Answer.**\n",
    "<div class=\"voila\">\n",
    "    You should see <a href=\"https://www.python.org/dev/peps/pep-0020\">the Zen of Python</a>.\n",
    "</div>    "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"exercise\">\n",
    "    What happens when you import the <code>antigravity</code> module?\n",
    "</div> "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "import antigravity"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Answer.**\n",
    "<div class=\"voila\">\n",
    "    You should be taken to an <a href=\"https://xkcd.com/2343/\">XKCD</a> cartoon.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### This notebook is brought to you by tolerance.\n",
    "\n",
    "<blockquote>\n",
    "What is tolerance? It is the consequence of humanity.  We are all formed of frailty and error;\n",
    "let us pardon reciprocally each other’s folly - that is the first law of nature. <br/><br/>\n",
    "\n",
    "Qu’est-ce que la tolérance? c’est l’apanage de l’humanité.  Nous sommes tous pétris de faiblesses et d’erreurs;\n",
    "pardonnons-nous réciproquement nos sottises, c’est la première loi de la nature.<br/>\n",
    "&ndash; Voltaire (1694-1778), Dictionnaire philosophique, “Tolérance” (1764)\n",
    "</blockquote>"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
