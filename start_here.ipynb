{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "nbpresent": {
     "id": "abaeef91-5553-4553-8ebf-2c22a87d7137"
    }
   },
   "source": [
    "<img src=\"http://www.cs.wm.edu/~rml/images/wm_horizontal_single_line_full_color.png\">\n",
    "\n",
    "<h1 style=\"text-align:center; color: #115750;\">CSCI 141-01, Fall 2023 course notes</h1>\n",
    "<h1 style=\"text-align:center;\">Getting started</h1>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Contents\n",
    "\n",
    "* [About this course](#About-this-course)\n",
    "* [About Python](#About-Python)\n",
    "* [Don't panic!](#Don't-panic!)\n",
    "* [&iexcl;Cuidado, llamas!](#%C2%A1Cuidado!--%C2%A1llamas!--)\n",
    "* [About the Jupyter notebooks](#About-the-Jupyter-notebooks)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "jp-MarkdownHeadingCollapsed": true
   },
   "source": [
    "# About this course\n",
    "\n",
    "It is important to note that this course is named \"Computational Problem Solving\" rather than \"Introduction to Programming\".  By computational problem solving we mean the process of starting with a problem description, devising a solution, and decomposing the solution into smaller and smaller subtasks until the subtasks are simple enough to be performed with the basic operations of a computer language.\n",
    "\n",
    "So keep this in mind:\n",
    "<blockquote>\n",
    "First, solve the problem. Then, write the code. <br/>\n",
    "&ndash; John Johnson\n",
    "</blockquote>\n",
    "This is <b>very</b> important.  Solving the problem is the \"what\"; programming is the \"how\".  A common mistake is to try to do both at the same time &ndash; but how can you implement a solution before you know what it is?  Hence the adage:\n",
    "<blockquote>\n",
    "Weeks of programming can save you hours of planning.\n",
    "</blockquote>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# About Python <a id=\"python\"/>\n",
    "\n",
    "This course will use the Python programming language, which dates to the late 1980s.  It was originally developed by Guido van Rossum at the Centrum Wiskunde &amp; Informatica in the Netherlands.  The name comes not from the large and scary snake, but from a 1970s English comedy troupe, Monty Python's Flying Circus.  \n",
    "\n",
    "van Rossum continued to oversee the development of Python as its Benevolent Dictator for Life (BDFL), but stepped down from this role in July 2018 following the controversy over the introduction of a new language feature called the *assignment expression* in Python 3.8.\n",
    "\n",
    "The popularity of Python in data science has placed Python at the top of the Institute of Electronic and Electrical Engineers (IEEE) [most recent list of top programming languages](https://spectrum.ieee.org/the-top-programming-languages-2023)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Don't panic!\n",
    "\n",
    "If you find yourself struggling at first, don't worry!  This subject is objectively difficult.\n",
    "<img src=\"http://www.cs.wm.edu/~rml/images/dont_panic.png\" style=\"float: right; padding: 8px; width: 200px; height: auto;\"/>\n",
    "\n",
    "You should keep the following in mind, especially if you are new to programming.  As my colleague Prof. Kemper points out,\n",
    "<blockquote>\n",
    "    <b>Programming is a skill, not a gift.</b>\n",
    "</blockquote>\n",
    "That means you can learn to program with work and effort.\n",
    "\n",
    "As you work your way through these notes, you will see a lot of material.  Don't feel you need to assimilate everything on first (or second, or third, or &hellip;) reading.  As you find your own programming voice you will find yourself drawing on a particular subset of the language again and again, and it will become familiar to you."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# &iexcl;Cuidado!  &iexcl;llamas!  <a id=\"llamas\"/>\n",
    "\n",
    "<div class=\"danger\"></div> \n",
    "We will use the metric road sign for dangerous curves (also known as the \n",
    "<a href=\"https://en.wikipedia.org/wiki/Bourbaki_dangerous_bend_symbol\">Bourbaki dangerous bend symbol</a>) to indicate important information such as common pitfalls and mistakes.  Two such signs will be used to indicate especially important things.\n",
    "\n",
    "We will use the exploding head emoticon, &#x1f92f;, to indicate more advanced topics that can be skipped on a first reading.\n",
    "\n",
    "We will use the sceam emoticon, 😱 , to indicate particularly frightening topics.\n",
    "\n",
    "Discussions of things that lead to programming errors are marked with a 🐞."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# About the Jupyter notebooks\n",
    "\n",
    "This section assumed you have installed the file <code>custom.css</code> that is distributed as part of the notes.  See the Gitlab repository for more details.\n",
    "\n",
    "Unvisited hyperlinks appear in green; visted hyperlinks appear in gold.\n",
    "\n",
    "Cells with a grey background contain Python code.  The code in the cell can be executed by clicking on the cell and then entering \"shift + enter\" (i.e., hold down the shift key and then press the enter/return key).  Executing a cell advances you one cell."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<p class=\"try_it\">\n",
    "<b>Try it yourself.</b> Click on the following cell and execute it.\n",
    "</p>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "print('Yuck! go wash your hands!')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can add cells of your own using the Edit tab on the toolbar above.  You can insert cells above or below the currently active cell (text or Python code).  By default the new cell will be for Python code, but you can change it to text by choosing the Markdown option of the Code/Markdown pulldown menu on the toolbar.\n",
    "\n",
    "There are also a lot of helpful [keyboard shortcuts](https://medium.com/towards-artificial-intelligence/jupyter-notebook-keyboard-shortcuts-for-beginners-5eef2cb14ce8) that will make you more productive."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When you see <b>Answer</b> followed by an empty box, there should be a hidden text within the box.  You can see the text by hovering over the box with the mouse.  For example,\n",
    "\n",
    "<b>Answer</b> <div class=\"voila\">Is your dog getting enough cheese? (Though this is a question, not an answer.)</div>\n",
    "\n",
    "and\n",
    "\n",
    "<b>Answer</b> <br/>\n",
    "<div class=\"voila\">\n",
    "Two wrongs do not make a right,\n",
    "<br/>\n",
    "but three lefts do.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## What to do if a notebook freezes\n",
    "\n",
    "Sometimes a notebook will fail to respond.  If that happens, go to the toolbar pulldown menus at the top and choose <code>Kernel => Restart &amp; Clear Outputs of All Cells</code>.  This will put the notebook in its original state.  You may need to re-execute cells to pick up where you got stuck."
   ]
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
