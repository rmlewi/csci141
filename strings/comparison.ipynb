{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"http://www.cs.wm.edu/~rml/images/wm_horizontal_single_line_full_color.png\">\n",
    "\n",
    "<h1 style=\"text-align:center;\">CSCI 141, Fall 2023</h1>\n",
    "<h1 style=\"text-align:center;\">Logical operations involving strings</h1>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Contents\n",
    "* [The <code class=\"kw\">in</code> statement](#in)\n",
    "* [String comparison](#comparison)\n",
    "* [The ASCII character sequence](#ascii)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# The <code class=\"kw\">in</code> statement\n",
    "\n",
    "The <code>in</code> statement allows us to quickly check if a character appears in a string:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "True\n",
      "False\n"
     ]
    }
   ],
   "source": [
    "print('s' in 'cats')\n",
    "print('s' in 'cat')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "word = 'ghost'\n",
    "if ('g' in word):\n",
    "    print('boo!')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# String comparison <a id=\"comparison\"/>\n",
    "\n",
    "String comparison is useful when sorting strings in alphabetical order.\n",
    "\n",
    "String comparison is a bit complicated because of the way characters are ordered:\n",
    "* upper case letters come before lower case;\n",
    "* digits before upper case letters;\n",
    "* symbols such as &lt;, @, ^, etc., are scattered in between the digits and letters.\n",
    "\n",
    "The exact ordering is determined by the [ASCII character sequence](#ascii)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Upper case letters come before lower case.\n",
    "print('A' < 'a')\n",
    "print('Z' < 'a')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Digits come before upper case letters.\n",
    "print('1' < 'A')\n",
    "print('9' < 'A')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Symbols are all over the place.\n",
    "print('1' < '@')\n",
    "print('@' < 'A')\n",
    "print('@' < '!')\n",
    "print('Z' < '^')\n",
    "print('{' < 'a')\n",
    "print('{' < '}')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If two strings are of equal length, then their relative order is determined by the relative order of the characters at the point where the strings diverge."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# 'car' < 'cat' since 'r' < 't'\n",
    "print('car' < 'cat')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here the strings diverge from the start:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('hello, world' > 'aardvark')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('h' > 'aardvark')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If one string is a substring of another string, then the longer string is considered to be \"larger\"."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('hello, world' > 'hello')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('horseness' > 'horseness is simply horseness')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"try_it\">\n",
    "<b>Try it yourself.</b>\n",
    "What will be printed by the following statements?\n",
    "<code>\n",
    "print('man' > 'manbearpig')\n",
    "print('Death Valley' > 'aardvark')\n",
    "print('W&M' > 'UVa')\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "String equality works like you would expect:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s = 'dolphin'\n",
    "t = 'porpoise'\n",
    "if (s != t):\n",
    "    print('A dolphin and a porpoise are not the same.  They exist for different porpoises.')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# The ASCII character sequence <a id=\"ascii\"/>\n",
    "\n",
    "In computer science the default ordering of basic Latin characters is almost always the ASCII character sequence.  ASCII stands for the [American Standard Code for Information Interchange](http://www.ascii-code.com/).\n",
    "\n",
    "There are 128 characters (characters 0 - 127) that are the core of the ASCII sequence.  ASCII characters require one byte (8 bits) to represent.  Since each bit is either 0 or 1, there are $2^{8} = 256$ possible combinations.  However, ASCII uses only 7 bits to represent characters (making $2^{7} = 128$ possible combinations) and uses the remaining bit as a [parity bit](https://en.wikipedia.org/wiki/Parity_bit) to detect transmission errors."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## ASCII characters 0-31 🤯\n",
    "\n",
    "The first 32 ASCII characters are non-printable control characters used to communicate with devices such as printers.  These include characters such as 'carriage return' (CR) telling a printer to start a new line.  We have encountered some of these before when we talked about representing special characters such as tab via escaping ('\\t').\n",
    "\n",
    "<pre>\n",
    "      Symbol  Description                   Symbol  Description\n",
    "0     NUL     Null char               16    DLE     Data Line Escape\n",
    "1     SOH     Start of Heading        17    DC1     Device Control 1 (oft. XON)\n",
    "2     STX     Start of Text           18    DC2     Device Control 2\n",
    "3     ETX     End of Text             19    DC3     Device Control 3 (oft. XOFF)\n",
    "4     EOT     End of Transmission     20    DC4     Device Control 4\n",
    "5     ENQ     Enquiry                 21    NAK     Negative Acknowledgement\n",
    "6     ACK     Acknowledgment          22    SYN     Synchronous Idle\n",
    "7     BEL     Bell                    23    ETB     End of Transmit Block\n",
    "8     BS      Back Space              24    CAN     Cancel\n",
    "9     HT      Horizontal Tab          25    EM      End of Medium\n",
    "10    LF      Line Feed               26    SUB     Substitute\n",
    "11    VT      Vertical Tab            27    ESC     Escape\n",
    "12    FF      Form Feed               28    FS      File Separator\n",
    "13    CR      Carriage Return         29    GS      Group Separator\n",
    "14    SO      Shift Out / X-On        30    RS      Record Separator\n",
    "15    SI      Shift In / X-Off        31    US      Unit Separator\n",
    "</pre>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"try_it\">\n",
    "<b>Try it yourself.</b>\n",
    "\n",
    "Fire up a Python shell in a terminal window (<b>not in a Jupyter notebook</b>) and try <code>print('\\a')</code> to see if your machine will let  you ring the audible or visible bell.  The escaped 'a' is the ASCII character BEL and stands for 'alert' or 'alarm'.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## ASCII characters 32-127\n",
    "\n",
    "Characters 32 to 127 are the printable characters (those that make sense to humans).  Observe that numbers precede all letters, and capital letters precede lower-case letters.\n",
    "\n",
    "<pre>\n",
    "       Symbol  Description                    Symbol  Description                    Symbol  Description\n",
    "32             Space                   64     @       At sign                 96     `       Grave accent\n",
    "33     !       Exclamation mark        65     A       Uppercase A             97     a       Lowercase a\n",
    "34     \"       Double quote            66     B       Uppercase B             98     b       Lowercase b\n",
    "35     #       Number                  67     C       Uppercase C             99     c       Lowercase c\n",
    "36     $       Dollar                  68     D       Uppercase D             100    d       Lowercase d\n",
    "37     %       Percent sign            69     E       Uppercase E             101    e       Lowercase e\n",
    "38     &       Ampersand               70     F       Uppercase F             102    f       Lowercase f\n",
    "39     '       Single quote            71     G       Uppercase G             103    g       Lowercase g\n",
    "40     (       Left parenthesis        72     H       Uppercase H             104    h       Lowercase h\n",
    "41     )       Right parenthesis       73     I       Uppercase I             105    i       Lowercase i\n",
    "42     *       Asterisk                74     J       Uppercase J             106    j       Lowercase j\n",
    "43     +       Plus                    75     K       Uppercase K             107    k       Lowercase k\n",
    "44     ,       Comma                   76     L       Uppercase L             108    l       Lowercase l\n",
    "45     -       Hyphen                  77     M       Uppercase M             109    m       Lowercase m\n",
    "46     .       Period                  78     N       Uppercase N             110    n       Lowercase n\n",
    "47     /       Slash                   79     O       Uppercase O             111    o       Lowercase o\n",
    "48     0       Zero                    80     P       Uppercase P             112    p       Lowercase p\n",
    "49     1       One                     81     Q       Uppercase Q             113    q       Lowercase q\n",
    "50     2       Two                     82     R       Uppercase R             114    r       Lowercase r\n",
    "51     3       Three                   83     S       Uppercase S             115    s       Lowercase s\n",
    "52     4       Four                    84     T       Uppercase T             116    t       Lowercase t\n",
    "53     5       Five                    85     U       Uppercase U             117    u       Lowercase u\n",
    "54     6       Six                     86     V       Uppercase V             118    v       Lowercase v\n",
    "55     7       Seven                   87     W       Uppercase W             119    w       Lowercase w\n",
    "56     8       Eight                   88     X       Uppercase X             120    x       Lowercase x\n",
    "57     9       Nine                    89     Y       Uppercase Y             121    y       Lowercase y\n",
    "58     :       Colon                   90     Z       Uppercase Z             122    z       Lowercase z\n",
    "59     ;       Semicolon               91     [       Left  bracket           123    {       Left brace\n",
    "60     <       Less than               92     \\       Backslash               124    |       Vertical bar\n",
    "61     =       Equals                  93     ]       Right bracket           125    }       Right brace\n",
    "62     >       Greater than            94     ^       Caret                   126    ~       Tilde\n",
    "63     ?       Question mark           95     _       Underscore              127            Delete\n",
    "</pre>\n",
    "\n",
    "From this table you can see why '@' < '^', since '@' is character 64 while '^' is character 94:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('@' < '^')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Python has two built-in functions, [<code>ord()</code>](https://docs.python.org/3/library/functions.html#ord) and [<code>chr()</code>](https://docs.python.org/3/library/functions.html#chr), which map characters to their ASCII sequence number and vice versa:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "64 94\n"
     ]
    }
   ],
   "source": [
    "print(ord('@'), ord('^'))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "@ ^\n"
     ]
    }
   ],
   "source": [
    "print(chr(64), chr(94))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Exercises <a id=\"exercises\"/>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "<div class=\"exercise\">\n",
    "<b>Exercise.</b>\n",
    "What will be printed by the following code (try thinking before experimenting):\n",
    "<code>\n",
    "print('cat' < 'dog')\n",
    "print('thing1' < 'thing2')\n",
    "print('Texas' > 'California')\n",
    "print('kale' > 'bacon')\n",
    "</code>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b>Answer.</b>\n",
    "<div class=\"voila\">\n",
    "<pre>\n",
    "True\n",
    "True\n",
    "False\n",
    "True\n",
    "False\n",
    "</pre>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "<div class=\"exercise\">\n",
    "<b>Exercise.</b>\n",
    "Write a function <code>is_integer(s)</code> that takes a string <code>s</code> and returns <code class=\"kw\">True</code> if <code>s</code> represents an integer and <code class=\"kw\">False</code> otherwise.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b>Answer.</b>\n",
    "<div class=\"voila\">\n",
    "We use the fact that an integer can only consist of digits between 0 and 9, and 0 and 9 are the first and last digits, respectively.\n",
    "<pre>\n",
    "def is_integer(s):\n",
    "    for c in s:\n",
    "        # bail out immediately if we encounter a non-digit.\n",
    "        if (c < '0' or c > '9'):\n",
    "            return False\n",
    "    return True\n",
    "</pre>\n",
    "\n",
    "In practice, you should use the <code>is_numeric()</code> method of the string class.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"exercise\">\n",
    "<b>Exercise.</b>\n",
    "The following is part of the output from using the Linux <code>ls</code> to list the contents of a directory.  Can you explain why the file names are ordered as they are?\n",
    "<pre>\n",
    "Lec1Practice.ipynb\n",
    "Makefile\n",
    "README\n",
    "Untitled.ipynb\n",
    "Untitled1.ipynb\n",
    "Untitled2.ipynb\n",
    "bits_and_bytes.ipynb\n",
    "bkp\n",
    "checkerboard.png\n",
    "classes_intro.ipynb\n",
    "classes_overloading.ipynb\n",
    "classes_polymorphism.ipynb\n",
    "</pre>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b>Answer.</b>\n",
    "<div class=\"voila\">\n",
    "The <code>ls</code> command is listing the file names alphabetically in the same way Python compares strings, namely, according to the ASCII character sequence.  Thus, file names beginning with capital letters come first.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h4>This notebook was brought to you by remembrance:</h4>\n",
    "\n",
    "The only thing that will be remembered about my enemies after they're dead is the nasty things I've said about them. <br/>\n",
    "&ndash; Camille Paglia"
   ]
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
