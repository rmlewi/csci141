{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"http://www.cs.wm.edu/~rml/images/wm_horizontal_single_line_full_color.png\">\n",
    "\n",
    "<h1 style=\"text-align:center;\">CSCI 141-01, Fall 2023 course notes</h1>\n",
    "<h1 style=\"text-align:center;\">String formatting II</h1>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Contents\n",
    "\n",
    "* [String formatting](#string_formatting)\n",
    "* [The string format method](#format_method)\n",
    "    * [Indicating outputs by position](#positional_arguments)\n",
    "    * [Indicating outputs by name](#named_arguments)\n",
    "* [% formatting](#%_formatting)    \n",
    "* [String templates](#string_templates\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Perhaps we don't want so many digits to the right of the decimal point, or perhaps we'd like the same number of digits to be printed for both numbers.  What do we do?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Other ways to control the format of strings <a id=\"string_formatting\"></a>\n",
    "\n",
    "Here we take a quick look at older ways to format strings other than f-strings:\n",
    "1. the <code>format()</code> method of the string class (since Python 3.0)\n",
    "2. % formatting (since the beginning)\n",
    "3. string templates (since Python 2.4)\n",
    "\n",
    "You may encounter these in older code."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# The str.format() method <a id=\"format_method\"/>\n",
    "\n",
    "The string method <b>format()</b> was in vogue from [Python 3.0](https://docs.python.org/release/3.0/whatsnew/2.6.html#pep-3101) to Python 3.6.\n",
    "\n",
    "It is similar to f-strings, but is more verbose and error-prone.  It requires you do more work to map the things you want to format to where they occur in the string.\n",
    "\n",
    "Fortunately the format codes are the same as for f-strings, so there is not a whole other set of codes to learn."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Indicating outputs by position <a id=\"positional_arguments\"/>\n",
    "\n",
    "Let's start with a really simple case. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "scrolled": true
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "The Goops they lick their fingers,\n",
      "And the Goops they lick their knives,\n",
      "They spill their broth on the tablecloth—\n",
      "Oh, they lead disgusting lives!\n"
     ]
    }
   ],
   "source": [
    "print('The Goops they lick their {0},'.format('fingers'))\n",
    "print('And the Goops they lick their {0},'.format('knives'))\n",
    "print('They spill their {0} on the {1}—'.format('broth', 'tablecloth'))\n",
    "print('Oh, they lead disgusting lives!')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Per the [Python documentation for format strings](https://docs.python.org/3/library/string.html#format-string-syntax).\n",
    "<blockquote>\n",
    "Format strings contain \"replacement fields\" surrounded by curly braces {}. Anything that is not contained in braces is considered literal text, which is copied unchanged to the output.\n",
    "</blockquote>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the preceding examples the numbers inside the {} indicate the positional argument of <code>format()</code> that should be printed at that part of the string.  Argument 0 is the first argument, argument 1 is the second, and so on.\n",
    "\n",
    "Thus, in \n",
    "```python\n",
    "print('They spill their {0} on the {1}—'.format('broth', 'tablecloth'))\n",
    "```\n",
    "we replace {0} with *broth* and {1} with *tablecloth*.\n",
    "\n",
    "Notice that the following is also valid, though it ain't poetical."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('They spill their {1} on the {0}—'.format('broth', 'tablecloth'))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here are some more examples."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('{0}, {1}, {2}'.format('f', 'o', 'o'))\n",
    "print('{0}{1}{2}'.format('f', 'o', 'o'))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# We can use an argument more than once.\n",
    "print('{0},{1},{1}'.format('f', 'o'))\n",
    "print('h{0}-p{0}'.format('ocus'))\n",
    "s = 'h{0}-p{0}'.format('ocus')\n",
    "print(type(s))\n",
    "print(s)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"danger\"></div>  A common error is incorrectly mapping the things being printed to where they appear in the string."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For instance, consider"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('{0:c}{1:c}{2:c}'.format(38, 77, 87))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Of course, what we meant was"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('{2:c}{0:c}{1:c}'.format(38, 77, 87))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As code changes, errors are prone to creep in if you indicate outputs by position.  For instance, suppose we drop the output <code>b</code> from the statement"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = 42\n",
    "b = 54\n",
    "c = 31\n",
    "print('{0:d}, {1:d}, {2:d}'.format(a, b, c))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "but fail to update the string formatting correctly:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = 42\n",
    "b = 54\n",
    "c = 31\n",
    "print('{0:d}, {2:d}'.format(a, c))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As you can see, adding or removing an output when using the <code>format()</code> method involves making more changes to the code than if we used an f-string.  This is why f-strings are to be preferred."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Indicating outputs by name <a id=\"named_arguments\"/>\n",
    "\n",
    "We can also indicate outputs by name.  Here you seen the close kinship with f-strings."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('W&M is located at {latitude}, {longitude}.'.\n",
    "      format(latitude='37 16 12.42 N', longitude='76 42 42.12 W'))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "latitude  = '37 16 12.42 N'\n",
    "longitude = '76 42 42.12 W'\n",
    "print('W&M is located at {0}, {lat}, {lon}.'.\n",
    "      format('hello', lat=latitude, lon=longitude))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# % formatting 🤯 <a id=\"%_formatting\"></a>\n",
    "\n",
    "This is the oldest way of formatting strings in Python.  It is derived from the format specifications of the <code>printf()</code> function in the C programming language.  We include it here since you may encounter it in older code, particularly code that was originally written in Python 2.\n",
    "\n",
    "The format specification codes are also the same as for f-strings (in fact, [they are inherited from C](https://en.wikipedia.org/wiki/Printf_format_string))."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fee = 42\n",
    "fie = 3.14\n",
    "foe = 2.78\n",
    "fum = 'boo!'\n",
    "\n",
    "print('%d %f %e %s' % (fee, fie, foe, fum))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s = '%d %f %e %s' % (fee, fie, foe, fum)\n",
    "print(s)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('%12d %8.5f %7.2e %9s' % (fee, fie, foe, fum))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "One place you will encounter the % style of formatting is in Python's [logging module](https://docs.python.org/3/library/logging.html)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# String templates <a id=\"string_templates\"></a>\n",
    "\n",
    "I've never encountered string templates or used them myself.  Since I know little about them beyond their existence,  I'll keep quiet on the subject.  You can read about them <a href=\"https://www.python.org/dev/peps/pep-0292/\">here</a>."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h4>This notebook was brought to you by our best and brightest:</h4>\n",
    "\n",
    "On two occasions I have been asked &#91;by members of Parliament&#93;, 'Pray, Mr. Babbage, if you put into the machine wrong figures, will the right answers come out?'  I am not able rightly to apprehend the kind of confusion of ideas that could provoke such a question. <br/><br/>\n",
    "&ndash; Charles Babbage, discussing his <a href=\"https://www.computerhistory.org/babbage/engines/\">Difference Engine</a>"
   ]
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
