{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"http://www.cs.wm.edu/~rml/images/wm_horizontal_single_line_full_color.png\">\n",
    "\n",
    "<h1 style=\"text-align:center;\">CSCI 141, Fall 2023 course notes</h1>\n",
    "<h1 style=\"text-align:center;\">\n",
    "Substrings and slices\n",
    "</h1>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Contents\n",
    "* [String indexing](#indexing)\n",
    "* [Strings are immutable](#immutable)\n",
    "* [Slicing](#slicing)\n",
    "* [Negative indices](#negative_indices)\n",
    "* [A useful Python idiom for the ends of strings](#idiom)\n",
    "* [Slices with strides](#strides)\n",
    "* [A useful Python idiom for reversing strings](#reversing)\n",
    "* [Exercises](#exercises)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Strings as sequences of characters <a id=\"iteration\"/>\n",
    "\n",
    "In this lesson we will look at how to access individual characters in a string, as well as substrings of the string."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# String indexing <a id=\"indexing\"/>\n",
    "\n",
    "Suppose are given a string and we wish to check whether the first letter is \"A\" or \"a\".  How do we do this?\n",
    "\n",
    "We access an individual character in a string by the character's <b>index</b>.  The first character has index 0, the second character index 1, and so on."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s = 'hello, world!'\n",
    "print(s[0])\n",
    "print(s[1])\n",
    "print(s[2])\n",
    "print(s[3])\n",
    "print(s[4])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"try_it\">\n",
    "<b>Try it yourself.</b>\n",
    "    Print the characters at indices 5, 6, and 7 for the preceding string <code>s</code>.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Why does the indexing start with 0?!\n",
    "\n",
    "The indexing starts with 0 since the first character is 0 characters past the beginning of the string.  \n",
    "\n",
    "Indexing starting from 0 may seem odd at first, but it is very common in computer science, and we will see it again in other parts of Python."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"danger\"></div> Until you get used to indexing from zero you will likely make mistakes because of it.  After you get used to indexing from zero you will still make mistakes because of it."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you can avoid working with explicit indexing, do so &ndash; it is a very error-prone activity. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "What happens if we try an index past the end of the string?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "print(s[42])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"try_it\">\n",
    "<b>Try it yourself.</b>\n",
    "Generate your own instance of <code>IndexError</code>.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Strings are immutable <a id=\"immutable\"/>\n",
    "\n",
    "Now that we can access individual characters in a string, let's try modifying a character.\n",
    "\n",
    "What happens if we try to change a character in a string?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s = 'puppy potpie'\n",
    "print(s)\n",
    "\n",
    "# Can we change the string to read 'puppy_potpie'?\n",
    "s[5] = '_'\n",
    "print(s)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"danger\"></div>\n",
    "In Python, strings are <b>immutable</b> objects &ndash; they cannot be altered once they are created."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "While we cannot change a string, we have seen that there are [string methods](methods.ipynb) that return modified copies of a string."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"danger\"></div>\n",
    "String methods do not modify the string they act on (since strings are immutable).  They return modified copies of the string."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"try_it\">\n",
    "<b>Try it yourself.</b>\n",
    "Try changing a character of a string of your own choosing.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Nothing about the immutability of strings prevents us from using the characters in a string on the right-hand side of assignments:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s = 'puppy potpie'\n",
    "c = s[0]\n",
    "print(c)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# How to refer to characters in a string\n",
    "\n",
    "Since <code>s[0]</code> is the beginning character of the string <code>s</code>, should we call <code>s[0]</code> the zeroth character or the first character in the string?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"danger\"></div> \n",
    "As we are about to see, we should call <code>s[0]</code> the first character in the string, but remember that it is at index 0.  So we will use the standard ordinals (first, second, third, &hellip;) to refer to their ordinal location, but the index (0, 1, 2, 3, &hellip;) to refer to the index."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Ranges of indices (slicing) <a id=\"slicing\"/>\n",
    "\n",
    "Now that we have seen how to access individual characters in a string, how can we access substrings of a string?\n",
    "\n",
    "The technique for accessing substrings (ranges of characters) is called <b>slicing</b>.  It is most easily explained by example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s = 'puppy pot pie'\n",
    "\n",
    "# Print the substring starting with s[0] and ending with s[2].\n",
    "print(s[0:3])\n",
    "\n",
    "# Print the substring starting with s[4] and ending with s[8].\n",
    "print(s[4:9])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"danger\"></div> \n",
    "Note the peculiar behavior: the slice <code>s[m:n]</code> starts at <code>s[m]</code> and ends at <code>s[n-1]</code>.  It does <b>not</b> include <code>s[n]</code>!! <br/>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A common error is to expect <code>s[n]</code> to be part of the slice (this is an example of what is called an [<b>off-by-one error</b>, or OBOE](https://en.wikipedia.org/wiki/Off-by-one_error))."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There is a method to this madness.  The slice notation is a curious hybrid of string indexing and counting characters.  Think of <code>s[m:n]</code> as meaning the substring that \n",
    "<ul>\n",
    "    <li>starts at index m (character <code>s[m]</code>) and</li>\n",
    "    <li>ends at the n-th character in the string.  Since the indexing begins at 0, the n-th character in the string is <code>s[n-1]</code>.</li>\n",
    "</ul>\n",
    "So, in the two preceding examples, <code>s[0:3]</code> is the first three characters of the string, while <code>s[4:9]</code> starts at index 4 (the fifth character in the string) and ends at the ninth character."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"try_it\">\n",
    "<b>Try it yourself.</b>\n",
    "Use slicing to print the substring \"hello\" from the string <code>s = 'hello, world!'</code>.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b>Answer.</b>\n",
    "<div class=\"voila\">\n",
    "<code>print(s[0:5])</code>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If we omit the upper limit of the range, we will get all the characters from the starting index to the end of the string:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s = 'puppy pot pie'\n",
    "print(s[1:])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Substrings are also immutable:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s = 'puppy'\n",
    "s[0:2] = 'gu'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Negative indices <a id=\"negative_indices\"/>\n",
    "\n",
    "What do you think will happen if we try a negative index?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s = 'hello, world!'\n",
    "print(s[-1])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This prints the last character!  Why?!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When it comes to negative indices, Python treats the string as if it were circular, with the characters appearing clockwise in the order they appear left-to-right in the string, with the beginning character at twelve o'clock.  For example, the string <tt>hello, world!</tt> is ordered as follows:\n",
    "<pre>\n",
    "     h\n",
    "   !   e\n",
    "   d   l\n",
    "   l   l\n",
    "   r   o\n",
    "   o   ,\n",
    "   w    \n",
    "</pre>\n",
    "For negative indices, Python counts counterclockwise around this diagram."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"danger\"></div> Negative indexing is easy to keep straight if you observe that if <code>s</code> is a string and <code>k > 0</code>,<br/> then <code>s[-k]</code> is the same character as <code>s[len(s)-k]</code>."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Negative indexing allows us to dispense with the need to use <code>len(s)</code>:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s = 'hello, world!'\n",
    "print(s[-6:-1])\n",
    "print(s[len(s)-6:len(s)-1])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s = 'hello, world!'\n",
    "print(s[-4:])\n",
    "print(s[len(s)-4:])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The slicing rules seem more complicated if you mix negative and nonnegative numbers, but as you long as you remember the indexing rule we just discussed it is straightforward.\n",
    "\n",
    "For example, <code>s[3:-2]</code> starts at index 3 and ends three characters from the end of the string:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s = 'hello, world!'\n",
    "print(s[3:-2])\n",
    "print(s[3:len(s)-2])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "On the other hand, <code>s[-2:3]</code> is empty:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s = 'hello, world!'\n",
    "print(s[-2:3])\n",
    "print(s[len(s)-2:3])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# A useful Python idiom for the ends of strings <a id=\"idiom\"></a>\n",
    "\n",
    "At first glance you might think that negative indices are only a curiosity.  However, they are very useful if you want to refer to the end of a string.\n",
    "\n",
    "Suppose we want to look at the last two characters in a string.  One approach would be to be to compute the length of the string, subtract two, and print the resulting range:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s = 'hello, world!'\n",
    "print(s[len(s)-2:])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can write this much more succinctly using negative indices:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s = 'hello, world!'\n",
    "print(s[-2:])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In general, if <code>s</code> is a string and <code>k > 0</code>, then <code>s[-k:]</code> refers to the last <code>k</code> characters in the string.  That is, <code>s[-k:]</code> is <code>s[len(s)-k:]</code>."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"try_it\">\n",
    "<b>Try it yourself.</b>\n",
    "Print the last 7 characters from the string <code>s</code>.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Slices with strides <a id=\"strides\"/>\n",
    "\n",
    "We can specify a stride for a range of indices.  For instance, suppose we want to skip the even indices in s:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s = 'hello, world!'\n",
    "print(s[1::2])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The general syntax is <code>m:n:k</code>.  The limits <code>m</code> and <code>n</code> act as before, while <code>k</code> is the <b>stride</b> (the amount by which we increment the indices as we move through the slice.\n",
    "\n",
    "In the preceding, we omitted the upper limit, so <code>s[0::2]</code> means \"the characters starting with index 0 and going to the end by twos\"."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s = 'hello, world!'\n",
    "\n",
    "# The use of len() is not necessary.\n",
    "print(s[0:len(s):2])\n",
    "print(s[::2])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s = 'hello, world!'\n",
    "print(s[1:5:2])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# A useful Python idiom for reversing strings <a id=\"reversing\"></a>\n",
    "\n",
    "Strides can be negative.  This makes it easy to reverse a string in Python.  The following says to walk over the string with a stride of -1 (i.e., backwards)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s = 'hello, world!'\n",
    "print(s[::-1])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"try_it\">\n",
    "<b>Try it yourself.</b>\n",
    "Print the reverse of the string in the following code cell.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s = 'Never count your chickens before they rip your lips off.'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Exercises <a id=\"exercises\"/>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"exercise\">\n",
    "<b>Exercise.</b>\n",
    "How can we use <code>len()</code> and a <code>for</code>-loop to print out all the characters of a string named <code>s</code> on a single line, separated by spaces, so\n",
    "<code>\n",
    "s = \"Hello, world!\"\n",
    "</code>\n",
    "would appear as\n",
    "<code>\n",
    "H e l l o ,   w o r l d ! \n",
    "</code>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Working with time.\n",
    "\n",
    "The next three exercises should be done in order."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"exercise\">\n",
    "<b>Exercise: AM or PM?</b>\n",
    "Suppose you are given a string that represents time using a 12 hour clock.  The strings always have the form\n",
    "<code>xx:xx am</code> or <code>xx:xx pm</code>, where <code>xx:xx</code> represents the time.\n",
    "\n",
    "Write a logical statement that takes a string <code>s</code> of this form and evaluates to <code class=\"kw\">True</code> if and only if the time is after noon.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b>Answer.</b>\n",
    "<div class=\"voila\">\n",
    "<p>\n",
    "Multiple solutions are possible; here are a few.\n",
    "</p>\n",
    "\n",
    "<b>Solution 0.</b>\n",
    "<pre>\n",
    "'pm' in s\n",
    "</pre>    \n",
    "<b>Solution 1.</b>\n",
    "<pre>\n",
    "s[6:8] == 'pm'\n",
    "</pre>\n",
    "<b>Solution 2.</b>\n",
    "<pre>\n",
    "s[6:] == 'pm'\n",
    "</pre>\n",
    "<b>Solution 3.</b>\n",
    "<pre>\n",
    "s[-2:] == 'pm'\n",
    "</pre>\n",
    "<b>Solution 4.</b>\n",
    "<pre>\n",
    "s.find('pm') >= 0\n",
    "</pre>\n",
    "Solution 0 is the best since it only requires us to know that am/pm are present in the string and doesn't require any other knowledge about the format of the string, unlike the other solutions.  Solution 0 is also the shortest.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"exercise\">\n",
    "<p>\n",
    "<b>Exercise: converting 12 hour time to 24 hour time.</b>\n",
    "</p>\n",
    "<p>\n",
    "Given a string <code>s</code> in 12 hour time (<code>xx:xx am</code> or <code>xx:xx pm</code>), convert it to a string containing the equivalent time in 24 hour clock](https://en.wikipedia.org/wiki/24-hour_clock) format as <code>xxxx</code>.\n",
    "</p>\n",
    "<p>\n",
    "Thus, if <code>s = '09:34 pm'</code> then the result should be <code>'2134'</code>.\n",
    "</p>\n",
    "<p>\n",
    "You may need to some converting back and forth from strings to integers as part of your solution.\n",
    "</p>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b>Answer.</b>\n",
    "<div class=\"voila\">\n",
    "We will use code from the previous exercise for detecting pm vs am.\n",
    "    \n",
    "<code>\n",
    "s = '11:34 pm'\n",
    "\n",
    "hour   = s[:2]  # Get the hour and minute.\n",
    "minute = s[3:5]\n",
    "    \n",
    "hour = int(hour)  # Get the hour and minute.\n",
    "if 'pm' in s:  # Afternoon.\n",
    "    hour += 12\n",
    "        \n",
    "if (hour < 10):  # Stitch things back together, taking account of leading zeros.\n",
    "    hour = '0' + str(hour)\n",
    "else:\n",
    "    hour = str(hour)\n",
    "\n",
    "t = (hour + minute)\n",
    "\n",
    "print(t)\n",
    "</code>\n",
    "When we talk about formatted output later in the semester we will see a cleaner way to handle the leading zero issue.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"exercise\">\n",
    "<p>\n",
    "<b>Exercise: converting 12 hour time to minutes since midnight.</b>\n",
    "</p>\n",
    "<p>\n",
    "Take a time as a string of the form <code>xx:xx am</code> or <code>xx:xx pm</code> and convert it to the equivalent time in minutes from the start of the day (as an integer).\n",
    "</p>\n",
    "<p>\n",
    "For instance, if <code>s = '09:14 am'</code> then the result should be <code>554</code>.\n",
    "</p>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b>Answer.</b>\n",
    "<div class=\"voila\">\n",
    "\n",
    "<code>\n",
    "s = '09:14 pm'\n",
    "\n",
    "hour   = int(s[:2])  # Get the hour and minute.\n",
    "minute = int(s[3:5])\n",
    "\n",
    "if 'pm' in s:  # Adjust times after noon.\n",
    "    hour += 12\n",
    "\n",
    "time_in_minutes = (60*hour + minute)\n",
    "\n",
    "print(time_in_minutes)\n",
    "</code>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"exercise\">\n",
    "<b>Exercise.</b>\n",
    "How can you test whether a string is a <a href=\"https://en.wikipedia.org/wiki/Palindrome\">palindrome</a>?  For the purposes of this exercise you should ignore case; i.e., \"Able was I ere I saw Elba\" is considered to be a palindrome.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b>Answer.</b>\n",
    "<div class=\"voila\">\n",
    "We use the string method <code>lower()</code> to turn all letters to lower case (upper case would work fine, too) and then check whether the string is the same when reversed.\n",
    "<pre>\n",
    "s = \"Able was I ere I saw Elba\"\n",
    "is_palindrome = (s.lower()[::-1] == s.lower())\n",
    "print(f'Is \"{s}\" a palindrome?', is_palindrome)\n",
    "</pre>\n",
    "See the related <code>casefold()</code> string method.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h4>This notebook was brought to you by Ignorance:</h4>\n",
    "\n",
    "&hellip;ignorance more frequently begets confidence than does knowledge&hellip;<br/>\n",
    "&ndash; Charles Darwin, The Descent of Man"
   ]
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
