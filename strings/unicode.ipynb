{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"http://www.cs.wm.edu/~rml/images/wm_horizontal_single_line_full_color.png\">\n",
    "\n",
    "<h1 style=\"text-align:center;\">CSCI 141, Fall 2023</h1>\n",
    "<h1 style=\"text-align:center;\">Unicode 😱</h1>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Contents\n",
    "\n",
    "* [Non-ASCII characters](#non-ascii)\n",
    "* [¡Cuidado, llamas!](#llamas)\n",
    "* [More examples](#examples)\n",
    "* [Unicode sequence numbers](#sequence)\n",
    "* [Fun with formatting](#fun)\n",
    "* [Divertissement: Unicode identifiers &#128561;](#identifiers)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Non-ASCII characters <a id=\"non-ascii\"></a>\n",
    "\n",
    "Suppose we wish to use characters outside of the ASCII character set:\n",
    "<blockquote>\n",
    "Señoras, señores y señoritas; buenas noches.<br/>\n",
    "\n",
    "Esta noche presentamos con mucho gusto información interesante acerca de la llama.<br/> \n",
    "\n",
    "La llama es un cuadrúpedo que vive en los grandes ríos del Amazonas. Tiene dos orejas, un corazón, una frente y\n",
    "un pico para comer miel. Pero, está provista de aletas para nadar.<br/> \n",
    "\n",
    "Las llamas son más grandes que las ranas.<br/>\n",
    "\n",
    "Las llamas son peligrosas, si ve una llama y hay gente nadando, usted debe gritar: ¡Cuidado, llamas!<br/>\n",
    "\n",
    "Cuidado, cuidado, cuidado, hay llamas.\n",
    "</blockquote>    \n",
    "One way is to cut and paste the desired characters from something that correctly renders them. In this case, they were copied from a web browser.\n",
    "\n",
    "However, this is awkward and slow.  Moreover, it may be the case that our editor cannot render the character, even though it understands it, so we won't be able to see it in our code.\n",
    "\n",
    "To obtain non-ASCII characters in a portable way, we need to use [Unicode](http://www.unicode.org/charts/), which is a character set that vastly expands ASCII."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Unicode\n",
    "\n",
    "ASCII characters all require only one byte of space.  A byte is 8 bits, each of which can be in one of two states, on or off, meaning it is possible to represent $2^{8} = 256$ characters if characters are allowed only one byte.\n",
    "\n",
    "Unicode expands on this by allowing multi-byte characters while being compatible with ASCII.  We distinguish between the character and the way it is encoded; the most commonly used encoding scheme is called [UTF-8](https://en.wikipedia.org/wiki/UTF-8).  UTF-8 is the most common because it is very efficient in the use of space (two other encodings you might encounter are UTF-16 and UTF-32)..\n",
    "\n",
    "When discussing Unicode character we distinguish between the **code point**, which is a number identifying the character, and the **glyph**, which is the actual symbol used to represent the character."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "c = '@'\n",
    "print(f'code point: {ord(c):6d}  glyph: {c}')\n",
    "\n",
    "c = '\\N{sauropod}'\n",
    "print(f'code point: {ord(c):6d}  glyph: {c}')\n",
    "\n",
    "c = '🤯'\n",
    "print(f'code point: {ord(c):6d}  glyph: {c}')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As of Fall 2023 there are nearly 150,000 Unicode code-points."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# ¡Cuidado, llamas! <a id=\"llamas\"></a>\n",
    "\n",
    "Let's look at the following sentence:\n",
    "<blockquote>\n",
    "    La llama es un cuadrúpedo que vive en los grandes ríos del Amazonas. \n",
    "</blockquote>\n",
    "\n",
    "We need to look up the Unicode values for the accented characters ú and í.  Let's look in [the Unicode character name index](https://www.unicode.org/charts/charindex.html).  The chararcter ú is 'LATIN SMALL LETTER U WITH ACUTE', which has the associated code point 00fa, while í is 'LATIN SMALL LETTER I WITH ACUTE', which has the associated code point 00ed.  The codes are actually hexadecimal (base-16) numbers.\n",
    "\n",
    "Since most Unicode characters are not representable in ASCII, we need a way to represent them.  This is done using either escaped characters, \\u or \\U, and the code for the character.  A \\u means the following four characters are a Unicode character, while \\U means the following eight characters are a Unicode character.\n",
    "\n",
    "In our example, we insert \\u00fa to obtain ú, and \\u00ed to obtain í:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "print('La llama es un cuadr\\u00fapedo que vive en los grandes r\\u00edos del Amazonas.')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The ¡ character is 'INVERTED EXCLAMATION MARK', with code point 00a1:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('\\u00a1Cuidado, llamas!')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The code point for the exploding head is 1f92f, which needs to be padded at the start with zeros so we end up with an eight digit hexadecimal (base-16) number:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('\\U0001f92f')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"try_it\">\n",
    "    <b>Try it yourself</b>.  What happens if you omit the three leading zeros?\n",
    "</div>    "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"try_it\">\n",
    "    <b>Try it yourself</b>.  Look up and print another emoji.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can also print Unicode characters with \\N followed by their official name in brackets:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('\\N{GREEK CAPITAL LETTER DELTA}')\n",
    "print('\\N{INVERTED EXCLAMATION MARK}Cuidado, llamas!')\n",
    "print('\\N{sauropod} \\N{T-rex}')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"danger\"></div>  I have found using the official name sometimes doesn't work.  Using the code point, on the other hand, has never failed."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# More examples <a id=\"examples\"></a>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The degree sign\n",
    "\n",
    "Let's use the degree sign, which is not part of the ASCII character set.\n",
    "[The degree sign is represented as \\u00b0](https://en.wikipedia.org/wiki/Degree_symbol)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('This is the degree sign: \\u00b0')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's find ourselves:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "platitude='37\\u00b0 16\\' 12.42\" N'\n",
    "plongitude='76\\u00b0 42\\' 42.12\\\" W'\n",
    "print(f'We are currently located at {platitude}, {plongitude}.')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Basic smiley faces\n",
    "\n",
    "On a cheerier note, Unicode also includes some basic smiley faces:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Some basic smileys.\n",
    "print('\\u263a')\n",
    "print('\\u263b')\n",
    "print('\\u2639')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Emoticons\n",
    "\n",
    "If the basic smileys are not enough, there are \n",
    "<a href=\"https://en.wikipedia.org/wiki/Emoticons_(Unicode_block)\">Unicode emoticons</a>.\n",
    "\n",
    "These characters require eight character encodings, so they start with \\U:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('\\U0001f600')\n",
    "print('\\U0001f601')\n",
    "\n",
    "# angel, devil.\n",
    "print('\\U0001f607 \\U0001f608')\n",
    "\n",
    "# See no evil, hear no evil, speak no evil.\n",
    "print('\\U0001f648', '\\U0001f649', '\\U0001f64a')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('\\u00F7')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Unicode code points <a id=\"sequence\"></a>\n",
    "\n",
    "We can obtain the Unicode code point in the same ways we can obtain code points for ASCII characters, using the [ord() function](https://docs.python.org/3/library/functions.html#ord)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(ord('\\U0001f600'))\n",
    "print(ord('😀'))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To go the other way with the [chr() function](https://docs.python.org/3/library/functions.html#chr), we need to work with the hexadecimal Unicode encoding.  We can tell Python to interpret our number as a hexadecimal number by starting the number with 0x:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(chr(0x0001f600))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can also use string formatting to find out about Unicode characters:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(f'{0x0001f600:c}')\n",
    "print(f'{0x0001f600:d}')\n",
    "print(f'{129429:c}')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Fun with formatting <a id=\"fun\"></a>\n",
    "\n",
    "Let's try to reproduce a\n",
    "<a href=\"https://en.wikipedia.org/wiki/Emoticons_(Unicode_block)\">table of emoticons</a>.\n",
    "\n",
    "Here is the table.  Depending on the Unicode support of your browser, it may look slightly wonky.\n",
    "<pre>\n",
    "           0      1\t 2\t 3\t  4\t 5\t 6\t 7\t  8     9     A     B     C      D     E     F\n",
    "U+1F60x\t😀\t😁\t😂\t😃\t😄\t😅\t😆\t😇\t😈\t😉\t😊\t😋\t😌\t😍\t😎\t😏\n",
    "U+1F61x\t😐\t😑\t😒\t😓\t😔\t😕\t😖\t😗\t😘\t😙\t😚\t😛\t😜\t😝\t😞\t😟\n",
    "U+1F62x\t😠\t😡\t😢\t😣\t😤\t😥\t😦\t😧\t😨\t😩\t😪\t😫\t😬\t😭\t😮\t😯\n",
    "U+1F63x\t😰\t😱\t😲\t😳\t😴\t😵\t😶\t😷\t😸\t😹\t😺\t😻\t😼\t😽\t😾\t😿\n",
    "U+1F64x\t🙀\t🙁\t🙂\t🙃\t🙄\t🙅\t🙆\t🙇\t🙈\t🙉\t🙊\t🙋\t🙌\t🙍\t🙎\t🙏\n",
    "</pre>\n",
    "\n",
    "The thing to notice is that the numbers across the top are the base-16 digits!\n",
    "\n",
    "What we want to produce are the unicode sequences\n",
    "<pre>\n",
    "\\U0001f600, \\U0001f601, ..., \\U0001f60f\n",
    "\\U0001f610, \\U0001f611, ..., \\U0001f61f\n",
    "\\U0001f620, \\U0001f621, ..., \\U0001f62f\n",
    "\\U0001f630, \\U0001f631, ..., \\U0001f63f\n",
    "</pre>\n",
    "We can do this in a <code>for</code> loop by converting the character to its ordinal value in Unicode using <code>ord()</code>, incrementing this value, and converting the incremented value back to a character using <code>chr()</code>:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for m in range(0,5):\n",
    "    for n in range(0,16):\n",
    "        # write out the unicode character in hexadecimal as a string\n",
    "        s = f'0x0001f6{m:d}{n:x}'\n",
    "    \n",
    "        # convert the hexadecimal string to an integer\n",
    "        k = int(s, 16) # the 16 indicates the base used to interpret s\n",
    "    \n",
    "        # convert the integer to the associated character\n",
    "        print(chr(k)+ '  ', end='')\n",
    "    print()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Divertissement: Unicode identifiers  <a id=\"identifiers\"/>\n",
    "\n",
    "Python allows Unicode characters in the names (identifiers) of variables and functions, so long as the characters are letters, broadly understood.\n",
    "\n",
    "This means you could use [Younger Futhark runes](https://en.wikipedia.org/wiki/Younger_Futhark)&hellip;"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# We can use the runes for Harald Bluetooth!\n",
    "def ᚼᛒ():\n",
    "    print('bloo!')\n",
    "    \n",
    "ᚼᛒ()\n",
    "\n",
    "ᚴ = 42\n",
    "print(ᚴ)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "&hellip;or [Etruscan](https://en.wikipedia.org/wiki/Etruscan_alphabet)&hellip;"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Old italic / Etruscan\n",
    "def 𐌈𐌌𐌔𐌄𐌅𐌀():\n",
    "    print('𐌀𐌁𐌂𐌃𐌄𐌅𐌆𐌇𐌈𐌉𐌊𐌋𐌌𐌍𐌎𐌏𐌐𐌑𐌒𐌓𐌔𐌕𐌖𐌗𐌘𐌙𐌚𐌛𐌜𐌝𐌞𐌟')\n",
    "    \n",
    "𐌈𐌌𐌔𐌄𐌅𐌀()    \n",
    "\n",
    "𐌔 = 42\n",
    "print(𐌔)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "&hellip;or [Tifinagh](https://en.wikipedia.org/wiki/Tifinagh)&hellip;"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Tifinagh\n",
    "def ⴱⴻⵞⵥⵖⵐ():\n",
    "    print('ⵁⴽⵑ')\n",
    "    \n",
    "ⴱⴻⵞⵥⵖⵐ()\n",
    "\n",
    "ⵞ = 42\n",
    "print(ⵞ)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "&hellip;or [Glagolitic](https://en.wikipedia.org/wiki/Glagolitic_script)!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Glagolitic\n",
    "def ⰀⰈⱔⰖⰇⰗⰺⰧⱒⱖⰝⰭⰁ():\n",
    "    print('ⰀⰁⰂⰃⰄⰅⰆⰇ')\n",
    "    \n",
    "ⰀⰈⱔⰖⰇⰗⰺⰧⱒⱖⰝⰭⰁ()    \n",
    "\n",
    "Ⱒ = 42\n",
    "print(Ⱒ)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "I'm partial to the [multiocular O](https://en.wikipedia.org/wiki/Multiocular_O):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# We can also use \"eth\"...\n",
    "def ð():\n",
    "    print('ð')\n",
    "\n",
    "# ...and multiocular O!\n",
    "def ꙮ():\n",
    "    print('ꙮ')\n",
    "    \n",
    "ð()\n",
    "ꙮ()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Only characters that are \"letters\" are acceptable; mathematical symbols, for instance, are not:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# The integral sign is not acceptable to Python:\n",
    "def ∫():\n",
    "    pass"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Neither is the nabla (the triangular character):\n",
    "def ∇f():\n",
    "    pass"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Sadly, emoticons are also not acceptable,"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def 😛():\n",
    "    pass"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "nor are dinosaurs:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "🦕 = 42"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[Egyptian hieroglyphics](https://en.wikipedia.org/wiki/Egyptian_Hieroglyphs_&#40;Unicode_block&#41;), on the other hand, are fine:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def 𓃰():\n",
    "    pass\n",
    "\n",
    "print(𓃰.__name__)\n",
    "\n",
    "𓄿 = 42\n",
    "print(𓄿)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "While it would be entertaining to use Egyptian hieroglyphs in function and variables names, the result would be difficult to read or debug &ndash; how would you read\n",
    "```python\n",
    "def 𓀾𓁙𓃜():\n",
    "    pass\n",
    "```\n",
    "aloud???\n",
    "\n",
    "Also, depending on your familiarity with the language and its Unicode glyphs, some characters may be difficult to distinguish.  For example, the Japanese katakana characters tu, ツ, and su, シ, are very similar:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def ツ():\n",
    "    print('🤷')\n",
    "\n",
    "def シ():    \n",
    "    print('😛')\n",
    "    \n",
    "ツ()\n",
    "シ()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In fact, the situation can be even worse (example courtesy of [this site](https://github.com/satwikkansal/wtfpython#section-appearances-are-deceptive)):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "value = 6*9\n",
    "valuе = 42\n",
    "print(value)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The trick is that one 'e' is a Latin 'e' and the other is Cyrillic, but they look identical:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(ord('e'))  # ASCII e\n",
    "print(ord('е'))  # Unicode Cyrillic e"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"danger\"></div> It's probably best to stick to the ASCII characters for identifiers."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h4>This notebook was brought to you by <a href=\"https://en.wikipedia.org/wiki/Echidna\">puggles</a>:</h4>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from IPython.display import YouTubeVideo\n",
    "YouTubeVideo('HFOIPJ7-CaE')"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
