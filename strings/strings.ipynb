{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "nbpresent": {
     "id": "abaeef91-5553-4553-8ebf-2c22a87d7137"
    }
   },
   "source": [
    "<img src=\"http://www.cs.wm.edu/~rml/images/wm_horizontal_single_line_full_color.png\">\n",
    "\n",
    "<h1 style=\"text-align:center;\">CSCI 141, Fall 2023 course notes</h1>\n",
    "\n",
    "<h1 style=\"text-align:center;\">Introduction to strings</h1>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Contents\n",
    "\n",
    "* [Strings](#strings)\n",
    "* <a href=\"#input\">Getting input from the user</a>\n",
    "* <a href=\"#quotes\">Single and double quotes</a>\n",
    "    * <a href=\"#verboten\">You can't mix single and double quotes as delimiters</a>\n",
    "* <a href=\"#empty_strings\">Empty strings</a>\n",
    "  * <a href=\"#print\">The <code>print()</code> function    \n",
    "* <a href=\"#triple_quotes\">Triple quotes</a>\n",
    "* <a href=\"#quotation_marks\">Printing quotation marks</a>\n",
    "* <a href=\"#special_chars\">Special characters</a> \n",
    "    * [The backslash character and raw strings &#x1F92F;](#backslash)\n",
    "* <a href=\"#str_exercises\">Exercises</a>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbpresent": {
     "id": "def994af-b739-4604-9024-2ae607286774"
    }
   },
   "source": [
    " # Strings <a id=\"strings\"></a>\n",
    " \n",
    "A **character string**, or **string** for short, is just that &ndash; a string of characters.  Python refers to the string type as <code class=\"kw\">str</code>."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s = 'hello, world!'\n",
    "print(type(s))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Getting input from the user <a id=\"input\"></a>\n",
    "\n",
    "Since there is no telling what user's might type when prompted, Python's <code>input()</code> function reads everything a user types as a string, since anything they can type is guaranteed to be a string."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s = input('What is your name? ')\n",
    "print(s)\n",
    "s = input('What is your quest?')\n",
    "print(s)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "(Observe that we leave a space at the end of the prompt.  If we don't, when we run the code outside of a Jupyter notebook our response will be munged together with the prompt and the result is ugly.)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s = input('What is the airspeed of a European swallow? ')\n",
    "t = input('What is the airspeed of an African swallow? ')\n",
    "print('Combined airspeed:', s + t)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbpresent": {
     "id": "42268b94-8e34-4c8d-babf-cf0e4ca5e4fc"
    }
   },
   "source": [
    "# Single and double quotes <a id=\"quotes\"></a>\n",
    "\n",
    "Strings are delimited by either single or double quotes.  This makes it easy to place quotation marks inside strings.\n",
    "\n",
    "The quotes that delimit a character string at the beginning and end are not part of the string itself."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbpresent": {
     "id": "ce378991-56d2-4dd2-9fea-f48fb8d83930"
    }
   },
   "outputs": [],
   "source": [
    "x = 'Hello, world!'\n",
    "print(x)\n",
    "y = \"Hello, world!\"\n",
    "print(y)\n",
    "z = 'With a last gasp he said, \"It was the squirrels!\"'\n",
    "print(z)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that the delimiting quotes are not printed."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbpresent": {
     "id": "55cae99d-d8fb-4a47-a42d-c52c29b8c788"
    }
   },
   "source": [
    "## You can't mix single and double quotes as delimiters <a id=\"verboten\"></a>\n",
    "\n",
    "While you can either delimit a string with a pair of single quotes or a pair double quotes, you can't use different types of quotes to delimit a character string. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"exercise\">\n",
    "    <strong>Exercise:</strong>\n",
    "    Suppose you could: what would have happened in the assignment to <b>z</b> we just did?\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b>Answer.</b>\n",
    "<div class=\"voila\">\n",
    "The right-hand side would consist of the string\n",
    "<pre>\n",
    "'With a last gasp he said, \"\n",
    "</pre>\n",
    "followed by \n",
    "<pre>\n",
    "It was the squirrels!\"'\n",
    "</pre>\n",
    "However, the latter is not a string since it lacks a quotation mark at the beginning.  This would cause an error.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Python will let you know if you fail to correctly delimit a string as soon as it recognizes your mistake.  In the following example it has to go to the end of the line before it can be sure the string is not <code>\"foo''\"</code>."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbpresent": {
     "id": "559433c8-795d-4116-a0ba-4ef95f78208d"
    },
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# This will produce an error.  \"EOL\" stands for \"end of line\".\n",
    "x = \"foo'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The error message is telling you that the Python interpreter encountered the end of the line while it was still looking for the end of the string.  This was because the string started with a double quote, and the interpreter was still looking for the terminating double quote when it hit the end of the line."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Should I use single quotes or double quotes?\n",
    "\n",
    "You can either delimit a string with a pair of single quotes or pair of double quotes.\n",
    "\n",
    "This is not the case in many other languages.  For instance, in C, C++, or Java, character strings are indicated with double quotes, while in Fortran and Matlab character strings are indicated by single quotes.\n",
    "\n",
    "FWIW, I find single quotes a bit easier to type: double quotes require using the shift key while single quotes do not."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"danger\"></div>  \n",
    "While Python allows you to use either single quotes or double quotes, you should try to be consistent in your usage of one or the other.  Stylistic changes can confuse people reading your code &ndash; they think there must be a reason for a stylistic change and are puzzled when they cannot see one."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbpresent": {
     "id": "3a98bf9c-e1d6-4c82-9d16-f63a88827244"
    }
   },
   "source": [
    "# Empty strings <a id=\"empty_strings\"></a>\n",
    "\n",
    "It's OK for a string to be empty!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbpresent": {
     "id": "5af6b20d-645f-4018-972d-fc33b0d49b34"
    }
   },
   "outputs": [],
   "source": [
    "s = ''\n",
    "print('s is of type', type(s))\n",
    "print('s =', s)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Empty strings are useful when you need to initialize a string variable but you don't have anything to put in it yet."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Do not confuse an empty string with a string that consists of a single space character:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s = ''   # An empty string.\n",
    "t = ' '  # Not the same as s!!\n",
    "\n",
    "# Print the length of each.\n",
    "print('length of s:', len(s))\n",
    "print('length of t:', len(t))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A space is an actual character.  On the printed page a space is simply the absence of ink.  In computing, we need a character that indicates the absence of anything and is interpreted as a space."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Empty strings and the <code>print()</code> function <a id=\"print\"></a>\n",
    "\n",
    "We have made use of the built-in Python <code>print()</code> function, which is a standard part of the Python language. \n",
    "\n",
    "One convenient option you can specify to this function controls what happens when <code>print()</code> is done printing.\n",
    "\n",
    "By default, <code>print()</code> begins a new line after it prints.  You can change this behavior by specifying the <code>end</code> options, which tells <code>print</code> to print a specified character when done.\n",
    "\n",
    "For instance, you could have <code>print()</code> end with the letter \"X\" and then advance to the next line."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# End the printing with the letter X and then a newline character.\n",
    "print(\"boo-\", end=\"X\\n\")\n",
    "print(\"hoo!\")\n",
    "print(\"foo!\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"boo-\", end=\"X\")\n",
    "print(\"hoo!\")\n",
    "print(\"foo!\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A more useful way to use the <code>end</code> option is to tell <code>print()</code> not to print anything when it's done.  This is done with an empty string: <code>end=\"\"</code>."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# End the printing with nothing, and don't begin a new line.\n",
    "print(\"boo-\", end=\"\")\n",
    "\n",
    "# Print in the usual way.\n",
    "print(\"hoo!\")\n",
    "print(\"foo!\")\n",
    "\n",
    "# End the printing with nothing, and don't begin a new line.\n",
    "print(\"prefrobnicating...\", end=\"\")\n",
    "# Code for prefrobnication...\n",
    "print(\"done!\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Triple quotes <a id=\"triple_quotes\"/>\n",
    "\n",
    "Strings delimited by triple quotes preserves the exact format of the string as typed.  If the string spans several lines, then the newlines (line breaks) will be preserved in the string.\n",
    "\n",
    "For instance, suppose we wish to print the following poem exactly as written.\n",
    "<pre>\n",
    "THE WOMBAT\n",
    "by Ogden Nash\n",
    "The wombat lives across the seas,\n",
    "Among the far Antipodes.\n",
    "He may exist on nuts and berries,\n",
    "Or then again, on missionaries;\n",
    "His distant habitat precludes\n",
    "Conclusive knowledge of his moods,\n",
    "But I would not engage the wombat\n",
    "In any form of mortal combat.\n",
    "</pre>\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If we try just to put the original text in standard quotes, we run into an error:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "poem = 'THE WOMBAT\n",
    "        by Ogden Nash\n",
    "        The wombat lives across the seas,\n",
    "        Among the far Antipodes.\n",
    "        He may exist on nuts and berries,\n",
    "        Or then again, on missionaries;\n",
    "        His distant habitat precludes\n",
    "        Conclusive knowledge of his moods,\n",
    "        But I would not engage the wombat\n",
    "        In any form of mortal combat.'\n",
    "print(poem)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can use **triple quotes** to retain the formatting, including the line breaks."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "poem = '''THE WOMBAT\n",
    "          by Ogden Nash\n",
    "          The wombat lives across the seas,\n",
    "          Among the far Antipodes.\n",
    "          He may exist on nuts and berries,\n",
    "          Or then again, on missionaries;\n",
    "          His distant habitat precludes\n",
    "          Conclusive knowledge of his moods,\n",
    "          But I would not engage the wombat\n",
    "          In any form of mortal combat.'''\n",
    "print(poem)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Woops!  notice that because there are no leading spaces before \"THE WOMBAT\" the title is offset relative to the rest of the poem.  In an exercise you are asked to fix this."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<p class=\"try_it\">\n",
    "<b>Try it yourself.</b> Create a triply quoted multiline string and print it.\n",
    "</p>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Printing quotation marks <a id=\"quotation_marks\"></a>\n",
    "\n",
    "How do we print quotation marks since quotation marks are also used to delimit strings?\n",
    "\n",
    "We may be able to use the single-quote / double-quote trick like we did above:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"'The horror! the horror!'\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "However this will not always work, as the following example illustrates."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"'I want \"scare quotes\" in this quote.'\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "One solution is to use triple quotes:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"\"\"'I want \"scare quotes\" in this quote.'\"\"\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Special characters and escape sequences<a id=\"special_chars\"></a>\n",
    "\n",
    "Another solution to the quotes-inside-quotes problem is provided by our good friend the backslash, <code>&#92;</code>.  We use the backslash followed by a quotation mark to let Python know that we are printing a quotation mark character rather than delimiting a character string."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('\\'')\n",
    "print('\\\"')\n",
    "print(\"\\'\")\n",
    "print(\"\\\"\")\n",
    "print('\\'The horror! the horror!\\'')\n",
    "print(\"'I want \\\"scare quotes\\\" in this quote.'\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The practice of using a special symbol to alter the meaning of the character that follows is called <b>escaping</b>.  In this context, the backslash is the <b>escape character</b>, and the combination of the escape character and the character that follows is called an <b>escape sequence</b>."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Two special characters that are good to know are the **newline character**, denoted by '\\n', and the **tab character**, denoted by '\\t'.\n",
    "\n",
    "The newline character is interpreted as a line break:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('boo!\\n')\n",
    "print('print() added its own newline after the newline in print(boo!\\\\n)')\n",
    "print('boo!\\nhoo!')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The tab character behaves like the tabs on a typewriter, adding horizontal space:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('boo!\\thoo!')\n",
    "print('boo!\\t\\thoo!')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The backslash character and raw strings &#x1F92F; <a id=\"backslash\"/>\n",
    "\n",
    "What happens if we try to put a backslash in a string?  Because the backslash is an escape character that alters the meaning of the character that follows, it turns out to be a bit tricky.\n",
    "\n",
    "First, let's just try including a backslash.  The <code>|</code> character is there so we can see where the string begins and ends."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s = '|\\moo|'\n",
    "print(s)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s = '|\\foo|'\n",
    "print(s)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Pathnames in Windows are the classic illustration of this headache.\n",
    "s = 'C:\\Users\\foo'\n",
    "print(s)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Three different strings, three different behaviors!\n",
    "* In the first string things turn out as we would expect.\n",
    "* In the second string, <code>\\f</code> is another escape sequence, representing a <b>formfeed character</b>.  In the old days this would tell line printers to start a new page of paper.  It is ignored in this context.\n",
    "* In the third string, <code>\\U</code> is an escape sequence that introduces a <b>Unicode character</b>.  We will study Unicode later.  In this example, what follows the <code>\\U</code> is not valid Unicode.\n",
    "\n",
    "Here is the entire set of special escaped characters.\n",
    "* <code>\\a</code>\tAlert (audible beep or bell)\n",
    "* <code>\\b</code>\tBackspace\n",
    "* <code>\\f</code>\tFormfeed / Page Break\n",
    "* <code>\\n</code>\tNewline / Line Feed\n",
    "* <code>\\r</code>\tCarriage Return\n",
    "* <code>\\t</code>\tHorizontal Tab\n",
    "* <code>\\v</code>\tVertical Tab\n",
    "* <code>&#92;&#92;</code>\tBackslash\n",
    "* <code>&#92;'</code>\tApostrophe or single quotation mark\n",
    "* <code>&#92;\"</code>\tDouble quotation mark\n",
    "* <code>\\u</code>\tUnicode code point below 0x10000 in hexadecimal (base-16)\n",
    "* <code>\\U</code> \tUnicode code point where h is a hexadecimal digit\n",
    "* <code>\\N</code>   Unicode character by name."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So how do we include a backslash in a string?\n",
    "\n",
    "One approach is to escape it with another backslash:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s = '|\\\\moo|'\n",
    "print(s)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s = '|\\\\foo|'\n",
    "print(s)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s = 'C:\\\\Users\\\\foo'\n",
    "print(s)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "However, this can get ugly fast if you have a lot of backslashes (this happens with pathnames in Windows).  As an alternative, Python provides <b>raw strings</b>, which treat the backslash (and everything else) as regular characters.  A raw string is prefaced by an <code>r</code> or </code>R<code> **outside** the left delimiting quote(s):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# A raw string is prefaced by an r or R.\n",
    "s = r'C:\\Users\\foo'\n",
    "t = R\"\\foo\"\n",
    "u = r'''\\a\\b\\c\\d'''\n",
    "print(s)\n",
    "print(t)\n",
    "print(u)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Exercises <a id=\"exercises\"/>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"exercise\">\n",
    "<b>Wombattery.</b>\n",
    "Modify the code below so that the title \"THE WOMBAT\" lines up with the remaining lines of text when printed.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "poem = '''THE WOMBAT\n",
    "        by Ogden Nash\n",
    "        The wombat lives across the seas,\n",
    "        Among the far Antipodes.\n",
    "        He may exist on nuts and berries,\n",
    "        Or then again, on missionaries;\n",
    "        His distant habitat precludes\n",
    "        Conclusive knowledge of his moods,\n",
    "        But I would not engage the wombat\n",
    "        In any form of mortal combat.'''\n",
    "print(poem)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b>Answer.</b> \n",
    "<div class=\"voila\">\n",
    "<b>Solution 1 (an ugly kludge).</b>  Add spaces to the first line:\n",
    "<pre>\n",
    "poem = '''          THE WOMBAT\n",
    "          by Ogden Nash\n",
    "          The wombat lives across the seas,\n",
    "          Among the far Antipodes.\n",
    "          He may exist on nuts and berries,\n",
    "          Or then again, on missionaries;\n",
    "          His distant habitat precludes\n",
    "          Conclusive knowledge of his moods,\n",
    "          But I would not engage the wombat\n",
    "          In any form of mortal combat.'''\n",
    "</pre>\n",
    "<b>Solution 2.</b> Move the title to a new line with same indentation as the other lines, and add a backslash immediately after the triple quotes on the first line.  Remember that the backslash breaks the statement begun by ''' onto the next line.  In this instance it prevents an extra line being printed before the title.\n",
    "<pre>\n",
    "poem = '''\\\n",
    "          THE WOMBAT\n",
    "          by Ogden Nash\n",
    "          The wombat lives across the seas,\n",
    "          Among the far Antipodes.\n",
    "          He may exist on nuts and berries,\n",
    "          Or then again, on missionaries;\n",
    "          His distant habitat precludes\n",
    "          Conclusive knowledge of his moods,\n",
    "          But I would not engage the wombat\n",
    "          In any form of mortal combat.'''\n",
    "</pre>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"exercise\">\n",
    "<b>Format this!</b>\n",
    "Print the following as it appears (ignore the spaces at the start of each line):\n",
    "    \n",
    "<pre>\n",
    "The curious task of economics is to demonstrate to men how little they \n",
    "really know about what they imagine they can design.\n",
    "\n",
    "Friedrich von Hayek\n",
    "</pre>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b>Answer.</b> \n",
    "<div class=\"voila\">\n",
    "Here is one solution:\n",
    "    \n",
    "<pre>\n",
    "s = '''\n",
    "The curious task of economics is to demonstrate to men how little they \n",
    "really know about what they imagine they can design.\n",
    "\n",
    "Friedrich von Hayek\n",
    "'''\n",
    "print(s)\n",
    "</pre>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"exercise\">\n",
    "<b>Print this!</b>\n",
    "Write code that will print \n",
    "<pre style=\"margin-left:0;\">\n",
    "\"I never heard of 'Uglification,'\" Alice ventured to say. \"What is it?\" \n",
    "</pre>\n",
    "    <b>exactly</b> as it appears, including all quotation marks.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b>Answer.</b>\n",
    "<div class=\"voila\">\n",
    "Here are two possible answers:\n",
    "    \n",
    "<code>\n",
    "print(\"\\\"I never heard of 'Uglification,'\\\" Alice ventured to say. \\\"What is it?\\\"\")\n",
    "print('''\"I never heard of 'Uglification,'\" Alice ventured to say. \"What is it?\"''')\n",
    "</code>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"exercise\">\n",
    "<b>Print this!</b>\n",
    "Write code that will print the Windows style pathname <code>C:\\Users\\meg\\aardvark</code>.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b>Answer.</b>\n",
    "<div class=\"voila\">\n",
    "Either of the following will work:\n",
    "<pre>\n",
    "print(r\"C:\\Users\\meg\\aardvark\")\n",
    "print(\"C:\\\\Users\\\\meg\\\\aardvark\")\n",
    "</pre>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h4>This notebook was brought to you by the study of history.</h4>\n",
    "\n",
    "Who knows only his own generation remains always a child.\n",
    "\n",
    "<i>Nescire autem quid ante quam natus sis acciderit, id est semper esse puerum.</i><br/>\n",
    "To be ignorant of what occurred before you were born is to remain always a child.\n",
    "\n",
    "&ndash; Marcus Tullius Cicero, De Oratore, chapter XXXIV (section 120)"
   ]
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
