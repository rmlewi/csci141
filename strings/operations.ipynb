{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"http://www.cs.wm.edu/~rml/images/wm_horizontal_single_line_full_color.png\">\n",
    "\n",
    "<h1 style=\"text-align:center;\">CSCI 141, Fall 2023 course notes</h1>\n",
    "<h1 style=\"text-align:center;\">Basic string operations</h1>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Contents\n",
    "* <a href=\"#len\">Finding the length of a string</a>\n",
    "* <a href=\"#concatenation\">Concatenating strings</a>\n",
    "* <a href=\"#multiplication\">String multiplication</a>\n",
    "* <a href=\"#conversion\">Converting other types into strings</a>\n",
    "    * <a href=\"#gotcha\">A gotcha concerning type names</a>\n",
    "* [Exercises](#exercises)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Finding the length of a string <a id=\"len\"></a>\n",
    "\n",
    "We can find the number of characters in a string (its length) using the function <code>len()</code>:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('\"cat\" contains', len('cat'), 'characters')\n",
    "print('\"hello, world!\" contains', len('hello, world!'), 'characters')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s = 'length'\n",
    "print(s, 'contains', len(s), 'characters')\n",
    "\n",
    "s = 'pneumonoultramicroscopicsilicovolcanoconiosis'\n",
    "print(s, 'contains', len(s), 'characters')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We will encounter <code>len()</code> in a number of places applied to objects that can contain a variable number of things."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<p class=\"try_it\"><b>Try it yourself.</b> Print the length of some of your favorite strings.</p>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Combining strings (concatenation) <a id=\"concatenation\"></a>\n",
    "\n",
    "We can combine strings using the <b>+</b> operator.\n",
    "\n",
    "Combining strings is called <b>concatenation</b>, which the Oxford English Dictionary\n",
    "defines as \"the action of linking things together in a series\"."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "hello = \"hello\"\n",
    "world = \"world\"\n",
    "space = \" \"\n",
    "hello_world = hello + space + world\n",
    "print(hello_world)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s = 'there is more than one way to kill a cat'\n",
    "t = 'than to choke it to death on butter'\n",
    "print(s + ' ' + t)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<p class=\"try_it\"><b>Try it yourself.</b> Concatenate some of your favorite strings.</p>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# String multiplication <a id=\"multiplication\"><a>\n",
    "\n",
    "You can obtain repetitions of a string by multiplying it by a whole number."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s = 'buffalo '\n",
    "print(3 * s)\n",
    "\n",
    "n = 8\n",
    "print(n * s)\n",
    "\n",
    "t = 4 * s\n",
    "print(t)\n",
    "\n",
    "print(9 * '$')\n",
    "print(9 * '$,')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "String concatenation and multiplication can be combined:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s = \"a horse! \"\n",
    "t = \"my kingdom for \"\n",
    "print(2*s + t + s)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<p class=\"try_it\"><b>Try it yourself.</b> Multiply some of your favorite strings.</p>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Converting other types into strings\n",
    "\n",
    "Objects of other types can be converted to strings using the function <code>str()</code>.  The name of this function is the same as the short-hand Python uses for the string type:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "n = 42      # n is an integer (whole number)\n",
    "s = str(n)  # s is a string\n",
    "\n",
    "print(n, type(n))\n",
    "print(s, type(s))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<p class=\"try_it\"><b>Try it yourself.</b> Convert 3.14 to a string and print the result.</p>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## <div class=\"danger\"></div> A gotcha: type names are not keywords <a id=\"gotcha\"></a>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In Python, type names are not keywords (in most other languages they are).\n",
    "\n",
    "This means, for example, that you are free to use <code>str</code>, which is the type for a character string in Python, as the name of a variable."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "str = 'this is a string.'  # This is legal, but clobbers the other meaning of str!\n",
    "print(str)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "However, as with many things in life (such as getting a tattoo or mixing beer and wine), just because you can do something doesn't make it a good idea.\n",
    "\n",
    "If you use type names as variable names, you may break things.  For instance, suppose we now want to convert an integer to a string:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "str = 'this is a string.'\n",
    "print(str)\n",
    "\n",
    "s = str(42)\n",
    "print(s)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This triggers an error because now <code>str</code> refers to a variable rather than a type.  The syntax <code>str(42)</code> looks like the syntax for calling a function (we will discuss functions anon), which makes no sense for a variable."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"danger\"></div> Don't use type names as variable names."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<p class=\"try_it\">\n",
    "<b>Try it yourself.</b>\n",
    "Replicate the type of error just discussed using the type name <code>int</code> (for integer) rather than <code>str</code>.\n",
    "</p>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Exercises <a id=\"exercises\"/>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"exercise\">\n",
    "<b>Exercise.</b>\n",
    "Use string concatenation to print the cry \"Thalassa! Thalassa!\" from Xenophon's <i>Anabasis</i>.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b>Answer.</b>\n",
    "<div class=\"voila\">\n",
    "Here is one solution:\n",
    "<pre>\n",
    "t = \"Thalassa! \"\n",
    "print(t + t)\n",
    "</pre>\n",
    "    \n",
    "If you want to be strict and avoid the space at the end, use   \n",
    "<pre>\n",
    "t = \"Thalassa!\"\n",
    "print(t + ' ' + t)\n",
    "</pre>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"exercise\">\n",
    "<b>Exercise.</b>\n",
    "Use string multiplication to print \"Thalassa! Thalassa!\".\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b>Answer.</b>\n",
    "<div class=\"voila\">\n",
    "Here is one solution:\n",
    "<pre>\n",
    "t = \"Thalassa! \"\n",
    "print(2*t)\n",
    "</pre>\n",
    "This solution results in an extra space at the end."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"exercise\">\n",
    "<b>Brevity is the soul of wit.</b>\n",
    "Write the most concise code you can to print 9 blank lines.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b>Answer.</b> \n",
    "<div class=\"voila\">\n",
    "Did you do better than this?\n",
    "<pre>\n",
    "print(9 * '\\n')\n",
    "</pre>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"exercise\">\n",
    "<b>Garbage in, garbage out.</b>\n",
    "Write code that will prompt the user for a string <code>s</code> and then print the string 7 times, once per line, using only a single <code>print()</code>.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b>Answer.</b> \n",
    "<div class=\"voila\">\n",
    "Here is one possible solution:\n",
    "<pre>\n",
    "s = input('Enter your string: ')\n",
    "print(7 * (s + '\\n'))\n",
    "</pre>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"exercise\">\n",
    "<b>Seeing stars.</b>  Write code that will prompt the user for a number <code>n</code> and then print <code>n</code> stars.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b>Answer.</b> \n",
    "<div class=\"voila\">\n",
    "Here is one possible solution.  Note the need to convert the string returned by the <code>input()</code> function to an integer.\n",
    "<code>\n",
    "n = int(input('Enter the desired number of stars: '))\n",
    "print(n &#42; '&#42;')\n",
    "</code>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"exercise\">\n",
    "<b>Your name.</b>\n",
    "Store your first, middle, and last name in separate variables, and then combine them into a single variable to print out your full name."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b>Answer.</b> \n",
    "<div class=\"voila\">\n",
    "For instance, if you were <a href=\"https://en.wikipedia.org/wiki/Norman_Borlaug\">Norm Borlaug</a>,\n",
    "<pre>\n",
    "a = \"Norman\"\n",
    "b = \"Ernest\"\n",
    "c = \"Borlaug\"\n",
    "s = a + \" \" + b + \" \" + c\n",
    "print(s)\n",
    "</pre>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h4>This notebook was brought to you by computers:</h4>\n",
    "\n",
    "A computer lets you make more mistakes faster than any other invention in human history, with the possible exception of handguns and tequila. <br/>\n",
    "&ndash; Mitch Radcliffe"
   ]
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
