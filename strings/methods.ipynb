{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"http://www.cs.wm.edu/~rml/images/wm_horizontal_single_line_full_color.png\">\n",
    "\n",
    "<h1 style=\"text-align:center;\">CSCI 141, Fall 2023 course notes</h1>\n",
    "<h1 style=\"text-align:center;\">String methods</h1>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Contents\n",
    "* [String methods](#methods)\n",
    "* [Chaining methods](#chaining)\n",
    "* [Exercises](#exercises)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# String methods <a id=\"methods\"/>\n",
    "\n",
    "String <b>methods</b> are built-in functions that act on strings.\n",
    "\n",
    "The string class has many methods that manipulate strings. [A complete list](https://docs.python.org/3/library/stdtypes.html#string-methods) of string methods can be found in the Python documentation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "joke = 'I could tell you a UDP joke, but you might not get it!'  # This cracks me up every time."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For example, the <code>lower()</code> and <code>upper()</code> methods convert the string to lower case and upper case, respectively:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(joke, end='\\n\\n')\n",
    "lo = joke.lower()\n",
    "print(lo, end='\\n\\n')\n",
    "up = joke.upper()\n",
    "print(up, end='\\n\\n')\n",
    "print(joke)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"danger\"></div> You should take a look at the complete list of string methods as they can save you a lot of time and effort."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Invoking string methods\n",
    "\n",
    "Observe how a string method is invoked</b>: rather than write <code>lower(joke)</code>, we write <code>joke.lower()</code>.\n",
    "\n",
    "This syntax makes clear that we are talking about the built-in string method <code>lower()</code> rather than some other function of the same name, and that we are applying it to <code>joke</code>."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The <code>count()</code> method counts the incidences of a string inside another string."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Count the number of times 'wombat' appears.\n",
    "print(joke.count('wombat'))\n",
    "print(joke.count('UDP'))\n",
    "print(joke.count('udp'))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"try_it\">\n",
    "<b>Try it yourself.</b>\n",
    "Use <code>count()</code> to count the number of times the word \"you\" appears in the joke.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The <code>count()</code> method is case-sensitive (i.e., it distinguishes between lower and upper case):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s = \"That that that that that refers to is not that that that I meant.\"\n",
    "print(s.count('that'))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s = \"Buffalo buffalo, Buffalo buffalo buffalo, buffalo Buffalo buffalo.\"\n",
    "print(s.count(\"buffalo\"))\n",
    "print(s.count(\"Buffalo\"))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Method chaining <a id=\"chaining\"/>\n",
    "\n",
    "String methods return strings, so we can invoke string methods on strings returned by string methods (got that?)\n",
    "\n",
    "For instance, here we use the <code>rstrip()</code> to remove all whitespace at the right end of the string, and then convert the result to lower case using <code>lower()</code>:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s = 'POSSUMBURGERS     '\n",
    "print(s + '|')\n",
    "\n",
    "# Remove any whitespace at the end and convert to lower case.\n",
    "# The vertical line shows the end of the string.\n",
    "print(s.rstrip().lower() + '|')\n",
    "print(s.lower().rstrip() + '|')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s = 'cat souffle'\n",
    "print(s.rstrip)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"try_it\">\n",
    "<b>Try it yourself.</b>\n",
    "Use <code>lstrip()</code> to remove the leading whitespace from the string\n",
    "<code>\n",
    "s = '    Python loves indentation'\n",
    "</code>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's apply method chaining to another string."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s = 'one potato, two potato, three potato, four'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The <code>find()</code> method can be used to find the location of the first character where a specified string is found, <b>where we start counting at zero</b>."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(s.find('potato'))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If the string does not appear, <code>find()</code> returns -1:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(s.find('yam'))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can use the <code>replace()</code> method to replace all instances of a substring:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(s.replace('potato', 'mississippi'))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"danger\"></div>\n",
    "The <code>replace()</code> method returns a new string &ndash; the original string is not changed by this operation:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(s)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's change 'potato' to 'mississippi' and count the number of times 'iss' appears:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(s.replace('potato', 'mississippi').count('iss'))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Before writing your own functions to manipulate strings you should see whether you cannot accomplish your goal using the built-in string methods together with method chaining."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(s.title())"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(s.replace('potato', 'mississippi').title())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Exercises <a id=\"exercises\"/>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"exercise\">\n",
    "<b>Exercise.</b>\n",
    "Let\n",
    "<pre>\n",
    "s = 'badger, badger, badger, badger'\n",
    "</pre>\n",
    "How would you convert all instances of \"badger\" to \"mushroom\" using string methods?\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b>Answer.</b>\n",
    "<div class=\"voila\">\n",
    "<pre>\n",
    "s = 'badger, badger, badger, badger'\n",
    "t = s.replace('badger', 'mushroom')\n",
    "print(t)\n",
    "</pre>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"exercise\">\n",
    "<b>Exercise.</b>\n",
    "Let\n",
    "<pre>\n",
    "s = 'badger, badger, badger, badger'\n",
    "</pre>\n",
    "How would you convert the string to read \"mushroom, mushroom\" using string methods?\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b>Answer.</b>\n",
    "<div class=\"voila\">\n",
    "<pre>\n",
    "s = 'badger, badger, badger, badger'\n",
    "t = s.replace('badger, badger', 'mushroom')\n",
    "print(t)\n",
    "</pre>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h4>This notebook was brought to you by software engineering:</h4>\n",
    "\n",
    "Programming today is a race between software engineers striving to build bigger and better idiot-proof programs, and the Universe trying to produce bigger and better idiots.  So far, the Universe is winning. <br/>\n",
    "&ndash; Douglas Adams"
   ]
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
