{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"http://www.cs.wm.edu/~rml/images/wm_horizontal_single_line_full_color.png\">\n",
    "\n",
    "\n",
    "<h1 style=\"text-align:center;\">CSCI 141-01, Fall 2023 course notes</h1>\n",
    "<h1 style=\"text-align:center;\">Exceptions</h1>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Contents\n",
    "\n",
    "* [Exceptions](#exceptions)\n",
    "* [Getting information about an exception](#info)\n",
    "* [Never trust users](#users)\n",
    "    * [The <code class=\"kw\">else</code> clause](#else)\n",
    "* [Raising an exception yourself](#raise)\n",
    "* [Re-raising an exception](#Rethrowing-exceptions)\n",
    "* [Exercises](#exercises)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Exceptions <a id=\"exceptions\"/>\n",
    "\n",
    "Python, like many other languages (e.g., C++), has [<b>exceptions</b>](https://docs.python.org/3/tutorial/errors.html).  You can think of an exception as a signal of an error.  Python has quite a few [built-in exceptions](https://docs.python.org/3/library/exceptions.html#concrete-exceptions).\n",
    "\n",
    "Rather than just falling over and dying when an exception occurs, Python allows you the chance to try to recover from problems by means of <b>exception handling</b>.\n",
    "\n",
    "For instance, let's look at what happens when I try to convert a random string to an integer."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s = \"I'm not an integer!\"\n",
    "n = int(s)\n",
    "print(n)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The <code class=\"kw\">try</code> statement <a id=\"try\"/>\n",
    "\n",
    "Rather than just crashing with an error, it would be more graceful to recover and tell the user they made a mistake. \n",
    "\n",
    "We can respond to exceptions with the [<code class=\"kw\">try</code>](https://docs.python.org/3/reference/compound_stmts.html?highlight=try#the-try-statement) statement.\n",
    "\n",
    "A <code class=\"kw\">try</code> statement consists of the following parts:\n",
    "```python\n",
    "try: \n",
    "    # Attempt to execute the block of code here.\n",
    "except:\n",
    "    # Execute this block of code if and only an exception occured in the try block.  \n",
    "    # There may be multiple blocks to handle different errors, but at most one will be executed.\n",
    "else: (# optional)\n",
    "    # Like the else block in a while or for statement, this code is executed after the try block\n",
    "    # if the try block does not raise an exception.\n",
    "finally: (# optional)\n",
    "    # This block is executed after the try-except-else blocks; it is typically used to clean up.\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Bogus string masquerading as an integer: \"I'm not an integer!\"\n"
     ]
    }
   ],
   "source": [
    "s = \"I'm not an integer!\"\n",
    "\n",
    "try:    # Try converting the string to an integer.\n",
    "    n = int(s)\n",
    "    print(n)\n",
    "except: # Deal with all exceptions.  Catch-all blocks like this one are considered bad practice.\n",
    "    print(f'''Bogus string masquerading as an integer: \"{s}\"''')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It is possible that a single statement could throw more than one error.  Consider the assignment to <code>c</code> in the two following blocks of code."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = 1\n",
    "b = 1\n",
    "c = a[0]/b"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "a = [1]\n",
    "b = 0\n",
    "c = a[0]/b"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the first block, <code>a[0]/b</code> triggered a <b>TypeError</b> because <code>a</code> is not a list.\n",
    "\n",
    "In the second block, <code>a[0]/b</code> triggered a <b>ZeroDivisionError</b> because <code>b</code> is zero.\n",
    "\n",
    "As introduced above, the <b>try-except</b> construct is a blunt instrument that catches both exceptions without discriminating between them."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = 1\n",
    "b = 0\n",
    "try:\n",
    "    c = a[0]/b\n",
    "except:\n",
    "    print('Something went wrong!')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "However, this is not very helpful.  Frequently our responses to two different exceptions are not the same.\n",
    "\n",
    "Happily, the <b>try-except</b> construct allows us to differentiate between the two exceptions using the names of the exceptions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "The numerator is not subscriptable!\n",
      "1.0\n"
     ]
    }
   ],
   "source": [
    "a = 1\n",
    "b = 1\n",
    "try:\n",
    "    c = a[0]/b\n",
    "except TypeError:\n",
    "    print('The numerator is not subscriptable!')\n",
    "    c = a/b\n",
    "except ZeroDivisionError:\n",
    "    print('The denominator is zero!')\n",
    "    c = float('inf')  # This is floating point infinity.\n",
    "\n",
    "print(c)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "The denominator is zero!\n",
      "inf\n"
     ]
    }
   ],
   "source": [
    "a = [1]\n",
    "b = 0\n",
    "try:\n",
    "    c = a[0]/b\n",
    "except TypeError:\n",
    "    print('The numerator is not indexable!')\n",
    "    c = a/b\n",
    "except ZeroDivisionError:\n",
    "    print('The denominator is zero!')\n",
    "    c = float('inf')\n",
    "    \n",
    "print(c)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If the following example, the first exception encountered is <code>a[0]</code>.  The attempt to index <code>a</code> raises an exception before there is an attempt to divide by zero."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = 1\n",
    "b = 0\n",
    "try:\n",
    "    c = a[0]/b\n",
    "except TypeError:\n",
    "    print('The numerator is not indexable!')\n",
    "except ZeroDivisionError:\n",
    "    print('The denominator is zero!')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"danger\"></div>\n",
    "You should be as specific in handling exceptions as possible.  A  good practice is to only catch the errors you are interested in catching."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A generic <code class=\"kw\">except</code> class that handles all exceptions is considered bad form, as it suggests you don't know what exceptions could occur and maybe you should think some more.  Some exceptions are relatively benign and can be ignored."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Exceptions vs assertions\n",
    "\n",
    "You should use assertions to find programming errors.  Assertions are used to detect situations that should never occur.\n",
    "\n",
    "You should use exceptions to handle problems that can arise due to external causes even when the program is perfect. For example, the square root function expects a nonnegative input:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from math import sqrt\n",
    "print(sqrt(-1))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Rather than quitting the function raises an exception and returns control to the user (provided they use a <code class=\"kw\">try</code> block)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Getting information about an exception <a id=\"info\"/>\n",
    "\n",
    "We can get more information about an exception by printing it.\n",
    "\n",
    "The simplest approach is to do the following:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = 1\n",
    "b = 0\n",
    "try:\n",
    "    c = a/b\n",
    "except ZeroDivisionError as err:\n",
    "    print('Coping with', err)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "or"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "try:\n",
    "    c = a/b\n",
    "except Exception as ex:\n",
    "    print('Coping with', type(ex).__name__)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Never trust users <a id=\"users\"/>\n",
    "\n",
    "Users are an enormous annoyance to software developers, hence terms like \"PEBKAC\" (problem exists between keyboard and chair) and \"ID10T error\"."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"danger\"></div>\n",
    "Error checking and exception handling is especially important in dealing with user input &ndash; you can't spell \"abuser\" without \"user\"!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For instance, look what happens if we enter a float when asked for an integer.  Enter 42.1 at the prompt:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "m = int(input('Enter an integer: '))\n",
    "print('m:', m)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "What went wrong here?\n",
    "\n",
    "<b>Answer</b> <br/>\n",
    "<div class=\"voila\">\n",
    "We tried to convert the string <code>'42.1'</code> to an integer, but it doesn't look like one.  On the other hand, <code>int(42.1)</code> works just fine since the float <code>42.1</code> is unambiguously a number.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This is an excellent place for a <code class=\"kw\">try</code> block.  Enter 42.1 at the prompt:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "try:\n",
    "    s = input('Enter an integer: ')\n",
    "    n = int(s)\n",
    "except ValueError as err:\n",
    "    print('Your input \"' + s + '\" was garbage.  Go away.')\n",
    "\n",
    "print('n:', n)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The <code class=\"kw\">else</code> clause  <a id=\"else\"/>\n",
    "\n",
    "We still have a problem here &ndash; if the <code class=\"kw\">try</code> block fails, then the variable <code>n</code> is not defined, and we get into trouble later when we try to print <code>n</code>.\n",
    "\n",
    "We can fix this using an <code class=\"kw\">else</code> clause, since such a clause will only be executed if the <code class=\"kw\">try</code> block did not raise an exception:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "try:\n",
    "    n = int(input('Enter an integer: '))\n",
    "except:\n",
    "    print('Your input was garbage. Go away.')\n",
    "else:\n",
    "    print('n:', n)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, we probably want to use a <code class=\"kw\">while</code> loop to keep prompting the user for acceptable input.  Note the location of\n",
    "```python\n",
    "not_done = False\n",
    "```\n",
    "which causes the loop to terminate in the following code:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "not_done = True\n",
    "while (not_done):\n",
    "    try:\n",
    "        n = int(input('Enter an integer: '))\n",
    "    except:\n",
    "        print('Your input was garbage. Try again.')\n",
    "    else:\n",
    "        print('n:', n)\n",
    "        not_done = False"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Raising an exception yourself <a id=\"raise\"></a>\n",
    "\n",
    "You can raise exceptions yourself (in other languages this is called \"throwing an exception\", which sounds more fun).\n",
    "\n",
    "You can throw a generic exception, but this does not convey any information other than \"something went wrong\".  That is a pretty blunt instrument and provides little guidance on how to respond."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"danger\"></div> Use the most specific exception that fits what you wish to convey."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Some common types of exceptions are\n",
    "* [<code>RuntimeError</code>](https://docs.python.org/3/library/exceptions.html#RuntimeError),\n",
    "* [<code>TypeError</code>](https://docs.python.org/3/library/exceptions.html#TypeError),\n",
    "* [<code>ValueError</code>](https://docs.python.org/3/library/exceptions.html#ValueError).\n",
    "\n",
    "Suppose we wrote our own square root function, and we wish to take exception to negative inputs.  We could do so as follows:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def sqrt(x):\n",
    "    if (x < 0):\n",
    "        raise ValueError('sqrt() was given a negative input!')\n",
    "    # blah, blah, blah..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let's provoke the exception."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "try:\n",
    "    i = sqrt(-1)\n",
    "except ValueError as ex:\n",
    "    print('Well, that didn\\'t go well!', ex)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Re-raising exceptions\n",
    "\n",
    "Sometimes you'll encounter an exception raised by a function called inside another function.  If the calling function can't fix the problem, it will likely want to raise the exception again to alert whoever called it that there is a problem.  This is called \"re-raising\" or \"rethrowing\" the exception.\n",
    "\n",
    "Here is an illustration using the geometric mean.  The geometric mean of $a$ and $b$ is $\\sqrt{ab}$; this is only defined if $a$ and $b$ have the same sign.  Here we catch a <code class=\"kw\">ValueError</code> exception that <code>math.sqrt()</code> raises."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Ack! a bad input was encountered: math domain error\n"
     ]
    }
   ],
   "source": [
    "import math\n",
    "\n",
    "def geomean(a, b):\n",
    "    '''Compute geometric mean of two numbers.'''\n",
    "    try:\n",
    "        math.sqrt(a*b)\n",
    "    except ValueError:  # Let the caller know something went wrong.\n",
    "        raise\n",
    "\n",
    "try:\n",
    "    g = geomean(1, -1)\n",
    "    print(g)\n",
    "except ValueError as ex:\n",
    "    print('Ack! a bad input was encountered:', ex)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Exercises <a id=\"exercises\"/>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"exercise\">\n",
    "<b>Exercise.</b>\n",
    "    \n",
    "How would you use <code class=\"kw\">try</code> to safeguard against taking the square root of a negative number when you call <code>math.sqrt()</code> from the <code>math</code> module?\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b>Answer.</b>\n",
    "<div class=\"voila\">\n",
    "Here is one idea:\n",
    "<pre>\n",
    "import math\n",
    "\n",
    "try:\n",
    "    i = math.sqrt(-1)\n",
    "except:\n",
    "    print('Oops! the call to sqrt() failed!')\n",
    "</pre>    \n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h4>This notebook is brought to you by first lines:</h4>\n",
    "\n",
    "He was born with a gift of laughter and a sense that the world was mad. <br/>\n",
    "&ndash; Rafael Sabatini, Scaramouche"
   ]
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
