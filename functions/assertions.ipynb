{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"http://www.cs.wm.edu/~rml/images/wm_horizontal_single_line_full_color.png\">\n",
    "\n",
    "\n",
    "<h1 style=\"text-align:center;\">CSCI 141-01, Fall 2023 course notes</h1>\n",
    "<h1 style=\"text-align:center;\">Assertions and <code class=\"kw\">&lowbar;&lowbar;debug&lowbar;&lowbar;</code></h1>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Contents\n",
    "\n",
    "* [Assertions](#assertions)\n",
    "* [When and why to use assertions](#when_and_why)\n",
    "* [An illustration](#illustration)\n",
    "* [The built-in variable <code class=\"kw\">&lowbar;&lowbar;debug&lowbar;&lowbar;</code>](#debug)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Assertions <a id=\"assertions\"></a>\n",
    "\n",
    "The <code class=\"kw\">assert</code> statement terminates execution if a specified condition is not satisfied:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def foo(x):\n",
    "    assert x > 0, 'Negative argument encountered in foo()!!'\n",
    "    \n",
    "foo(-42)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The syntax is <code>assert boolean expression [, optional string to print if boolean is false]</code>\n",
    "\n",
    "The error message is optional:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def foo(x):\n",
    "    assert x > 0\n",
    "    \n",
    "foo(-42)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "However, the assertion should provide helpful information.  Here we print the offending input value:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def foo(x):\n",
    "    assert x > 0, f'The offending value is {x}.'\n",
    "    \n",
    "foo(-42)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# When and why to use assertions <a id=\"when_and_why\"></a>\n",
    "\n",
    "Here are some reasons you should include assertions in your code:\n",
    "* Assertions give run-time checks of assumptions that you would have otherwise put in code comments.\n",
    "* An assertion expresses (and enforces!) in code what you assume to be true about data (preconditions) at a particular point in your program.\n",
    "* With assertions, failures appear earlier and closer to the locations of the errors, which make them easier to diagnose and fix.\n",
    "* Bugs often appear because you didn't understand all the conditions under which a piece of code can be executed, and assertions can catch this.\n",
    "* Assertions help catch errors that creep in when rewriting code."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "From the <a href=\"https://wiki.python.org/moin/UsingAssertionsEffectively\">Python wiki</a>:\n",
    "<blockquote>\n",
    "Assertions are a systematic way to check that the internal state of a program is as the programmer expected, with the goal of catching bugs. In particular, they're good for catching false assumptions that were made while writing the code, or abuse of an interface by another programmer. In addition, they can act as in-line documentation to some extent, by making the programmer's assumptions obvious. (\"Explicit is better than implicit.\")\n",
    "\n",
    "&hellip;    \n",
    "\n",
    "Places to consider putting assertions:\n",
    "\n",
    "* checking parameter types, classes, or values\n",
    "* checking data structure invariants\n",
    "* checking \"can't happen\" situations (duplicates in a list, contradictory state variables.)\n",
    "* after calling a function, to make sure that its return is reasonable\n",
    "\n",
    "The overall point is that if something does go wrong, we want to make it completely obvious as soon as possible.\n",
    "\n",
    "It's easier to catch incorrect data at the point where it goes in than to work out how it got there later when it causes trouble.\n",
    "\n",
    "Assertions are not a substitute for unit tests or system tests, but rather a complement. Because assertions are a clean way to examine the internal state of an object or function, they provide \"for free\" a clear-box assistance to a black-box test that examines the external behaviour.\n",
    "\n",
    "Assertions should *not* be used to test for failure cases that can occur because of bad user input or operating system/environment failures, such as a file not being found. Instead, you should raise an exception, or print an error message, or whatever is appropriate. One important reason why assertions should only be used for self-tests of the program is that assertions can be disabled at compile time.\n",
    "</blockquote>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "That last paragraph is important.  If you encounter bad user input or some other situation out of your control, you should use an [exception](fun_exceptions.ipynb).  Assertions are for catching situations that should never occur."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"danger\"></div>\n",
    "Don't use an assertion where you should use an exception, and vice versa."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# An illustration <a id=\"illustration\"></a>\n",
    "\n",
    "Consider the following example, which is modeled on a student bug from the Fall 2018 semester.  It illustrates an idea mentioned above:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"danger\"></div>\n",
    "If something goes wrong, we want to make it completely obvious as soon as possible.  It's easier to catch incorrect data at the point where it can first be detected than to work out how it got there later when it causes trouble."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This is sometimes called <b>fail-fast</b> programming."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here's the buggy code:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def foo(code):\n",
    "    '''\n",
    "    Prefrobnicate the whatsit.\n",
    "    \n",
    "    Arguments:\n",
    "    code -> action code (1, 3, or 42)\n",
    "    '''\n",
    "    if code == 1:\n",
    "        return 29\n",
    "    elif code == 3:\n",
    "        return 28\n",
    "    elif code == 42:\n",
    "        return 54\n",
    "    \n",
    "def bar(whatsit):\n",
    "    '''\n",
    "    Complete the frobnication of the whatsit.\n",
    "    \n",
    "    Arguments:\n",
    "    whatsit -> the prefrobnicated whatsit from foo() (29, 28, or 54)\n",
    "    '''\n",
    "    if whatsit == 29:\n",
    "        return 17\n",
    "    else:\n",
    "        return whatsit\n",
    "    \n",
    "answer = foo(7)\n",
    "\n",
    "# ...20 more lines of code...\n",
    "\n",
    "answer = bar(answer)\n",
    "x = 2 * answer"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Can you spot the bug?\n",
    "\n",
    "<b>Answer</b>\n",
    "<div class=\"voila\">\n",
    "If the input to <code>foo()</code> is not 1, 3, or 42, we fall off the end of the function and return <code>None</code>.  This value is then passed to <code>bar()</code> and returned unchanged.  The bug doesn't reveal itself until we attempt <code>2*None</code>.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"danger\"></div> What made this bug so vexing, and why the student couldn't find it, was that the location of the bug was far from the place where it revealed itself.  He spent a lot of time looking at <code>bar()</code> when the problem was in <code>foo()</code>."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Observe that in this example we have carefully documented <code>foo()</code>, including the assumptions on its input.  However, comments don't affect the code (there's an old saying, \"Debug the code, not the comments\").  We could do better if we checked those assumptions, rather than just including them as a comment."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def foo(code):\n",
    "    '''\n",
    "    Prefrobnicate the whatsit.\n",
    "    \n",
    "    Arguments:\n",
    "    code    -> action code (1, 3, or 42)\n",
    "    '''\n",
    "    assert code in [1, 3, 42], f'Bogus input to foo(): {code}.'\n",
    "    if code == 1:\n",
    "        return 29\n",
    "    elif code == 3:\n",
    "        return 28\n",
    "    elif code == 42:\n",
    "        return 54\n",
    "    \n",
    "def bar(whatsit):\n",
    "    '''\n",
    "    Complete the frobnication of the whatsit.\n",
    "    \n",
    "    Arguments:\n",
    "    whatsit -> the prefrobnicated whatsit from foo() (29, 28, or 54)\n",
    "    '''\n",
    "    if whatsit == 29:\n",
    "        return 17\n",
    "    else:\n",
    "        return whatsit\n",
    "    \n",
    "answer = foo(7)\n",
    "\n",
    "# ...20 more lines of code...\n",
    "\n",
    "answer = bar(answer)\n",
    "x = 2 * answer"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we catch the bug where it lurks."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# The built-in variable <code class=\"kw\">&lowbar;&lowbar;debug&lowbar;&lowbar;</code>  <a id=\"debug\"/>\n",
    "\n",
    "Python has a globally visible built-in variable named [<code class=\"kw\">&lowbar;&lowbar;debug&lowbar;&lowbar;</code>](https://docs.python.org/3/library/constants.html#debug__).  Its value is fixed at the start of execution.\n",
    "\n",
    "[Per the Python documentation](https://docs.python.org/3/reference/simple_stmts.html#assert), the assertion"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "ename": "AssertionError",
     "evalue": "Bogus input to foo(): 54.",
     "output_type": "error",
     "traceback": [
      "\u001b[0;31m---------------------------------------------------------------------------\u001b[0m",
      "\u001b[0;31mAssertionError\u001b[0m                            Traceback (most recent call last)",
      "\u001b[0;32m<ipython-input-2-3e015bae957f>\u001b[0m in \u001b[0;36m<module>\u001b[0;34m\u001b[0m\n\u001b[1;32m      1\u001b[0m \u001b[0mcode\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0;36m54\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m----> 2\u001b[0;31m \u001b[0;32massert\u001b[0m \u001b[0mcode\u001b[0m \u001b[0;32min\u001b[0m \u001b[0;34m[\u001b[0m\u001b[0;36m1\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0;36m3\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0;36m42\u001b[0m\u001b[0;34m]\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0;34mf'Bogus input to foo(): {code}.'\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m",
      "\u001b[0;31mAssertionError\u001b[0m: Bogus input to foo(): 54."
     ]
    }
   ],
   "source": [
    "code = 54\n",
    "assert code in [1, 3, 42], f'Bogus input to foo(): {code}.'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "is implemented as if it were"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "ename": "AssertionError",
     "evalue": "Bogus input to foo(): 54.",
     "output_type": "error",
     "traceback": [
      "\u001b[0;31m---------------------------------------------------------------------------\u001b[0m",
      "\u001b[0;31mAssertionError\u001b[0m                            Traceback (most recent call last)",
      "\u001b[0;32m<ipython-input-3-0d1b947ed867>\u001b[0m in \u001b[0;36m<module>\u001b[0;34m\u001b[0m\n\u001b[1;32m      1\u001b[0m \u001b[0;32mif\u001b[0m \u001b[0m__debug__\u001b[0m\u001b[0;34m:\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m      2\u001b[0m     \u001b[0;32mif\u001b[0m \u001b[0mcode\u001b[0m \u001b[0;32mnot\u001b[0m \u001b[0;32min\u001b[0m \u001b[0;34m[\u001b[0m\u001b[0;36m1\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0;36m3\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0;36m42\u001b[0m\u001b[0;34m]\u001b[0m\u001b[0;34m:\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m----> 3\u001b[0;31m         \u001b[0;32mraise\u001b[0m \u001b[0mAssertionError\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34mf'Bogus input to foo(): {code}.'\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m",
      "\u001b[0;31mAssertionError\u001b[0m: Bogus input to foo(): 54."
     ]
    }
   ],
   "source": [
    "if __debug__:\n",
    "    if code not in [1, 3, 42]:\n",
    "        raise AssertionError(f'Bogus input to foo(): {code}.')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The variable <code class=\"kw\">&lowbar;&lowbar;debug&lowbar;&lowbar;</code> always has the value <code class=\"kw\">True</code> unless Python is invoked with the <code>-O</code> option (for \"optimize\"), in which case it is <code class=\"kw\">False</code>.  That is, the <code>-O</code> option disables your assertions, in case you are concerned about cost of checking the assertions.  This is what is meant above by \n",
    "<blockquote>\n",
    "One important reason why assertions should only be used for self-tests of the program is that assertions can be disabled at compile time.\n",
    "</blockquote>    "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(__debug__)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"try_it\"><b>Try it yourself.</b> What happens if you try to change the value of <code class=\"kw\">&lowbar;&lowbar;debug&lowbar;&lowbar;</code>?</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h4>This notebook was brought to you by artificial intelligence:</h4>\n",
    "\n",
    "My answer is, 'Hey, it's pretty cheap to produce humans, some of whom can think pretty well.'<br/>\n",
    "&ndash; David Kuck, on the need for a thinking computer"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
