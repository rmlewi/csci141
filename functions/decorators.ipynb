{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"http://www.cs.wm.edu/~rml/images/wm_horizontal_single_line_full_color.png\">\n",
    "\n",
    "<h1 style=\"text-align:center;\">CSCI 141-01, Fall 2023 course notes</h1>\n",
    "<h1 style=\"text-align:center;\">Function decorators 🤯</h1>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Contents\n",
    "\n",
    "* [Functions as objects](#objects)\n",
    "* [Modifying the behavior of a function by hand](#transmogrification)\n",
    "* [Decorators](#decorators)\n",
    "* [Decorators in action](#numba)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Functions as objects <a id=\"objects\"></a>\n",
    "\n",
    "We have seen that in Python, ints, floats, &c., are objects.\n",
    "\n",
    "Functions are also objects in Python.  This means that functions can take functions as their inputs and return functions as their output.  Because functions can take functions as inputs and return functions as outputs, we can modify functions using other functions.\n",
    "\n",
    "We can either do so by hand, or by using Python <b>decorators</b>."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Modifying the behavior of a function by hand <a id=\"transmogrification\"></a>\n",
    "\n",
    "Below is a function <code>add_hello()</code> that takes a function <code>fun()</code> as its input.\n",
    "\n",
    "It then declares a new function, <code>friendly_fun()</code>, that calls <code>fun()</code> but adds a friendly print statement introducing <code>fun()</code> before the call.\n",
    "\n",
    "The function <code>friendly_fun()</code> is returned by <code>add_hello()</code> and can then be used elsewhere."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def make_friendly(fun):\n",
    "    \"\"\"\n",
    "    Add a friendly welcome to a function.\n",
    "    \"\"\"\n",
    "    def friendly_fun(*args, **kwds):\n",
    "        print(f'Hello. My name is {fun.__name__}.')\n",
    "        fun(*args, **kwds)\n",
    "\n",
    "    return friendly_fun"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def sq(x):\n",
    "    print(x**2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x = 8\n",
    "sq(x)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "friendly_sq = make_friendly(sq)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "friendly_sq(6)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Decorators <a id=\"decorators\"></a>\n",
    "\n",
    "Python <b>decorators</b> are a shorthand for making changes to functions.  Here is the syntax:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "@make_friendly\n",
    "def cube(x):\n",
    "    print(x**3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The preceding is equivalent to\n",
    "```python\n",
    "cube = make_friendly(cube)\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cube(3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Decorators aren't truly necessary, but they make the programming idiom prettier. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Decorators in action  <a id=\"numba\"/>\n",
    "\n",
    "Decorators seem rather esoteric, but they can be dead useful.  Here we look at two examples."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Gradescope\n",
    "\n",
    "Gradescope provides a `gradescope_utils` module that, among other things, contains several decorators.\n",
    "\n",
    "The Gradescope decorators I use the most in the autograders are\n",
    "<pre>\n",
    "@number\n",
    "@weight\n",
    "@partial_credit\n",
    "</pre>\n",
    "The `@number` decorator alters the test functions so they output a sequence number.  This allows the test results to be presented to you in an order I specify.\n",
    "\n",
    "The `@weight` decorator alters the test functions so that they output a specified score for grading.\n",
    "\n",
    "The `@partial_credit` decorator alters the test functions in a way that makes it possible to assign partial credit."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Numba \n",
    "\n",
    "Numba is another example of decorators in action.  [Numba](https://numba.pydata.org) is a compiler for Python that generates machine level (compiled) code, which runs <b>much</b> faster than interpreted code.  You can use it to compile functions that are computationally intensive.  Best of all, applying Numba involves nothing more adding a decorator.\n",
    "\n",
    "Numba is an example of a <b>just in time (JIT)</b> compiler.  JIT compilers wait until they encounter the code they are to compile during execution of the program.\n",
    "\n",
    "First let's install Numba:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!pip3 install numba"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's start with regular Python code, and print how long each function call takes:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import random\n",
    "\n",
    "def monte_carlo_pi(nsamples):\n",
    "    '''\n",
    "    Estimate π using Monte Carlo integration.\n",
    "    '''\n",
    "    acc = 0\n",
    "    for i in range(nsamples):\n",
    "        x = random.random()\n",
    "        y = random.random()\n",
    "        if (x**2 + y**2) < 1.0:\n",
    "            acc += 1\n",
    "    return 4.0 * (acc / nsamples)\n",
    "\n",
    "nsamples = 10_000_000\n",
    "\n",
    "from time import perf_counter\n",
    "\n",
    "t_start = perf_counter()\n",
    "monte_carlo_pi(nsamples)\n",
    "t_stop = perf_counter()\n",
    "print(f'elapsed time {t_stop - t_start:f} sec')\n",
    "\n",
    "t_start = perf_counter()\n",
    "monte_carlo_pi(nsamples)\n",
    "t_stop = perf_counter()\n",
    "print(f'elapsed time {t_stop - t_start:f} sec')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Applying Numba is simple &ndash; you import the <code>numba</code> module and add a decorator to the function you want to compile:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from numba import jit\n",
    "import random\n",
    "\n",
    "@jit(nopython=True)\n",
    "def monte_carlo_pi(nsamples):\n",
    "    '''\n",
    "    Estimate π using Monte Carlo integration.\n",
    "    '''\n",
    "    acc = 0\n",
    "    for i in range(nsamples):\n",
    "        x = random.random()\n",
    "        y = random.random()\n",
    "        if (x**2 + y**2) < 1.0:\n",
    "            acc += 1\n",
    "    return 4.0 * (acc / nsamples)\n",
    "\n",
    "nsamples = 10_000_000\n",
    "\n",
    "from time import perf_counter\n",
    "\n",
    "t_start = perf_counter()\n",
    "monte_carlo_pi(nsamples)\n",
    "t_stop = perf_counter()\n",
    "print(f'elapsed time {t_stop - t_start:f} sec')\n",
    "\n",
    "t_start = perf_counter()\n",
    "monte_carlo_pi(nsamples)\n",
    "t_stop = perf_counter()\n",
    "print(f'elapsed time {t_stop - t_start:f} sec')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The first call is slower than the second because that is when we incur the cost of the JIT compilation.\n",
    "\n",
    "As you can see, the compiled code is considerably faster."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h4>This notebook was brought to you by doubt:</h4>\n",
    "\n",
    "Doubt is not a pleasant condition, but certainty is an absurd one. <br/>\n",
    "&ndash; Voltaire, Lettres du Prince Royal de Prusse"
   ]
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
