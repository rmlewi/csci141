{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"http://www.cs.wm.edu/~rml/images/wm_horizontal_single_line_full_color.png\">\n",
    "\n",
    "<h1 style=\"text-align:center;\">CSCI 141-01, Fall 2023 course notes</h1>\n",
    "<h1 style=\"text-align:center;\">Function arguments</h1>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Contents\n",
    "* [Positional arguments](#positional)\n",
    "* [Default values and keyword parameters](#default_values)\n",
    "* [Exercises](exercises)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Positional arguments <a id=\"positional\"></a>\n",
    "\n",
    "So far we have defined and called functions using only what are called <b>positional</b> arguments.  This means that the function interprets its inputs based on their ordering in the input list.\n",
    "\n",
    "For instance, consider the following function for computing $x^{n}$:\n",
    "```python\n",
    "def pow(x,n):\n",
    "    return x**n\n",
    "```\n",
    "When we call the function,\n",
    "```python\n",
    "y = pow(42,3)\n",
    "```\n",
    "the first argument <code>42</code> is interpreted as the value of <code>x</code> in the function, and the second argument <code>n</code> is interpreted as the value of <code>n</code> in the function, based on their positions in the calling sequence.\n",
    "\n",
    "However, Python allows greater flexibility that this.  It allows us to set default values for inputs so we can omit them most of the time, and it allows us to pass inputs by name rather than relying on a particular ordering of the inputs.  It also allows us to have functions where the number of inputs can vary."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Default values and keywords <a id=\"default_values\"/>\n",
    "\n",
    "You can set default values for arguments passed to a function.  It is often the case that  there is a natural default value that will almost always be used, but where you still want the option of overriding the default value.\n",
    "\n",
    "For instance, consider the area of a circle of radius $r$: $\\pi r^{2}$.\n",
    "Suppose we also want to be able to compute area in the metric system, where $\\pi = 10$.  Then we could write an area function that looked like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "def area(r, pi=3.1416):\n",
    "    return pi * r**2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We have assigned a default value to <code>pi</code> which is the value we would typically use.  However, we can change the value of <code>pi</code>, say, if we are working in the metric system."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let's see how we can call this one function to find both regular and metric areas:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "English: 12.5664\n",
      "Metric:  40\n",
      "Metric:  40\n"
     ]
    }
   ],
   "source": [
    "r = 2\n",
    "# Regular usage: use the default value of pi, and we can omit pi as an input.\n",
    "print('English:', area(r))\n",
    "\n",
    "# Metric system usage: now pi has the value of 10.\n",
    "print('Metric: ', area(r, 10))\n",
    "\n",
    "# This also works for metric.\n",
    "print('Metric: ', area(r, pi=10))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Thus example shows the use of <b>keywords</b> and <b>default values</b> in function calls.  In this case, in the call to the function <code>pi</code> is a keyword indicating which variable we are talking about.\n",
    "\n",
    "Keywords allow us to pass arguments <b>without regard to their position in the argument list in the function's definition</b>."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "40\n"
     ]
    }
   ],
   "source": [
    "# Out of order.\n",
    "print(area(pi=10, r=2))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "However, we cannot do so willy-nilly:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "ename": "SyntaxError",
     "evalue": "positional argument follows keyword argument (1701539858.py, line 1)",
     "output_type": "error",
     "traceback": [
      "\u001b[0;36m  Cell \u001b[0;32mIn[5], line 1\u001b[0;36m\u001b[0m\n\u001b[0;31m    print(area(pi=10, 2))\u001b[0m\n\u001b[0m                       ^\u001b[0m\n\u001b[0;31mSyntaxError\u001b[0m\u001b[0;31m:\u001b[0m positional argument follows keyword argument\n"
     ]
    }
   ],
   "source": [
    "print(area(pi=10, 2))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The reason for the error is that when Python encounters the argument <code>2</code> after the argument <code>pi=10</code>, it is uncertain which argument is meant since the <code>pi=10</code> indicates that things could be out of order.  Positional arguments, <code>2</code> in this case, must precede keyword arguments in the function call."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"try_it\">\n",
    "<b>Try it yourself.</b>\n",
    "\n",
    "Call the function <code>area()</code> to find the area of a circle of radius 3 in the metric system.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## When should pass variables by keyword?\n",
    "\n",
    "Calling functions by keyword is especially useful when you have a function with a large number of arguments with default settings, and you only want to change one or two of the defaults.\n",
    "\n",
    "Since you pass the arguments by keyword, you don't have to know the order in which they appear in the function definition argument list.\n",
    "\n",
    "For instance, do you really want to have to know all the arguments for a function [like this one](http://scikit-learn.org/stable/modules/generated/sklearn.neural_network.MLPClassifier.html#sklearn.neural_network.MLPClassifier) and pass them in the correct order with acceptable values every time you call the function?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# When should we set defaults?\n",
    "\n",
    "I suggest you set defaults whenever there are sensible default values and it is not likely users will want to specify all the inputs to a function.  \n",
    "\n",
    "For instance, consider [this function to create a support vector classifier](https://scikit-learn.org/stable/modules/generated/sklearn.svm.NuSVC.html).  Most of the arguments require you know the mathematical formulation of how the classification scheme is computed, as well as the numerical algorithms for its solution.  This is likely beyond the ken of many users so we should set reasonable default values while allowing the expert the ability to experiment by varying them."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Exercises  <a id=\"exercises\"/>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"exercise\">\n",
    "Implement your own version of <a href=\"https://docs.python.org/3/library/math.html#math.isclose\">the <code>isclose()</code> function</a>  in the Python <code>math</code> module.  You should use the same default values for the inputs.\n",
    "</div>    "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Answer.**\n",
    "<div class=\"voila\">\n",
    "My solution sacrifices efficiency for readability:    \n",
    "<pre>    \n",
    "def isclose(a, b, *, rel_tol=1e-09, abs_tol=0.0):\n",
    "    abs_close = (abs(a-b) <= abs_tol)\n",
    "    rel_close = (abs(a-b) <= rel_tol * max(abs(a), abs(b)))\n",
    "    return (abs_close or rel_close)\n",
    "\n",
    "print(isclose(1,2))\n",
    "print(isclose(1_000_000_000, 1_000_000_001))\n",
    "</pre>\n",
    "</div>    "
   ]
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
