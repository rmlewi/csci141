{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"http://www.cs.wm.edu/~rml/images/wm_horizontal_single_line_full_color.png\">\n",
    "\n",
    "<h1 style=\"text-align:center;\">CSCI 141-01, Fall 2023 course notes</h1>\n",
    "<h1 style=\"text-align:center;\">Lambda functions</h1>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Contents\n",
    "\n",
    "* [Lambda functions](#lambdas)\n",
    "* [What are lambdas good for?](#usage)\n",
    "    * [Functions that take simple functions as inputs](#lightweight)  \n",
    "    * [Functions that create functions](#returning_functions)\n",
    "    * [Binding function arguments](#binding)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Lambda functions <a id=\"lambdas\"/>\n",
    "\n",
    "<b>Lambda expressions</b> are a special way of defining functions.  They are very handy for defining tiny functions called [<b>lambda functions</b> or <b>anonymous functions</b>](https://en.wikipedia.org/wiki/Anonymous_function#Python) that are a single expression.  The name comes from the [lambda calculus](https://en.wikipedia.org/wiki/Lambda_calculus), a mathematical model of computation developed by the American logician [Alonzo Church](https://en.wikipedia.org/wiki/Alonzo_Church) in the 1930s.\n",
    "\n",
    "There are special rules concerning lambdas:\n",
    "* The body can consist of only one line.\n",
    "* The body can only can contain an expression (no statements or other elements).\n",
    "* The result of the expression is automatically returned; no <code class=\"kw\">return</code> statement is needed.\n",
    "\n",
    "Here are two versions of the absolute value function (of course, you should really use [the built-in <code>abs()</code> function!](https://docs.python.org/3/library/functions.html#abs)).  The lambda expression is indicated by the keyword <code class=\"kw\">lambda</code>."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def abs(x):\n",
    "    if (x >= 0):\n",
    "        return  x\n",
    "    else:\n",
    "        return -x\n",
    "\n",
    "abs_lambda = lambda x: x if (x >= 0) else -x"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "y = -42\n",
    "\n",
    "print(abs(y))\n",
    "print(abs_lambda(y))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"try_it\">\n",
    "<b>Try it yourself.</b>\n",
    "\n",
    "Rewrite\n",
    "<code>\n",
    "def area(r):\n",
    "    pi = 3.1416\n",
    "    return pi &#42; r&#42;&#42;2\n",
    "</code>\n",
    "as a lambda function.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A lambda function is just another function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(type(abs))\n",
    "print(type(abs_lambda))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The general syntax of a [lambda expression](https://docs.python.org/3/reference/expressions.html#lambda) is\n",
    "```python\n",
    "lambda argument_list : expression\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Lambdas can have multiple arguments.  Let's compute the mean of two numbers."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "meanie = lambda a, b: (a+b)/2\n",
    "\n",
    "print(meanie(3, 9))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The harmonic average shows up a lot in computational hydrology and petroleum reservoir simulation.  It is the reciprocal of the average of the reciprocals:\n",
    "$$\n",
    "\\frac{1}{\\frac{1}{2}\\left(\\frac{1}{a} + \\frac{1}{b}\\right)}.\n",
    "$$\n",
    "Here it is as a lambda:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "harm = lambda a, b: 1/(0.5*(1/a + 1/b))\n",
    "\n",
    "print(harm(1, 100))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The term <b>anonymous function</b> comes from the fact that a lambda function need not be tied to a name like regular functions are through a <code class=\"kw\">def</code> statement.\n",
    "\n",
    "For instance, we can assign the value of a lambda like we would other types of values:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "meanie = lambda a, b: (a+b)/2\n",
    "meaner = meanie\n",
    "\n",
    "print(meanie(3, 9))\n",
    "print(meaner(3, 9))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# What are lambdas good for? <a id=\"usage\"></a>\n",
    "\n",
    "Lambdas are not absolutely necessary, but they can be convenient.\n",
    "\n",
    "According to the [Python History and Design FAQ](https://docs.python.org/3/faq/design.html#why-can-t-lambda-expressions-contain-statements),\n",
    "<blockquote>\n",
    "Unlike lambda forms in other languages, where they add functionality, Python lambdas are only a shorthand notation if you’re too lazy to define a function.\n",
    "</blockquote>\n",
    "\n",
    "If you find yourself assigning a lambda function to a variable, then you should probably just define a function in the usual manner.  That is, there is no real advantage (other the more cryptic syntax) to\n",
    "```python\n",
    "harm = lambda a, b: 1/(0.5*(1/a + 1/b))\n",
    "```\n",
    "over \n",
    "```python\n",
    "def harm(a, b):\n",
    "    return 1/(0.5*(1/a + 1/b))\n",
    "```\n",
    "\n",
    "However, there are occasions where lambdas are handy."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Functions that take functions as inputs <a id=\"lightweight\"></a>\n",
    "\n",
    "The built-in function [<code>sorted()</code>](https://docs.python.org/3/library/functions.html#sorted) takes an iterable input and sorts it (by default, in increasing order).\n",
    "\n",
    "Suppose we have a dictionary of objects and prices and we want to sort by price:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "price = {'cat':15, 'dog':7, 'possum':5, 'wombat':27}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If we apply the <code>sorted()</code> function, we don't get the desired response.  Instead, we get the values sorted alphabetically by key:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "print(sorted(price.items()))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "However, <code>sorted()</code> has an optional argument <code>key</code>:\n",
    "<blockquote>\n",
    "    <code>sorted(iterable, *, key=None, reverse=False)</code><br/>   \n",
    "    <code>key</code> specifies a function of one argument that is used to extract a comparison key from each list element: <code>key=str.lower</code>. The default value is <code>None</code> (compare the elements directly).\n",
    "</blockquote>\n",
    "\n",
    "In our case we want to sort by the value of each entry in the dictionary, not by the key.  We can do this by passing a function that returns the **second** part of each key-value tuple:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(sorted(price.items(), key=lambda kv: kv[1]))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Alternatively, we could write"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def get_value(kv):\n",
    "    return kv[1]\n",
    "\n",
    "print(sorted(price.items(), key=get_value))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "However, since <code>get_value()</code> serves no purpose outside of the call to <code>sorted()</code> it is cleaner to use the lambda."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here is a similar example.  The list [sort method](https://docs.python.org/3/library/stdtypes.html#list.sort) takes an optional keyword argument <code>key</code> that can be used to modify how the entries of the list are sorted.\n",
    "\n",
    "For instance, suppose we use <code>sort()</code> to sort the following list:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = ['sort', 'lists', 'Tuples', 'Sets']\n",
    "a.sort()\n",
    "print(a)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Can you explain what's going on?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Answer.**\n",
    "<div class=\"voila\">\n",
    "Upper-case letters come before lower-case letters in the ASCII character sequence.\n",
    "</div>    "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's sort them ignoring capitalization.  We do that by providing a little function that says \"compare the lower-case versions of the strings\":"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a.sort(key=lambda s: s.lower())\n",
    "print(a)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This looks much more reasonable."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Functions that create functions <a id=\"returning_functions\"></a>\n",
    "\n",
    "Lambdas can be handy in functions that create functions.  For instance, in the following example the function <code>make_divider(n)</code> returns a function that raises its input to the power <code>n</code>."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def make_pow (n): \n",
    "    '''\n",
    "    Return a function that computes the n-th power.\n",
    "    '''\n",
    "    return lambda x: x**n\n",
    "\n",
    "square_me = make_pow(2) # I compute x**2.\n",
    "cube_me   = make_pow(3) # I compute x**3.\n",
    "\n",
    "print(type(square_me))\n",
    "print(type(cube_me))\n",
    "\n",
    "print(square_me(3))\n",
    "print(cube_me(3))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Binding function arguments  <a id=\"binding\"/>\n",
    "\n",
    "Another use of lambdas is in **binding** function arguments to specific values.\n",
    "\n",
    "Consider the following code:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def f(a, n):\n",
    "    return a**n\n",
    "\n",
    "def g(h, n):\n",
    "    return h(n)\n",
    "\n",
    "x = 2\n",
    "h = lambda n: f(x, n)\n",
    "\n",
    "for n in range(9):\n",
    "    print(g(h, n))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The effect of the lambda definition of <code>h</code> is that we have\n",
    "```python\n",
    "h(n) = f(x,n) = f(2,n)\n",
    "```\n",
    "without having to explicitly pass around the value of <code>x</code>.  We say that we have **bound** the argument <code>a</code> in the call to <code>f(a,n)</code> to <code>f(x,n)</code>, leaving only a function of <code>n</code>.\n",
    "\n",
    "There is a potential gotcha here &ndash; **the bound value is the value at the time of function invocation, not binding**.  If we change <code>x</code>, the value in the definition of <code>h()</code> changes:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def f(x, n):\n",
    "    return x**n\n",
    "\n",
    "def g(h, n):\n",
    "    return h(n)\n",
    "\n",
    "x = 2\n",
    "h = lambda n: f(x, n)\n",
    "\n",
    "x = 3  # <- we change x.\n",
    "\n",
    "for n in range(9):\n",
    "    print(g(h, n))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here is another example.\n",
    "\n",
    "The formula for how far something falls in time $t$ is given by $s = \\frac{1}{2} g t^{2}$, where $g$ is the acceleration under gravity.  On Earth $g \\approx \\mbox{$32.2$ feet per second}$ while on Jupiter $g \\approx \\mbox{$81.3$ feet per second}$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def s(g, t):\n",
    "    return 0.5 * g * t**2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "While $g$ varies by planet, once we have chosen a planet and start doing lots of calculations for it we don't need to haul $g$ around, so it's convenient to use argument binding:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "g = 81.3  # gravitational acceleration on Jupiter, in feet per second.\n",
    "\n",
    "d = lambda t: 0.5 * g * t**2\n",
    "\n",
    "for t in range(10):\n",
    "    print(d(t))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Remember &ndash; the less stuff we have to pass around and the more stuff we can encapsulate, the better.\n",
    "\n",
    "You could also accomplish something similar with classes, but that would be a much more heavyweight solution."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Exercises"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"exercise\">\n",
    "Write a lambda function that computes the area of a rectangle given the width and height.    \n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Answer.**\n",
    "<div class=\"voila\">\n",
    "<code>\n",
    "area = lambda width, height: width*height\n",
    "print(area(3,4))\n",
    "</code>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h4>This notebook was brought to you by progress:</h4>\n",
    "\n",
    "What we call progress is the exchange of one nuisance for another nuisance. <br/>\n",
    "&ndash; Havelock Ellis"
   ]
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
