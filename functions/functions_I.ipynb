{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"http://www.cs.wm.edu/~rml/images/wm_horizontal_single_line_full_color.png\">\n",
    "\n",
    "<h1 style=\"text-align:center;\">CSCI 141-01, Fall 2023 course notes</h1>\n",
    "<h1 style=\"text-align:center;\">Introduction to functions</h1>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Contents\n",
    "* [Functions](#functions)\n",
    "* [Why use functions?](#advantages)\n",
    "* [More examples](#more_examples)\n",
    "* [Functions without inputs](#no_inputs)\n",
    "* [Functions without <code class=\"kw\">return</code> statements](#no_return)\n",
    "    * [<code class=\"kw\">None</code>](#none)\n",
    "* [Function stubs](#stubs)\n",
    "* [Docstrings](#docstrings)\n",
    "* [Exercises](#exercises)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Functions <a id=\"functions\"></a>\n",
    "\n",
    "<b>Functions</b> are a fundamental notion of computer languages.  Functions are a set of statements that are grouped together and given their own identity. Functions can be passed <b>arguments</b> (inputs) to which the functions statements will be applied.  This encapsulation allows us to use the set of statements repeatedly without having to duplicate the code."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Our first function\n",
    "\n",
    "We have already seen examples of Python's built-in functions.  \n",
    "\n",
    "Let's write our own function.  This function takes the input <code>x</code>, squares it, and returns the result.\n",
    "\n",
    "First we define the function.\n",
    "* Functions are introduced with the keyword <code class=\"kw\">def</code>.\n",
    "* Then comes the name of the function, parentheses surrounding any inputs to the function, and then a colon.  \n",
    "* The statements that make up the function are indented with the same amount of indentation.  \n",
    "* Then comes an optional <code class=\"kw\">return</code> statement to return a result; we will see shortly that functions are free not to return anything."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def square_me(x):\n",
    "    return x**2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let's <b>call</b> or <b>invoke</b> the function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "y = square_me(3)\n",
    "print(y)\n",
    "print(square_me(4))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"try_it\">\n",
    "<b>Try it yourself.</b>\n",
    "Write a function <code>add8</code> that takes its input, adds 8 to it, and returns the result.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"try_it\">\n",
    "<b>Try it yourself.</b>\n",
    "What happens if you omit the parenthesis, even if the function has no inputs?\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def foo:\n",
    "    return 1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Why use functions? <a id=\"advantages\"></a>\n",
    "\n",
    "<blockquote>\n",
    "Controlling complexity is the essence of computer programming.<br/>\n",
    "&ndash; Brian Kernighan\n",
    "</blockquote>\n",
    "\n",
    "There are many good reasons to use functions.\n",
    "* <b>Code reuse</b>. Using functions we write a set of instructions once. This saves a lot of work in larger programs.  \n",
    "* <b>DRY vs WET</b>.  Functions are a key aspect of a software development principle known as DRY, for Don't Repeat Yourself.  A competing philosophy is known as WET, for Write Everything Twice (or We Enjoy Typing).  DRY is preferable to WET.\n",
    "* <b>Modularity</b>.  Functions reflect the steps in our solution to the problem we are solving.\n",
    "* <b>Debugging and testing</b>. Writing small functions that do only one thing well simplifies code development.\n",
    "   - Because the functions are small and intended to do only one thing, we can test them in isolation more easily.\n",
    "   - Since they are small, we can debug them more quickly and with greater confidence.\n",
    "* <b>Reliability</b>. Once we know a function works, we don't have to worry about that code anymore. \n",
    "   - Every time you repeat code in your program, you introduce an opportunity for a bug.\n",
    "   - Using a function means there is one place to fix bugs associated with that code. When those bugs are fixed, the function will work correctly whenever it is called.\n",
    "* <b>Maintainability</b>.  If we modify a function's behavior, that change affects all calls to the program.\n",
    "   - This is much better than having to change repeated code in many different places in a program.\n",
    "* <b>Information hiding</b>.  Functions hide unnecessary detail from view.\n",
    "   - The less code we have to look at, the easier it is to understand what the program does.\n",
    "   - It's a good idea to hide details from users &ndash; we don't want them to build code based on details that later change."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# More examples <a id=\"more_examples\"/>\n",
    "\n",
    "The next function cubes its input:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def cube_me(x):\n",
    "    return x**3"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "y = cube_me(3)\n",
    "print(y)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Functions can take multiple arguments.  The following function computes the mean (average) of its two inputs."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def meanie(a, b):\n",
    "    return (a+b)/2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "print(meanie(42, 54))\n",
    "print(meanie(-3, 3))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Functions can call other functions, including themselves.  In the following example, the innermost call to <code>square_me</code> results in the value of 4, which is then passed to the outer call to <code>square_me</code>, resulting in the value 16."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "y = square_me(square_me(2))\n",
    "print(y)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"try_it\">\n",
    "<b>Try it yourself.</b>\n",
    "Pass the output of your <code>add8</code> function to itself, as in the preceding example.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Functions can also return multiple values:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "5.0 4.0\n"
     ]
    }
   ],
   "source": [
    "from math import sqrt\n",
    "\n",
    "def means(a,b):\n",
    "    '''Compute the arithmetic and geometric means.'''\n",
    "    arithmetic_mean = (a+b)/2\n",
    "    geometric_mean = sqrt(a*b)\n",
    "    return arithmetic_mean, geometric_mean\n",
    "    \n",
    "a, g = means(2, 8)\n",
    "print(a, g)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Functions without inputs <a id=\"no_inputs\"></a>\n",
    "\n",
    "Functions need not have any input arguments.  "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"danger\"></div>\n",
    "However, even if there are no arguments we still need to declare and call the function using <code>()</code>."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def foo():\n",
    "    return 42"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x = foo()\n",
    "print(x)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The following is valid in Python, but it's probably not what we intended: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x = foo\n",
    "print(x)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"danger\"></div>\n",
    "The fact that it is legal to refer to the function without <code>()</code> is a source of bugs.  Watch out!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Functions without  <code style=\"kw\">return</code> statements <a id=\"no_return\"></a>\n",
    "\n",
    "Functions need not return a value (though they typically do).  For instance, if our function printed a string a particular way it would not need to return a value:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "def my_print(name, grade):\n",
    "    if (grade > 96):\n",
    "        print('You are doing well, ' + name + '!')\n",
    "    else:\n",
    "        print('You are doomed, ' + name + '!')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "You are doing well, grasshopper!\n",
      "You are doomed, mr potato head!\n"
     ]
    }
   ],
   "source": [
    "my_print('grasshopper', 97)\n",
    "my_print('mr potato head', 89)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When a function has no <code class=\"kw\">return</code> statement, the special value <code class=\"kw\">None</code> is still returned by the function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s = my_print('Ali Gator', 50)\n",
    "print(s)\n",
    "print(type(s))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This behavior can lead to bugs.  For instance, consider the following function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def sign(x):\n",
    "    '''sign(x): return the sign of x'''\n",
    "    if (x > 0):\n",
    "        return +1\n",
    "    elif (x < 0):\n",
    "        return -1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "What happens if $x = 0$?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(sign(0))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Oops!  we fell off the end of the function and returned a <code class=\"kw\">None</code>!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"danger\"></div>\n",
    "Be careful that you don't fall off the end of a function.  This is a common error."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Function stubs <a id=\"stubs\"></a>\n",
    "\n",
    "It is possible to have functions that do nothing.  These are sometimes called <b>stubs</b>.  The body of a function cannot be empty, so we use the <code class=\"kw\">pass</code> statement as a placeholder:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def empty_function():\n",
    "    pass\n",
    "\n",
    "print(empty_function())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Stubs are useful during code development as they allow you to lay out the logic of your program, including function calls, without having to have the functions working."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"try_it\">\n",
    "<b>Try it yourself.</b> \n",
    "What happens when you try to create a function with an empty body, as in the following?\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def funny():\n",
    "    "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Docstrings <a id=\"docstrings\"/>\n",
    "\n",
    "The Python <b>docstring</b> feature allows you to document your functions in a way that is accessible to the outside world.\n",
    "\n",
    "Docstrings appear immediately after the line with the function <code class=\"kw\">def</code> and are enclosed in triple quotes.\n",
    "\n",
    "The docstring can then be accessed as the <code>&#95;&#95;doc&#95;&#95;</code> attribute of the function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def teapot():\n",
    "    \"\"\"I'm a little teapot, short and stout.\"\"\"\n",
    "    pass\n",
    "\n",
    "print(teapot.__doc__)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"try_it\">\n",
    "<b>Try it yourself.</b>  The following function computes the area of a circle of radius <code>r</code>.  Add a docstring and print it as the <code>&#95;&#95;doc&#95;&#95;</code> attribute of the function.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def area(r):\n",
    "    PI = 3.1416\n",
    "    return PI * r**2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Your docstrings can be as long as you want."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def teapot():\n",
    "  '''\n",
    "    I'm a little teapot,\n",
    "      short and stout.\n",
    "    Here is my handle,\n",
    "      here is my spout.\n",
    "  '''\n",
    "  pass\n",
    "\n",
    "print(teapot.__doc__)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you are working interactively (e.g., at the Python console), you can summon the docstrings using <code>help()<code>."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "help(teapot)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here are some examples taken from the <code>math</code> module.  \n",
    "\n",
    "All three docstrings state what the functions do.\n",
    "\n",
    "The docstrings for the two trigonometric functions helpfully remind us that they work with radians rather than degrees."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import math\n",
    "\n",
    "print(math.sqrt.__doc__)\n",
    "print('--------------------------------------------------')\n",
    "print(math.cos.__doc__)\n",
    "print('--------------------------------------------------')\n",
    "print(math.atan.__doc__)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Docstrings a l&agrave; the Python style guide\n",
    "\n",
    "I find the [\"official\" Python recommendations for docstrings](https://www.python.org/dev/peps/pep-0257/) to be overly prescriptive and bureaucratic.  Rather than be in the thrall of the Python overlords you should develop your own inimtable style and use it consistently."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Exercises <a id=\"exercises\"/>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"exercise\">\n",
    "Write a function <code>ells_to_barleycorns()</code> that takes as its input a length in (English) ells and returns the equivalent length in barleycorns.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\n",
    "\n",
    "print(ells_to_barleycorns(1))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b>Answer.</b>\n",
    "<div class=\"voila\">\n",
    "For instance,\n",
    "<pre>\n",
    "def ells_to_barleycorns(ells):\n",
    "    BARLEYCORNS_PER_INCH = 3\n",
    "    INCHES_PER_ELL = 45\n",
    "    barleycorns = BARLEYCORNS_PER_INCH &#42; INCHES_PER_ELL &#42; x\n",
    "    return barleycorns\n",
    "</pre>\n",
    "Avoid unexplained magic numbers in your code &ndash; in this example we make clear the different conversion factors that are involved.  Following the <a href=\"http://legacy.python.org/dev/peps/pep-0008/#constants\">Python style guide PEP 8</a> we write constants in uppercase with underscores separating words.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"exercise\">\n",
    "The following function is intended to sum the integers from $1$ to $n$:\n",
    "$$\n",
    "1 + 2 + 3 + \\cdots + n.\n",
    "$$\n",
    "Is it correct?  If not, fix it.\n",
    "<pre>\n",
    "def sum_it(n):\n",
    "    sum = 0\n",
    "    i = 0\n",
    "    while (i < n):\n",
    "        sum += i\n",
    "        return sum\n",
    "</pre>\n",
    "You should look at the code first rather than just running it, as one of the errors is rather serious.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b>Answer.</b>\n",
    "<div class=\"voila\">\n",
    "There are three errors:\n",
    "<ol>\n",
    "<li>\n",
    "The stopping criterion in the <code>while</code> loop makes the sum stop at $n-1$ rather than $n$.\n",
    "</li>\n",
    "<li>\n",
    "Because the <code>return</code> statement is inside the <code>for</code> loop, we incorrectly exit the function on the first loop iteration.\n",
    "</li>\n",
    "<li>\n",
    "Once we fix the preceding bug, then, because we fail to increment the counter <code>i</code>, we have an infinite loop!\n",
    "</li>\n",
    "</ol>\n",
    "<pre>\n",
    "def sum_it(n):\n",
    "    sum = 0\n",
    "    i = 0\n",
    "    while (i <= n):\n",
    "        sum += i\n",
    "        i   += 1\n",
    "    return sum\n",
    "</pre> \n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"exercise\">\n",
    "<p>\n",
    "Python has a built-in random number module named <a href=\"https://docs.python.org/3/library/random.html\"><code>random</code></a>. \n",
    "</p>\n",
    "<p>\n",
    "We can <a href=\"https://docs.python.org/3/library/random.html#functions-for-integers\">generate a random integer</a> $n$ satisfying $a \\leq n \\leq b$ with the function call \n",
    "</p>\n",
    "<pre>\n",
    "import random\n",
    "n = random.randint(a,b)\n",
    "</pre>\n",
    "<p>\n",
    "Each integer between $a$ and $b$ is equally likely (i.e., the integers are drawn from a uniform distribution).  This means half the time <code>random.randint(0,1)</code> will be $0$ and half the time it will be $1$.\n",
    "</p>\n",
    "<p>\n",
    "Write a function <code>flip()</code> that simulates a toss of a fair coin (i.e., the propabilities of heads and tails are both 1/2).  The function takes no arguments and returns either a 'H' or 'T' depending on the outcome of the toss.\n",
    "</p>\n",
    "<p>\n",
    "Call <code>flip()</code> to simulate 200 tosses of the coin, accumulating the results as a single string and then printing the string, so the output looks like this:\n",
    "<pre>\n",
    "HHHTTHTTHTTHTTHHTHTTTTHTHHTTTHHTTTTTTHTTHTHTTHTTHTTTHHHTTHHHTTHHTHTHTTHTTHTTTHTHHTTHHHTTTTTHTHTTTHHHTTTHHTHHHTTHTTHTTTHTHHHHHHTHHHTHHHHTTTTTTTHHTHHTTHHTHHTHTTTTTHTHTTTHTTHHHTHHTHHTTHHTHHHTHTTTTTTHHHHT\n",
    "</pre>\n",
    "</p>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b>Answer.</b>\n",
    "<div class=\"voila\">\n",
    "Here is one solution.\n",
    "<pre>\n",
    "import random\n",
    "\n",
    "def flip():\n",
    "    n = random.randint(0,1)\n",
    "    if (n == 0):\n",
    "        return 'H'\n",
    "    else:\n",
    "        return 'T'\n",
    "\n",
    "s = ''\n",
    "for i in range(0, 200):\n",
    "    s += flip()\n",
    "print(s)    \n",
    "</pre>\n",
    "<p>\n",
    "Note that we place the <code>import</code> statement at the very beginning of the code.  This is a good practice, so you and others will know what other code you are using.\n",
    "</p>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h4>This notebook was brought to you by Rev. 6:8.</h4>\n",
    "\n",
    "<i>And I looked, and behold a pale horse, and his name who sat on him was Death&hellip;</i>\n",
    "\n",
    "<img src=\"http://www.cs.wm.edu/~rml/teaching/csci141/jupyter/death.gif\">"
   ]
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
