{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"http://www.cs.wm.edu/~rml/images/wm_horizontal_single_line_full_color.png\">\n",
    "\n",
    "<h1 style=\"text-align:center;\">CSCI 141-01, Fall 2023 course notes</h1>\n",
    "<h1 style=\"text-align:center;\">Generators 🤯</h1>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Contents\n",
    "\n",
    "* [Generators](#generators)\n",
    "* [How does <code class=\"kw\">range()</code> work?](#How-does-range-work?)\n",
    "* [Generators to the rescue!](#Generators-to-the-rescue!)\n",
    "* [Example: the Fibonacci numbers](#fib)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Generators <a id=\"generators\"></a>\n",
    "\n",
    "<b>Generators</b> are a special type of function that allow you to create lightweight iterable objects.  By \"lightweight\" we mean that the objects do not occupy large regions of memory, even though they conceptually represent large objects."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# How does <code class=\"kw\">range</code> work?\n",
    "\n",
    "Let's think about how a function like <code class=\"kw\">range()</code> might work.\n",
    "\n",
    "Suppose it actually returned an honest-to-goodness list, like the following function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def my_range(start, stop):\n",
    "    return [i for i in range(start, stop)]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's use a utility from the [<code>sys</code> module](https://docs.python.org/3/library/sys.html#sys.getsizeof) to see how many bytes of memory the list returned by <code>my_range()</code> uses."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sys import getsizeof\n",
    "n = [10**6, 10**7, 10**8]\n",
    "for k in n:\n",
    "    mange = my_range(0, k)\n",
    "    print('bytes required to store mange:', getsizeof(mange))\n",
    "    print('bytes per item in mange:      ', getsizeof(mange)/k)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Great Jumping Jehosophat!  it looks like each of the items in the list <code>mange</code> takes over 8 bytes, on average. \n",
    "\n",
    "This suggests we will need over 8 gigabytes of memory if we call <code>my_range(1, 10**9)</code>!!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Generators to the rescue!\n",
    "\n",
    "What we really want is something that yields the sequence of integers from 1 to 1,000,000,000 one at a time &ndash;\n",
    "we don't actually need to compute all of the terms at once.\n",
    "\n",
    "This is what <b>generators</b> do for us.\n",
    "\n",
    "Generators use the [<code class=\"kw\">yield</code> statement](https://docs.python.org/3/reference/expressions.html#yieldexpr) (rather than <code class=\"kw\">return</code>).  When a <code class=\"kw\">yield</code> is encountered, the generator returns the next value and pauses until the subsequent value is needed."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def my_range(start, stop):\n",
    "    i = start\n",
    "    while (i < stop):\n",
    "        yield i\n",
    "        i = i + 1"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mange = my_range(0, 10)\n",
    "for i in mange:\n",
    "    print(i)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let's build bigger ranges of integers and look at the sizes of the resulting objects."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sys import getsizeof\n",
    "mange = my_range(0, 1_000_000)\n",
    "print('number of bytes needed for mange:', getsizeof(mange))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sys import getsizeof\n",
    "mange = my_range(0, 1_000_000_000)\n",
    "print('number of bytes needed for mange:', getsizeof(mange))\n",
    "print(type(mange))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Whatever a generator object is, it's a lot smaller than the list of numbers it represents!\n",
    "\n",
    "The <code class=\"kw\">range()</code> function in Python 2 returned lists.  Because of this excessive memory requirement <code class=\"kw\">range()</code> it was replaced in Python 2 by a function <code class=\"kw\">xrange()</code> that behaves like <code class=\"kw\">range()</code> does in Python 3."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's spiff up our generator with an optional argument for the stride.  We'll also christen it <code>mange()</code>, a portmanteau word from my + range."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def mange(start, stop, stride=1):\n",
    "  i = start\n",
    "  while (i < stop):\n",
    "    yield i\n",
    "    i = i + stride"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let's use this critter the same way we would <code>range()</code>:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for i in mange(0, 10, 2):\n",
    "    print(i)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Example: the Fibonacci numbers <a id=\"fib\"></a>\n",
    "\n",
    "Let's create a generator for our old friends the Fibonacci numbers $F_{n}$:\n",
    "\\begin{align*}\n",
    "  F_{0} &= 0 \\\\\n",
    "  F_{1} &= 1 \\\\\n",
    "  F_{n} &= F_{n-1} + F_{n-2}, \\quad n \\geq 2.\n",
    "\\end{align*}\n",
    "The first few are\n",
    "$$\n",
    "  0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, \\ldots\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This generator will handle the initial cases $F_{0}$ and $F_{1}$ separately from the cases $F_{n}$, $n \\geq 2$.  This example illustrates how we can use control flow in a generator."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def fib(n):\n",
    "    \"\"\"Compute the first n Fibonacci numbers 0, 1, 1, 2, ...\"\"\"\n",
    "    previous = 0\n",
    "    current  = 1\n",
    "    for k in range(0, n):\n",
    "        if (k == 0):\n",
    "            yield 0\n",
    "        elif (k == 1):\n",
    "            yield 1;\n",
    "        else:\n",
    "            current, previous = current + previous, current\n",
    "            yield current\n",
    "        \n",
    "F = fib(13);\n",
    "for f in F:\n",
    "    print(f)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h4>This notebook was brought to you by lunatics.</h4>\n",
    "\n",
    "There are always some lunatics about.  It would be a dull world without them. <br/>\n",
    "&ndash; Sherlock Holmes, The Adventure of the Three Gables"
   ]
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
