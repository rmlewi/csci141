{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"http://www.cs.wm.edu/~rml/images/wm_horizontal_single_line_full_color.png\">\n",
    "\n",
    "<h1 style=\"text-align:center;\">CSCI 141-01, Fall 2023 course notes</h1>\n",
    "<h1 style=\"text-align:center;\">More about function argument lists</h1>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Contents\n",
    "\n",
    "* [Variable length argument lists](#varargs)\n",
    "* [Positional argument tuples](#positional_varargs)\n",
    "    * [Example](#example_star)\n",
    "    * [Entirely optional positional arguments](#all_star)\n",
    "    * [Passing positional argument tuples between functions](#passing_star)\n",
    "* [Keyword argument dictionaries](#keyword_varargs)\n",
    "    * [Example](#example_starstar)\n",
    "    * [Entirely optional keyword arguments](#all_starstar)\n",
    "    * [Passing keyword argument dictionaries between functions](#passing_starstar)\n",
    "* [Exercises](#exercises)    \n",
    "* [Lagniappe: positional-only parameters 🤯](#lagniappe)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Variable-length argument lists <a id=\"varargs\"/>\n",
    "\n",
    "We have seen functions whose argument lists can have varying numbers of inputs, for instance,\n",
    "* <code>print()</code>\n",
    "* <code>set.union()</code>, <code>set.intersection()</code>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print()\n",
    "print('fee')\n",
    "print('fee', 'fie')\n",
    "print('fee', 'fie', 'foe')\n",
    "print('fee', 'fie', 'foe', 'fum')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "A = set([1,2,3])\n",
    "B = set([4,5,6])\n",
    "C = set([7,8,9])\n",
    "print(A.union(B))\n",
    "print(A.union(B,C))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Functions that can take variable numbers of inputs are called [**variadic functions**](https://en.wikipedia.org/wiki/Variadic_function).\n",
    "\n",
    "How do we specify argument lists with variable numbers of arguments?\n",
    "* We can collect positional arguments in a tuple.\n",
    "* We can collect keyword arguments in a dictionary.\n",
    "\n",
    "We now discuss each approach."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Positional argument tuples <a id=\"positional_varargs\"></a>\n",
    "\n",
    "In Python, we use a special parameter that gathers one or more extra positional arguments.  This parameter is indicated by a <code>*</code> in front of its name.  This sweeps all the positional arguments after some point into a catch-all tuple.  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def foo(a, b, *args):  # Arguments a and b are required; everything else is optional.\n",
    "    print('a, b:', a, b)\n",
    "    print('varargin:', args)\n",
    "    \n",
    "foo(1,2)\n",
    "foo(1,2,3)\n",
    "foo(1,2,3,4)\n",
    "foo(1,2,3,4,5)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def foo(a, b, c, *doofus):\n",
    "      print('a, b, c:', a, b, c)\n",
    "      print('doofus:', *doofus)\n",
    "    \n",
    "foo(0,1,2)\n",
    "foo(0,1,2,3)\n",
    "foo(0,1,2,3,4,5,6,7,8,9)    "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that the arguments not in the <code>*</code> variable argument tuple <b>must</b> be passed:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "foo(1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Example <a id=\"example_star\"></a>\n",
    "\n",
    "How could we design a function <code>foo()</code> that can be called either as\n",
    "```python\n",
    "foo(month, day, year)\n",
    "```\n",
    "or\n",
    "```python\n",
    "foo(month, year)\n",
    "```\n",
    "?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def foo(*args):\n",
    "    if (len(args) == 2):\n",
    "        month = args[0]\n",
    "        year  = args[1]\n",
    "        print('month, year =', month, year)\n",
    "    elif (len(args) == 3):\n",
    "        month = args[0]\n",
    "        day   = args[1]     \n",
    "        year  = args[2]\n",
    "        print('month, day, year = ', month, day, year)\n",
    "    else:\n",
    "        print('ack!')\n",
    "\n",
    "foo(7, 1776)        \n",
    "foo(7, 4, 1776)\n",
    "foo(7)       "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Entirely optional positional arguments <a id=\"all_star\"></a>\n",
    "\n",
    "If we want, the entire argument list can be unspecified in advance (<code>print()</code> is like this):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "def foo(*a):\n",
    "    # print(a)\n",
    "    print(*a)\n",
    "\n",
    "foo()\n",
    "foo(1)\n",
    "foo(1,2)\n",
    "foo(1,2,3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's make a function that computes the average of an arbitrary number of inputs."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def meanie(*args):\n",
    "    if (len(args) == 0): # watch out for no arguments!\n",
    "        return None\n",
    "    else:\n",
    "        sum = 0\n",
    "        for x in args:\n",
    "            sum = sum + x\n",
    "        return sum/len(args)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(meanie(1,2,3))\n",
    "print(meanie(1,2,3,4))\n",
    "print(meanie(1,2,3,4,5,6,7))\n",
    "print(meanie())\n",
    "print(meanie(1, 99))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Passing positional argument tuples between functions <a id=\"passing_star\"></a>\n",
    "\n",
    "Note the following difference between referring to a variable length input with and without the star &#42; :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def foo(*args):\n",
    "    print(args)  # A tuple of inputs.\n",
    "    print(*args) # The inputs themselves!\n",
    "    \n",
    "foo(1,2,3,4)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This leads to the following subtle but important use of the star &#42; when working with variable length inputs.\n",
    "\n",
    "Suppose we want to pass a variable length input from one function to another.  As a first attempt we might try the following:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def foo(*args):\n",
    "    print('foo: args = ', args)\n",
    "    bar(args)\n",
    "    \n",
    "def bar(*args):\n",
    "    print('bar: args = ', args)\n",
    "    print(args[0], args[1], args[2], args[3])  # Ack!\n",
    "    \n",
    "foo(1,2,3,4)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "What went wrong?\n",
    "\n",
    "When we enter <code>foo</code>, <code>args</code> is a tuple of inputs:\n",
    "<pre>\n",
    "foo: args =  (1, 2, 3, 4)\n",
    "</pre>\n",
    "This tuple is then passed to <code>bar()</code> <i>as the contents of the tuple of inputs that <code>bar()</code> is expecting</i>:\n",
    "<pre>\n",
    "bar: args =  ((1, 2, 3, 4),)\n",
    "</pre>\n",
    "So <code>bar()</code> has ended up with a tuple of length 4 wrapped in a tuple of length 1. Because the latter tuple has length 1, we get an <code class=\"error\">IndexError</code>."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here is how we fix the problem.  Rather than pass <code>args</code> to <code>bar()</code>, we will pass <code>*args</code>.  This passes the arguments inside <code>args</code> rather than <code>args</code> as a tuple:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def foo(*args):\n",
    "    print('foo:  args = ',  args)\n",
    "    print('foo: *args = ', *args)\n",
    "    bar(*args)\n",
    "    \n",
    "def bar(*args):\n",
    "    print('bar:  args = ',  args)\n",
    "    print('bar: *args = ', *args)\n",
    "    print(args[0], args[1], args[2], args[3])  # Yay!\n",
    "    \n",
    "foo(1,2,3,4)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This use of <code>*</code> doesn't seem to have a Pythonic name, but in [Ruby](https://www.ruby-lang.org/en/) it is called the **splat operator**."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Keyword argument dictionaries  <a id=\"keyword_varargs\"></a>\n",
    "\n",
    "We can also do something similar with keyword arguments.  Here we sweep them all into a dictionary indicated by <code>**</code>."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def foo(a, b, **everything_else):\n",
    "    print(everything_else)\n",
    "    for thing in everything_else:\n",
    "        print(thing, 'is', everything_else[thing])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "foo(1, 2,\n",
    "    war='peace', \n",
    "    freedom='slavery', \n",
    "    ignorance='strength',\n",
    "    truth='beauty')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(foo(17,34))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Example <a id=\"example_starstar\"></a>\n",
    "\n",
    "Let's revisit the design of the function <code>foo()</code> that can be called either as\n",
    "```python\n",
    "foo(month, day, year)\n",
    "```\n",
    "or\n",
    "```python\n",
    "foo(month, year)\n",
    "```\n",
    "?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def foo(**kw):\n",
    "    month = day = year = None\n",
    "    if ('month' in kw):\n",
    "        month = kw['month']\n",
    "    if ('day' in kw):\n",
    "        day = kw['day']\n",
    "    if ('year' in kw):\n",
    "        year  = kw['year']\n",
    "    print('month, day, year = ', month, day, year)\n",
    "\n",
    "foo(month=7, year=1776)        \n",
    "foo(month=7, day=4, year=1776)\n",
    "foo(month=7)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Entirely optional keyword arguments <a id=\"all_starstar\"></a>\n",
    "\n",
    "As with positional arguments we can make all keyword arguments optional:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def foobar(**kwargs):\n",
    "    for thing in kwargs:\n",
    "        print(thing, 'is', kwargs[thing])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "foobar(fake_gnus=False, good_gnus=True, pi=3.14)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "foobar()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "truth = True\n",
    "beauty = True\n",
    "foobar(poetry=(truth is beauty))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Passing keyword argument dictionaries between functions <a id=\"passing_starstar\"></a>\n",
    "\n",
    "If we want to pass keyword argument lists between functions we need to use <code>&#42;&#42;</code> (**double splat**) similar to how we needed to use <code>&#42;</code> (**splat**) when we pass positional argument lists.  Here is an illustration."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# First attempt: no *.\n",
    "def foo(**kw):\n",
    "    print('foo: kw = ', kw)\n",
    "    bar(kw)\n",
    "    \n",
    "def bar(**kw):\n",
    "    print('bar: kw = ', kw)\n",
    "    \n",
    "foo(month=7, day=20, year=1969)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Second attempt: only one *.\n",
    "def foo(**kw):\n",
    "    print('foo:  kw = ',  kw)\n",
    "    print('foo: *kw = ', *kw)\n",
    "    bar(*kw)\n",
    "    \n",
    "def bar(**kw):\n",
    "    print('bar: kw = ', kw)\n",
    "    \n",
    "foo(month=7, day=20, year=1969)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Third attempt: **.\n",
    "def foo(**kw):\n",
    "    print('foo: kw = ', kw)\n",
    "    bar(**kw)\n",
    "    \n",
    "def bar(**kw):\n",
    "    print('bar: kw = ', kw)\n",
    "    \n",
    "foo(month=7, day=20, year=1969)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Exercises  <a id=\"exercises\"/>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"exercise\">\n",
    "    Write a function <code>prod()</code> that takes an arbitrary number of inputs and multiplies them together.\n",
    "</div>    "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Answer.**\n",
    "<div class=\"voila\">\n",
    "Here is one solution:\n",
    "<pre>\n",
    "def prod(*args):\n",
    "    product = 1\n",
    "    for arg in args:\n",
    "        product *= arg\n",
    "    return product\n",
    "\n",
    "print(prod(1,2,3,4,5))\n",
    "</pre>\n",
    "</div>    "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"exercise\">\n",
    "Use the splat operator to map the list <code>a</code> to the arguments of <code>f()</code> (i.e., use the splat operator to achieve the effect of <code>f(a[0], a[1], a[2])</code>).\n",
    "</div>    "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Answer.**\n",
    "<div class=\"voila\">\n",
    "Here is my solution:    \n",
    "<pre>\n",
    "from math import sqrt\n",
    "\n",
    "def quadratic_formula(a, b, c):\n",
    "    roots = ((-b + sqrt(b**2 - 4*a*c))/(2*a), (-b - sqrt(b**2 - 4*a*c))/(2*a))\n",
    "    return roots\n",
    "\n",
    "a = [1, 0, -1]\n",
    "\n",
    "roots = quadratic_formula(*a)\n",
    "print(roots)\n",
    "</pre>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"exercise\">\n",
    "Write a function that takes a single positional argument <code>x</code> and an arbitrary number of coefficients for a polynomial and evaluates the polynomial.  That is, <code>f(x, a, b, c)</code> should return $ax^{2} + bx + c$ while <code>f(x, a, b, c, d)</code> should return $ax^{3} + bx^{2} + cx + d$, and so on.\n",
    "</div>    "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def poly_eval(x, *coeff):\n",
    "    pass\n",
    "    \n",
    "print(poly_eval(2, 3, 2, 1))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Answer.**\n",
    "<div class=\"voila\">\n",
    "This problem is easier if you evaluate the polynomial using <a href=\"https://en.wikipedia.org/wiki/Horner's_method\">Horner's method</a>.\n",
    "\n",
    "Here is a solution that avoids determining the degree of the polynomial and all the index arithmetic that goes with it:\n",
    "<pre>\n",
    "def poly_eval(x, *coeff):\n",
    "    \"\"\"Evaluate a polynomial using Horner's method.\"\"\"\n",
    "    if (coeff):\n",
    "        px = 0\n",
    "        # compute c[0] * x**n + c[1] * x**(n-1) + ... + c[-2] * x\n",
    "        for c in coeff[:-1]:\n",
    "            px += c\n",
    "            px *= x\n",
    "        px += coeff[-1]  # add the constant term.\n",
    "        return px\n",
    "    else:\n",
    "        return None\n",
    "    \n",
    "print(poly_eval(2, 3, 2, 1))\n",
    "</pre>\n",
    "</div>    "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Lagniappe: positional-only parameters 🤯  <a id=\"lagniappe\"/>\n",
    "\n",
    "Positional-only parameters are [a new feature in Python 3.8](https://docs.python.org/3/whatsnew/3.8.html#positional-only-parameters):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def f(a, b, /, c, d):\n",
    "    print(a, b, c, d)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The slash <code>/</code> means that the arguments that precede it **must** be positional arguments and **can never** be passed as keyword arguments:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# these calls are ok.\n",
    "\n",
    "f(1, 2, 3, 4)\n",
    "f(1, 2, d=4, c=3)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# this is not ok.\n",
    "\n",
    "f(a=1, 2, 3, 4)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# this is not ok.\n",
    "\n",
    "f(d=4, c=3, b=2, a=1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can read the rationale for this new feature at the link above.  I mention it only so that you aren't puzzled by it if you encounter it."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h4>This notebook was brought to you by ideas:</h4>\n",
    "\n",
    "Virtually no idea is too ridiculous to be accepted, even by very intelligent and highly educated people, if it provides a way for them to feel special and important.  Some confuse that feeling with idealism. <br/>\n",
    "&ndash; Thomas Sowell"
   ]
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
