{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"http://www.cs.wm.edu/~rml/images/wm_horizontal_single_line_full_color.png\">\n",
    "\n",
    "<h1 style=\"text-align:center;\">CSCI 141-01, Fall 2023 course notes</h1>\n",
    "<h1 style=\"text-align:center;\">Recursion</h1>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Contents\n",
    "\n",
    "* <a href=\"#recursion\">Recursion</a>\n",
    "* <a href=\"#factorial\">The factorial</a>\n",
    "* <a href=\"#exponentiation\">Exponentiation</a>\n",
    "* <a href=\"#stack_overflow\">Infinite recursion and stack overflow</a>\n",
    "* <a href=\"#fibonacci\">The Fibonacci numbers</a>\n",
    "* [Exercises](#exercises)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# What is recursion? <a id=\"recursion\"></a>\n",
    "\n",
    "In order to understand recursion, [you need to understand recursion](https://www.google.com/search?q=recursion).\n",
    "\n",
    "A <b>recursive function</b> is a function that calls itself.\n",
    "\n",
    "More generally, <b>recursion</b> refers to solving a problem by solving smaller instances of the problem, which are themselves solved recursively.\n",
    "\n",
    "Recursion forms the logical basis for some of the most important fundamental algorithms used today, including quicksort, mergesort, depth first search, and the Fast Fourier Transform."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Example: the factorial <a id=\"factorial\"></a>\n",
    "\n",
    "For a positive integer $n$, let $f(n)$ be the **factorial** of $n$:\n",
    "\\begin{align*}\n",
    "f(n) &= n! \\\\\n",
    "&= n \\times (n-1) \\times (n-2) \\times \\cdots \\times 2 \\times 1 \\\\\n",
    "&= n \\times (n-1)!.\n",
    "\\end{align*}\n",
    "\n",
    "This means the factorial can be computed in terms of itself: if $n > 1$, then\n",
    "$$\n",
    "f(n) = n \\times f(n-1).\n",
    "$$\n",
    "\n",
    "However, we can't use this relation forever.  It's got to stop somewhere, so for $n = 1$ we will use the fact that $f(1) = 1$.  This is called the **base case** for the recursion; recursive solutions **must** have at least one base case so that we don't have infinite recursion.  There may be more than one base case; these are situations where we know the answer and can stop the recursion."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def factorial(n):\n",
    "  # A recursive factorial function.\n",
    "  if n == 1:\n",
    "    return 1\n",
    "  else:\n",
    "    return n * factorial(n-1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "n = 5\n",
    "print(f'{n:d}! = {factorial(n):d}')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here is an equivalent nonrecursive factorial."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def factorial2(n):\n",
    "  # A nonrecursive factorial function.\n",
    "    f = 1\n",
    "    for i in range(1, n+1):\n",
    "        f = i*f\n",
    "    return f"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "n = 5\n",
    "print(f'{n:d}! = {factorial(n):d}')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true,
    "jupyter": {
     "outputs_hidden": true
    }
   },
   "source": [
    "If we want, we can make our recursive function [even more cryptic](lambdas.ipynb):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "factorial = lambda n : 1 if n == 1 else n * factorial(n-1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "n = 5\n",
    "print(f'{n:d}! = {factorial(n):d}')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "However, let's try to make it more intelligible:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "m = n\n",
    "def factorial(n):\n",
    "    if n == 1:\n",
    "        print((m-n)*' ' + f'computing {n:d}!')\n",
    "        fact = 1\n",
    "    else:\n",
    "        print((m-n)*' ' + f'computing {n:d}!; waiting on call to factorial({n-1:d})...')\n",
    "        fact = n * factorial(n-1)\n",
    "        print((m-n)*' ' + f'done with {n:d}!')\n",
    "    \n",
    "    return fact"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "n = 5\n",
    "print(f'{n}! = {factorial(n)}')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## What goes on during recursion\n",
    "\n",
    "The preceding code gives a good idea of what goes on inside recursion.\n",
    "\n",
    "When a function calls itself recursively, it halts until the recursive call is completed.  It can then pick up where it stopped.\n",
    "\n",
    "As a function is called recursively, you can think of a new instance of the function being created with its own input arguments and local variables."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Exponentiation <a id=\"exponentiation\"></a>\n",
    "\n",
    "In the factorial example there was no advantage to using a recursive implementation over an iterative implementation.\n",
    "\n",
    "In both cases, if we want to compute $n!$, the number of calculations in both approaches is proportional to $n$.\n",
    "\n",
    "Exponentiation ($x^{n}$, $n$ a positive integer) on the other hand, is a problem where recursion can lead to a much more efficient algorithm."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Nonrecursive exponentiation\n",
    "\n",
    "Let's first look at exponentiation in the obvious manner:\n",
    "$$\n",
    "x^{n} = \\underbrace{x * x * x * \\cdots * x}_{\\mbox{$n-1$ multiplications}}.\n",
    "$$\n",
    "The number of multiplications is proportional to $n$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def pow0(x, n):\n",
    "    xn = x\n",
    "    for i in range(1, n):\n",
    "        xn = xn * x\n",
    "    return xn"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# this may take a few seconds; be patient...\n",
    "\n",
    "a = [[3.14, 2], [2, 1000000], [0.5, 10]]\n",
    "for pair in a:\n",
    "    print(f'{pair[0]}^{pair[1]} = {pow0(pair[0], pair[1])}')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Recursive exponentiation\n",
    "\n",
    "The recursive implementation of exponentiation is on the observation that \n",
    "\\begin{align*}\n",
    "x^{n} &= x^{n/2} * x^{n/2} \\quad \\mbox{if $n$ is even}, \\\\\n",
    "x^{n} &= x^{(n-1)/2} * x^{(n-1)/2} * x \\quad \\mbox{if $n$ is odd}.\n",
    "\\end{align*}\n",
    "This allows us to compute $x^{n}$ much more efficiently.  \n",
    "\n",
    "For instance, the standard approach to computing $x^{62}$ requires 61 multiplications.\n",
    "\n",
    "With the recursive approach we can do so using only 9 multiplications:\n",
    "\\begin{align*}\n",
    "x^{62} &= (x^{31})^{2} \\\\\n",
    "x^{31} &= (x^{15})^{2} * x \\\\\n",
    "x^{15} &= (x^{7})^{2} * x \\\\\n",
    "x^{7} &= (x^{3})^{2} * x \\\\\n",
    "x^{3} &= (x)^{2} * x.\n",
    "\\end{align*}\n",
    "\n",
    "With the recursive approach the number of multiplications is proportional to $\\log_{2} n$, the base-2 logarithm of $n$.\n",
    "\n",
    "If we compare $\\log_{2} n$ with $n$ we see that this is a <b>dramatic</b> improvement."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "import matplotlib.pyplot as plt\n",
    "from math import log2\n",
    "import numpy as np\n",
    "\n",
    "N = np.arange(1, 1000, 1)\n",
    "log2_N = np.log2(N)\n",
    "plt.plot(N, N, 'b', label=\"$n$\")\n",
    "plt.plot(N, log2_N, 'r', label=\"$log_{2} n$\")\n",
    "plt.xlabel('n')\n",
    "plt.title('Growth of $n$ vs $log_{2} n$')\n",
    "plt.legend(loc=2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So, let's implement recursive exponentiation.\n",
    "\\begin{align*}\n",
    "x^{n} &= x^{n/2} * x^{n/2} \\quad \\mbox{if $n$ is even}, \\\\\n",
    "x^{n} &= x^{(n-1)/2} * x^{(n-1)/2} * x \\quad \\mbox{if $n$ is odd}.\n",
    "\\end{align*}\n",
    "This we could implement as\n",
    "``` Python\n",
    "def pow1(x, n):\n",
    "    if ((n % 2) == 0):\n",
    "        a = pow1(x, n/2)\n",
    "        return a*a\n",
    "    else:\n",
    "        a = pow1(x, (n-1)/2)\n",
    "        return a*a*x\n",
    "```\n",
    "Observe that the use of the variable <code>a</code> to store the result allows us to avoid calling <code>pow1()</code> twice in the recursion.  If we did make two calls, it turns out that the number of multiplications becomes proportional to $n$ again.\n",
    "\n",
    "How do we terminate the recursion?  Let's look at a few cases.\n",
    "* <code>pow1(x, 1)</code> leads to a call <code>pow1(x, 0)</code>;\n",
    "* <code>pow1(x, 2)</code> leads to a call <code>pow1(x, 1)</code>;\n",
    "* <code>pow1(x, 3)</code> leads to a call <code>pow1(x, 1)</code>;\n",
    "* <code>pow1(x, 4)</code> leads to a call <code>pow1(x, 2)</code>.\n",
    "\n",
    "From this we deduce that all the calls will eventually end with a call to <code>pow1(x, 0)</code>."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def pow1(x, n):\n",
    "    if n == 0:\n",
    "        return 1\n",
    "\n",
    "    if n % 2 == 0:\n",
    "        a = pow1(x, n//2)\n",
    "        return a*a\n",
    "    else:\n",
    "        a = pow1(x, (n-1)//2)\n",
    "        return a*a*x"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = [[3.14, 2], [2, 1000], [0.5, 10]]\n",
    "for pair in a:\n",
    "    print(f'{pair[0]}^{pair[1]} = {pow1(pair[0], pair[1])}')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Let's check our function.\n",
    "print(pow1(2, 1000) - 2**1000)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's compare the speed of our two functions:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%timeit\n",
    "\n",
    "pow0(3, 10_000)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%timeit\n",
    "\n",
    "pow1(3, 10_000)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The recursive version is considerably faster, at least on my machine &ndash; microseconds vs milliseconds."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There is nothing wrong with introducing more than one case that (correctly) terminates the recursion.  This can sometimes lead to improved efficiency by short-circuiting some of the recursive calls."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def pow1_5(x, n):\n",
    "    if n == 0:\n",
    "        return 1\n",
    "    elif n == 1:\n",
    "        return x\n",
    "    elif n == 2:\n",
    "        return x*x\n",
    "\n",
    "    if n % 2 == 0:\n",
    "        a = pow1(x, n//2)\n",
    "        return a*a\n",
    "    else:\n",
    "        a = pow1(x, (n-1)//2)\n",
    "        return a*a*x"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Let's check our function.\n",
    "print(pow1_5(2, 2000) - 2**2000)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%timeit\n",
    "\n",
    "pow1_5(3, 10_000)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## An alternative recursive version\n",
    "\n",
    "Observe that \n",
    "\\begin{align*}\n",
    "x^{n} &= (x^{2})^{n/2} \\quad \\mbox{if $n$ is even}, \\\\\n",
    "x^{n} &= (x^{2})^{(n-1)/2} * x \\quad \\mbox{if $n$ is odd}.\n",
    "\\end{align*}\n",
    "We can use this to implement another recursive version.  This one is even simpler, since it avoids the problem of possibly making two recursive calls at each level of the recursion."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def pow2(x, n):\n",
    "    if n == 0:\n",
    "        return 1\n",
    "\n",
    "    if n % 2 == 0:\n",
    "        return pow2(x*x, n/2)\n",
    "    else:\n",
    "        return pow2(x*x, n//2) * x"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(pow2(2, 1000))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Let's check our function.\n",
    "print(pow2(2, 2000) - 2**2000)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's check its speed:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%timeit\n",
    "\n",
    "pow2(3, 10_000)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Infinite (or just really deep finite) recursion <a id=\"stack_overflow\"></a>\n",
    "\n",
    "A common error in recursive functions is failing to terminate the recursion, resulting in an infinite recursion.\n",
    "\n",
    "For instance, suppose we forgot the terminating case $f(1) = 1$ in our recursive factorial.  The recursion will never stop!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def factorial(n):\n",
    "  # A broken recursive factorial function.\n",
    "    return n * factorial(n-1)\n",
    "\n",
    "print(factorial(6))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Stack overflow\n",
    "\n",
    "Left unchecked, infinite recursion (or even really deep, but finite recursion) will lead to a <b>stack overflow</b>.  This occurs when too much memory is used in the <b>call stack</b>, which is where the copies of the recursively called functions and their local variables are kept.\n",
    "\n",
    "\"If something can't go on forever, it won't.\" <br/>\n",
    "&ndash; Herbert Stein"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Internal limits on recursion\n",
    "\n",
    "Fortunately, Python is on to our tricks and has a limit on the number of levels (depth) of recursion that are allowed.\n",
    "\n",
    "You can check the maximum number of levels of recursion allowed using the function [<code>getrecursionlimit()</code> from the <code>sys</code> module](https://docs.python.org/3/library/sys.html#sys.getrecursionlimit)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import sys\n",
    "print(f'Maximum recursion limit: {sys.getrecursionlimit()}')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's modify our factorial code to see where the recursion fails.  We add a second argument that keeps track of the recursion level."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def factorial(n, level):\n",
    "    # A broken recursive factorial function.\n",
    "    print(f'recursion level: {level}')\n",
    "    level += 1\n",
    "    return n * factorial(n-1, level)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(factorial(6, 0))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It looks like we encounter a stack overflow before we hit the hard limit on the levels of recursion."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The fact that Python enforces a limit on the number of levels of recursion means that even valid recursion may fail if the number of recursive calls is too great.\n",
    "\n",
    "The following is a working recursive factorial function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def factorial(n, level):\n",
    "    # A working recursive factorial function.\n",
    "    print(f'recursion level: {level}')\n",
    "    level += 1\n",
    "    if n == 1:\n",
    "        return 1\n",
    "    else:\n",
    "        return n * factorial(n-1, level)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's give this function an input that leads to many levels of recursion."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(factorial(1000, 0))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once again we encounter the maximum recursion depth, even though the function is correct.\n",
    "\n",
    "We can change the maximum recursion limit using the function \n",
    "[<code>setrecursionlimit()</code> in the <code>sys</code> module](https://docs.python.org/3/library/sys.html#sys.setrecursionlimit).\n",
    "\n",
    "Here we double the limit, which allows for a larger call stack to be used."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import sys\n",
    "sys.setrecursionlimit(2000)\n",
    "print('1000! = {factorial(1000, 0)}')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# The Fibonacci numbers <a id=\"fibonacci\"></a>\n",
    "\n",
    "Sometimes recursion can be a Bad Idea.  The Fibonacci numbers are the classic example.\n",
    "\n",
    "The Fibonacci numbers are defined to be\n",
    "\\begin{align*}\n",
    "F_{0} &= 0 \\\\\n",
    "F_{1} &= 1 \\\\\n",
    "F_{n} &= F_{n-1} + F_{n-2} \\quad \\mbox{for $n > 1$}.\n",
    "\\end{align*}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We begin with a nonrecursive function that computes the n-th Fibonacci number."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def fib0(n):\n",
    "    a = 0  # F[0] = 0\n",
    "    b = 1  # F[1] = 1\n",
    "    i = n\n",
    "    while i > 0:\n",
    "        c = a + b  # F[n]   = F[n-2] + F[n-1]\n",
    "        a = b      # F[n-2] = F[n-1] (swap)\n",
    "        b = c      # F[n-1] = F[n] (swap)\n",
    "        i = i - 1\n",
    "    return a"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import time\n",
    "\n",
    "start = time.clock()\n",
    "for n in range(0, 40):\n",
    "    print(f'F[{n:2d}]: {fib0(n):>8d}')\n",
    "end = time.clock()\n",
    "print(f'elapsed time: {end-start:f} seconds')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we try an obvious recursive version, based on the fact that \n",
    "\\begin{align*}\n",
    "F_{0} &= 0, \\\\\n",
    "F_{1} &= 1, \\\\\n",
    "F_{n} &= F_{n-1} + F_{n-2}, \\quad n > 1.\n",
    "\\end{align*}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def fib1(n):\n",
    "    if n == 0:\n",
    "        return 0\n",
    "    elif n == 1:\n",
    "        return 1\n",
    "    else:\n",
    "        return fib1(n-1) + fib1(n-2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from time import perf_counter\n",
    "\n",
    "for n in range(0, 40):\n",
    "    start = perf_counter()\n",
    "    print(f'F[{n:2d}]: {fib1(n):>8d}', end='  ')\n",
    "    end = perf_counter()\n",
    "    print(f'elapsed time: {end-start:f} seconds')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true,
    "jupyter": {
     "outputs_hidden": true
    }
   },
   "source": [
    "## Ack!  why is recursive Fibonacci so slow?\n",
    "\n",
    "Recursive Fibonacci is slow because the recursive calls end up recomputing things over and over.\n",
    "\n",
    "For instance, consider\n",
    "<pre>\n",
    "fib1(35) + fib1(34).\n",
    "</pre>\n",
    "The call to <code>fib1(35)</code> involves a call to <code>fib1(34)</code>, but we don't reuse this value.  Likewise, both <code>fib1(35)</code> and <code>fib1(34)</code> compute the value <code>fib1(33)</code>.  There is a tremendous amount of repeated work.\n",
    "\n",
    "To quantify the slowness of recursive Fibonacci, we will build a model of how long it takes when the input is $n$.  \n",
    "\n",
    "The runtime analysis of algorithms is an entire field of computer science.  At the undergraduate level we cover it in CSCI 303."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### The model\n",
    "\n",
    "Let $T(n)$ be the runtime of the call <code>fib1(n)</code>.\n",
    "\n",
    "Most of the time is spent computing <code>fib(n-1)</code> and <code>fib(n-2)</code>, so we have\n",
    "$$\n",
    "T(n) \\approx T(n-1) + T(n-2).\n",
    "$$\n",
    "The actual cost is a bit higher because of the logical tests and the addition, but this is close enough.\n",
    "\n",
    "In addition, we will assume that $T(0)$ and $T(1)$ are take approximately the same time, which we will call time 1 as our reference unit of time."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Growth of T(n)\n",
    "\n",
    "Observe that \n",
    "\\begin{align*}\n",
    "T(0) &= 1 \\\\\n",
    "T(1) &= 1 \\\\\n",
    "T(2) &= T(1) + T(0) = 2 \\\\\n",
    "T(3) &= T(2) + T(1) = 3 \\\\\n",
    "T(4) &= T(3) + T(2) = 5 \\\\\n",
    "T(5) &= T(4) + T(3) = 8 \\\\\n",
    "\\vdots &= \\vdots\n",
    "\\end{align*}\n",
    "Does this look familiar?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "That's right!  $T(n)$ is growing as fast as the Fibonacci numbers!\n",
    "\n",
    "Let's plot out how quickly the Fibonacci numbers grow."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def fib1(n):\n",
    "    F = [0, 1]\n",
    "    for k in range(2, n+1):\n",
    "        F.append(F[k-1] + F[k-2])\n",
    "    return F"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "F = fib1(40)\n",
    "plt.plot(F)\n",
    "plt.ylabel('runtime T(n) relative to T(1)')\n",
    "plt.xlabel('n')\n",
    "plt.title('Growth of T(n)')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The exact formula for the $n$-th Fibonacci number is\n",
    "$$\n",
    "F_{n} = \\frac{1}{\\sqrt{5}} \\left[\n",
    "\\left(\\frac{1 + \\sqrt{5}}{2}\\right)^{n}\n",
    "- \\left(\\frac{1 - \\sqrt{5}}{2}\\right)^{n}\n",
    "\\right]\n",
    "$$\n",
    "\n",
    "For \"large\" $n$, the Fibonacci numbers increase by roughly a factor of the \n",
    "[Golden Ratio&#0153;](https://en.wikipedia.org/wiki/Golden_ratio):\n",
    "$$\n",
    "\\phi = \\frac{1 + \\sqrt{5}}{2} \\approx 1.618.\n",
    "$$\n",
    "\n",
    "This means that \n",
    "\\begin{align*}\n",
    "T(30) &\\approx 1,860,450 \\times T(1) \\\\\n",
    "T(40)  &\\approx 228,826,127\\times T(1).\n",
    "\\end{align*}\n",
    "'Zounds!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Exercises  <a id=\"exercises\"/>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"exercise\">\n",
    "    What is the output of the following function?  Think before running the code.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def foo(n):\n",
    "    # Assume n is an int and n >= 1.\n",
    "    if (n > 1):\n",
    "         foo(n - 1)\n",
    "    print(n * '*')\n",
    "    \n",
    "foo(5)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Answer.**\n",
    "<div class=\"voila\">\n",
    "<pre>    \n",
    "*\n",
    "**\n",
    "***\n",
    "****\n",
    "*****\n",
    "</pre>\n",
    "Can you see why?\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"exercise\">\n",
    "    What is the output of the following function?  Think before running the code.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def foo(n):\n",
    "    # Assume n is an int and n >= 1.\n",
    "    print(n * '*')\n",
    "    if (n > 1):\n",
    "         foo(n - 1) \n",
    "\n",
    "foo(5)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Answer.**\n",
    "<div class=\"voila\">\n",
    "<pre>    \n",
    "*****\n",
    "****\n",
    "***\n",
    "**\n",
    "*\n",
    "</pre>\n",
    "Can you see why?    \n",
    "</div>    "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"exercise\">\n",
    "    Write a recursive function to sum the numbers from $1$ to $n$.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Answer.**\n",
    "<div class=\"voila\">\n",
    "Here is my solution.\n",
    "<pre>    \n",
    "def sum_n(n):\n",
    "    if n == 1:\n",
    "        return 1\n",
    "    else:\n",
    "        return n + sum_n(n-1)\n",
    "\n",
    "for n in range(1, 10):\n",
    "    print(sum_n(n))\n",
    "</pre>\n",
    "This is not an effective use of recursion and will lead to stack overflow if $n$ is large since there are $n$ levels of recursion.    \n",
    "</div>    "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"exercise\">\n",
    "    What does the following recursive function do?\n",
    "    \n",
    "```python\n",
    "\n",
    "def foo(n):\n",
    "    # Assume that n is an int and n >= 1.\n",
    "    if (n == 1):\n",
    "        return 0\n",
    "    else:\n",
    "        return 1 + foo(n // 2)  \n",
    "```\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Answer.**\n",
    "<div class=\"voila\">\n",
    "The call <code>foo(n)</code> will compute the floor of the $\\log_{2} n$, the base-2 logarithm of $n$.\n",
    "</div>    "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### This notebook was brought to you by comedy:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "This world is a comedy to those that think, a tragedy to those that feel&hellip; <br/>\n",
    "&ndash; Horace Walpole"
   ]
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
