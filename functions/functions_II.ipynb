{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"http://www.cs.wm.edu/~rml/images/wm_horizontal_single_line_full_color.png\"/>\n",
    "\n",
    "<h1 style=\"text-align:center;\">CSCI 141-01, Fall 2023 course notes</h1>\n",
    "<h1 style=\"text-align:center;\">Care and feeding of functions</h1>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Contents\n",
    "* [Definition vs invocation](#definition_invocation)\n",
    "* [Namespaces and scope](#namespaces_and_scope)\n",
    "* [Accessing nonlocal variables](#nonlocal)\n",
    "* [Errors in the wild](#student_errors)\n",
    "    * [Functions and flow control](#flow_control)\n",
    "    * [The not callable error](#not_callable)\n",
    "    * [Forgetting the ( )](#no_parens)\n",
    "* [Exercises](#exercises)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Definition vs invocation <a id=\"definition_invocation\"/>\n",
    "\n",
    "The function <b>definition</b> creates the function.  Function definition is indicated by the keyword <code class=\"kw\">def</code>.\n",
    "\n",
    "Function <b>invocation</b> (\"calling the function\") actually applies the function to its inputs (if it has any)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The undefined function error \n",
    "\n",
    "In Python, function definition <b>must</b> precede invocation.  This is because Python processes statements in a sequential fashion, so if a function is invoked before being defined, the Python interpreter has no idea what you are talking about, and an error occurs:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "funky(42)\n",
    "\n",
    "def funky(x):\n",
    "    return x + 12"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"try_it\">\n",
    "<b>Try it yourself.</b>\n",
    "Write code that results in an error because the function has not been defined before being invoked.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Namespaces and scope <a id=\"namespaces_and_scope\"/>\n",
    "\n",
    "The following example illustrates an important point that sometimes confuses beginning programmers.  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def double(x):\n",
    "    x = 2*x\n",
    "    return x\n",
    "\n",
    "x = 3\n",
    "y = double(x)\n",
    "print(f'{x = }, {y = }')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that the variable <code>x</code> appears in both the function definition and the invocation of the function. However, these two uses of <code>x</code> do NOT refer to the same entity. Thus, when we modify the value of <code>x</code> inside the function, the value of <code>x</code> outside the function remains unchanged.\n",
    "\n",
    "Variables that appear only in the definition of a function are <b>local</b> to the function. This is in keeping with the idea that a function encapsulates a set of statements. If the variables in a function were the same as those of the same name outside the function, it would defeat the goal of encapsulation since operations inside the function could magically affect variables outside the function.\n",
    "\n",
    "<b>Scope</b> refers to the region of code where a variable or function is visible, while a <b>namespace</b> is at its simplest a collection of names of variables and functions.\n",
    "\n",
    "You can think of a namespace as a big list of all the names of variables and functions that you’ve defined, either explicitly or by importing modules. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"danger\"></div>\n",
    "This is why <code>from math import *</code> is so dangerous &ndash; it dumps everything from the math module into the local namespace."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"danger"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Function scope <a id=\"function_scope\"/>\n",
    "\n",
    "As another illustration of the notion of scope, let's define a function inside another function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def foo(x):\n",
    "    def bar(x):\n",
    "        return x**2\n",
    "    return bar(x) + 7\n",
    "\n",
    "print(foo(2))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now watch what happens when we try to call <code>bar()</code> from outside <code>foo()</code>."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(bar(2))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Python can't find the function <code>bar()</code>!  This is because <code>bar()</code> has <b>function scope</b>, meaning it is available inside <code>foo()</code> but not outside it.  In this regard <code>bar()</code> behaves like the variables that are local to <code>foo()</code>."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"try_it\">\n",
    "<b>Try it yourself.</b>\n",
    "Write your own example of nested functions and confirm that you cannot call the inner function from outside the function that encompasses it.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Accessing nonlocal variables <a id=\"nonlocal\"/>\n",
    "\n",
    "Under certain circumstances functions can see <b>nonlocal</b> variables, i.e., variables that are defined outside them.\n",
    "\n",
    "For instance, if Python encounters a variable inside a function and can't find a local variable local to the function when you refer to something it looks in the main function for the variable:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x = 2\n",
    "\n",
    "def foo():\n",
    "    return x\n",
    "\n",
    "print(foo())\n",
    "\n",
    "del x  # Clear x so it isn't defined when we start the next cell."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Variables in the main program are <b>global variables</b> and are visible everywhere after they are defined."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A global variable is visible to a function so long as the variable is defined outside of a before the function is called:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def foo():\n",
    "    return x\n",
    "\n",
    "x = 2\n",
    "\n",
    "print(foo())\n",
    "\n",
    "del x  # Clear x so it isn't defined when we start the next cell."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If the global variable isn't defined at the time of the function call, you get an error:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "def foo():\n",
    "    return x\n",
    "\n",
    "print(foo())\n",
    "\n",
    "x = 2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If a function is defined inside another function, then the variables inside the encompassing function are also visible to the encompassed function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "y = 42\n",
    "def foo():\n",
    "    x = 2\n",
    "    def bar():\n",
    "        print(x)\n",
    "        print(y)\n",
    "    bar()\n",
    "        \n",
    "foo()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"danger\"></div>\n",
    "Writing a function involving variables other than the ones explicitly passed to it can be dangerous.  It is confusing and makes it easy to introduce nonlocal dependencies and bugs that are hard to debug.  Avoid it."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The one exception to this advice is named constants.  The [Python style guide](https://legacy.python.org/dev/peps/pep-0008/#constants) recommends defining them outside functions at the module (file) level:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "INCHES_PER_FOOT = 12\n",
    "\n",
    "def feet_to_inches(length):\n",
    "    '''Convert feet to inches.'''\n",
    "    return length * INCHES_PER_FOOT\n",
    "\n",
    "print(feet_to_inches(1.5))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "By default, you cannot change the value of a global variable inside a function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x = 42\n",
    "\n",
    "def foo():\n",
    "    x = 6*9\n",
    "    \n",
    "foo()    \n",
    "print(x)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Why is this?  \n",
    "\n",
    "When Python encounters <code>x = 6*9</code>, it creates a local variable <code>x</code>.\n",
    "\n",
    "If you really, really, really want to change the value of a global variable from inside a function, you can do so using the <code class=\"kw\">global</code> keyword:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x = 42\n",
    "\n",
    "def foo():\n",
    "    global x\n",
    "    x = 6*9\n",
    "    \n",
    "foo()    \n",
    "print(x)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This is called a [<b>side effect</b>](https://en.wikipedia.org/wiki/Side_effect_(computer_science)):\n",
    "\n",
    "<blockquote>\n",
    "In computer science, an operation, function or expression is said to have a side effect if it modifies some state variable value(s) outside its local environment, that is to say has an observable effect besides returning a value (the main effect) to the invoker of the operation.\n",
    "</blockquote>   \n",
    "\n",
    "Repeat after me:\n",
    "* <b>Don't program with side effects!</b>\n",
    "* <b>Don't program with side effects!</b>\n",
    "* <b>Don't program with side effects!</b>\n",
    "\n",
    "Imagine if operating an automobile had side effects, say, turning on the windshield wipers also made the vehicle accelerate to 120 mph &ndash; would you say that's a good design?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"danger\"></div>\n",
    "Avoid accessing non-local variables in functions.  Named constants are OK, as their value should not change."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Should I ever use non-local variables?\n",
    "\n",
    "About the only place I would consider using a global variable would be to control different levels of output:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "verbose = True\n",
    "\n",
    "def foo():\n",
    "    if (verbose): print('hello from foo()!')\n",
    "        \n",
    "def bar():\n",
    "    if __debug__:  # We will talk about __debug__ later.\n",
    "        print('hello from bar()!')        \n",
    "        if verbose:\n",
    "            print('here\\'s some more output!')\n",
    "    \n",
    "foo()\n",
    "bar()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In general, though, there are better ways to achieve the effect of global variables to pass data around.  We will return to this point when we discuss Python classes."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Function-related errors from student code <a id=\"student_errors\"/>\n",
    "\n",
    "Things not to do, taken from student code."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Functions inside flow control <a id=\"flow_control\"/>\n",
    "\n",
    "Defining functions inside flow control is legal, but it is a Bad Idea&trade;.  Let's look at an example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x = 7\n",
    "if x < 42:\n",
    "    def foo(x):\n",
    "        return x**2\n",
    "    x = foo(x)\n",
    "    \n",
    "print(foo(5))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "All is cool.  Now let's modify the example slightly."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "x = 54\n",
    "if x < 42:\n",
    "    def bar(x):\n",
    "        return x**2\n",
    "    x = bar(x)\n",
    "print(bar(5))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Yikes!  Now the code doesn't work!  Can you think of why this is?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b>Answer.</b>\n",
    "<div class=\"voila\">\n",
    "<p>\n",
    "In the first example, the code where <code>foo()</code> was defined was executed, thereby defining the function.\n",
    "</p>\n",
    "<p>\n",
    "In the second example, the code where <code>bar()</code> was defined was <b>not</b> executed, so when the function is called later, Python doesn't know what we're talking about.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the Fall 2016 semester an inexplicable number of students did this in the first project and it caused no end of problems for grading."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"danger\"></div>\n",
    "Moral: do not define functions inside conditionally executed code."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"try_it\">\n",
    "<b>Try it yourself.</b>\n",
    "Create your own example of this behavior.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Not callable <a id=\"not_callable\"/>\n",
    "\n",
    "Here is another error that shows up from time to time.  Suppose we want to implement the mathematical statement\n",
    "$$\n",
    "x = 2ab.\n",
    "$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = 3\n",
    "b = 4\n",
    "x = 2(a*b)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This error occurs because we forgot the <code>&#42;</code> after the 2.  However, the error Python sees and reports is the resulting syntactic error, namely, that it looks like we are trying to call a function <code>2()</code>.\n",
    "\n",
    "Here is a variant of the same bug."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = 3\n",
    "b = 2\n",
    "c = 7\n",
    "x = a(b*c)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"try_it\">\n",
    "<b>Try it yourself.</b>\n",
    "Write your own code that manifests this error.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Forgetting the ( )  <a id=\"no_parens\"/>\n",
    "\n",
    "The following is another popular bug:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from random import randint \n",
    "\n",
    "help(randint)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = randint\n",
    "b = 54\n",
    "\n",
    "c = a + b\n",
    "print(c)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The problem is we forgot the pass arguments to <code>randint</code>.  In Python functions are objects, so the assignment <code>a = randint</code> is valid.  However, <code>c = a + b</code> is trying to add a function and and int, which is invalid.\n",
    "\n",
    "Here is the correct code:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = randint(0, 42)\n",
    "b = 54\n",
    "\n",
    "c = a + b\n",
    "print(c)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Even if your function has no inputs you still need to use <code>()</code> to invoke it:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def foo():\n",
    "    '''Return a numeric value.'''\n",
    "    return 42\n",
    "\n",
    "a = foo\n",
    "b = foo()\n",
    "\n",
    "print(f'a: {a}')\n",
    "print(f'b: {b}')\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Exercises <a id=\"exercises\"/>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"exercise\">\n",
    "What will the following code print (or will it just result in an error)?\n",
    "<pre>\n",
    "def thing():\n",
    "    print('Thing One says hey!')\n",
    "    def thing():\n",
    "        print('Thing Two says hey!')\n",
    "        def thing():\n",
    "            print('Thing Three says hey!')\n",
    "            def thing():\n",
    "                print('Thing Four says, hey! hold my beer and watch this!')\n",
    "            thing()\n",
    "        thing()\n",
    "    thing()     \n",
    "thing() \n",
    "</pre>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    " "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b>Answer.</b>\n",
    "<div class=\"voila\">\n",
    "This code actually works.  At each level, a different version of the function <code>thing()</code> is visible.  \n",
    "\n",
    "But don't write code like this.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h4>This notebook was brought to you by children:</h4>\n",
    "\n",
    "It is no wonder that people are so horrible when they start life as children. <br/>\n",
    "&ndash; Kingsley Amis"
   ]
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
