{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"http://www.cs.wm.edu/~rml/images/wm_horizontal_single_line_full_color.png\">\n",
    "\n",
    "<h1 style=\"text-align:center;\">CSCI 141-01, Fall 2023 course notes</h1>\n",
    "<h1 style=\"text-align:center;\">Conditional execution: the <code class=\"kw\">if</code> statement</h1>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Contents\n",
    "* [The <code class=\"kw\">if</code> statement](#if_statement)\n",
    "* [Syntax](#syntax)\n",
    "* [Parentheses are optional](#optional)\n",
    "* [The <code class=\"kw\">else</code> and <code class=\"kw\">elif</code> clauses](#alt_branches)\n",
    "    * [An <code class=\"kw\">elif</code> gotcha 🐞](#elif_gotcha)\n",
    "    * [Performance considerations](#performance)\n",
    "* [The curious <code class=\"kw\">pass</code> statement](#pass_statement)\n",
    "* [Nested <code class=\"kw\">if</code> statements](#nested)\n",
    "* [An <code class=\"kw\">if</code> idiom](#idiom)\n",
    "* [Logical structure](#structure)\n",
    "* [Indentation in Python](#indentation)\n",
    "* [Exercises](#exercises)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Condition execution\n",
    "\n",
    "We say a segment of code is **conditionally executed** if it is only executed if some condition is satisfied.  For instance, **if** we are talking about a leap year, then it has 366 days; otherwise it has 365 days.  "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# The <code class=\"kw\">if</code> statement  <a id=\"if_statement\"></a>\n",
    "\n",
    "The <code class=\"kw\">if</code> statement controls the conditional execution of code depending on whether a specified boolean variable or boolean expression is <code class=\"kw\">True</code> or <code class=\"kw\">False</code>."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x = 3\n",
    "if x < 5:\n",
    "    print(\"x < 5!\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x = 42\n",
    "if x < 5:\n",
    "    print(\"x < 5!\")\n",
    "print(\"We is not in the if block!\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Multiple statements can be under the control of an if-statement:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fake_gnus = True\n",
    "if fake_gnus:\n",
    "    print(\"war is peace!\")\n",
    "    print(\"slavery is freedom!\")\n",
    "    print(\"ignorance is strength!\")\n",
    "    print(\"we have always been at war with Eastasia!\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "if 54 > 42:\n",
    "    print('''\\\n",
    "    If you can keep your head when all about you\n",
    "    Are losing theirs and blaming it on you,\n",
    "    If you can trust yourself when all men doubt you,\n",
    "    But make allowance for their doubting too;\n",
    "    If you can wait and not be tired by waiting,\n",
    "    Or being lied about, don't deal in lies,\n",
    "    Or being hated, don't give way to hating,\n",
    "    And yet don't look too good, nor talk too wise:''')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"try_it\">\n",
    "    <b>Try it yourself</b>.  Write and execute some code that uses an <code class=\"kw\">if</code> statement.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Syntax of an <code class=\"kw\">if</code> block <a id=\"syntax\"></a>\n",
    "\n",
    "The syntax of an <code class=\"kw\">if</code> block is\n",
    "\n",
    "<pre>\n",
    "if (boolean variable or expression):\n",
    "    blah\n",
    "    blah\n",
    "    blah\n",
    "</pre>\n",
    "All of the statements in the <code class=\"kw\">if</code> block must be indented at least one space and all statements must have the same indentation.  The [Python style guide](https://peps.python.org/pep-0008/#indentation) recommends an indentation of four spaces.  Whatever indentation you choose, be consistent throughout your code.\n",
    "\n",
    "Two common errors are:\n",
    "* forgetting the colon, and\n",
    "* messing up the indentation.\n",
    "\n",
    "Fortunately, Python will let you know very quickly if you commit either error:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# No colon.\n",
    "if True\n",
    "    print('Woops!  no colon!')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# One space too few.\n",
    "if True:\n",
    "    x = 42\n",
    "   crooked = 54\n",
    "    y = 6*9"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# One space too many.\n",
    "if True:\n",
    "    x = 42\n",
    "     crooked = 54\n",
    "    y = 6*9"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Parentheses are optional <a id=\"optional\"></a>\n",
    "\n",
    "One does not need to enclose the controlling boolean variable or expression in parentheses.  The following is perfectly valid:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "if 3 > 2:\n",
    "    print('Colorless green ideas sleep furiously!')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "foo = 42\n",
    "bar = 54\n",
    "if foo <= bar:\n",
    "    print('Things are as they should be.')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "However, in many other languages,  the boolean expression in an <code class=\"kw\">if</code> statement **must** be enclosed by parentheses, so out of habit (or for clarity) I often use them in Python."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The boolean expression in an <code class=\"kw\">if</code> statement can be as complex as you'd like:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "x = 42\n",
    "y = 54\n",
    "z =  6\n",
    "if (x > 7) and (y < 99) and (6 % 2 == 0) and (2+2 == 4):\n",
    "    print('The earth is like a tiny grain of sand,')\n",
    "    print('only much, much larger.')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "you_can_keep_your_head = (8**(1/2) < 3) and (27**(2/3) > 4)\n",
    "\n",
    "if you_can_keep_your_head:\n",
    "    print('''\\\n",
    "    If you can keep your head when all about you\n",
    "    Are losing theirs and blaming it on you,\n",
    "    If you can trust yourself when all men doubt you,\n",
    "    But make allowance for their doubting too;\n",
    "    If you can wait and not be tired by waiting,\n",
    "    Or being lied about, don't deal in lies,\n",
    "    Or being hated, don't give way to hating,\n",
    "    And yet don't look too good, nor talk too wise:''')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# The <code class=\"kw\">else</code> and <code class=\"kw\">elif</code> clauses <a id=\"alt_branches\"/>\n",
    "\n",
    "The <code class=\"kw\">else</code> and <code class=\"kw\">elif</code> (a portmanteau word meaning \"else if\") statements allow you to define alternative paths through your code."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "animal = input('What animal are you interested in eating? ')\n",
    "if (animal == 'flounder'):\n",
    "    edible = True\n",
    "elif (animal == 'blobfish'):\n",
    "    edible = False\n",
    "elif (animal == 'squirrel'):\n",
    "    edible = True\n",
    "else: # default: don't eat it!\n",
    "    edible = False\n",
    "\n",
    "if edible:\n",
    "    yn = ''\n",
    "else:\n",
    "    yn = 'not'\n",
    "\n",
    "print(f'The {animal} is {yn} edible.')    "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you are not familiar with the blobfish, [here](http://www.smithsonianmag.com/smart-news/in-defense-of-the-blobfish-why-the-worlds-ugliest-animal-isnt-as-ugly-as-you-think-it-is-6676336/?no-ist) is an article about it."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## How <code class=\"kw\">else</code> and  <code class=\"kw\">elif</code> work\n",
    "\n",
    "In a structure like the following,\n",
    "<pre>\n",
    "if A:\n",
    "  execute the A block\n",
    "elif B:\n",
    "  execute the B block\n",
    "elif C:\n",
    "  execute the C block\n",
    "  .\n",
    "  .\n",
    "  .\n",
    "else:\n",
    "  execute the else block\n",
    "</pre>\n",
    "the logic goes as follows:\n",
    "* If A is true, then execute the A block\n",
    "* If A is false and B is true, then execute the B block.\n",
    "* If A and B are false and C is true, then we execute the C block.\n",
    "* ...\n",
    "* If the tests in the preceding <code class=\"kw\">if</code> and <code class=\"kw\">elif</code>  have all returned false, then we execute the <code class=\"kw\">else</code> block."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## An <code class=\"kw\">elif</code> gotcha 🐞 <a id=\"elif_gotcha\"/>\n",
    "\n",
    "Here is a feature of <code class=\"kw\">elif</code> that sometimes slips past beginning (and experienced) programmers.\n",
    "\n",
    "In a structure like the following,\n",
    "<pre>\n",
    "if A:\n",
    "  execute the A block\n",
    "elif B:\n",
    "  execute the B block\n",
    "</pre>\n",
    "the logic goes as follows:\n",
    "* If A is true, then execute the A block\n",
    "* Otherwise (i.e., if A is false), and B is true, then execute the B block."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"danger\"></div>\n",
    "As a consequence, <b>if A is true, then even if B is true the B block will not be executed.</b>\n",
    "The following examples illustrate this point."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "if True:\n",
    "    print('I would like a bloater paste sandwich, please.')\n",
    "elif True:\n",
    "    print('I would like the puppy pot pie, please.')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "if True:\n",
    "    print('On the one hand, this statement will execute.')\n",
    "elif True:\n",
    "    print('On the other hand, this will not.')\n",
    "elif True:\n",
    "    print('On the third hand, neither will this one.')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<p class=\"try_it\">\n",
    "<b>Try it yourself.</b>\n",
    "Create your own example of this behavior.\n",
    "</p>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Performance considerations <a id=\"performance\"/>\n",
    "\n",
    "The following two blocks of code are equivalent in the sense that <code>holiday</code> will have the same value when both are done.\n",
    "\n",
    "However, there is a difference in their performance.  We will use the Jupyter notebook magic command <code>timeit</code> to run each cell repeatedly (by default, 10,000,000 times), and print the average of the fastest 3 runs.\n",
    "\n",
    "Note: ns = nanoseconds (billionths of a second)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "month = \"Jan\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%timeit \n",
    "\n",
    "if month == \"Jan\":\n",
    "    holiday = \"Bubblewrap Appreciation Day (Jan 30)\"\n",
    "elif month == \"Feb\":\n",
    "    holiday = \"Groundhog Day (Feb 2)\"\n",
    "elif month == \"Mar\":\n",
    "    holiday = \"National Proofreadin Day (Mar 8)\"\n",
    "elif month == \"Apr\":\n",
    "    holiday = \"National Ferret Day (Apr 2)\"\n",
    "elif month == \"May\":\n",
    "    holiday = \"Towel Day (May 25)\"\n",
    "elif month == \"Jun\":\n",
    "    holiday = \"First Day of Summer (Jun 21)\"\n",
    "elif month == \"Jul\":\n",
    "    holiday = \"Independence Day (Jul 4)\"\n",
    "elif month == \"Aug\":\n",
    "    holiday = \"National Diatomaceous Earth Day (Aug 31)\"\n",
    "elif month == \"Sep\":\n",
    "    holiday = \"International Talk Like A Pirate Day (Sep 19)\"\n",
    "elif month == \"Oct\":\n",
    "    holiday = \"Halloween (Oct 31)\"\n",
    "elif month == \"Nov\":\n",
    "    holiday = \"World Toilet Day (Nov 19)\"\n",
    "elif month == \"Dec\":\n",
    "    holiday = \"Christmas Day (Dec 25)\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%timeit \n",
    "\n",
    "if month == \"Jan\":\n",
    "    holiday = \"Bubblewrap Appreciation Day (Jan 30)\"\n",
    "if month == \"Feb\":\n",
    "    holiday = \"Groundhog Day (Feb 2)\"\n",
    "if month == \"Mar\":\n",
    "    holiday = \"National Proofreadin Day (Mar 8)\"\n",
    "if month == \"Apr\":\n",
    "    holiday = \"National Ferret Day (Apr 2)\"\n",
    "if month == \"May\":\n",
    "    holiday = \"Towel Day (May 25)\"\n",
    "if month == \"Jun\":\n",
    "    holiday = \"First Day of Summer (Jun 21)\"\n",
    "if month == \"Jul\":\n",
    "    holiday = \"Independence Day (Jul 4)\"\n",
    "if month == \"Aug\":\n",
    "    holiday = \"National Diatomaceous Earth Day (Aug 31)\"\n",
    "if month == \"Sep\":\n",
    "    holiday = \"International Talk Like A Pirate Day (Sep 19)\"\n",
    "if month == \"Oct\":\n",
    "    holiday = \"Halloween (Oct 31)\"\n",
    "if month == \"Nov\":\n",
    "    holiday = \"World Toilet Day (Nov 19)\"\n",
    "if month == \"Dec\":\n",
    "    holiday = \"Christmas Day (Dec 25)\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Observe that the second cell takes significantly longer to run than the first cell.\n",
    "\n",
    "This is because in the first cell, the use of <code class=\"kw\">elif</code> means that as soon we find the month is January, we do not perform any of the checks of the following months.\n",
    "\n",
    "In the second cell, on the other hand, we always check all twelve months.\n",
    "\n",
    "If you change <code>month</code> to <code>'Dec'</code>, then the performance of the two cells is similar, since in the first cell we have to check all the months up to December."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"danger\"></div>Usually this sort of thing doesn't matter much, but it is something to keep in mind if performance is critical."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<p class=\"try_it\">\n",
    "<b>Try it yourself.</b>\n",
    "Try using the Jupyter magic <code>timeit</code> command to time a cell of code.\n",
    "</p>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# The curious <code class=\"kw\">pass</code> statement <a id=\"pass_statement\"/>\n",
    "\n",
    "The <code class=\"kw\">pass</code> statement does nothing. It is a placeholder you can use where Python insists you put something, but you don't know yet what to put.\n",
    "\n",
    "For example, we can use it as an empty body of a block in an <code class=\"kw\">if</code> statement."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x = 42\n",
    "y = 54\n",
    "z = 28\n",
    "\n",
    "if x > y:\n",
    "    print(\"\"\"Whoa!  this shouldn't happen!\"\"\")\n",
    "else:\n",
    "    pass  # I'll finish this later."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"danger\"></div>Python does not allow for empty blocks in <code class=\"kw\">if</code> statements."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x = 42\n",
    "y = 54\n",
    "z = 28\n",
    "\n",
    "if x > y:\n",
    "    print(\"Whoa!  this shouldn't happen!\")\n",
    "else:\n",
    "    # I'll finish this later."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the error message EOF means \"end of file\".  Python was looking for a statement following the <code class=\"kw\">else</code> when it hit the end of the cell."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Nested <code class=\"kw\">if</code> statements <a id=\"nested\"/>\n",
    "\n",
    "<code class=\"kw\">if</code> statements may be nested:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x = 42\n",
    "y = 54\n",
    "if x <= 42:\n",
    "    if (y > 50):\n",
    "        print('x <= 42 and y > 50!')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Observe that the preceding logic can also be written as"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x = 42\n",
    "y = 54\n",
    "if (x <= 42) and (y > 50):\n",
    "    print('x <= 42 and y > 50!')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The latter is preferred, as it is easier to understand the conditions under which the print statement will be executed."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<p class=\"try_it\">\n",
    "<b>Try it yourself.</b>\n",
    "Create your own nested <code class=\"kw\">if</code> code.\n",
    "</p>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# An <code class=\"kw\">if</code> idiom <a id=\"idiom\"></a>\n",
    "\n",
    "You may encounter the following programming idiom.  Suppose we wish to set a boolean variable (a <b>flag</b>) that indicates whether a condition is true if the decibel sound measurement stored in a variable <code>db</code> is too large:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "threshold = 60\n",
    "db = 72\n",
    "\n",
    "if db > threshold:\n",
    "    too_loud = True\n",
    "else:\n",
    "    too_loud = False\n",
    "    \n",
    "if too_loud:\n",
    "    print('pipe down!')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This can be rewritten more succinctly as"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "threshold = 60\n",
    "db = 72\n",
    "\n",
    "too_loud = (db > threshold)\n",
    "\n",
    "if too_loud:\n",
    "    print('pipe down!')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you find yourself writing an <code class=\"kw\">if</code>-<code class=\"kw\">else</code> block whose sole purpose is to set a boolean variable, you might consider using this idiom."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Logical structure  <a id=\"structure\"/>\n",
    "\n",
    "It is easy to write really confusing <code class=\"kw\">if</code> constructs, and because they are confusing they are liable to be buggy.\n",
    "\n",
    "Let's look at an example adapted from [*The Elements of Programming Style*](https://www.amazon.com/Elements-Programming-Style-2nd/dp/0070342075/ref=sr_1_5?dchild=1&keywords=elements+of+programming+style+3rd+edition&qid=1596577083&sr=8-5) by Kernighan and Plaugher (K&amp;P).  The goal is to find the smallest of three numbers <code>x, y, z</code>.\n",
    "\n",
    "Let's start with some bad (but correct) code."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Bad code.\n",
    "\n",
    "x = 2; y = 3; z = 1;\n",
    "\n",
    "if x < y:\n",
    "    if x < z:\n",
    "        s = x\n",
    "    else:\n",
    "        s = z\n",
    "elif y < z:\n",
    "    s = y\n",
    "else:\n",
    "    s = z\n",
    "\n",
    "print(s)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "An <code class=\"kw\">if</code> statement immediately followed by another <code class=\"kw\">if</code> statement is a sign the logic is growing the wrong way.  The further indented statements are in your code, the more complicated the logic to get there.\n",
    "\n",
    "K&amp;P observe\n",
    "<blockquote>\n",
    "The general rule is: after you make a decision, <b>do something</b>.  Don't just go somewhere or make\n",
    "another decision.  If you follow each decision by the action that goes with it, you can see at a glance\n",
    "what each decision implies.\n",
    "</blockquote>\n",
    "\n",
    "K&amp;P recommend restructuring the code with <code class=\"kw\">elif</code> and <code class=\"kw\">else</code> statements."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# An improved version.\n",
    "\n",
    "x = 2; y = 3; z = 1;\n",
    "\n",
    "if (x <= y) and (x <= z):\n",
    "    s = x\n",
    "elif y <= z:\n",
    "    s = y\n",
    "else:\n",
    "    s = z\n",
    "\n",
    "print(s)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A little ratiocination leads to an even better version:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# The code done the Right Way.\n",
    "\n",
    "x = 2; y = 3; z = 1;\n",
    "\n",
    "s = x\n",
    "if y < s:\n",
    "    s = y\n",
    "if z < s:\n",
    "    s = z\n",
    "    \n",
    "print(s)    "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"danger\"></div>\n",
    "Don't immediately follow an <code class=\"kw\">if</code> statement with another <code class=\"kw\">if</code> statement.  After you make a decision, do something &ndash; don't make another decision. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"danger\"></div>\n",
    "If your code has become deeply indented because of deeply nested ifs, that's generally a sign that you should simplify the logic."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Indentation in Python <a id=\"indentation\"></a>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"danger\"></div>\n",
    "<b>In Python, the indentation of statements is extremely important!</b>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Other languages use squiggly brackets <code>{}</code> or keywords like <code>end</code> or <code>endif</code> to indicate the end of the statements controlled by an <code class=\"kw\">if</code>.  Python is one of a relatively small number of programming languages that uses indentation to indicate statement blocks, such as the block inside an <code class=\"kw\">if</code> block.  Such languages are said to respect the [off-side rule](https://en.wikipedia.org/wiki/Off-side_rule).  Most languages pay no attention to indentation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x = 42\n",
    "if x < 5:\n",
    "    print('x < 5!')\n",
    "print('This statement is outside the if-block!')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The indentation for <b>all</b> the statements in an if-block must be the same or Python will be unhappy."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "foo = True\n",
    "if foo:\n",
    "    print(\"war is peace!\")\n",
    "    print(\"slavery is freedom!\")\n",
    "   print(\"ignorance is strength!\")\n",
    "    print(\"we've always been at war with Eastasia!\") "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In general, spurious indentation results in an <code class=\"error\">IndentationError</code>:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x = 42\n",
    "  y = 54"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "foo = True\n",
    "if foo:\n",
    "    print(\"war is peace!\")\n",
    "\n",
    "# The next statement is out of the if-block.\n",
    "print(\"slavery is freedom!\")\n",
    "    # Let's indent for fun!\n",
    "    print(\"ignorance is strength!\")\n",
    "    print(\"we've always been at war with Eastasia!\")    "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<p class=\"try_it\">\n",
    "<b>Try it yourself.</b>\n",
    "Write some code that yields an <code class=\"error\">IndentationError</code>.\n",
    "</p>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## How much indentation?\n",
    "\n",
    "The <a href=\"https://www.python.org/dev/peps/pep-0008/#indentation\">Python Style Guide</a> recommends 4 spaces.\n",
    "\n",
    "However, the amount of indentation in an <code class=\"kw\">if</code> block doesn't matter, so long as it is consistent <b>inside each block</b>. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "foo = True\n",
    "if foo:\n",
    "                print(\"war is peace!\")\n",
    "                print(\"slavery is freedom!\")\n",
    "\n",
    "bar = False\n",
    "if not bar:\n",
    " print(\"ignorance is strength!\")\n",
    " print(\"we've always been at war with Eastasia!\")\n",
    " print(\"pineapple belongs on a pizza!\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"try_it\">\n",
    "<b>Try it yourself.</b>\n",
    "Create <code>if</code> blocks with different levels of indentation in the same piece of code.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Indentation practices\n",
    "\n",
    "That said, the Pythonic convention is that [one should use 4 spaces](https://www.python.org/dev/peps/pep-0008/#indentation).\n",
    "\n",
    "Whatever spacing you should use, you should be consistent throughout your code.  So, if you use 2 spaces in one place, you should use 2 spaces throughout."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"danger\"></div>  <a href=\"https://www.python.org/dev/peps/pep-0008/#tabs-or-spaces\" target=\"_blank\">Do not try to mix tabs and spaces for indentation in Python &ndash; it won't work</a>.  Mixing spaces and tabs could be made to work in Python 2, but even then it was like mixing beer and wine &ndash; something you would later regret."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Spaces are preferred over tabs for indentation.  Since the visual indentation set by a tab can vary from one environment to the next, the use of tabs rather than spaces means that you have no control over how your code might appear to a user."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## <a href=\"https://www.youtube.com/watch?v=FbwkkXGmFrI\" style=\"color:black; text-decoration:none!important;\">Cuidado &ndash; llamas!!</a>        "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"danger\"></div>\n",
    "The use of indentation to indicate critical program structure is a Pythonism.  You will <b>NOT</b> see it in most other languages."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Maybe it was Pythonic habits that led to [Apple's \"goto fail\" bug](http://www.zdnet.com/article/apples-goto-fail-tells-us-nothing-good-about-cupertinos-software-delivery-process/)?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Exercises <a id=\"exercises\"/>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"exercise\">\n",
    "Write code so that if the variable <code>duration</code> is 1 you print \n",
    "<code>\n",
    "Remind me 1 day in advance.\n",
    "</code>\n",
    "while if the value is 2 or more, you print \"days\" instead of \"day\".  E.g.,\n",
    "<code>\n",
    "Remind me 42 days in advance.\n",
    "</code>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b>Answer.</b>\n",
    "<div class=\"voila\">\n",
    "There are multiple solutions; here is a straightforward one:\n",
    "<code>\n",
    "if duration == 1:\n",
    "    print('Remind me 1 day in advance.')\n",
    "elif duration > 1:\n",
    "    print(f'Remind me {duration} days in advance.')\n",
    "</code>\n",
    "An alternative is\n",
    "<code>\n",
    "if duration == 1:\n",
    "    unit = 'day'\n",
    "elif duration > 1:\n",
    "    unit = 'days'\n",
    "print(f'Remind me {duration} {unit} in advance.')\n",
    "</code>\n",
    "An advantage of the latter code is that the bulk of the message only appears in one place, rather than two.  This simplifies maintaining the code.  For instance, if we want to change the message to\n",
    "<code>\n",
    "Please remind me 42 days in advance.\n",
    "</code>\n",
    "we only need to add \"Please\" in one location.\n",
    "    \n",
    "Here is a cleverer solution:\n",
    "<code>\n",
    "print(f'Remind me {duration} day{(duration > 1)*\"s\"} in advance.')\n",
    "</code>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"exercise\">\n",
    "<p>\n",
    "<b>It's possum packing time.</b>\n",
    "</p>\n",
    "<p>\n",
    "Suppose you are given the task of packing possums into baskets.\n",
    "</p>\n",
    "<p>\n",
    "Let the integer variable <code>capacity</code> be the number of possums you can pack in each basket.  Write code that will take the integer variable <code>num_possums</code>, the number of possums you need to pack, and compute the integer variable <code>num_baskets</code>, the minimal number of baskets needed.  The number of baskets must be a whole number.\n",
    "</p>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b>Answer.</b>\n",
    "<div class=\"voila\">\n",
    "Suppose the capacity is 3, and the number of possums is 6.  Then we need 2 baskets.  On the other hand, if the number of possums is 7 or 8, we'll need 3 baskets.  From this we see that we have two cases, when the capacity evenly divides the number of possums, and when it does not.\n",
    "<code>\n",
    "if num_possums % capacity == 0:\n",
    "  num_baskets = num_possums // capacity\n",
    "else:\n",
    "  num_baskets = num_possums // capacity + 1\n",
    "</code>\n",
    "Observe that we are using integer division <code>//</code> rather than regular division <code>/</code>.  Why?\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"exercise\">\n",
    "Suppose we have two variables\n",
    "<ul>\n",
    "<li><code>month</code>, which is a string that can take on one of the values <code>'Jan', 'Feb', &hellip;, 'Nov', 'Dec'</code>, and</li>\n",
    "<li><code>day</code>, which is an integer indicating the day of the month.</li>\n",
    "</ul>\n",
    "Write code that prints \n",
    "<ul>\n",
    "<li>“Merry Christmas” if the date falls lies between December 1 and Christmas Day (inclusive),</li>\n",
    "<li>\"Merry Christmas and a Happy Gnu Year!\" if the date lies between Boxing Day and New Year's Eve (inclusive), and </li>\n",
    "<li>“Happy Gnu Year!” if the date lies between New Year's Day and Epiphany (inclusive).\n",
    "</li>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "month = 'Jan'\n",
    "day = 6\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b>Answer.</b>\n",
    "<div class=\"voila\">\n",
    "Here is one solution.\n",
    "<code>\n",
    "if (month == 'Dec') and (day <= 25):\n",
    "    print('Merry Christmas!')\n",
    "elif month == 'Dec':\n",
    "    print('Merry Christmas and a Happy Gnu Year!')\n",
    "elif (month == 'Jan') and (day <= 6):\n",
    "    print('Happy Gnu Year!')\n",
    "</code>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"exercise\">\n",
    "Consider the following code:\n",
    "<code>\n",
    "if a or (not b):\n",
    "  if m <= n:\n",
    "    print('boo!')\n",
    "  elif (not a):\n",
    "    print('hoo!')\n",
    "</code>\n",
    "Find <code class=\"kw\">True, False</code> values for the variables <code>a,b</code> and integer values for <code>m,n</code> that cause the code to print 'boo!' and 'hoo!'.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b>Answer.</b>\n",
    "<div class=\"voila\">\n",
    "<p>\n",
    "To obtain any output at all, we need either <code>a == True</code> or <code>b == False</code> (since then <code>(not b) == True</code>).\n",
    "</p>\n",
    "<p>\n",
    "Then, to obtain 'boo!', any values of <code>m,n</code> for which <code>m <= n</code> will do (e.g., m = 42 and n = 54).\n",
    "</p>\n",
    "<p>\n",
    "To obtain 'hoo!' we need <code>m > n</code> (e.g., m = 54 and n = 42), bue we also need <code>(not a) == True</code>, so <code>a</code> must be false.\n",
    "</p>\n",
    "<p>\n",
    "Thus, \n",
    "<ul>\n",
    "<li>to obtain 'boo!' we need either (<code>a == True</code> or <code>b == False</code>) and <code>m <= n</code>, while</li>\n",
    "<li>to obtain 'hoo!' we need (<code>a == False</code> and <code>b == False</code>), and <code>m > n</code>.</li>\n",
    "</ul>\n",
    "</p>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"exercise\">\n",
    "Suppose <code>a,b,c</code> are three integers.  Replace each <code>?</code> with one of the values <code>a,b,c</code> so that when the code is finished executing, the variable <code>max</code> will contain the maximum of the three values.\n",
    "<pre>\n",
    "if ? > ?:\n",
    "  if ? > ?:\n",
    "    max = ?\n",
    "  else:\n",
    "    max = ?\n",
    "elif ? > ?:\n",
    "  max = ?\n",
    "else:\n",
    "  max = ?\n",
    "</pre>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b>Answer.</b>\n",
    "<div class=\"voila\">\n",
    "There are a multiple solutions to this problem.  Here is one in which the ordering of the logical tests reflects the alphabetical ordering of the variable names.\n",
    "<code>\n",
    "if a > b:\n",
    "  if a > c:\n",
    "    max = a\n",
    "  else:\n",
    "    max = c\n",
    "elif b > c:\n",
    "  max = b\n",
    "else:\n",
    "  max = c\n",
    "</code>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"exercise\">\n",
    "Try finding the maximum of <code>a,b,c</code> in fewer lines of code.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b>Answer.</b>\n",
    "<div class=\"voila\">\n",
    "Here is one solution. It is slightly less efficient than the code in the previous exercise since in the worst case it needs 4 comparisons to find the maximum rather than 3.  On the other hand, the logic is easier to follow.\n",
    "<code>\n",
    "if (a <= c) and (b <= c):\n",
    "  max = c\n",
    "elif (a <= b) and (c <= b):\n",
    "  max = b\n",
    "else:\n",
    "  max = a\n",
    "</code>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h4>This notebook is brought to you by the power of prayer:</h4>\n",
    "\n",
    "My prayer to God is a very short one: \"O Lord, make my enemies ridiculous.\"  God has granted it. <br/>\n",
    "&ndash; Voltaire, Letter to M. Damilaville (May 16, 1767)"
   ]
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
