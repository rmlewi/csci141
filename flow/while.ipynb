{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"http://www.cs.wm.edu/~rml/images/wm_horizontal_single_line_full_color.png\">\n",
    "\n",
    "<h1 style=\"text-align:center;\">CSCI 141, Fall 2023 course notes</h1>\n",
    "<h1 style=\"text-align:center;\">Iteration: <code class=\"kw\">while</code> loops</h1>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Contents\n",
    "\n",
    "* [The <code class=\"kw\">while</code> statement](#while_statement)\n",
    "* [Syntax](#syntax)\n",
    "* [The <code class=\"kw\">break</code> statement](#break_statement)\n",
    "* [Infinite loops](#infinite_loops)\n",
    "* [Some case studies](#loops)\n",
    "    * [Smallest divisor](#smallest_divisor)\n",
    "    * [Extracting digits](#digits)\n",
    "* [Exercises](#exercises)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# The <code class=\"kw\">while</code> statement <a id=\"while_statement\"/>\n",
    "\n",
    "The <code class=\"kw\">while</code> statement allows you to execute a block of code repeatedly until a condition is satisfied (more precisely, until a specified boolean expression is <code class=\"kw\">True</code>). \n",
    "\n",
    "The following while loop will execute until the counter <code>i</code> reaches the value 7."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "i = 0\n",
    "while i < 7:\n",
    "    print(i)\n",
    "    i = i + 1\n",
    "\n",
    "print(\"th-th-that's all, folks!\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Syntax of a <code class=\"kw\">while</code> loop <a id=\"syntax\"/>\n",
    "\n",
    "The syntax of a <code class=\"kw\">while</code> block is\n",
    "\n",
    "<pre>\n",
    "while (boolean variable or expression):\n",
    "    blah\n",
    "    blah\n",
    "    blah\n",
    "</pre>\n",
    "\n",
    "Here is the order of events:\n",
    "1. We check the value of the boolean variable or expression controlling the <code class=\"kw\">while</code> block.\n",
    "2. If the boolean is false we skip over the contents of the <code class=\"kw\">while</code> block and resume execution at the first statment after the block.\n",
    "3. If the boolean is true we execute the statements inside the <code class=\"kw\">while</code> block.  We then return to the top of the block and repeat step 1.\n",
    "\n",
    "This repeated execution of the block and the return to the top of the block is why <code class=\"kw\">while</code> blocks are also called <code class=\"kw\">while</code> loops."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once again, Python uses indentation to indicate the statements in a <code class=\"kw\">while</code> block."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "i = 0\n",
    "while i < 7:\n",
    "    print(i)\n",
    "       i = i + 1\n",
    "\n",
    "print(\"th-th-that's all, folks!\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# The <code class=\"kw\">break</code> statement <a id=\"break_statement\"/>\n",
    "\n",
    "Sometimes you want to exit a <code class=\"kw\">while</code> loop prematurely.\n",
    "\n",
    "For instance, consider the following loop, which looks for the smallest divisor (other than 1) of a given integer n. \n",
    "\n",
    "* If n is 1, it returns 1.\n",
    "* Otherwise, starting from 2 it successively tests whether it has found an integer that evenly divides n.\n",
    "* When if finds one, it uses the <code class=\"kw\">break</code> statement to jump out of the loop."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "n = int(input('Enter n: '))\n",
    "\n",
    "if n == 1:\n",
    "    d = 1\n",
    "else:\n",
    "    d = 2\n",
    "    while d < n:\n",
    "        if n % d == 0:\n",
    "            break\n",
    "        d += 1\n",
    "\n",
    "print('The least divisor of', n, 'is', d)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There are some <b>very</b> [strong feelings](https://www.google.com/search?q=python+break+is+evil) against the use of <code class=\"kw\">break</code> statements to the point that some people assert that they are only used by bad programmers.\n",
    "\n",
    "I take a more measured view of language features &ndash; a poor workman blames his tools, as the saying goes.  That said, it is frequently the case we can (and should) rewrite the logic of a loop to avoid a <code class=\"kw\">break</code> statement. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"danger\"></div>\n",
    "If you find that you have many different ways of exiting a loop, you should reexamine the logic to see if it can be simplified.  Multiple exits from loops often lead to trouble &ndash; depending on how you exited, variables may end up in unexpected states."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For instance, we can rewrite the preceding code in the following, break-free way:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "n = int(input('Enter n: '))\n",
    "\n",
    "if n == 1:\n",
    "    d = 1\n",
    "else:\n",
    "    d = 2\n",
    "    while (d < n) and (n % d != 0):\n",
    "        d += 1\n",
    "\n",
    "print('The least divisor of', n, 'is', d)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"danger\"></div>\n",
    "Take care using <code class=\"kw\">break</code> statements as they can make the logic difficult to understand, and may be a sign that you don't understand the logic yourself.  Try restructuring the code to avoid them."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# The <code class=\"kw\">else</code> clause in <code class=\"kw\">while</code> statements <a id=\"else_clause\"/>\n",
    "\n",
    "You can also include an <code class=\"kw\">else</code> clause as part of a <code class=\"kw\">while</code> statement.  This clause will be executed after the <code class=\"kw\">while</code> loop terminates <b>normally</b> &ndash; not through an exit due to a <code class=\"kw\">break</code> statement.\n",
    "\n",
    "A <code class=\"kw\">break</code> statement in the <code class=\"kw\">while</code> loop will jump over the <code class=\"kw\">else</code> block."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "n = int(input('Enter n: '))\n",
    "\n",
    "i = 0\n",
    "while i < n:\n",
    "    if i == 7:\n",
    "        print('Skipping the else clause!')\n",
    "        break\n",
    "    print(i)\n",
    "    i += 1\n",
    "else:\n",
    "    print('The loop successfully completed!')\n",
    "    \n",
    "print('Done with the loop!')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Infinite loops  <a id=\"infinite_loops\"/>\n",
    "\n",
    "It is entirely possible that you can enter a <code class=\"kw\">while</code> loop and never exit it.  This situation is called an <b>infinite loop</b>.\n",
    "\n",
    "Infinite loops are typically a bug."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here is an example of an infinite loop.  Rather than mess up the execution of this Jupyter notebook, copy the code to a file and execute it there.\n",
    "```python\n",
    "k = 1\n",
    "while k < 10:\n",
    "    print(k)\n",
    "    k -= 1  # Woops! this is going the wrong way!\n",
    "```\n",
    "You can halt the inifnite loop by entering CTRL-C (press the control key and the c key at the same time) in the terminal window where the code is running."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"danger\"></div> Watch out for infinite loops!<br/><br/>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Infinite loops can lead to more unpleasant things! \n",
    "* If your infinite loop is printing output to a file, then it will eventually fill up the drive!  If you share the storage device with others this will make you very unpopular.\n",
    "* If you are paying for computer time (e.g., Amazon web services), an infinite loop will make you really pay!\n",
    "\n",
    "I've been told that an out-of-control program in CSCI 140 filled up a terabyte (one trillion bytes) of disk space."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here is a popular way to end up with an infinite loop in Python.\n",
    "```python\n",
    "k = 0\n",
    "while k < 100:\n",
    "    print(k)\n",
    "k += 1 \n",
    "```\n",
    "Can you see the problem?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Answer.**\n",
    "<div class=\"voila\">\n",
    "The increment <code>k += 1</code> is not inside the loop because of its indentation.  This means that <code>k</code> always remains 0 inside the loop so the end condition <code>k >= 100</code> is never satisfied.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Infinite loops that aren't\n",
    "\n",
    "There is one programming idiom where an infinite loop is not a bug (because it's not really infinite).\n",
    "\n",
    "Suppose we want to keep executing a block of code until an event in the code tells us to exit the loop.  For example, suppose imagine the following game, in which the user is prompted to enter a number; the user is then immediately taunted by the code.  We want the game to continue indefinitely, until the user enters \"q\" to quit."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "while (True):\n",
    "    s = input('Enter a number (or \"q\" to quit): ')\n",
    "    if s == 'q':\n",
    "        break\n",
    "    else:\n",
    "        print('Wrong!')\n",
    "    \n",
    "print('Bye!')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So don't be shocked if you encounter a working loop that begins\n",
    "```python\n",
    "while True:\n",
    "    ...\n",
    "```\n",
    "It may be an instance of this idiom (of course, it could also be a mistake)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Some <code class=\"kw\">while</code> loop case studies <a id=\"loops\"/>\n",
    "\n",
    "The typical programming pattern for <code class=\"kw\">while</code> loops is\n",
    "<pre>\n",
    "initialize some variables used in the loop\n",
    "while (not done):\n",
    "  do loopy stuff\n",
    "  be sure to change variables so that eventually we exit the loop\n",
    "</pre>\n",
    "Let's look at a some examples."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Finding the least divisor <a id=\"smallest_divisor\"/>\n",
    "\n",
    "Our earlier code for finding the least divisor can be improved.  If we know that we start with <code>d &lt; n</code>, then we can omit the test for this condition since eventually <code>d</code> will increase until <code>d == n</code>, at which point <code>n % d == 0</code>."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "n = 37*37\n",
    "\n",
    "if n == 1:\n",
    "    d = 1\n",
    "else:\n",
    "    while n % d != 0:\n",
    "        d += 1\n",
    "    \n",
    "print('the smallest non-trivial divisor of', n, 'is', d)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can improve the efficiency by observing that if $n$ is even we know the answer is 2, while if $n$ is odd we need only check odd numbers as possible divisors.  This makes the code run twice as fast if $n$ is odd, and really fast if $n$ is even."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "n = 37*37\n",
    "\n",
    "if n == 1:\n",
    "    d = 1\n",
    "if n % 2 == 0: # n is even\n",
    "    d = 2\n",
    "else:            # n is odd\n",
    "    d = 3\n",
    "    while n % d != 0:\n",
    "        d += 2\n",
    "    \n",
    "print('the smallest non-trivial divisor of', n, 'is', d)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Extracting digits <a id=\"digits\"/>\n",
    "\n",
    "Next we will look at the question of printing off the digits in a positive integer.  This is what is done in integer to string conversion."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's try some examples and see if we can derive a general way to do this.\n",
    "\n",
    "Consider the number 139.  Notice that\n",
    "```python\n",
    "9 = 139 % 10 \n",
    "3 = 13 % 10 = (139 // 10) % 10\n",
    "1 = (13 // 10)\n",
    "```\n",
    "This looks promising &ndash; reading up the page, the left-hand sides are the digits in their proper order.\n",
    "\n",
    "Let's try another number, say, 4096:\n",
    "```python\n",
    "6 = 4096 % 10\n",
    "9 = 409 % 10 = (4096 // 10) % 10\n",
    "0 = 40 % 10 = (409 // 10) % 10\n",
    "4 = 40 // 10\n",
    "0 = 0 // 10 = (4 // 10) % 10\n",
    "```\n",
    "Now we can see a stopping criterion &ndash; once the integer division by 10 yields a zero.\n",
    "\n",
    "Now let's try a power of 10, say, 1000:\n",
    "```python\n",
    "0 = 1000 % 10 \n",
    "0 = 100 % 10 = (1000 // 10) % 10\n",
    "0 = 10 % 10 = (100 // 10) % 10\n",
    "1 = 10 // 10\n",
    "0 = 0 // 10 = (1 // 10) % 10\n",
    "```\n",
    "Let's try to write out an algorithm:\n",
    "```python\n",
    "given a positive integer n, \n",
    "rightmost digit = n % 10\n",
    "replace n with (n // 10)\n",
    "next rightmost digit = n % 10\n",
    "replace n with (n // 10)\n",
    "...\n",
    "stop once (n // 10) is zero\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# n > 1\n",
    "n = 1234\n",
    "\n",
    "# Initialize stuff.\n",
    "s = ''\n",
    "done = (n == 0)\n",
    "while not done:\n",
    "    # Build up the digits, right to left.\n",
    "    s = str(n % 10) + s\n",
    "    # Replace n by (n // 10).\n",
    "    n = n // 10\n",
    "    # See if we are done.\n",
    "    done = (n == 0)\n",
    "    \n",
    "print('the answer:', s)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Exercises <a id=\"exercises\"/>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"exercise\">\n",
    "<p>\n",
    "<b>buffalo, buffalo, buffalo...</b>\n",
    "</p>\n",
    "<p>\n",
    "Write code that takes a variable <code>n</code> and writes the string \"buffalo\" <code>n</code> times, separating the instances of \"buffalo\" by commas and spaces and ending with an ellipsis (\"...\").\n",
    "</p>\n",
    "<p>\n",
    "For instance,\n",
    "</p>\n",
    "<pre>\n",
    "n = 3\n",
    "\\# Your code here\n",
    "</pre>\n",
    "<p>\n",
    "should produce the output\n",
    "</p>\n",
    "<pre>\n",
    "buffalo, buffalo, buffalo...\n",
    "</pre>\n",
    "<p>\n",
    "If <code>n</code> is zero or negative you should print an empty string.\n",
    "</p>\n",
    "<p>\n",
    "When testing your code, make sure you test the situation where <code>n</code> is 0 and negative.  Inputs that require special handling (and thus special testing) are sometimes called <b>edge cases</b>.\n",
    "</p>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "n = 5 # for instance...\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b>Answer</b>.\n",
    "<div class=\"voila\">\n",
    "Here is one elegant solution:\n",
    "<pre class=\"voila\">\n",
    "if n <= 0:\n",
    "    print(\"\")\n",
    "else:\n",
    "    s = (n - 1)* (\"buffalo, \") # Replicate \"buffalo, \" (n-1) times...\n",
    "    s = s + \"buffalo...\"       # ... and concatenate \"buffalo...\"\n",
    "    print(s)\n",
    "</pre>\n",
    "Here is another solution, using <code>while</code>:\n",
    "<pre>\n",
    "if n <= 0:\n",
    "    print(\"\")\n",
    "else:\n",
    "    i = 0\n",
    "    while i < n-1:\n",
    "        print(\"buffalo, \", end=\"\") # All but the last is \"buffalo, \".\n",
    "        i = i + 1\n",
    "    print(\"buffalo...\")            # End with \"buffalo...\"\n",
    "</pre>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"exercise\">\n",
    "How would you modify our digit finding code so that rather than build up a string containing the base-10 digits of <code>n</code>, it produces a string of the base-$b$ digits of <code>n</code>?  For instance, if $b = 2$ we would generate the binary expansion.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b>Answer</b>.\n",
    "<div class=\"voila\">\n",
    "<p>    \n",
    "The modification is simple: replace all the instances of 10 with the new base $b$.\n",
    "</p>\n",
    "<code>\n",
    "n = 15\n",
    "b = 2\n",
    "\n",
    "\n",
    "s = ''  # Initialize stuff.\n",
    "done = (n == 0)\n",
    "while not done:\n",
    "    # Build up the digits, right to left.\n",
    "    s = str(n % b) + s\n",
    "    # Replace n by (n // b).\n",
    "    n = n // b\n",
    "    # See if we are done.\n",
    "    done = (n == 0)\n",
    "    \n",
    "print('the answer:', s)\n",
    "</code>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h4>This notebook is brought to you by the meaning of life.</h4>\n",
    "\n",
    "The very purpose of existence is to reconcile the glowing opinion we hold of ourselves with the appalling things that other people think about us. <br/>\n",
    "&ndash; Quentin Crisp"
   ]
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
