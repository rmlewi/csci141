{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"http://www.cs.wm.edu/~rml/images/wm_horizontal_single_line_full_color.png\">\n",
    "\n",
    "\n",
    "<h1 style=\"text-align:center;\">CSCI 141-01, Fall 2023 course notes</h1>\n",
    "<h1 style=\"text-align:center;\">Reading and writing files</h1>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Contents\n",
    "\n",
    "* [Working with files](#files)\n",
    "* [Opening a file](#open)\n",
    "* [Hoovering up an entire file](#hoover)\n",
    "* [Iterating over a file object](#iterating)\n",
    "* [Rereading a file](#rereading)\n",
    "* [Example: building a dictionary from a file](#dictionary)\n",
    "* [Writing to a file](#write)\n",
    "* [The <code class=\"kw\">seek()</code> function](#seek)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Getting ready\n",
    "\n",
    "For this lesson you will need the files \n",
    "* [alice.txt](alice.txt),\n",
    "* [icd10cm.txt](icd10cm.txt),\n",
    "* [american.txt](american.txt), and\n",
    "* [hello](hello).\n",
    "\n",
    "They can be downloaded from the same site as this notebook."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Working with files <a id=\"files\"/>\n",
    "\n",
    "One of the many strengths of Python is the ease with which one can manipulate text.  And if we want to work with a mass of text, it is convenient to read the text from a file.\n",
    "\n",
    "There are two basic flavors of files:\n",
    "1. text files, and\n",
    "2. binary files.\n",
    "\n",
    "We will discuss <b>text files</b>.  By this we mean files whose contents can be turned into strings that are meaningful (well, meaningful to humans).  \n",
    "\n",
    "There are also <b>binary files</b>, whose contents might make sense to a machine, but not to a human.  An example of a binary file would be a [zip file](https://en.wikipedia.org/wiki/Zip_&#40;file_format&#41;), which is a compressed format that is not readable by humans.  The file <code>hello</code> that you downloaded is also a binary file; it is the 'hello, world' program written in the C programming language and built to run under Mac OS X.\n",
    "\n",
    "The basic sequence when reading text from a file is\n",
    "1.  Open the file.\n",
    "2.  Read text from the file.\n",
    "3.  Close the file when done.\n",
    "\n",
    "Let's read part of [Alice's encounter with Humpty-Dumpty](https://www.gutenberg.org/files/12/12-h/12-h.htm#link2HCH0006) in *Through the Looking-Glass*, which is in the file <code>alice.txt</code>."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Opening a file <a id=\"open\"/>\n",
    "\n",
    "We use the [<code>open()</code>](https://docs.python.org/3/library/functions.html#open) function in a <code>with</code> statement to open a file and get a [<b>file object</b>](https://docs.python.org/3/glossary.html#term-file-object).  The latter is an object that allows us to access the file."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "with open('alice.txt', 'r') as vile:\n",
    "    print('vile of of type:', type(vile))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here we are calling the function <code>open()</code> with two arguments:\n",
    "\n",
    "1. the name of the file to read, <code>alice.txt</code>, and\n",
    "2. the <b>i/o mode</b>, in this case, read-only, indicated by 'r'.  This means we can only read from the file; we cannot write to it."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Hoovering up an entire file <a id=\"hoover\"/>\n",
    "\n",
    "It is easy to read the entire file in one go, obtaining the contents of the file as one big string."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "with open('alice.txt', 'r') as vile:\n",
    "    content = vile.read()\n",
    "\n",
    "print(content)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If we go this route, we can break the string up into lines using the string <b>split()</b> function.  Here we break up the string using the newline character as the character to split at.  This breaks the string up into lines."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "lines = content.split('\\n')\n",
    "for line in lines:\n",
    "    print(line)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We could also split the mega-string at the blanks, creating a list of individual words (with some punctuation thrown in)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "lines = content.split()\n",
    "for line in lines:\n",
    "    print(line)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Iterating over a file object <a id=\"iterating\"/>\n",
    "\n",
    "Text files are iterable.  If we iterate over a text file, we read the file <b>one line at a time</b>.\n",
    "\n",
    "Here we use a <code>for</code> loop to read the contents of the file, one line at a time."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "with open('alice.txt', 'r') as vile:\n",
    "    for line in vile:\n",
    "        print(line, end='')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b>Question.</b>  Why did we use <code>end=''</code> in \n",
    "```python\n",
    "print(line, end='') ?\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b>Answer.</b>\n",
    "<div class=\"voila\">\n",
    "The lines in the file contain newlines indicating the line breaks.  When we hoover them up into our program, the newlines are included.  If we did not use <code>end=''</code> we would end up with two newlines at the end of each line.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"try_it\">\n",
    "Create your own text file using your favorite editor or other technique.  Then open the file and print the contents using Python.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# The <code class=\"kw\">with</code> statement closes the file when done <a id=\"rereading\"/>\n",
    "\n",
    "What happens if we try to traipsy through the file again?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for line in vile:\n",
    "    print(line)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The <code class=\"kw\">with</code> statement automatically closes the file for i/o when it is done.\n",
    "\n",
    "So the second time we try to read from <code>vile</code> we get an error because the file is no longer open.\n",
    "\n",
    "Python is doing us a favor by automatically closing the file when it has been read in its entirety.  Open files consume system-wide resources and there is a limit on how many files a process may have open at one time."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Finer grained control\n",
    "\n",
    "You can also open files directly with [open()](https://docs.python.org/3/library/functions.html#open) outside of a <code>with</code> block.  In this case we will also need to take care of closing the file ourselves."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "f = open('alice.txt', 'r')  # Open the file.\n",
    "content = f.read()  # Read from the file.\n",
    "print(content)\n",
    "f.close()  # Close the file when done."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We open the file, read from it, and then close it when done, freeing up the operating system resources needed for the open file.  Using <code>open()</code> and <code>close()</code> we have complete control over when the file is closed."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Example: building a dictionary from a file <a id=\"dictionary\"/>\n",
    "\n",
    "Let's look at a more complicated example.\n",
    "\n",
    "The [International Classification of Diseases, 10th Revision, Clinical Modification (ICD-10-CM)](https://en.wikipedia.org/wiki/ICD-10_Clinical_Modification) is a medical diagnostic classification system used in recording and reporting medical information.\n",
    "\n",
    "The file <code>icd10cm.txt</code> contains a list of the ICD-10-CM codes.  There is one code per line, and the code appears at the beginning of each line.  Here are some sample codes:\n",
    "<pre>\n",
    "R460    Very low level of personal hygiene\n",
    "R461    Bizarre personal appearance\n",
    "R462    Strange and inexplicable behavior\n",
    "V9107XA Burn due to water-skis on fire, initial encounter\n",
    "V9733XD Sucked into jet engine, subsequent encounter\n",
    "W5922XA Struck by turtle, initial encounter\n",
    "</pre>\n",
    "\n",
    "We will read the file and create a Python dictionary with the ICD codes as the keys and the descriptions as the associated values.\n",
    "\n",
    "We begin with an empty dictionary."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "icd = {}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we will iterate over the file one line at a time.  For each line we will grab the code and the description.\n",
    "\n",
    "We can find where the code ends by looking for the first space in the line.  We do so using the [<code>find()</code> string method](https://docs.python.org/2/library/stdtypes.html#string-methods).\n",
    "\n",
    "We find the description by applying the [<code>lstrip()</code> string method](https://docs.python.org/2/library/stdtypes.html#string-methods) to remove the leading whitespace from the remainder of the line."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('starting...', end='')\n",
    "with open(\"icd10cm.txt\", \"r\") as phial:\n",
    "    for line in phial:\n",
    "        k = line.find(' ')\n",
    "        code = line[:k]\n",
    "        description = line[k:].lstrip()\n",
    "        icd[code] = description\n",
    "print('done!')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's print the first few entries in the dictionary.  Remember that dictionaries are unordered, so the keys will all be jumbled to together:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "n = 0\n",
    "nmax = 5\n",
    "for k,v in icd.items():\n",
    "    print(k, v, end='')\n",
    "    n += 1\n",
    "    if (n == nmax):\n",
    "        break"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we can search our dictionary for relevant ICD codes and afflictions.  For example, let's look up \"burning water-skis\":"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "term = input('Enter a term to search for: ')\n",
    "for k,v in icd.items():\n",
    "    if term in v.lower():\n",
    "        print(k, v, end='')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Writing to a file <a id=\"write\"/>\n",
    "\n",
    "Next let's try writing to a file. \n",
    "\n",
    "Kittens!\n",
    "\n",
    "<img src=\"http://www.cs.wm.edu/~rml/teaching/csci141/jupyter/kitten.jpg\" height=\"80\" width=\"128\"/> <br/>\n",
    "\n",
    "Everybody loves kittens, so let's use a poem about kittens!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "text = '''\\\n",
    "Hark! hark! the dogs do bark!\n",
    "  The Duke he likes his kittens!\n",
    "He likes to turn them inside-out,\n",
    "  and use them for his mittens!\n",
    "'''"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As with reading from a file, the basic sequence when writing to a file is\n",
    "\n",
    "1. Open the file.\n",
    "2. Write text to the file.\n",
    "3. Close the file when done.\n",
    "\n",
    "This time we must pass a mode to <code>open()</code> indicating that we wish to write to the file.  Here we use <code>w</code> for write."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "with open('kittens.txt', 'w') as bob:\n",
    "    print(text, file=bob, end='')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's open the file for reading and see what's in it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "with open('kittens.txt', 'r') as poem:\n",
    "    for line in poem:\n",
    "        print(line, end='')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let's open the file and write some more text to it.  We will open it with the i/o mode \"w\", for \"write\"."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "new_text = '''\n",
    "Amazing Fact!  If all the sand in the Sahara Desert were spread out,\n",
    "it would cover North Africa.\n",
    "'''\n",
    "\n",
    "with open('kittens.txt', 'w') as bob:\n",
    "    print(new_text, file=bob, end='')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let's see what's in the file."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "poem = open('kittens.txt', 'r')\n",
    "for line in poem:\n",
    "    print(line, end='')\n",
    "poem.close()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b>Where did the poem about kittens go?!</b>\n",
    "\n",
    "When we open a file with the mode <code>w</code>, we open the file at the beginning and overwrite its contents.\n",
    "\n",
    "In order to append new text to an existing file, we need to open it with the mode <code>a</code>, for <b>append</b>.\n",
    "\n",
    "Here are the some of the i/o modes:\n",
    "* 'r'\topen for reading (default)\n",
    "* 'w'\topen for writing, overwriting anything already there and creating the file if it does not exist\n",
    "* 'r+'\topen a file for reading and writing\n",
    "* 'w+'  open a file for reading and writing, creating the file if it does not exist\n",
    "* 'x'\topen for exclusive creation, failing if the file already exists\n",
    "* 'a'\topen for writing, appending to the end of the file if it exists\n",
    "\n",
    "Let's try again.  First, write the poem about kittens."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "with open('kittens.txt', 'w') as bob:\n",
    "    print(text, file=bob, end='')\n",
    "\n",
    "with open('kittens.txt', 'r') as poem:\n",
    "    for line in poem:\n",
    "        print(line, end='')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, let's add the Amazing Fact!, opening the file with mode <b>'a'</b>."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "with open('kittens.txt', 'a') as bob:\n",
    "    print(new_text, file=bob, end='')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let's see what we've got."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "with open('kittens.txt', 'r') as poem:\n",
    "    for line in poem:\n",
    "        print(line, end='')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# The <code class=\"kw\">seek()</code> function <a id=\"seek\"/>\n",
    "\n",
    "We can move around inside a file using the [<code class=\"kw\">seek()</code> function](https://docs.python.org/3/library/io.html#io.IOBase.seek).\n",
    "\n",
    "Calling <code>seek()</code> with an argument of 0 moves to the beginning of the file.  In general, the argument to <code>seek()</code> is the location we wish to move to in terms of **the number of bytes from the start of the file**.  ASCII characters are all one byte long; Unicode characters vary between one and four bytes.\n",
    "\n",
    "Let's move to the start of the file and print out the contents without the <code>end=''</code> option to <code>print()</code> so you can see the effect of the extra newline."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "with open('alice.txt', 'r') as vile:\n",
    "    # Read and print the first 5 lines.\n",
    "    for i in range(0, 5):\n",
    "        line = vile.readline()\n",
    "        print(line, end='')\n",
    "    \n",
    "    # Print a separator.\n",
    "    print(72*'-')\n",
    "    \n",
    "    # Now go back to the start of the file.\n",
    "    vile.seek(0)\n",
    "    line = vile.readline()\n",
    "    print(line, end='')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's use <code>seek()</code> to move to the start of the file + two bytes:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "with open('alice.txt', 'r') as vile:\n",
    "    vile.seek(2)\n",
    "    \n",
    "    text = vile.readline()\n",
    "    print(text, end='')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Notice that the initial letter is not printed.  This is because ASCII characters are each one byte (8 bits) in size.  By starting two bytes past the start of the file, we skip the initial two characters \"I."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"try_it\">\n",
    "<b>Try it yourself.</b>\n",
    "\n",
    "Use <code class=\"kw\">seek()</code> to start from some other offset from the start of <code>alice.txt</code> and see what you get.  Do you understand why the missing letters are missing?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h4>This notebook is brought to you by opening lines.</h4>\n",
    "\n",
    "Ludwig Boltzmann, who spent much of his life studying statistical mechanics, died in 1906, by his own hand.  Paul Ehrenfest, carrying on the work, died similarly in 1933.  Now it is our turn to study statistical mechanics.\n",
    "\n",
    "Perhaps it will be wise to approach the subject cautiously. <br/><br/>\n",
    "&ndash; David L. Goldstein, [States of Matter](https://www.amazon.com/States-Matter-Dover-Books-Physics/dp/048664927X)"
   ]
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
